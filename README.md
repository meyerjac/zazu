#  Zazu Events

## Zazu Events is a social application that aims to bring people closer together by offering user-generated and managed events in real-time. We aim to eliminate boredom. Every person deserves to find their people, and that's what we are aiming to do.

### This application is availiable on IOS only, in the app store in October 2018.



# Technology

---

    *Firebase Realtime Database
    *Firebase Functions
    *Firebase Storage
    *Firebase Cloud Messaging
    
    *55 different Pods (THANKYOU OPEN SOURCE COMMUNITY!)
    *CoreData
    *CLLocation
    *Push Notifications
    *Photo Picker
    *Group Messaging
    *Single(Dm) messaging

---

### Feel free to reach out!

__Jackson Meyer__
__jackson.meyer7@gmai.com__
__https://www.linkedin.com/in/jacksonmeyer/__
__Portland, OR__

## P.S - I am an MBA student that just loves coding, I am self-taught, so excuse some inefficiencies in the codebase


