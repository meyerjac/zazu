const functions = require('firebase-functions');
const admin = require('firebase-admin');
const mkdirp = require('mkdirp-promise');
// Include a Service Account Key to use a Signed URL
const gcs = require('@google-cloud/storage')({keyFilename: 'service-account-credentials.json'});
admin.initializeApp();

const spawn = require('child-process-promise').spawn;
const path = require('path');
const os = require('os');
const fs = require('fs');

// Max height and width of the thumbnail in pixels.
const THUMB_MAX_HEIGHT = 200;
const THUMB_MAX_WIDTH = 200;
// Thumbnail prefix added to file names.
const THUMB_PREFIX = 'thumb_';
const sg_api = require('./sg_API');

//SEND EVENT DELETED NOTIFICATION.
exports.sendEventDeleteNotifications = functions.database.ref(`EVENT_ATTENDEES/{country_id}/{state_id}/{city_id}/{event_uid}`)
.onDelete((snapshot, context) => {
  const countryUid = context.params.country_id;
  const stateUid = context.params.state_id;
  const cityUid = context.params.city_id;
  const eventUid = context.params.event_uid;
  var activeEventsRef = admin.database().ref(`EVENTS/${countryUid}/${stateUid}/${cityUid}/EVENTS_ACTIVE/${eventUid}`)
  var pastEventsRef = admin.database().ref(`PAST_EVENTS/${countryUid}/${stateUid}/${cityUid}/EVENTS_PAST/${eventUid}`)
  var notificationTitle;
  var notificationEventTitle;
  var eventOwnerUrl;
  var eventOwnerUid;
  var eventOwnerUserName;
  var deviceIds;
  var keys = []

  console.log(snapshot, "snapshot")

  snapshot.forEach(function (snapshot) {
    if (snapshot.key === "ATTENDEES") {
      snapshot.forEach(function (snapshot) {
        var key = snapshot.key
          keys.push(key)
      });
      firstMethod()
      }
      return null
    });

    function firstMethod() {
        admin.database().ref(`EVENTS/${countryUid}/${stateUid}/${cityUid}/EVENTS_ACTIVE/${eventUid}`).once("value", function(snapshot) {
          console.log(snapshot.val(), "snap")
          if (snapshot.val() !== null) {
            activeEventsRef.remove()
            console.log("SNAP WAS NOT NULL")
            notificationEventTitle = snapshot.val().title
            eventOwnerUserName = snapshot.val().owner_name
            eventOwnerUrl = snapshot.val().owner_url
            eventOwnerUid =  snapshot.val().owner_uid
            notificationTitle = "EVENT CANCELLED"
          } else {
            admin.database().ref(`PAST_EVENTS/${countryUid}/${stateUid}/${cityUid}/EVENTS_PAST/${eventUid}`).once("value", function(snapshot) {
            pastEventsRef.remove()
            console.log(snapshot, "snap2")
            console.log("SNAP WAS IN THE PAST")
            notificationEventTitle = snapshot.val().title
            eventOwnerUserName = snapshot.val().owner_name
            eventOwnerUrl = snapshot.val().owner_url
            eventOwnerUid =  snapshot.val().owner_uid
            notificationTitle = "EVENT CANCELLED"
            });
          }

        loopThrough(keys)
        });
        return null
    }

//THIS WORKS!
function loopThrough(keys) {
  //don't send notification to host
  var index = keys.indexOf(eventOwnerUid)
  if (index > -1) {  keys.splice(index, 1); }
  if (keys.length < 1) { return null }

  const allThePromises = keys.map(key => {
    let tokensSnapshot;
    let tokens;
    return new Promise((resolve, reject) => {
      const mentionUid = key;
      const payload = {
        notification: {
          title: notificationTitle,
          body: `[${eventOwnerUserName}] cancelled the event: ${notificationEventTitle}`,
          sound: 'default'
        },
        "data" : {
            "data_type" : ".eventDelete",
            "data_url" : eventOwnerUrl,
            "data_user_name" : eventOwnerUserName,
            "data_user_uid" : eventOwnerUid,
            "data_message" : eventOwnerUid,
            "data_message_uid" : eventOwnerUid,
            "data_event_uid" : eventOwnerUid,
            "data_event_title" : notificationEventTitle,
            "data_event_country_id" : countryUid,
            "data_event_state_id" : stateUid,
            "data_event_city_id" : cityUid
          }
      };

      const getDeviceTokensPromise = admin.database().ref(`USERS/${mentionUid}/device_tokens`).once('value');
      return getDeviceTokensPromise
        .then(result => {
          tokensSnapshot = result;
          if (!tokensSnapshot.hasChildren()) {
            console.log('There are no notification tokens to send to in the function: sendEventUpdateNotificationToAcceptedUsers.')
            return Promise.reject(console.log("ERROR"));
          }
          tokens = Object.keys(tokensSnapshot.val());
          return admin.messaging().sendToDevice(tokens, payload);
        })
        .then(response => {
          // For each message check if there was an error.
          const tokensToRemove = response.results.map((result, index) => {
            const error = result.error;
            if (error) {
              console.error('Failure sending notification to', tokens[index], error);
              // Cleanup the tokens who are not registered anymore.
              if (error.code === 'messaging/invalid-registration-token' ||
                error.code === 'messaging/registration-token-not-registered') {
                // Do these tokens have a remove method?
                return tokensSnapshot.ref.child(tokens[index]).remove();
              }
            } else {
              return null;
            }
          })
          if (tokensToRemove.length) {
            return Promise.reject(tokensToRemove);
          } else {
            return Promise.resolve();
          }
        })
    });
  });

  Promise.all(allThePromises)
    .then(results => {
      return null;
    })
    .catch(tokensToRemove => {
      return null;
    });
}
});


// /*********************
// MOVE EXPIRED EVENTS TO HISTORICAL STORAGE
// **********************/
exports.event_cleanup_job = functions.pubsub
  .topic('clean_active_events_node')
  .onPublish((message) => {

  admin.database().ref('EVENTS').once("value", function(snapshot) {
    var country_code;
    var state_code;
    var city_code;

    snapshot.forEach(function (snapshot) {
      country_code = snapshot.key

      snapshot.forEach(function (snapshot) {
        state_code = snapshot.key

        snapshot.forEach(function (snapshot) {
          city_code = snapshot.key

          snapshot.forEach(function (snapshot) {
            var key = snapshot.key
            if (key === 'EVENTS_ACTIVE') {
              //active events objects
              const count = snapshot.numChildren();

              snapshot.forEach(function (snapshot) {
                //active events object (not objects)
                var event = snapshot.val();

                var start_interval = event.end_interval
                var current_date = new Date();
                var current_interval = Math.round(current_date.getTime() / 1000)

                  if (current_interval > start_interval) {
                    const historical_events_ref = admin.database().ref(`PAST_EVENTS/${country_code}/${state_code}/${city_code}/EVENTS_PAST/${event.uid}`);
                    const current_events_ref = admin.database().ref(`EVENTS/${country_code}/${state_code}/${city_code}/EVENTS_ACTIVE/${event.uid}`);

                      historical_events_ref.set(event)
                      .then(function() {

                          return current_events_ref.remove()
                          .then(function() {
                            return console.log("remove succeeded.")
                          })
                          .catch(function(error) {
                            return ("Remove failed: " + error.message)
                          });
                      })
                      .catch(function(error) {
                        return ("add failed: " + error.message)
                      });
                  }
                });
              }
            });
          });
        });
      });
      return null
    });
    return null
  });

    /*********************
    CURRENT EVENTS UPDATE TALLY
    **********************/
    //Keeps track of the number of active events of each category under EVENT_TALLIES
    exports.updateEventTally = functions.database.ref('/EVENTS/{country_id}/{state_id}/{city_id}/EVENTS_ACTIVE/{event_uid}').onWrite(
        (change) => {
          var valueAfter = change.after.val();
          var valueBefore = change.before.val();
          var event_category;
          var event_category1;
          var increment = 0
          var increment1 = 0

          if (!change.after.exists()) {
              //delted or removed
              event_category = valueBefore.category
          } else {
            if (change.before.exists()) {
              //updated
              event_category = valueAfter.category
              event_category1 = valueBefore.category
            } else {
              //new write
              event_category = valueAfter.category
            }
          }

          const collectionRef = change.after.ref.parent;
          const countRef = collectionRef.parent.child(`EVENTS_TALLY/${event_category}`);
          const countRef1 = collectionRef.parent.child(`EVENTS_TALLY/${event_category1}`);

          if (change.after.exists() && !change.before.exists()) {
            increment = 1;
          } else if (!change.after.exists() && change.before.exists()) {
            increment = -1;
          } else if (change.after.exists() && change.before.exists()) {
            increment = 1;
            increment1 = -1;
          }else {
            return null;
          }

          // Return the promise from countRef.transaction() so our function
          // waits for this async event to complete before it exits.

        return countRef.transaction((current) => {
           if ((current + increment) < 0) {
             return null
           } else {
               return (current || 0) + increment;
           }
         }).then(() => {
           if (increment1 !==0) {
             return countRef1.transaction((current) => {
                if ((current + increment1) < 0) {
                  return null
                } else {
                    return (current || 0) + increment1;
                }
              }).then(() => {
                return null
              });
           } else {
             return null
           }
         });
       });

       /*********************
       PAST EVENTS UPDATE TALLY
       **********************/
       //Keeps track of the number of events of each category that have concluded under EVENT_TALLIES
       exports.updatePastEventTally = functions.database.ref('/PAST_EVENTS/{country_id}/{state_id}/{city_id}/EVENTS_PAST/{event_uid}').onWrite(
        (change) => {
          var valueAfter = change.after.val();
          var event_category;

          if (!change.after.exists()) {
              var valueBefore = change.before.val();
              event_category = valueBefore.category
          } else {
              event_category = valueAfter.category
          }

          const collectionRef = change.after.ref.parent;
          const countRef = collectionRef.parent.child(`EVENTS_TALLY/${event_category}`);

          let increment;
          if (change.after.exists() && !change.before.exists()) {
            increment = 1;
          } else if (!change.after.exists() && change.before.exists()) {
            increment = -1;
          } else {
            return null;
          }

          // Return the promise from countRef.transaction() so our function
          // waits for this async event to complete before it exits.

        return countRef.transaction((current) => {
           if ((current + increment) < 0) {
             return null
           } else {
               return (current || 0) + increment;
           }
         }).then(() => {
           return null
         });
       });


/*********************
ATTENDEE COUNT
**********************/

//Keeps track of the length of the EVENT 'attendees' child list in a separate property.
exports.countEventAttendees = functions.database.ref('/EVENT_ATTENDEES/{country_id}/{state_id}/{city_id}/{event_uid}/ATTENDEES/{user_uid}').onWrite(
    (change, context) => {
      const countryUid = context.params.country_id;
      const stateUid = context.params.state_id;
      const cityUid  = context.params.city_id;
      const eventUid = context.params.event_uid;

      const collectionRef = change.after.ref.parent;
      const countRef = collectionRef.parent.child('ATTENDEES_COUNT');

      let increment;
      if (change.after.exists() && !change.before.exists()) {
        increment = 1;
      } else if (!change.after.exists() && change.before.exists()) {
        increment = -1;

      } else {
        return null;
      }

      // Return the promise from countRef.transaction() so our function
      // waits for this async event to complete before it exits.
      return countRef.transaction((current) => {
        if ((current + increment) < 0) {
          return null
        } else {
          if ((current + increment) === 0) {
            console.log("ZERO")
            const userAcceptedRef = admin.database().ref(`EVENT_ATTENDEES/${countryUid}/${stateUid}/${cityUid}/${eventUid}`);
            userAcceptedRef.remove()
          } else {
            console.log( "NOT ZERO")
            return (current || 0) + increment;
          }
        }
      }).then(() => {
        return null
      });
    });

    /*********************
    ATTENDEE ACCEPTED
    **********************/

    //Changes Users 'attendee_accepted' property on their events and sends Notification.
    exports.changeUsersGroupToAccepted = functions.database.ref('/EVENT_ATTENDEES/{country_id}/{state_id}/{city_id}/{event_uid}/ATTENDEES/{user_uid}').onUpdate(
        (change, context) => {
          var value = change.after.val();
          const countryUid = context.params.country_id;
          const stateUid = context.params.state_id;
          const cityUid  = context.params.city_id;
          const eventUid = value.event_uid;

          var acceptedBool = value.accepted;
          var acceptedUserUid = value.uid;
          var notificationEventTitle;
          var eventOwnerUid;
          var eventOwnerUrl;

          //need to check if user exists before writing to.
          const userAcceptedRef = admin.database().ref(`USERS_EVENTS/${acceptedUserUid}/${countryUid}/${stateUid}/${cityUid}/${eventUid}/accepted`);
          userAcceptedRef.set(acceptedBool)

          //if changed to attendee accepted === false, Return, issue with handling removal from group
          if (acceptedBool === false) { return null }

          //Event Title get
          function firstMethod() {
              admin.database().ref(`EVENTS/${countryUid}/${stateUid}/${cityUid}/EVENTS_ACTIVE/${eventUid}`).once("value", function(snapshot) {
              notificationEventTitle = snapshot.val().title
              console.log(notificationEventTitle, "title")
              notificationEventOwnerUserName = snapshot.val().owner_name
              eventOwnerUrl = snapshot.val().owner_url
              eventOwnerUid =  snapshot.val().owner_uid

              secondMethod()
              });
              return null
          }

          function secondMethod() {
            // Get the list of device notification tokens.
            const getDeviceTokensPromise = admin.database().ref(`USERS/${acceptedUserUid}/device_tokens`).once('value');
            // Get the follower profile.
            const getFollowerProfilePromise = admin.auth().getUser(acceptedUserUid);
            // The snapshot to the user's tokens.
            let tokensSnapshot;
            // The array containing all the user's tokens.
            let tokens;

            return Promise.all([getDeviceTokensPromise, getFollowerProfilePromise]).then(results => {
              tokensSnapshot = results[0];
              const follower = results[1];

              // Check if there are any device tokens.
              if (!tokensSnapshot.hasChildren()) {
                console.log('There are no notification tokens to send to.')
                return null
              }

              // Notification details.
              const payload = {
                notification: {
                  title: `${notificationEventTitle}`,
                  body: `[${notificationEventOwnerUserName}] accepted you to their event!`,
                  sound: 'default'
                },
                "data" : {
                    "data_type" : ".accepted",
                    "data_url" : eventOwnerUrl,
                    "data_user_name" : notificationEventOwnerUserName,
                    "data_user_uid" : eventOwnerUid,
                    "data_message" : "accepted you to their event: ",
                    "data_event_uid" : eventUid,
                    "data_event_title" : notificationEventTitle,
                    "data_event_country_id": countryUid,
                    "data_event_state_id" : stateUid,
                    "data_event_city_id" : cityUid
                  }
              };

              // Listing all tokens as an array.
              tokens = Object.keys(tokensSnapshot.val());
              // Send notifications to all tokens.
              return admin.messaging().sendToDevice(tokens, payload);
            }).then((response) => {
              // For each message check if there was an error.
              const tokensToRemove = [];
              response.results.forEach((result, index) => {
                const error = result.error;
                if (error) {
                  console.error('Failure sending notification to', tokens[index], error);
                  // Cleanup the tokens who are not registered anymore.
                  if (error.code === 'messaging/invalid-registration-token' ||
                      error.code === 'messaging/registration-token-not-registered') {
                    tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
                  }
                }
              });
              return Promise.all(tokensToRemove);
            });
          }

          firstMethod()
          return null
        });

/***********************************************************************
Notifications to host of people who joined or requestion to join group
************************************************************************/

/**
 * Triggers when a group gets a new user and sends a notification.
 * joinee add a flag to `/{eventJoinedUid}/{joineeUid}`.
 * Users save their device notification tokens to `/USERS/{eventJoinedUid}/notificationTokens/{notificationToken}`.
 **/
exports.sendHostNotificationOfJoinee = functions.database.ref('/EVENT_ATTENDEES/{country_id}/{state_id}/{city_id}/{event_uid}/ATTENDEES/{user_uid}')
    .onWrite((change, context) => {
      const countryUid = context.params.country_id;
      const stateUid = context.params.state_id;
      const cityUid  = context.params.city_id;
      const joineeUid = context.params.user_uid;
      const eventUid = context.params.event_uid;
      var notificationJoinRequestString;
      var notificationEventTitle;
      var eventOwnerUid;
      var eventIsPrivate;
      var attendeeUrl;

      // If user leaves event we exit the function.
      if (!change.after.val()) {
        return null
      }

      // If host owner accepts a request, this function fires and escapes without sending any notifications
      if (change.before.val()) {
        return null
      }

      var value = change.after.val();
      var notificationNewAttendeeUserName = value.user_name;
      attendeeUrl = value.url

      function firstMethod() {
          admin.database().ref(`EVENTS/${countryUid}/${stateUid}/${cityUid}/EVENTS_ACTIVE/${eventUid}`).once("value", function(snapshot) {

          eventOwnerUid = snapshot.val().owner_uid
          notificationEventTitle = snapshot.val().title

          if (snapshot.val().is_private === true) {
              eventIsPrivate = true
              notificationJoinRequestString = " requested to join your event"
          } else {
              eventIsPrivate = false
              notificationJoinRequestString = " joined your event"
          }

          //author wont get notification
          if (eventOwnerUid === joineeUid) {
            return null
          }

          //fire notification
          secondMethod()
          });
          return null
      }

      function secondMethod() {
        // Get the list of device notification tokens.
        const getDeviceTokensPromise = admin.database().ref(`USERS/${eventOwnerUid}/device_tokens`).once('value');
        // Get the follower profile.
        const getFollowerProfilePromise = admin.auth().getUser(joineeUid);
        // The snapshot to the user's tokens.
        let tokensSnapshot;
        // The array containing all the user's tokens.
        let tokens;

        return Promise.all([getDeviceTokensPromise, getFollowerProfilePromise]).then(results => {
          tokensSnapshot = results[0];
          const follower = results[1];

          // Check if there are any device tokens.
          if (!tokensSnapshot.hasChildren()) {
            console.log('There are no notification tokens to send to.')
            return null
          }

          // Notification details.
          const payload = {
            notification: {
              title: `[${notificationNewAttendeeUserName}]` + notificationJoinRequestString,
              body: notificationEventTitle,
              sound: 'default'
            },
            "data" : {
                "data_type" : ".joinedrequested",
                "data_url" : attendeeUrl,
                "data_user_name" : notificationNewAttendeeUserName,
                "data_user_uid" : joineeUid,
                "data_message" : notificationJoinRequestString,
                "data_event_uid" : eventUid,
                "data_event_title" : notificationEventTitle,
                "data_event_country_id": countryUid,
                "data_event_state_id" : stateUid,
                "data_event_city_id" : cityUid
              }
          };

          // Listing all tokens as an array.
          tokens = Object.keys(tokensSnapshot.val());
          // Send notifications to all tokens.
          return admin.messaging().sendToDevice(tokens, payload);
        }).then((response) => {
          // For each message check if there was an error.
          const tokensToRemove = [];
          response.results.forEach((result, index) => {
            const error = result.error;
            if (error) {
              console.error('Failure sending notification to', tokens[index], error);
              // Cleanup the tokens who are not registered anymore.
              if (error.code === 'messaging/invalid-registration-token' ||
                  error.code === 'messaging/registration-token-not-registered') {
                tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
              }
            }
          });
          return Promise.all(tokensToRemove);
        });
      }

      firstMethod()
      return null
    });

    // /***********************************************************************
    // EDITED EVENT Notifications send to people accepted to event
    // ************************************************************************/
    //Triggers when a event message has a mention.

    exports.sendEventUpdateNotificationToAcceptedUsers = functions.database.ref(`EVENTS/{country_id}/{state_id}/{city_id}/EVENTS_ACTIVE/{event_uid}`)
    .onUpdate((change, context) => {
      const countryUid = context.params.country_id;
      const stateUid = context.params.state_id;
      const cityUid = context.params.city_id;
      const eventUid = context.params.event_uid;
      var ref = admin.database().ref('USERS')
      let notificationEventTitle;
      var eventOwnerUrl;
      var eventOwnerUid;
      var eventOwnerUserName;
      var deviceIds;


      firstMethod()

      function firstMethod() {
        var keys = []

        admin.database().ref(`EVENTS/${countryUid}/${stateUid}/${cityUid}/EVENTS_ACTIVE/${eventUid}`).once("value", function(snapshot) {
            eventOwnerUrl = snapshot.val().owner_url
            eventOwnerUid = snapshot.val().owner_uid
            notificationEventTitle = snapshot.val().title
            eventOwnerUserName = snapshot.val().owner_name

        //get all event attendees
          admin.database().ref(`EVENT_ATTENDEES/${countryUid}/${stateUid}/${cityUid}/${eventUid}/ATTENDEES`).once("value", function(snapshot) {
          snapshot.forEach(function(childSnapshot) {
            var key = childSnapshot.key; // you will get your key here
            keys.push(key)
          });
            var index = keys.indexOf(eventOwnerUid)
            if (index > -1) {
              keys.splice(index, 1);
            }

            if (keys.length < 1) {
              return null
            }

            //fire notifications
            loopThrough(keys)
            });
          });
        return null
      }

      function loopThrough(keys) {
        const allThePromises = keys.map(key => {
          let tokensSnapshot;
          let tokens;
          return new Promise((resolve, reject) => {
            const mentionUid = key;
            const payload = {
              notification: {
                title:`[${eventOwnerUserName}] edited the event details`,
                body: `[${notificationEventTitle}]`,
                sound: 'default'
              },
              "data" : {
                  "data_type" : ".eventEdit",
                  "data_url" : eventOwnerUrl,
                  "data_user_name" : eventOwnerUserName,
                  "data_user_uid" : eventOwnerUid,
                  "data_message" : eventUid,
                  "data_message_uid" : eventUid,
                  "data_event_uid" : eventUid,
                  "data_event_title" : notificationEventTitle,
                  "data_event_country_id" : countryUid,
                  "data_event_state_id" : stateUid,
                  "data_event_city_id" : cityUid
                }
            };

            const getDeviceTokensPromise = admin.database().ref(`USERS/${mentionUid}/device_tokens`).once('value');
            return getDeviceTokensPromise
              .then(result => {
                tokensSnapshot = result;
                if (!tokensSnapshot.hasChildren()) {
                  console.log('There are no notification tokens to send to in the function: sendEventUpdateNotificationToAcceptedUsers.')
                  return Promise.reject(console.log("ERROR"));
                }
                tokens = Object.keys(tokensSnapshot.val());
                return admin.messaging().sendToDevice(tokens, payload);
              })
              .then(response => {
                // For each message check if there was an error.
                const tokensToRemove = response.results.map((result, index) => {
                  const error = result.error;
                  if (error) {
                    console.error('Failure sending notification to', tokens[index], error);
                    // Cleanup the tokens who are not registered anymore.
                    if (error.code === 'messaging/invalid-registration-token' ||
                      error.code === 'messaging/registration-token-not-registered') {
                      // Do these tokens have a remove method?
                      return tokensSnapshot.ref.child(tokens[index]).remove();
                    }
                  } else {
                    return null;
                  }
                })
                if (tokensToRemove.length) {
                  return Promise.reject(tokensToRemove);
                } else {
                  return Promise.resolve();
                }
              })
          });
        });

        Promise.all(allThePromises)
          .then(results => {
            return null;
          })
          .catch(tokensToRemove => {
            return null;
          });
      }
    });


    // /***********************************************************************
    // Notifications of new Message
    // ************************************************************************/
    //triggers only when flag isn't present or on false

    exports.sendNewDmNotification = functions.database.ref('/DIRECT_MESSAGING/{current_user_uid}/{other_user_uid}/messages/{message_uid}')
        .onWrite((change, context) => {

          //deleted, escapes
          var value = change.after.val();
            if (!value) {
              return null
          }

          const otherUserUid = context.params.other_user_uid;
          const recievingUserUid = context.params.current_user_uid;
          const messageUid = context.params.message_uid;

          var messageOwnerUserName = value.owner_user_name;
          var sendingOwnerUid = value.owner_uid
          var ownerMessageUrl  = value.owner_url
          var messageIntervalReference  = value.interval_reference
          var messageMedia = value.media
          var newMessageText;

          //doesn't fire a notification if incoming message was sent by current user
          if (sendingOwnerUid === recievingUserUid) {
            return null
          }

          if (messageMedia === true) {
            newMessageText = "Attachment: 1 image"
          } else {
            newMessageText = value.text
          }

          function getFlagStatus() {
            //getting flag of other user to see if there are present
              admin.database().ref(`DIRECT_MESSAGING/${otherUserUid}/${recievingUserUid}/is_present`).once("value", function(snapshot) {
                if (snapshot.exists()) {
                  isPresentBool = snapshot.val()

                  if (isPresentBool === true) {
                      return null
                  } else {
                       var notificationTitleDM =  `You have a new message from ${messageOwnerUserName}`
                       sendNotification(notificationTitleDM)
                  }
              } else {
                notificationTitleDM = `You have a new message from ${messageOwnerUserName}`
                sendNotification(notificationTitleDM)
              }
              });
              return null
          }

          function sendNotification(notificationTitleDM) {
            // Get the list of device notification tokens.
            const getDeviceTokensPromise = admin.database().ref(`USERS/${recievingUserUid}/device_tokens`).once('value');
            // Get the follower profile.
            const getIncomingDmProfilePromise = admin.auth().getUser(otherUserUid);
            // The snapshot to the user's tokens.
            let tokensSnapshot;
            // The array containing all the user's tokens.
            let tokens;

            return Promise.all([getDeviceTokensPromise, getIncomingDmProfilePromise]).then(results => {
              tokensSnapshot = results[0];
              const follower = results[1];

              // Check if there are any device tokens.
              if (!tokensSnapshot.hasChildren()) {
                console.log('There are no notification tokens to send to.')
                return null
              }

              // Set the message as high priority and have it expire after 24 hours.
              var options = {
                contentAvailiable: 1,
                content_availiable: true,
              };

                // Notification details.
                const payload = {
                  notification: {
                    title: notificationTitleDM,
                    body: newMessageText,
                    sound: 'default',
                  },
                  "data" : {
                      "data_type" : ".dm",
                      "data_url" : ownerMessageUrl,
                      "data_user_name" : messageOwnerUserName,
                      "data_user_uid" : otherUserUid,
                      "data_message" : newMessageText,
                      "data_message_uid" : messageUid,
                      "data_time_recieved" : messageIntervalReference.toString(),
                      "data_event_country_id": "",
                      "data_state_id" : "",
                      "data_city_id" : "",
                      "data_country_id": ""
                    }
                };

              // Listing all tokens as an array.
              tokens = Object.keys(tokensSnapshot.val());
              // Send notifications to all tokens.
              return admin.messaging().sendToDevice(tokens, payload, options);
            }).then((response) => {
              // For each message check if there was an error.
              const tokensToRemove = [];
              response.results.forEach((result, index) => {
                const error = result.error;
                if (error) {
                  console.error('Failure sending notification to', tokens[index], error);
                  // Cleanup the tokens who are not registered anymore.
                  if (error.code === 'messaging/invalid-registration-token' ||
                      error.code === 'messaging/registration-token-not-registered') {
                    tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
                  }
                }
              });
              return Promise.all(tokensToRemove);
            });
          }

          getFlagStatus()
          return null
        });

    // /***********************************************************************
    // Notifications to people mentioned in event
    // ************************************************************************/
    //Triggers when a event message has a mention.

    exports.sendNotificationToMentionedUsers = functions.database.ref(`EVENT_MESSAGING/{country_id}/{state_id}/{city_id}/{event_uid}/MESSAGES/{message_uid}/mentions`)
		.onWrite((change, context) => {
      const countryUid = context.params.country_id;
			const stateUid = context.params.state_id;
			const cityUid = context.params.city_id;
			const eventUid = context.params.event_uid;
			const messageUid = context.params.message_uid;
			let notificationEventTitle;
      let eventOwnerUrl;
      let userNameOfMentioner;
      let messageOwnerUid;
      let message;
      let mentionerUrl

			//variables that are filled later on
			var ref = admin.database().ref('USERS')
			var numberOfMentions;
			var deviceIds;

			// If message/event/data is deleted, we return so function doesnt fire
			if (!change.after.val()) {
				return null
			}

			//this starts the chain
			firstMethod()


			function firstMethod() {
				//get all uids of people who were mentioned,
				var keys = Object.keys(change.after.val());
				numberOfMentions = keys.length
				//just get event Title
				admin.database().ref(`EVENTS/${countryUid}/${stateUid}/${cityUid}/EVENTS_ACTIVE/${eventUid}`).once("value", function(snapshot) {
					eventOwnerUid = snapshot.val().owner_uid
					notificationEventTitle = snapshot.val().title
          eventOwnerUrl = snapshot.val().owner_url

          admin.database().ref(`EVENT_MESSAGING/${countryUid}/${stateUid}/${cityUid}/${eventUid}/MESSAGES/${messageUid}`).once("value", function(snapshot) {
            message = snapshot.val().text
            userNameOfMentioner = snapshot.val().owner_user_name
            messageOwnerUid = snapshot.val().owner_uid

            var index = keys.indexOf(messageOwnerUid)
            if (index > -1) {
              keys.splice(index, 1);
            }

            if (keys.length < 1) {
              return null
            }

            mentionerUrl = snapshot.val().owner_url

            //fire notifications
            loopThrough(keys)
          });
				});
				return null
			}

			function loopThrough(keys) {
				const allThePromises = keys.map(key => {
					let tokensSnapshot;
					let tokens;
					return new Promise((resolve, reject) => {
						const mentionUid = key;
						const payload = {
							notification: {
								title: `[${userNameOfMentioner}] mentioned you in the group chat`,
								body: `[${notificationEventTitle}]`,
								sound: 'default'
							},
              "data" : {
                  "data_type" : ".mention",
                  "data_url" : mentionerUrl,
                  "data_user_name" : userNameOfMentioner,
                  "data_user_uid" : eventOwnerUid,
                  "data_message" : message,
                  "data_message_uid" : messageUid,
                  "data_event_uid" : eventUid,
                  "data_event_title" : notificationEventTitle,
                  "data_event_country_id" : countryUid,
                  "data_event_state_id" : stateUid,
                  "data_event_city_id" : cityUid
                }
						};

						const getDeviceTokensPromise = admin.database().ref(`USERS/${mentionUid}/device_tokens`).once('value');
						return getDeviceTokensPromise
							.then(result => {
								tokensSnapshot = result;
								if (!tokensSnapshot.hasChildren()) {
									return Promise.reject(console.log("ERROR HIT"));
								}
								tokens = Object.keys(tokensSnapshot.val());
								return admin.messaging().sendToDevice(tokens, payload);
							})
							.then(response => {
								// For each message check if there was an error.
								const tokensToRemove = response.results.map((result, index) => {
									const error = result.error;
									if (error) {
										console.error('Failure sending notification to', tokens[index], error);
										// Cleanup the tokens who are not registered anymore.
										if (error.code === 'messaging/invalid-registration-token' ||
											error.code === 'messaging/registration-token-not-registered') {
											// Do these tokens have a remove method?
											return tokensSnapshot.ref.child(tokens[index]).remove();
										}
									} else {
                    return null;
                  }
								})
								if (tokensToRemove.length) {
									return Promise.reject(tokensToRemove);
								} else {
									return Promise.resolve();
								}
							}).catch(error => { console.error(error); return })
					});
				});

				Promise.all(allThePromises)
					.then(results => {
						return null;
					}).catch(tokensToRemove => {
						return null;
					});
			}
		});

    // /***********************************************************************
    // Notifications of Group Message Liked
    // ************************************************************************/
    //triggers only when flag isn't present or on false
    exports.sendGroupMessageLikeNotifications = functions.database.ref('/EVENT_MESSAGING/{country_id}/{state_id}/{city_id}/{event_uid}/MESSAGES/{message_uid}/likes/{likeOwnerUid}')
        .onWrite((change, context) => {
        var value = change.after.val();

          // If user unlikes message, we exit the function.
          if (!value) {
            return null
          }
          const countryUid = context.params.country_id;
          const stateUid = context.params.state_id;
          const cityUid = context.params.city_id;
          const eventUid = context.params.event_uid;
          const messageUid = context.params.message_uid;
          const messageLikeOwnerUid = context.params.likeOwnerUid;

          var eventLikePhotoUrl = value.thumbnail_url;
          var eventLikeUserUid = value.uid;
          var eventLikeUserName = value.user_name;
          var eventLikeMessageText;
          var eventLikeIntervalReference;
          var messageOwnerUid;

          let firstFive;

          function getMessageOwnerUid() {
            //getting info from event Message
              admin.database().ref(`/EVENT_MESSAGING/${countryUid}/${stateUid}/${cityUid}/${eventUid}/MESSAGES/${messageUid}`).once("value", function(snapshot) {
                if (snapshot.exists()) {
                  var snapValue = snapshot.val()
                  messageOwnerUid = snapValue.owner_uid
                  eventLikeMessageText = "liked your message" + snapValue.text
                  firstFive = eventLikeMessageText.substring(0,5)
                  if (firstFive === "https") {
                    eventLikeMessageText = "image message"
                  }

                  if (messageLikeOwnerUid !== messageOwnerUid) {
                    sendNotification()
                  }
                }
              });
              return null
          }

          function sendNotification() {
            // Get the list of device notification tokens.
            const getDeviceTokensPromise = admin.database().ref(`USERS/${messageOwnerUid}/device_tokens`).once('value');

            // Get the follower profile.
            const getFollowerProfilePromise = admin.auth().getUser(eventLikeUserUid);
            // The snapshot to the user's tokens.
            let tokensSnapshot;
            // The array containing all the user's tokens.
            let tokens;

            return Promise.all([getDeviceTokensPromise, getFollowerProfilePromise]).then(results => {
              tokensSnapshot = results[0];
              const follower = results[1];

              // Check if there are any device tokens.
              if (!tokensSnapshot.hasChildren()) {
                console.log('There are no notification tokens to send to.')
                return null
              }

                // Notification details.
                const payload = {
                  notification: {
                    body: `[${eventLikeUserName}]` + " liked your message ",
                    sound: 'default'
                  },
                  "data" : {
                      "data_type" : ".eventMessageLike",
                      "data_url" : eventLikePhotoUrl,
                      "data_user_name" : eventLikeUserName,
                      "data_user_uid" : messageLikeOwnerUid,
                      "data_message" : eventLikeMessageText,
                      "data_message_uid" : messageUid,
                      "data_event_uid" : eventUid,
                      "data_event_title" : "event title",
                      "data_time_recieved" : (new Date).getTime().toString(),
                      "data_event_country_id" : countryUid,
                      "data_state_id" : stateUid,
                      "data_city_id" : cityUid
                    }
                };

              // Listing all tokens as an array.
              tokens = Object.keys(tokensSnapshot.val());
              // Send notifications to all tokens.
              return admin.messaging().sendToDevice(tokens, payload);
            }).then((response) => {
              // For each message check if there was an error.
              const tokensToRemove = [];
              response.results.forEach((result, index) => {
                const error = result.error;
                if (error) {
                  console.error('Failure sending notification to', tokens[index], error);
                  // Cleanup the tokens who are not registered anymore.
                  if (error.code === 'messaging/invalid-registration-token' ||
                      error.code === 'messaging/registration-token-not-registered') {
                    tokensToRemove.push(tokensSnapshot.ref.child(tokens[index]).remove());
                  }
                }
              });
              return Promise.all(tokensToRemove);
            });
          }

          getMessageOwnerUid()
          return null
        });

        /*******************************************
        AUTO-CREATING THUMBNAIL USING IMAGEMAGICK
        ********************************************/

          exports.generateThumbnail = functions.storage.object().onFinalize((object) => {
          // File and directory paths.
          const filePath = object.name;
          const contentType = object.contentType; // This is the image MIME type
          const fileDir = path.dirname(filePath);
          const fileName = path.basename(filePath);
          const thumbFilePath = path.normalize(path.join(fileDir, `${THUMB_PREFIX}${fileName}`));
          const tempLocalFile = path.join(os.tmpdir(), filePath);
          const tempLocalDir = path.dirname(tempLocalFile);
          const tempLocalThumbFile = path.join(os.tmpdir(), thumbFilePath);
          var user_uid;
          var fileNameSplit;
          var thumbnailNumber;

          fileNameSplit = filePath.split("/");
          thumbnailNumber = fileNameSplit[2].match(/\d+/)[0];

          if (fileNameSplit.length === 3) {
             //correct string
             user_uid = fileNameSplit[1];
             if (thumbnailNumber !== "1") {
               //other profile images written
               return null;
             }
          } else {
            return null;
          }

          //Exit if the image is already a thumbnail.
          if (fileName.startsWith(THUMB_PREFIX)) {
            return null;
          }

          // Cloud Storage files.
          const bucket = gcs.bucket(object.bucket);
          const file = bucket.file(filePath);
          const thumbFile = bucket.file(thumbFilePath);
          const metadata = {
            contentType: contentType,
            // To enable Client-side caching you can set the Cache-Control headers here. Uncomment below.
            // 'Cache-Control': 'public,max-age=3600',
          };

          // Create the temp directory where the storage file will be downloaded.
          return mkdirp(tempLocalDir).then(() => {
            // Download file from bucket.
            return file.download({destination: tempLocalFile});
          }).then(() => {
            // Generate a thumbnail using ImageMagick.
            return spawn('convert', [tempLocalFile, '-thumbnail', `${THUMB_MAX_WIDTH}x${THUMB_MAX_HEIGHT}>`, tempLocalThumbFile], {capture: ['stdout', 'stderr']});
          }).then(() => {
            // Uploading the Thumbnail.
            return bucket.upload(tempLocalThumbFile, {destination: thumbFilePath, metadata: metadata});
          }).then(() => {
            // Once the image has been uploaded delete the local files to free up disk space.
            fs.unlinkSync(tempLocalFile);
            fs.unlinkSync(tempLocalThumbFile);
            // Get the Signed URLs for the thumbnail and original image.
            const config = {
              action: 'read',
              expires: '03-01-2500',
            };

            return Promise.all([
              thumbFile.getSignedUrl(config),
              file.getSignedUrl(config),
            ]);
          }).then((results) => {
            const thumbResult = results[0];
            const originalResult = results[1];
            const thumbFileUrl = thumbResult[0];
            const fileUrl = originalResult[0];
            // Add the URLs to the Database
            return admin.database().ref("USERS/" + user_uid + "/thumbnail").set(thumbFileUrl);
          }).then(() => console.log('Thumbnail URLs saved to database.'));
        });

        /*********************
        NEW USER WELCOME EMAIL
        **********************/
        // using SendGrid's v3 Node.js Library
        // https://github.com/sendgrid/sendgrid-nodejs
        exports.welcomeEmail = functions.database.ref('/USERS/{user_uid}').onCreate((change, context) => {
        const user_id = context.params.user_uid;
        const sgMail = require('@sendgrid/mail');
        var user_email;

        admin.auth().getUser(user_id)
          .then(function(userRecord) {
            // See the UserRecord reference doc for the contents of userRecord.
              user_email = userRecord.email
              sgMail.setApiKey(sg_api.getSgApi());
              const msg = {
                to: user_email,
                from: 'team@zazu.com',
                subject: 'Welcome to the zazu family',
                templateId: 'd-1d2a654d41d84a1cb5ceb5315d85a4d6',
              };
              return sgMail.send(msg);
            })
            .catch(function(error) {
              return console.log("Error fetching user data:", error);
            });
            return null
          });

          /*********************
          ACCOUNT DELETION EMAIL
          **********************/
          // using SendGrid's v3 Node.js Library
          // https://github.com/sendgrid/sendgrid-nodejs
          exports.deleteEmail = functions.database.ref('/USERS/{user_uid}').onDelete((snapshot, context) => {
            const user_id = context.params.user_uid;
            const sgMail = require('@sendgrid/mail');
            var user_email;

          admin.auth().getUser(user_id)
            .then(function(userRecord) {
              // See the UserRecord reference doc for the contents of userRecord.
                user_email = userRecord.email

                sgMail.setApiKey(sg_api.getSgApi());
                const msg = {
                  to: user_email,
                  from: 'team@zazu.com',
                  subject: 'You are leaving!?',
                  templateId: 'd-83e8cd103adb4f1bbb3ac12806f5fe48',
                };
                return sgMail.send(msg);
              })
              .catch(function(error) {
                return console.log("Error fetching user data:", error);
              });
              return null
            });
