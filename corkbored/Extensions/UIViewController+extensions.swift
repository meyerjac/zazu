//
//  UIViewController+extensions.swift
//  corkbored
//
//  Created by Jackson Meyer on 1/21/19.
//  Copyright © 2019 Jackson Meyer. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func getCurrentTimeAsEpoch() -> Int {
        let date = NSDate()
        let timeInterval = date.timeIntervalSince1970
        
        // convert to Integer
        let myInt = Int(timeInterval)
        return myInt
    }
    
    func eventIsValid(eventEndInt: Int) -> Bool {
        let date = NSDate()
        let timeInterval = date.timeIntervalSince1970
        
        // convert to Integer
        let myInt = Int(timeInterval)
        if eventEndInt < myInt {
            return true
        } else {
            return false
        }
    }
    
    //EVENT DETAILS, SEE IF EVENT IS IN PAST
    func isAPastEvent(eventTime: String) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        if let date = dateFormatter.date(from: eventTime) {
            let eventEndDateEpoch = date.timeIntervalSince1970
            
            //compare ot current time and return Bool
            let currentDate = NSDate()
            let currentDateTime = currentDate.timeIntervalSince1970
            
            // convert to Integer
            let currentInt = Int(currentDateTime)
            let eventInt = Int(eventEndDateEpoch)
            
            if currentInt < eventInt {
                return false
            } else {
                 return true
            }
        }
        return false
    }
}
