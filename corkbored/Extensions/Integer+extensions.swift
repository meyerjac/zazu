//
//  Integer+extensions.swift
//  corkbored
//
//  Created by Jackson Meyer on 1/18/19.
//  Copyright © 2019 Jackson Meyer. All rights reserved.
//

import Foundation

extension BinaryInteger {
    func getTimeStamp(interval: Int) -> String {
        var stringTimeStamp = ""
        let nowish = self.getCurrentTimeAsEpoch()
        let bunchDate = interval
        let difference = ((nowish - bunchDate)/60)
        let stringDifference = String(difference)
        let delimiter = "."
        var numbers = stringDifference.components(separatedBy: delimiter)
        guard let minutesSince = Int(numbers[0]) else { return "~5m"}
        if minutesSince < 5 {
            stringTimeStamp = "~5m"
            return stringTimeStamp
        } else if minutesSince <= 60 {
            stringTimeStamp = "\(minutesSince)m"
            return stringTimeStamp
        } else if ((minutesSince > 60) && (minutesSince < 1440)) {
            let hours = minutesSince/60
            stringTimeStamp = "\(hours)hr"
            return stringTimeStamp
        } else if minutesSince >= 1440 {
            let days = minutesSince/1440
            stringTimeStamp = "\(days)d"
            return stringTimeStamp
        } else if minutesSince >= 10080 {
            let weeks = minutesSince/10080
            stringTimeStamp = "\(weeks)w"
            return stringTimeStamp
        } else {
            return "now"
        }
    }
    
    func getCurrentTimeAsEpoch() -> Int {
        let date = NSDate()
        let timeInterval = date.timeIntervalSince1970
        
        // convert to Integer
        let myInt = Int(timeInterval)
        return myInt
    }
}

