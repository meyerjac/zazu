//
//  UINavigationBar+color.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/3/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

extension UINavigationBar {
    func setGradientBackground(colors: [UIColor]) {
        var updatedFrame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: 44)
        updatedFrame.size.height += self.frame.origin.y
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors)
        setBackgroundImage(gradientLayer.createGradientImage(), for: UIBarMetrics.default)
    }
}
