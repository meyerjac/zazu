//
//  Constants.swift
//  corkbored
//
//  Created by Jackson Meyer on 2/14/19.
//  Copyright © 2019 Jackson Meyer. All rights reserved.
//

import Foundation

struct Constants {
    static let feedViewControllerCategoryTitles = ["all", "in progress", "business", "crafts", "education","fitness", "food & drink", "games", "health", "lifestyle", "music",
                                                   "outdoors", "pets", "politics", "spirituality", "sports", "technology", "travel"]
    
    static let feedViewControllerCategoryImages = ["all", "clock", "business", "crafts","education", "fitness", "foodDrink", "games", "health", "lifestyle", "music",
                                                   "outdoor", "pets", "politics", "spirituality", "sports", "science", "travel"]
    
    static let CreateEventViewControllerCategoryTitles = ["select one", "business", "crafts", "education","fitness", "food & drink", "games", "health", "lifestyle", "music",
                                                          "outdoors", "pets", "politics", "spirituality", "sports", "technology", "travel"]
    
    static let CreateEventViewControllerCategoryImageTitles = ["all", "business", "crafts","education", "fitness", "foodDrink", "games", "health", "lifestyle", "music",
                                                            "outdoor", "pets", "politics", "spirituality", "sports", "science", "travel"]
    
    
    static var FeedViewControllerEventCategoryDictionary = [String: Int]()

    static let MapboxAccessToken = "pk.eyJ1IjoibWV5ZXJqYWMiLCJhIjoiY2pzbGM5ZmhtMWh6eDQ0cW9oeTdqa3VpbSJ9.zkXH5w0IT8zH6fXASU3wbg"

    //BIG CHANGES
}


