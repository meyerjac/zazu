//
//  UILabel+extensions.swift
//  corkbored
//
//  Created by Jackson Meyer on 3/30/19.
//  Copyright © 2019 Jackson Meyer. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func addCharacterSpacing(kernValue: Double = 1.1) {
        if let labelText = text, labelText.count > 0 {
            let attributedString = NSMutableAttributedString(string: labelText)
            attributedString.addAttribute(NSAttributedString.Key.kern, value: kernValue, range: NSRange(location: 0, length: attributedString.length - 1))
            attributedText = attributedString
        }
    }
}
