//
//  UIViewController+alert.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/26/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView

extension UIViewController {
    
      //MARK: - AGE ALERT
    func showAgeAlert(subtitle: String, title: String, buttonTitle: String) {
        if let titleFont = UIFont(name: "Metropolis-Medium", size: 18) {
            if let bodyFont = UIFont(name: "Metropolis-Medium", size: 14) {
                guard let accentColor = UIColor(named: "zazuPurple") else { return }
                
                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: titleFont,
                    kTextFont:  bodyFont,
                    kButtonFont: bodyFont,
                    showCloseButton: false, showCircularIcon: false, contentViewColor: UIColor.white)
                
                let alert = SCLAlertView(appearance: appearance)
                let button = alert.addButton(buttonTitle, target: self, selector: #selector(ViewController3.handleAlertClose))
                alert.showSuccess(title, subTitle: subtitle)
                button.backgroundColor = accentColor
                button.setTitleColor(UIColor.white, for: .normal)
                button.setTitleColor(UIColor.white, for: .selected)
            }
        }
    }
    
    //MARK: - ?????
    func showAlert(subtitle: String, title: String, buttonTitle: String) {
        if let titleFont = UIFont(name: "Metropolis-Medium", size: 18) {
            if let bodyFont = UIFont(name: "Metropolis-Medium", size: 14) {
                guard let accentColor = UIColor(named: "zazuBlue") else { return }
                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: titleFont,
                    kTextFont: bodyFont,
                    kButtonFont: bodyFont,
                    showCloseButton: false,  showCircularIcon: false)
                let alertView = SCLAlertView(appearance: appearance)
                
                alertView.addButton(buttonTitle, backgroundColor: accentColor, textColor: UIColor.white, target:self, selector: #selector(DetailsViewController.dismissME))
                
                alertView.showSuccess(title, subTitle: subtitle)
            }
        }
    }
    
    //MARK: - WARNING OF BLOCKED USER HOSTING EVENT YOU ARE JOINING
    func showWarningAlert(subtitle: String, title: String, buttonTitle: String) {
        if let titleFont = UIFont(name: "Metropolis-Medium", size: 18) {
            if let bodyFont = UIFont(name: "Metropolis-Medium", size: 14) {
                
                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: titleFont,
                    kTextFont: bodyFont,
                    kButtonFont: bodyFont,
                    showCloseButton: false,  showCircularIcon: false)
                let alertView = SCLAlertView(appearance: appearance)
        
                alertView.addButton(buttonTitle, backgroundColor: UIColor(named: "zazuWarning"), textColor: UIColor.white, target:self, selector: #selector(DetailsViewController.dismissME))
        
                alertView.showSuccess(title, subTitle: subtitle)
            }
        }
    }
    
    func showAlert1(subtitle: String, title: String, buttonTitle: String) {
        if let titleFont = UIFont(name: "Metropolis-Medium", size: 18) {
            if let bodyFont = UIFont(name: "Metropolis-Medium", size: 14) {
                guard let accentColor = UIColor(named: "zazuBlue") else { return }
                
                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: titleFont,
                    kTextFont: bodyFont,
                    kButtonFont: bodyFont,
                    showCloseButton: false,  showCircularIcon: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                
                alertView.addButton(buttonTitle, backgroundColor: accentColor, textColor: UIColor.white, target:self, selector: #selector(tabbedProfileViewController.dismissAlert))
                
                alertView.showSuccess(title, subTitle: subtitle)
            }
        }
    }
    
    func showNoEventsAlert(subtitle: String, title: String, buttonTitle: String) {
        if let titleFont = UIFont(name: "Metropolis-Medium", size: 18) {
            if let bodyFont = UIFont(name: "Metropolis-Medium", size: 14) {
                guard let accentColor = UIColor(named: "zazuBlue") else { return }
                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: titleFont,
                    kTextFont: bodyFont,
                    kButtonFont: bodyFont,
                    showCloseButton: false,  showCircularIcon: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                
                alertView.addButton(buttonTitle, backgroundColor: accentColor, textColor: UIColor.white, target:self, selector: #selector(FeedViewController.dismissAlert))
                
                alertView.showSuccess(title, subTitle: subtitle)
                
            }
        }
    }
    
    //MARK: - private bunch clicked in eventsTVC
    func showNotAcceptedYetAlert(subtitle: String, title: String, buttonTitle: String) {
        if let titleFont = UIFont(name: "Metropolis-Medium", size: 18) {
            if let bodyFont = UIFont(name: "Metropolis-Medium", size: 14) {
                guard let accentColor = UIColor(named: "zazuBlue") else { return }
                
                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: titleFont,
                    kTextFont: bodyFont,
                    kButtonFont: bodyFont,
                    showCloseButton: false,  showCircularIcon: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                
                alertView.addButton(buttonTitle, backgroundColor: accentColor, textColor: UIColor.white, target:self, selector: #selector(EventsTableViewController.dismissAlert))
                
                alertView.showSuccess(title, subTitle: subtitle)
            }
        }
    }
    
    //MARK: - app not live in users city, permissions VC
    func showNotLiveInCityAlert(subtitle: String, title: String, buttonTitle: String) {
        if let titleFont = UIFont(name: "Metropolis-Medium", size: 18) {
            if let bodyFont = UIFont(name: "Metropolis-Medium", size: 14) {
                guard let accentColor = UIColor(named: "zazuBlue") else { return }
                
                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: titleFont,
                    kTextFont: bodyFont,
                    kButtonFont: bodyFont,
                    showCloseButton: false,  showCircularIcon: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                
                alertView.addButton(buttonTitle, backgroundColor: accentColor, textColor: UIColor.white, target:self, selector: #selector(ViewControllerPermissions.dismissAlert))
                
                alertView.showSuccess(title, subTitle: subtitle)
            }
        }
    }
    
    //MARK: app not live in users city Feed VC
    func showNotLiveInCityAlertFeed(subtitle: String, title: String, buttonTitle: String) {
        if let titleFont = UIFont(name: "Metropolis-Medium", size: 18) {
            if let bodyFont = UIFont(name: "Metropolis-Medium", size: 14) {
                
                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: titleFont,
                    kTextFont: bodyFont,
                    kButtonFont: bodyFont,
                    showCloseButton: false,  showCircularIcon: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                
                alertView.showSuccess(title, subTitle: subtitle)
            }
        }
    }
    
    //MARK: - user doesn't exist when navigating to users profile
    func showNoUserExists(subtitle: String, title: String, buttonTitle: String) {
        if let titleFont = UIFont(name: "Metropolis-Medium", size: 18) {
            if let bodyFont = UIFont(name: "Metropolis-Medium", size: 14) {
                   guard let accentColor = UIColor(named: "zazuBlue") else { return }
                
                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: titleFont,
                    kTextFont: bodyFont,
                    kButtonFont: bodyFont,
                    showCloseButton: false,  showCircularIcon: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                
                alertView.addButton(buttonTitle, backgroundColor: accentColor, textColor: UIColor.white, target:self, selector: #selector(UsersProfileViewController.dismissUserDoesntExist))
                
                alertView.showSuccess(title, subTitle: subtitle)
            }
        }
    }
    
    //MARK: - event has been deleted, but the chat will live on
    func showNoEventExists(subtitle: String, title: String, buttonTitle: String) {
        if let titleFont = UIFont(name: "Metropolis-Medium", size: 18) {
            if let bodyFont = UIFont(name: "Metropolis-Medium", size: 14) {
                guard let accentColor = UIColor(named: "zazuBlue") else { return }
                
                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: titleFont,
                    kTextFont: bodyFont,
                    kButtonFont: bodyFont,
                    showCloseButton: false,  showCircularIcon: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                
                alertView.addButton(buttonTitle, backgroundColor: accentColor, textColor: UIColor.white, target:self, selector: #selector(GuestsDetailsViewController.dismissAlert))
                
                alertView.showSuccess(title, subTitle: subtitle)
            }
        }
    }
    
    //MARK: - standard Alert Error
    func showStandardAlert(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
            switch action.style{
            case .default:
                ()
            case .destructive:
                ()
            case .cancel:
                ()
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: - START time must be before After
    func startTimeBeforeEndTime(subtitle: String, title: String, buttonTitle: String) {
        if let titleFont = UIFont(name: "Metropolis-Medium", size: 18) {
            if let bodyFont = UIFont(name: "Metropolis-Medium", size: 14) {
                guard let accentColor = UIColor(named: "zazuBlue") else { return }
                
                let appearance = SCLAlertView.SCLAppearance(
                    kTitleFont: titleFont,
                    kTextFont: bodyFont,
                    kButtonFont: bodyFont,
                    showCloseButton: false,  showCircularIcon: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                
                alertView.addButton(buttonTitle, backgroundColor: accentColor, textColor: UIColor.white, target:self, selector: #selector(CreateEventVC1.dismissAlert))
                
                alertView.showSuccess(title, subTitle: subtitle)
            }
        }
    }
}
