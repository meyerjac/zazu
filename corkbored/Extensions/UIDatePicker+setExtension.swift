//
//  UIDatePicker+setExtension.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/10/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import Foundation
import UIKit

extension UIDatePicker {
    
    func setDate(from string: String, format: String, animated: Bool = true) {
        let formater = DateFormatter()
        formater.dateFormat = format
        let date = formater.date(from: string) ?? Date()
        setDate(date, animated: animated)
    }
}
