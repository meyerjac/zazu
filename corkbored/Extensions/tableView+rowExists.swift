//
//  tableView+rowExists.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/9/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

extension UITableView {
    
    func hasRowAtIndexPath(indexPath: NSIndexPath) -> Bool {
        return indexPath.section < self.numberOfSections && indexPath.row < self.numberOfRows(inSection: indexPath.section)
    }
}
