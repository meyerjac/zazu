//
//  String+extensions.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/24/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import Foundation
import UIKit

extension String {
    
    //MARK: - Birthday from date picker returns int age
    func calcAge(birthday: String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "MM dd yyyy"
        let birthdayDate = dateFormater.date(from: birthday)
        let calendar: NSCalendar? = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        if let birthday = birthdayDate {
            let calcAge = calendar?.components(.year, from: birthday, to: now, options: [])
            guard let age = calcAge?.year else { return 100 }
            return age
        }
        return 100
    }
    
    //MARK: - Birthday from date picker returns int age VC 2
    func DBBirthdayConvert(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        if let date = dateFormatter.date(from: date) {
            dateFormatter.dateFormat = "MM dd yyyy"
            return  dateFormatter.string(from: date)
        }
        return "unable to retreive birthday"
    }
    
    func calcAgeFromFB(birthday: String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "MM/dd/yyyy"
        let birthdayDate = dateFormater.date(from: birthday)
        let calendar: NSCalendar? = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        if let birthday = birthdayDate {
            let calcAge = calendar?.components(.year, from: birthday, to: now, options: [])
            guard let age = calcAge?.year else { return 100 }
            return age
        }
        return 100
    }
    
    func generateUserNameFromFirstAndLast(first: String, last: String) -> String {
        let index = last.index(last.startIndex, offsetBy: 0)
        let l = String(last[index])
        let FB_user_name = first + " " + l
        return FB_user_name
    }
    
    //MARK: - Feed & Details VC readble
    func getReadableDate(time: String) -> String {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        
        let myString = time // string purpose I add here
        // convert your string to date
        if let yourDate = formatter.date(from: myString) {
            //then again set the date format whhich type of output you need
            formatter.dateFormat = "EEEE, MMM d, h:mm a"
            // again convert your date to string
            let formalDate = formatter.string(from: yourDate)
            
            return formalDate
        }
        return "Unable to Retreive date"
    }
    
    func getBunchStartime(casualTime: String) -> String {
        let totalTimeArr = casualTime.components(separatedBy: "-")
        let startT = totalTimeArr[0]
        return startT
    }
    
    func getBunchTimeSection(stringBunchEndDate: String) -> String {
        var returnedDate = ""
        let endTime = stringBunchEndDate
        let dateFormatterCurrent = DateFormatter()
        dateFormatterCurrent.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        if let formatedEndDate = dateFormatterCurrent.date(from: endTime) {
            let currentDate = Date()
            let components = Set<Calendar.Component>([.second, .minute, .hour, .day, .month, .year])
            let differenceOfDate = Calendar.current.dateComponents(components, from: currentDate, to: formatedEndDate)
            //if differenceOfDate.second is negative, event end time has already occured
            
            if let second = differenceOfDate.second {
                if second > 0 { return "upcoming" }
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
                let dateFormatterPrint = DateFormatter()
                dateFormatterPrint.dateFormat = "MMM dd"
                if let date = dateFormatterGet.date(from: stringBunchEndDate) {
                    returnedDate = dateFormatterPrint.string(from: date)
                } else {
                    returnedDate = "whoops, problem occured"
                }
                return returnedDate
            }
        }
         return "couldn't find date"
    }

    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
            0x1F300...0x1F5FF, // Misc Symbols and Pictographs
            0x1F680...0x1F6FF, // Transport and Map
            0x2600...0x26FF,   // Misc symbols
            0x2700...0x27BF,   // Dingbats
            0xFE00...0xFE0F,   // Variation Selectors
            0x1F900...0x1F9FF, // Supplemental Symbols and Pictographs
            0x1F1E6...0x1F1FF: // Flags
                return true
            default:
                continue
            }
        }
        return false
    }
}
