//
//  Data+extensions.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/28/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import Foundation
import UIKit

extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
