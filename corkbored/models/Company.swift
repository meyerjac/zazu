//
//  company.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/17/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Firebase

class Company {
        var logo: String
        var name: String
        var domain: String
    
    init(logo: String, name: String, domain: String) {
        self.logo = logo
        self.name = name
        self.domain = domain
        }
    
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as? NSDictionary
        
        //check if any snapshot values are nil
        if (snapshotValue?["logo"] as? String != nil &&
            snapshotValue?["name"] as? String != nil &&
            snapshotValue?["domain"] as? String != nil){
            
            //if all are != nil then create object from snapshot
            self.logo = snapshotValue!["logo"] as! String
            self.name = snapshotValue!["name"] as! String
            self.domain = snapshotValue!["domain"] as! String
        }else{
            //if any value is == nil then create default object
            print("Failed to create company object from snapshot due to nil value, returning default object")
            self.logo = ""
            self.name = "default"
            self.domain = ""
        }
    }
    
    //default constructor so we can create empty objects if needed
    init(){
        self.logo = ""
        self.name = "default"
        self.domain = ""
    }
    
    //ryan - no changes just cleaned up formatting
    func toAnyObject() -> [String: AnyObject] {
        return ["logo": logo as AnyObject,
                "name": name as AnyObject,
                "domain": domain as AnyObject]
    }
}
