//
//  blockedUser.swift
//  corkbored
//
//  Created by Jackson Meyer on 12/19/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import Foundation
import Firebase

class BlockedUser {
    var blocked_userName: String
    var blocked_userUid: String
    var blocked_profilePhotoUrl: String
    
    init(blocked_userName: String, blocked_userUid: String, blocked_profilePhotoUrl: String) {
        self.blocked_userName = blocked_userName
        self.blocked_userUid = blocked_userUid
        self.blocked_profilePhotoUrl = blocked_profilePhotoUrl
    }
    
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as? NSDictionary
        
        //check if any snapshot values are nil
        if(snapshotValue?["blocked_userName"] as? String != nil &&
            snapshotValue?["blocked_userUid"] as? String != nil &&
            snapshotValue?["blocked_profilePhotoUrl"] as? String != nil){
            
            //if all are != nil then create object from snapshot
            self.blocked_userName = snapshotValue!["blocked_userName"] as! String
            self.blocked_userUid = snapshotValue!["blocked_userUid"] as! String
            self.blocked_profilePhotoUrl = snapshotValue!["blocked_profilePhotoUrl"] as! String
        }else{
            //if any value is == nil then create default object
            print("Failed to create blockedUser object from snapshot due to nil value, returning default object")
            self.blocked_userName = "default blocked user"
            self.blocked_userUid = ""
            self.blocked_profilePhotoUrl = ""
        }
    }
    
    //default constructor so we can create empty objects if needed
    init(){
        self.blocked_userName = "default blocked user"
        self.blocked_userUid = ""
        self.blocked_profilePhotoUrl = ""
    }
    
    //ryan - no changes just cleaned up formatting
    func toAnyObject() -> [String: AnyObject] {
        return ["blocked_userName": blocked_userName as AnyObject,
                "blocked_userUid": blocked_userUid as AnyObject,
                "blocked_profilePhotoUrl": blocked_profilePhotoUrl as AnyObject]
    }
}
