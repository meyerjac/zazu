//
//  UserModel.swift
//  corkbored
//
//  Created by Jackson Meyer on 12/6/17.
//  Copyright © 2017 Jackson Meyer. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Firebase

class User {
    var about: String
    var birthday: String
    var city: String
    var current_job: NSDictionary
    var state: String
    var dream_job: NSDictionary
    var education: NSDictionary
    var fb_id: String
    var first_name: String
    var last_name: String
    var profile_urls: NSDictionary
    var blocks: NSDictionary
    var thumbnail: String
    var social: NSDictionary
    
    init(about: String, birthday: String, city: String, current_job: NSDictionary, state: String, dream_job: NSDictionary, education: NSDictionary, fb_id: String, first_name: String, last_name: String, profile_urls: NSDictionary, blocks: NSDictionary, thumbnail: String, social: NSDictionary) {
        
        self.about = about
        self.birthday = birthday
        self.city = city
        self.current_job = current_job
        self.state = state
        self.dream_job = dream_job
        self.education = education
        self.fb_id = fb_id
        self.first_name = first_name
        self.last_name = last_name
        self.profile_urls = profile_urls
        self.blocks = blocks
        self.thumbnail = thumbnail
        self.social = social
    }
    
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as? NSDictionary
        
        self.about = snapshotValue!["about"] as! String
        self.birthday = snapshotValue!["birthday"] as! String
        self.city = snapshotValue!["city"] as! String
        self.current_job = snapshotValue?["current_job"] as? NSDictionary ?? ["":""]
        self.state = snapshotValue!["state"] as! String
        self.dream_job = snapshotValue?["dream_job"] as? NSDictionary ?? ["":""]
        self.education = snapshotValue?["education"] as? NSDictionary ?? ["":""]
        self.fb_id = snapshotValue!["fb_id"] as! String
        self.first_name = snapshotValue!["first_name"] as! String
        self.last_name = snapshotValue!["last_name"] as! String
        self.profile_urls = snapshotValue!["profile_urls"] as? NSDictionary ?? ["":""]
        self.blocks = snapshotValue?["blocks"] as? NSDictionary ?? ["":""]
        self.thumbnail = snapshotValue?["thumbnail"] as! String
        self.social = snapshotValue?["social"] as? NSDictionary ?? ["":""]
    }
    
    //default constructor so we can create empty objects if needed
    init(){
        let dictionary : [String:String] = [:]
        self.about = ""
        self.birthday = ""
        self.city = ""
        self.current_job = dictionary as NSDictionary
        self.state = ""
        self.dream_job = dictionary as NSDictionary
        self.education = dictionary as NSDictionary
        self.fb_id = ""
        self.first_name = "default"
        self.last_name = "user"
        self.profile_urls = dictionary as NSDictionary
        self.blocks = dictionary as NSDictionary
        self.thumbnail = ""
        self.social = dictionary as NSDictionary
    }
    
    //ryan - no changes just cleaned up formatting
    func toAnyObject() -> [String: AnyObject] {
        return ["about": about as AnyObject,
                "birthday": birthday as AnyObject,
                "city": city as AnyObject,
                "current_job": current_job as AnyObject,
                "state": state as AnyObject,
                "dream_job": dream_job as AnyObject,
                "education": education as AnyObject,
                "fb_id": fb_id as AnyObject,
                "first_name": first_name as AnyObject,
                "last_name": last_name as AnyObject,
                "profile_urls": profile_urls as AnyObject,
                "blocks": blocks as AnyObject,
                "thumbnail": thumbnail as AnyObject,
                "social": social as AnyObject]
    }
}
