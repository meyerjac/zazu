//
//  Attendee.swift
//  corkbored
//
//  Created by Jackson Meyer on 6/23/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Firebase

class Attendee {
    var url: String
    var user_name: String
    var uid: String
    var accepted: Bool
    var event_captain_uid: String
    var event_uid: String

    init(url: String, user_name: String, uid: String, accepted: Bool, event_captain_uid: String, event_uid: String) {
        
        self.url = url
        self.user_name = user_name
        self.uid = uid
        self.accepted = accepted
        self.event_captain_uid = event_captain_uid
        self.event_uid = event_uid
    }
    
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as? NSDictionary
        
        //check if any snapshot values are nil
        if (snapshotValue?["url"] as? String != nil &&
            snapshotValue?["user_name"] as? String != nil &&
            snapshotValue?["uid"] as? String != nil &&
            snapshotValue?["accepted"] as? Bool != nil &&
            snapshotValue?["event_captain_uid"] as? String != nil &&
            snapshotValue?["event_uid"] as? String != nil) {
            
            //if all are != nil then create object from snapshot
            self.url = snapshotValue!["url"] as! String
            self.user_name = snapshotValue!["user_name"] as! String
            self.uid = snapshotValue!["uid"] as! String
            self.accepted = snapshotValue!["accepted"] as! Bool
            self.event_captain_uid = snapshotValue!["event_captain_uid"] as! String
            self.event_uid = snapshotValue!["event_uid"] as! String
        }else{
            
            //if any value is == nil then create default object
            print("Failed to create attendee object from snapshot due to nil value, returning default object")
            self.url = ""
            self.user_name = "default attendee"
            self.uid = ""
            self.accepted = false
            self.event_captain_uid = ""
            self.event_uid = ""
        }
    }
    
    //default constructor so we can create empty objects if needed
    init(){
        self.url = ""
        self.user_name = "default attendee"
        self.uid = ""
        self.accepted = false
        self.event_captain_uid = ""
        self.event_uid = ""
    }
    
    //ryan - no changes just cleaned up formatting
    func toAnyObject() -> [String: AnyObject] {
        return ["url": url as AnyObject,
                "user_name": user_name as AnyObject,
                "uid": uid as AnyObject,
                "accepted": accepted as AnyObject,
                "event_captain_uid": event_captain_uid as AnyObject,
                "event_uid": event_uid as AnyObject]
    }
}

