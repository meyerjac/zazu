//
//  GroupMessage.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/20/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import Foundation
import FirebaseDatabase
import Firebase

class GroupMessage {
    var uid: String
    var owner_uid: String
    var owner_user_name: String
    var interval_reference: Int
    var text: String
    var owner_url: String
    var mentions: NSDictionary
    var likes: NSDictionary
    var media: Bool
    
    init(uid: String, owner_uid: String, owner_user_name: String, interval_reference: Int, text: String, owner_url: String, mentions: NSDictionary, likes: NSDictionary, media: Bool) {
        
        self.uid = uid
        self.owner_uid = owner_uid
        self.owner_user_name = owner_user_name
        self.interval_reference = interval_reference
        self.text = text
        self.owner_url = owner_url
        self.mentions = mentions
        self.likes = likes
        self.media = media
    }
    
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as? NSDictionary

        //check if any snapshot values are nil
        if(snapshotValue?["uid"] as? String != nil &&
            snapshotValue?["owner_uid"] as? String != nil &&
            snapshotValue?["owner_user_name"] as? String != nil &&
            snapshotValue?["interval_reference"] as? Int != nil &&
            snapshotValue?["text"] as? String != nil &&
            snapshotValue?["owner_url"] as? String != nil &&
            snapshotValue?["media"] as? Bool != nil){
            
            //if all are != nil then create object from snapshot
            self.uid = snapshotValue!["uid"] as! String
            self.owner_uid = snapshotValue!["owner_uid"] as! String
            self.owner_user_name = snapshotValue!["owner_user_name"] as! String
            self.interval_reference = snapshotValue!["interval_reference"] as! Int
            self.text = snapshotValue!["text"] as! String
            self.owner_url = snapshotValue!["owner_url"] as! String
            self.mentions = snapshotValue!["mentions"] as? NSDictionary ?? ["":""]
            self.likes = snapshotValue!["likes"] as? NSDictionary ?? ["":""]
            self.media = snapshotValue!["media"] as! Bool
        }else {
            //if any value is == nil then create default object
            print("Failed to create groupMessage object from snapshot due to nil value, returning default object")
            self.uid = ""
            self.owner_uid = ""
            self.owner_user_name = ""
            self.interval_reference = 0
            self.text = "this is a empty group message object if you see this then a message was object was created from a snapshot that did not contain all required data"
            self.owner_url = ""
            let dictionary : [String:String] = [:]
            self.mentions = dictionary as NSDictionary
            self.likes = dictionary as NSDictionary
            self.media = false
        }
    }
    
    //default constructor so we can create empty objects if needed
    init(){
        self.uid = ""
        self.owner_uid = ""
        self.owner_user_name = ""
        self.interval_reference = 0
        self.text = "this is a empty group message object if you see this then a message was object was created from a snapshot that did not contain all required data"
        self.owner_url = ""
        let dictionary : [String:String] = [:]
        self.mentions = dictionary as NSDictionary
        self.likes = dictionary as NSDictionary
        self.media = false
    }
    
    //ryan - no changes just cleaned up formatting
    func toAnyObject() -> [String: AnyObject] {
        return ["uid": uid as AnyObject,
                "owner_uid": owner_uid as AnyObject,
                "owner_user_name": owner_user_name as AnyObject,
                "interval_reference": interval_reference as AnyObject,
                "text": text as AnyObject,
                "owner_url": owner_url as AnyObject,
                "mentions": mentions as AnyObject,
                "likes": likes as AnyObject,
                "media": media as AnyObject]
    }
}

