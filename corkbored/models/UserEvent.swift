//
//  UserEvent.swift
//  corkbored
//
//  Created by Jackson Meyer on 4/7/19.
//  Copyright © 2019 Jackson Meyer. All rights reserved.
//


import Foundation
import FirebaseDatabase
import Firebase

//This is the model for any event a user creates
class UserEvent {
    var accepted: Bool
    var title: String
    var location: String
    var start_interval: Int
    var start_time: String
    var end_interval: Int
    var end_time: String
    var description: String
    var emojis: String
    var owner_uid: String
    var owner_name: String
    var owner_url: String
    var uid: String
    var interval_reference: Int
    var is_private: Bool
    var category: String
    var lat: Double
    var long: Double
    var type: Int
    var category_endInterval: String
    
    //initialization code
    init(accepted: Bool, title: String, location: String, start_interval: Int, start_time: String, end_interval: Int, end_time: String, description: String, emojis: String, owner_uid: String, owner_name: String, owner_url: String, uid: String, interval_reference: Int, is_private: Bool, category: String, lat: Double, long: Double, type: Int, category_endInterval: String) {
        
        self.accepted = accepted
        self.title = title
        self.location = location
        self.start_interval = start_interval
        self.start_time = start_time
        self.end_interval = end_interval
        self.end_time = end_time
        self.description = description
        self.emojis = emojis
        self.owner_uid = owner_uid
        self.owner_name = owner_name
        self.owner_url = owner_url
        self.uid = uid
        self.interval_reference = interval_reference
        self.is_private = is_private
        self.category = category
        self.lat = lat
        self.long = long
        self.type = type
        self.category_endInterval = category_endInterval
    }
    
    var image: UIImage?
    
    //This is required to retreive values from Firebase
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as? NSDictionary
        
        //check if any snapshot values are nil
        if(snapshotValue?["accepted"] as? Bool != nil &&
            snapshotValue?["title"] as? String != nil &&
            snapshotValue?["location"] as? String != nil &&
            snapshotValue?["start_interval"] as? Int != nil &&
            snapshotValue?["start_time"] as? String != nil &&
            snapshotValue?["end_interval"] as? Int != nil &&
            snapshotValue?["end_time"] as? String != nil &&
            snapshotValue?["description"] as? String != nil &&
            snapshotValue?["emojis"] as? String != nil &&
            snapshotValue?["owner_uid"] as? String != nil &&
            snapshotValue?["owner_name"] as? String != nil &&
            snapshotValue?["owner_url"] as? String != nil &&
            snapshotValue?["uid"] as? String != nil &&
            snapshotValue?["interval_reference"] as? Int != nil &&
            snapshotValue?["is_private"] as? Bool != nil &&
            snapshotValue?["category"] as? String != nil &&
            snapshotValue?["lat"] as? Double != nil &&
            snapshotValue?["long"] as? Double != nil &&
            snapshotValue?["type"] as? Int != nil &&
            snapshotValue?["category_endInterval"] as? String != nil){
            
            //if all are != nil then create object from snapshot
            self.accepted = snapshotValue!["accepted"] as! Bool
            self.title = snapshotValue!["title"] as! String
            self.location = snapshotValue!["location"] as! String
            self.start_interval = snapshotValue!["start_interval"] as! Int
            self.start_time = snapshotValue!["start_time"] as! String
            self.end_interval = snapshotValue!["end_interval"] as! Int
            self.end_time = snapshotValue!["end_time"] as! String
            self.description = snapshotValue!["description"] as! String
            self.emojis = snapshotValue!["emojis"] as! String
            self.owner_uid = snapshotValue!["owner_uid"] as! String
            self.owner_name = snapshotValue!["owner_name"] as! String
            self.owner_url = snapshotValue!["owner_url"] as! String
            self.uid = snapshotValue!["uid"] as! String
            self.interval_reference = snapshotValue!["interval_reference"] as! Int
            self.is_private = snapshotValue!["is_private"] as! Bool
            self.category = snapshotValue!["category"] as! String
            self.lat = snapshotValue!["lat"] as! Double
            self.long = snapshotValue!["long"] as! Double
            self.type = snapshotValue!["type"] as! Int
            self.category_endInterval = snapshotValue!["category_endInterval"] as! String
        }else{
            //if any value is == nil then create default object
            print("Failed to create userEvent object from snapshot due to nil value, returning default object")
            self.accepted = true
            self.title = "default user object"
            self.location = ""
            self.start_interval = 0
            self.start_time = ""
            self.end_interval = 0
            self.end_time = ""
            self.description = "Default user event object if you see this you have a problem"
            self.emojis = ""
            self.owner_uid = ""
            self.owner_name = ""
            self.owner_url = ""
            self.uid = ""
            self.interval_reference = 0
            self.is_private = false
            self.category = ""
            self.lat = 0.00
            self.long = 0.00
            self.type = 0
            self.category_endInterval = ""
        }
    }
    
    //default constructor so we can create empty objects if needed
    init(){
        self.accepted = true
        self.title = "default user object"
        self.location = ""
        self.start_interval = 0
        self.start_time = ""
        self.end_interval = 0
        self.end_time = ""
        self.description = "Default user event object if you see this you have a problem"
        self.emojis = ""
        self.owner_uid = ""
        self.owner_name = ""
        self.owner_url = ""
        self.uid = ""
        self.interval_reference = 0
        self.is_private = false
        self.category = ""
        self.lat = 0.00
        self.long = 0.00
        self.type = 0
        self.category_endInterval = ""
    }
    
    //ryan - no changes just cleaned up formatting
    //This is REQUIRED to cast objects to Firebase objects
    func toAnyObject() -> [String: AnyObject] {
        return ["accepted": accepted as AnyObject,
                "title": title as AnyObject,
                "location": location as AnyObject,
                "start_interval": start_interval as AnyObject,
                "start_time": start_time as AnyObject,
                "end_interval": end_interval as AnyObject,
                "end_time": end_time as AnyObject,
                "description": description as AnyObject,
                "emojis": emojis as AnyObject,
                "owner_uid": owner_uid as AnyObject,
                "owner_name": owner_name as AnyObject,
                "owner_url": owner_url as AnyObject,
                "uid": uid as AnyObject,
                "interval_reference": interval_reference as AnyObject,
                "is_private": is_private as AnyObject,
                "category": category as AnyObject,
                "lat": lat as AnyObject,
                "long": long as AnyObject,
                "type": type as AnyObject,
                "category_endInterval": category_endInterval as AnyObject]
    }
}
