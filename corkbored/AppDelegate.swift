//
//  AppDelegate.swift
//  corkbored
//
//  Created by Jackson Meyer on 11/29/17.
//  Copyright © 2017 Jackson Meyer. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import FBSDKCoreKit
import UserNotifications
import AudioToolbox
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate { 
    
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //not needed and caused warning
        //UIApplication.shared.statusBarStyle = .lightContent
        FirebaseApp.configure()
        setNavBarColor()
        setTabBarColor()
        
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
        return true
    }
    
    //MARK: - TAB AND NAV BAR COLORS
    private func setTabBarColor() {
        //selectedColor
        UITabBar.appearance().tintColor = UIColor.white
        //unselected color
        UITabBar.appearance().unselectedItemTintColor = UIColor.white
        //background
        UITabBar.appearance().barTintColor = UIColor(red:0.51, green:0.79, blue:1.00, alpha:1.0)
        
    }
    
    private func setNavBarColor() {
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().barTintColor = UIColor(red:0.51, green:0.79, blue:1.00, alpha:1.0)
      
        let attrs = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont(name: "nevis-Bold", size: 18)!
        ]
        
        UINavigationBar.appearance().titleTextAttributes = attrs
    }
    
    //MARK: - FACEBOOK LOGIN API
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? [UIApplication.OpenURLOptionsKey : Any])
        // Add any custom logic here.
        return handled;
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        //flip isPresent flag for most recent dm
        //LOGIC ISN'T WORKING
        let defaults = UserDefaults.standard
        if let lastDmUid = defaults.string(forKey: "lastDmUid") {
            if let uid = Auth.auth().currentUser?.uid {
                Database.database().reference().child("DIRECT_MESSAGING/\(lastDmUid)/\(uid)/is_present").setValue(false)
                Database.database().reference().child("DIRECT_MESSAGING/\(uid)/\(lastDmUid)/is_typing").setValue(false)
            }
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        PersistanceService.saveContext()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        //doesn't get called
        }
    }
    
    // [END receive_message]
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.hexString
        print(deviceTokenString, "notif")
    }

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        if let userInfo = notification.request.content.userInfo as? [String: AnyObject] {
            let notifEnum = userInfo["data_type"] as? String
            if notifEnum == ".dm" {
                let data_type = userInfo["data_type"] as? String
                let data_url = userInfo["data_url"] as? String
                let data_user_name = userInfo["data_user_name"] as? String
                let data_user_uid = userInfo["data_user_uid"] as? String
                let data_message = userInfo["data_message"] as? String
                let data_message_uid = userInfo["data_message_uid"] as? String
                let data_time_stamp = Double(getCurrentTimeAsEpoch())

                let fetchRequest: NSFetchRequest<Dm> = Dm.fetchRequest()
                if let data_user_uid = userInfo["data_user_uid"] as? String {
                    let predicate = NSPredicate(format: "dm_owner_uid = %@", argumentArray: [data_user_uid])
                    fetchRequest.predicate = predicate
                    do {
                        let result = try PersistanceService.context.fetch(fetchRequest)
                        let currentDm = result
                        if result.count == 0 {
                            //DM is a new dm
                            
                            let dmObject = Dm(context: PersistanceService.context)
                            dmObject.dm_any_messages_unread =  true
                            dmObject.dm_last_message = data_message
                            dmObject.dm_last_message_time_stamp = data_time_stamp
                            dmObject.dm_user_name = data_user_name
                            dmObject.dm_owner_uid = data_user_uid
                            
                            if let urll = data_url {
                                let url = URL(string: urll)
                                DispatchQueue.global().async {
                                    dmObject.dm_profile_photo_url = url
                                }
                            }
                        } else {
                            currentDm[0].dm_any_messages_unread = true
                            currentDm[0].dm_last_message = data_message
                            currentDm[0].dm_last_message_time_stamp = data_time_stamp
                            PersistanceService.saveContext()
                        }
                    } catch {
                        print("Failed")
                        //if failed handle
                    }
                }
                
                let notificationObject = CdNotification(context: PersistanceService.context)
                notificationObject.notification_type = data_type
                notificationObject.notification_url = data_url
                notificationObject.notification_user_name = data_user_name
                notificationObject.notification_user_uid = data_user_uid
                notificationObject.notification_string = data_message
                notificationObject.notification_uid = data_message_uid
                notificationObject.notification_event_uid = ""
                notificationObject.notification_event_title = ""
                notificationObject.notification_time_stamp = data_time_stamp
                notificationObject.notification_event_state_id = ""
                notificationObject.notification_event_city_id = ""
                notificationObject.notification_event_country_id = ""
                
                PersistanceService.saveContext()

            } else if notifEnum == ".mention" {
                let notificationObject = CdNotification(context: PersistanceService.context)
                notificationObject.notification_type = userInfo["data_type"] as? String
                notificationObject.notification_url = userInfo["data_url"] as? String
                notificationObject.notification_user_name = userInfo["data_user_name"] as? String
                notificationObject.notification_user_uid = userInfo["data_user_uid"] as? String
                notificationObject.notification_string = userInfo["data_message"] as? String
                notificationObject.notification_uid = userInfo["data_message_uid"] as? String
                notificationObject.notification_event_uid =  userInfo["data_event_uid"] as? String
                notificationObject.notification_event_title = userInfo["data_event_title"] as? String
                notificationObject.notification_time_stamp = Double(getCurrentTimeAsEpoch())
                notificationObject.notification_event_country_id = userInfo["data_event_country_id"] as? String
                notificationObject.notification_event_state_id = userInfo["data_event_state_id"] as? String
                notificationObject.notification_event_city_id = userInfo["data_event_city_id"] as? String
                PersistanceService.saveContext()
                
            } else if notifEnum == ".joinedrequested" {
                let notificationObject = CdNotification(context: PersistanceService.context)
                notificationObject.notification_type = userInfo["data_type"] as? String
                notificationObject.notification_url = userInfo["data_url"] as? String
                notificationObject.notification_user_name = userInfo["data_user_name"] as? String
                notificationObject.notification_user_uid = userInfo["data_user_uid"] as? String
                notificationObject.notification_string = userInfo["data_message"] as? String
                notificationObject.notification_uid = userInfo["data_event_uid"] as? String
                notificationObject.notification_event_uid = userInfo["data_event_uid"] as? String
                notificationObject.notification_event_title = userInfo["data_event_title"] as? String
                notificationObject.notification_time_stamp = Double(getCurrentTimeAsEpoch())
                notificationObject.notification_event_country_id = userInfo["data_event_country_id"] as? String
                notificationObject.notification_event_state_id = userInfo["data_event_state_id"] as? String
                notificationObject.notification_event_city_id = userInfo["data_event_city_id"] as? String
                PersistanceService.saveContext()
                
            } else if notifEnum == ".accepted" {
                let data_type = userInfo["data_type"] as? String
                let data_url = userInfo["data_url"] as? String
                let data_user_name = userInfo["data_user_name"] as? String
                let data_user_uid = userInfo["data_user_uid"] as? String
                let data_message = userInfo["data_message"] as? String
                let data_event_uid = userInfo["data_event_uid"] as? String
                let data_event_title = userInfo["data_event_title"] as? String
                let data_time_stamp = Double(getCurrentTimeAsEpoch())
                let data_event_country_id = userInfo["data_event_country_id"] as? String
                let data_event_state_id = userInfo["data_event_state_id"] as? String
                let data_event_city_id = userInfo["data_event_city_id"] as? String
                
                //change internal event object
                if let data_event_uid = userInfo["data_event_uid"] as? String {
                    let fetchRequest: NSFetchRequest<CdEvent> = CdEvent.fetchRequest()
                    let predicate = NSPredicate(format: "cd_event_uid = %@", argumentArray: [data_event_uid])
                    fetchRequest.predicate = predicate
                    do {
                        let result = try PersistanceService.context.fetch(fetchRequest)
                        let matchingEvent = result
                        print(matchingEvent, "MATCH NOTIF")
                        if result.count == 0 {
                            PersistanceService.saveContext()
                            //no matching event
                        } else {
                            matchingEvent[0].cd_event_accepted = true
                            PersistanceService.saveContext()
                        }
                    } catch {
                        print("Failed")
                        //if failed handle
                    }
                }
                
                //notification object
                let notificationObject = CdNotification(context: PersistanceService.context)
                
                notificationObject.notification_type = data_type
                notificationObject.notification_url = data_url
                notificationObject.notification_user_name = data_user_name
                notificationObject.notification_user_uid = data_user_uid
                notificationObject.notification_string = data_message
                notificationObject.notification_uid = ""
                notificationObject.notification_event_uid = data_event_uid
                notificationObject.notification_event_title = data_event_title
                notificationObject.notification_time_stamp = data_time_stamp
                notificationObject.notification_event_country_id = data_event_country_id
                notificationObject.notification_event_state_id = data_event_state_id
                notificationObject.notification_event_city_id = data_event_city_id
                PersistanceService.saveContext()
            } else if notifEnum == ".eventMessageLike" {
                
                let notificationObject = CdNotification(context: PersistanceService.context)
                notificationObject.notification_type = userInfo["data_type"] as? String
                notificationObject.notification_url = userInfo["data_url"] as? String
                notificationObject.notification_user_name = userInfo["data_user_name"] as? String
                notificationObject.notification_user_uid = userInfo["data_user_uid"] as? String
                notificationObject.notification_string = userInfo["data_message"] as? String
                notificationObject.notification_uid = userInfo["data_message_uid"] as? String
                notificationObject.notification_event_uid = userInfo["data_event_uid"] as? String
                notificationObject.notification_event_title = userInfo["data_event_title"] as? String
                notificationObject.notification_time_stamp = Double(getCurrentTimeAsEpoch())
                notificationObject.notification_event_country_id = userInfo["data_country_id"] as? String
                notificationObject.notification_event_state_id = userInfo["data_state_id"] as? String
                notificationObject.notification_event_city_id = userInfo["data_city_id"] as? String
                PersistanceService.saveContext()
                
            } else if notifEnum == ".eventEdit" {
            
                let notificationObject = CdNotification(context: PersistanceService.context)
                notificationObject.notification_type = userInfo["data_type"] as? String
                notificationObject.notification_url = userInfo["data_url"] as? String
                notificationObject.notification_user_name = userInfo["data_user_name"] as? String
                notificationObject.notification_user_uid = userInfo["data_user_uid"] as? String
                notificationObject.notification_string = userInfo["data_event_uid"] as? String
                notificationObject.notification_uid = userInfo["data_event_uid"] as? String
                notificationObject.notification_event_uid = userInfo["data_event_uid"] as? String
                notificationObject.notification_event_title = userInfo["data_event_title"] as? String
                notificationObject.notification_time_stamp = Double(getCurrentTimeAsEpoch())
                notificationObject.notification_event_country_id = userInfo["data_event_country_id"] as? String
                notificationObject.notification_event_state_id = userInfo["data_event_state_id"] as? String
                notificationObject.notification_event_city_id = userInfo["data_event_city_id"] as? String
                PersistanceService.saveContext()
            
            } else if notifEnum == ".deleteEvent" {
                let notificationObject = CdNotification(context: PersistanceService.context)
                notificationObject.notification_type = userInfo["data_type"] as? String
                notificationObject.notification_url = userInfo["data_url"] as? String
                notificationObject.notification_user_name = userInfo["data_user_name"] as? String
                notificationObject.notification_user_uid = userInfo["data_user_uid"] as? String
                notificationObject.notification_string = userInfo["data_message"] as? String
                notificationObject.notification_event_uid = userInfo["data_event_uid"] as? String
                notificationObject.notification_event_title = userInfo["data_event_title"] as? String
                notificationObject.notification_time_stamp = Double(getCurrentTimeAsEpoch())
                notificationObject.notification_event_country_id = userInfo["data_event_country_id"] as? String
                notificationObject.notification_event_state_id = userInfo["data_event_state_id"] as? String
                notificationObject.notification_event_city_id = userInfo["data_event_city_id"] as? String
            
                //change internal event object
                if let data_event_uid = userInfo["data_event_uid"] as? String {
                    let fetchRequest: NSFetchRequest<CdEvent> = CdEvent.fetchRequest()
                    let predicate = NSPredicate(format: "cd_event_uid = %@", argumentArray: [data_event_uid])
                    fetchRequest.predicate = predicate
                    do {
                        let result = try PersistanceService.context.fetch(fetchRequest)
                        let matchingEvent = result
                        if result.count != 0 {
                            
                            if let uid = matchingEvent[0].cd_event_uid {
                                if let userUid = Auth.auth().currentUser?.uid {
                                    if let validCountry = matchingEvent[0].cd_event_country {
                                        if let validState = matchingEvent[0].cd_event_state {
                                            if let validCity = matchingEvent[0].cd_event_city {
                                                
                                                let profileFeedRef = Database.database().reference().child("USERS_EVENTS/\(userUid)/\(validCountry)/\(validState)/\(validCity)/\(uid)")
                                                profileFeedRef.removeValue()
                                            }
                                        }
                                    }
                                }
                            }
                           PersistanceService.context.delete(matchingEvent[0])
                        }
                    } catch {
                        print("Failed")
                    }
                }
                PersistanceService.saveContext()
            } else {
                print("notification didn't come through properly")
                return
            }
            completionHandler([.alert, .badge, .sound])
        }
    }
    
    //MARK: - DID RECIEVE RESPONSE
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
         if let userInfo = response.notification.request.content.userInfo as? [String: AnyObject] {

//            let data_type = userInfo["data_type"] as? String
//            let data_url = userInfo["data_url"] as? String
//            let data_user_name = userInfo["data_user_name"] as? String
//            let data_user_uid = userInfo["data_user_uid"] as? String
//            let data_message = userInfo["data_message"] as? String
//            let data_event_uid = userInfo["data_event_uid"] as? String
//            let data_event_title = userInfo["data_event_title"] as? String
//            let data_time_stamp = Double(Date().timeIntervalSinceReferenceDate)
//            let data_event_state_id = userInfo["data_event_state_id"] as? String
//            let data_event_city_id = userInfo["data_event_city_id"] as? String
            
            let notifEnum = userInfo["data_type"] as? String
            if notifEnum == ".dm" {

            } else if notifEnum == ".mention" {
                
            } else if notifEnum == ".joinedrequested" {
                
            } else if notifEnum == ".accepted" {
                
            } else if notifEnum == ".eventMessageLike" {
                
            } else {
                
            }
        }
        completionHandler()
}
    
    //works, but isn't elegant and doesn't handle simulator launches
    func getCoreData(token: String) {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            
            if peep.count != 0 {
                //there is coredata present
                var currentUserCoreData = [Person]()
                currentUserCoreData = peep
                currentUserCoreData[0].device_token = token
                PersistanceService.saveContext()
            } else {
                //no core data, so create first model
                let person = Person(context: PersistanceService.context)
                person.accepted_terms = false
                person.device_token = token
                PersistanceService.saveContext()
            }
        } catch {
            print("error fetching")
        }
    }
}

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        getCoreData(token: fcmToken)
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }

    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    func getCurrentTimeAsEpoch() -> Int {
        let date = NSDate()
        let timeInterval = date.timeIntervalSince1970
        
        // convert to Integer
        let myInt = Int(timeInterval)
        return myInt
    }
}





