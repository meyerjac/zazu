//
//  EditProfileRootViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/4/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import ImagePicker
import Lightbox
import CoreData
import SDWebImage
import Firebase
import Crashlytics
class EditProfileRootViewController: UIViewController, ImagePickerDelegate, UITextViewDelegate {
    
    //MARK: - Variables
    var tappedImage: TinderImageView!
    var tappedView: UIView!
    var tappedViewTag: Int!
    var titlePlaceholder: String!
    var subtitlePlaceholder: String!
    var dismissedPicker: Bool = false
    var keyboardDif: CGFloat = 0.0
    var offset: CGFloat = 0.0
    var dbPhotoSetterIndex = 1
    var firstResponderTag: Int = 100
    var imageViewArray = [UIImageView]()
    var currentUserCoreData = [Person]()
    var initialTabBarFrame: CGRect!
    var aboutPlaceholder = "You are awesome, tell people about it!"
    var profile_urls = ["photo1": "", "photo2": "", "photo3": "", "photo4": "", "photo5": "", "photo6": "",]
    var imagePresentDictionary = [1: false, 2: false, 3: false, 4: false, 5: false, 6: false]
    var coreDataDictionary = [1: "photo1", 2: "photo2", 3: "photo3", 4: "photo4", 5: "photo5", 6: "photo6"]
    var intitialAbout: String! = ""
    var picturesChanged: Bool = false
    var aboutChanged: Bool = false
    var initialLoad: Bool = true
    
    //MARK: - Outlets
    @IBOutlet weak var image1: TinderImageView!
    @IBOutlet weak var image2: TinderImageView!
    @IBOutlet weak var image3: TinderImageView!
    @IBOutlet weak var image4: TinderImageView!
    @IBOutlet weak var image5: TinderImageView!
    @IBOutlet weak var image6: TinderImageView!
    @IBOutlet weak var about: UITextView!
    @IBOutlet weak var currentJobView: UIView!
    @IBOutlet weak var dreamJobView: UIView!
    @IBOutlet weak var educationView: UIView!
    @IBOutlet weak var snapchatHandleView: UIView!
    @IBOutlet weak var instagramHandleView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //JUST LABEL
    @IBOutlet weak var currentJobLabel: UILabel!
    @IBOutlet weak var dreamJobLabel: UILabel!
    @IBOutlet weak var educationLabel: UILabel!
    @IBOutlet weak var instagramLabel: UILabel!
    @IBOutlet weak var snapchatLabel: UILabel!
    
    //Three fields
    @IBOutlet weak var currentJobTitle: UILabel!
    @IBOutlet weak var currentJobSubtitle: UILabel!
    @IBOutlet weak var currentJobIcon: UIImageView!
    @IBOutlet weak var dreamJobTitle: UILabel!
    @IBOutlet weak var dreamJobSubtitle: UILabel!
    @IBOutlet weak var dreamJobIcon: UIImageView!
    @IBOutlet weak var educationTitle: UILabel!
    @IBOutlet weak var educationSubtitle: UILabel!
    @IBOutlet weak var educationIcon: UIImageView!

    //MARK: - VDL
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialTabBarFrame = self.tabBarController?.tabBar.frame
        self.initialLoad = false
    }
    
    override func viewDidLayoutSubviews() {
        let gradient: CAGradientLayer = CAGradientLayer()
        self.instagramHandleView.layoutIfNeeded()
        gradient.colors = [UIColor(red:1.00, green:0.85, blue:0.46, alpha:1.0).cgColor,
                           UIColor(red:0.98, green:0.49, blue:0.12, alpha:1.0).cgColor,
                           UIColor(red:0.84, green:0.16, blue:0.46, alpha:1.0).cgColor,
                           UIColor(red:0.59, green:0.18, blue:0.75, alpha:1.0).cgColor,
                           UIColor(red:0.31, green:0.36, blue:0.84, alpha:1.0).cgColor]
        gradient.locations = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.instagramHandleView.frame.size.width, height: self.instagramHandleView.frame.size.height)
        
        self.instagramHandleView.layer.insertSublayer(gradient, at: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupViews()
        addKeyboardObservers()
        getCoreData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool { return true }
    
    @objc func tapped() { view.endEditing(true) }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        saveProfile()
        
        NotificationCenter.default.removeObserver(self)
        if self.isMovingFromParent {
            tabBarController?.tabBar.frame = self.initialTabBarFrame
        }
    }
    
    //MARK - Keyboard
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(EditProfileRootViewController.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.about.inputAccessoryView = keyboardToolbar
    }
    
    func addKeyboardObservers() {
        setDoneOnKeyboard()
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileRootViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(EditProfileRootViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapped))
        view.addGestureRecognizer(tapGesture)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardFrame = keyboardFrame.cgRectValue
            guard let tabHeight = tabBarController?.tabBar.frame.size.height else { return }
            guard let aboutPoint = about.superview?.convert(about.frame.origin, to: nil) else { return }
            
            if firstResponderTag != 0 {
                self.firstResponderTag = 0
        
                if (self.view.frame.size.height - keyboardFrame.height - tabHeight) < aboutPoint.y {
                    let difference = (self.view.frame.size.height - aboutPoint.y - tabHeight - 8) - keyboardFrame.height
                    self.keyboardDif = difference
                    self.offset = scrollView.contentOffset.y
                    
                    scrollView.setContentOffset(CGPoint(x: 0, y: -difference + self.offset), animated: true)
                } else {
                    self.keyboardDif = 0.0
                    return
                }
                return
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.firstResponderTag = 300
        if self.keyboardDif != 0.0 {
            scrollView.setContentOffset(CGPoint(x: 0, y: self.offset), animated: true)
        }
        return
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    func setupViews() {
        self.dbPhotoSetterIndex = 1
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        let imageTap1 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        let imageTap2 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        let imageTap3 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        let imageTap4 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        let imageTap5 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        let imageTap6 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        
        let viewTap = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        let viewTap1 = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        let viewTap2 = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        let viewTap3 = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        let viewTap4 = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        
        self.image1.addGestureRecognizer(imageTap1)
        self.image1.isUserInteractionEnabled = true
        self.image2.addGestureRecognizer(imageTap2)
        self.image2.isUserInteractionEnabled = true
        self.image3.addGestureRecognizer(imageTap3)
        self.image3.isUserInteractionEnabled = true
        self.image4.addGestureRecognizer(imageTap4)
        self.image4.isUserInteractionEnabled = true
        self.image5.addGestureRecognizer(imageTap5)
        self.image5.isUserInteractionEnabled = true
        self.image6.addGestureRecognizer(imageTap6)
        self.image6.isUserInteractionEnabled = true
        
        self.currentJobView.addGestureRecognizer(viewTap)
        self.currentJobView.isUserInteractionEnabled = true
        self.dreamJobView.addGestureRecognizer(viewTap1)
        self.dreamJobView.isUserInteractionEnabled = true
        self.educationView.addGestureRecognizer(viewTap2)
        self.educationView.isUserInteractionEnabled = true
        self.instagramHandleView.addGestureRecognizer(viewTap3)
        self.instagramHandleView.isUserInteractionEnabled = true
        self.snapchatHandleView.addGestureRecognizer(viewTap4)
        self.snapchatHandleView.isUserInteractionEnabled = true
        
        //clear recieved image array
        self.imageViewArray.removeAll()
        self.imageViewArray.append(self.image1 as UIImageView)
        self.imageViewArray.append(self.image2 as UIImageView)
        self.imageViewArray.append(self.image3 as UIImageView)
        self.imageViewArray.append(self.image4 as UIImageView)
        self.imageViewArray.append(self.image5 as UIImageView)
        self.imageViewArray.append(self.image6 as UIImageView)
    }
    
    //MARK: - Core Data
    func getCoreData() {
            let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
            do {
                let peep = try PersistanceService.context.fetch(fetchRequest)
                currentUserCoreData = peep
                if (currentUserCoreData[0].about != nil) {
                    self.intitialAbout = currentUserCoreData[0].about
                    self.about.text = currentUserCoreData[0].about
                } else {
                    self.about.text = self.aboutPlaceholder
                    self.about.textColor = UIColor.lightGray
                }
                
                if !self.dismissedPicker {
                    for n in 1...6 {
                        let imageData = self.currentUserCoreData[0].value(forKey: self.coreDataDictionary[n]!)
                        if imageData != nil {
                            self.imageViewArray[n - 1].image = (UIImage(data: (imageData as! NSData) as Data))
                            self.imagePresentDictionary[n] = true
                        }
                    }
                }
                //handle instagram and snapchat fields
                if let handle = self.currentUserCoreData[0].instagram_handle {
                    instagramLabel.text = "@" + handle
                    instagramLabel.font = UIFont(name:"HelveticaNeue-Bold", size: 20.0)
                } else {
                    instagramLabel.text = "Instagram Handle"
                    instagramLabel.font = UIFont(name:"HelveticaNeue-Regular", size: 16.0)
                }
                
                if let handle = self.currentUserCoreData[0].snapchat_handle {
                    snapchatLabel.text = "@" + handle
                    snapchatLabel.font = UIFont(name:"AvenirNext-DemiBold", size: 16.0)
                } else {
                    snapchatLabel.text = "Snapchat Handle"
                    snapchatLabel.font = UIFont(name:"AvenirNext-DemiBold", size: 16.0)
                }
                
                //handle all the fields(about, jobs, education display ect. whether they are hidden or not, ect.
                if self.currentUserCoreData[0].current_job_employer != nil && self.currentUserCoreData[0].current_job_title != nil  {
                    //setting fields
                    self.currentJobLabel.isHidden = true
                    self.currentJobTitle.text = self.currentUserCoreData[0].current_job_title
                    self.currentJobTitle.isHidden = false
                    self.currentJobSubtitle.text = self.currentUserCoreData[0].current_job_employer
                    self.currentJobSubtitle.isHidden = false
                    let imageData = self.currentUserCoreData[0].value(forKey: "current_job_employer_logo")
                    if imageData != nil {
                        self.currentJobIcon.image = (UIImage(data: (imageData as! NSData) as Data))
                        self.currentJobIcon.isHidden = false
                        self.currentJobIcon.layer.cornerRadius = 5
                        self.currentJobIcon.clipsToBounds = true
                    }
                } else {
                    self.currentJobLabel.isHidden = false
                    self.currentJobTitle.isHidden = true
                    self.currentJobSubtitle.isHidden = true
                    self.currentJobIcon.isHidden = true
                }
                
                if self.currentUserCoreData[0].dream_job_employer != nil && self.currentUserCoreData[0].dream_job_title != nil  {
                    //setting fields
                    self.dreamJobLabel.isHidden = true
                    self.dreamJobTitle.text = self.currentUserCoreData[0].dream_job_title
                    self.dreamJobTitle.isHidden = false
                    self.dreamJobSubtitle.text = self.currentUserCoreData[0].dream_job_employer
                    self.dreamJobSubtitle.isHidden = false
                    let imageData = self.currentUserCoreData[0].value(forKey: "dream_job_employer_logo")
                    if imageData != nil {
                        self.dreamJobIcon.image = (UIImage(data: (imageData as! NSData) as Data))
                        self.dreamJobIcon.isHidden = false
                        self.dreamJobIcon.layer.cornerRadius = 5
                        self.dreamJobIcon.clipsToBounds = true
                    }
                }
                else {
                    self.dreamJobLabel.isHidden = false
                    self.dreamJobTitle.isHidden = true
                    self.dreamJobSubtitle.isHidden = true
                    self.dreamJobIcon.isHidden = true
                }
                
                if self.currentUserCoreData[0].education_level != nil && self.currentUserCoreData[0].education_institution != nil  {
                    //setting fields
                    self.educationLabel.isHidden = true
                    self.educationTitle.text = self.currentUserCoreData[0].education_level
                    self.educationTitle.isHidden = false
                    self.educationSubtitle.text = self.currentUserCoreData[0].education_institution
                    self.educationSubtitle.isHidden = false
                    let imageData = self.currentUserCoreData[0].value(forKey: "education_institution_logo")
                    if imageData != nil {
                        self.educationIcon.image = (UIImage(data: (imageData as! NSData) as Data))
                        self.educationIcon.isHidden = false
                        self.educationIcon.layer.cornerRadius = 5
                        self.educationIcon.clipsToBounds = true
                    }
                } else {
                    self.educationLabel.isHidden = false
                    self.educationTitle.isHidden = true
                    self.educationSubtitle.isHidden = true
                    self.educationIcon.isHidden = true
                }
                
            } catch {
                ()
            }
        }
    
    @objc func saveProfile() {
        self.dismissKeyboard()
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        //saving images and about, everything else gets saved on profileJob page
        if let uid = Auth.auth().currentUser?.uid {
            let ref = Database.database().reference()
            if self.picturesChanged {
                var lastIndex = 1
                //handle Saving images
                for n in 1...6 {
                    let storeRef = Storage.storage().reference().child("profileImages/\(uid)/\(lastIndex).jpg")
                    if imagePresentDictionary[n] != false {
                        let imageData = imageViewArray[n-1].image!.jpegData(compressionQuality: 0.2)
                        self.currentUserCoreData[0].setValue(imageData, forKey: "photo\(lastIndex)")
                        
                        storeRef.putData(imageData!, metadata: nil, completion: { (metadata, err) in
                            if err == nil {
                                storeRef.downloadURL(completion: { (url, error) in
                                    if error != nil {
                                       ()
                                        return
                                    }
                                    
                                    if url != nil {
                                        let num = String(self.dbPhotoSetterIndex)
                                        self.profile_urls["photo\(num)"] = url?.absoluteString
                                        ref.child("USERS/\(uid)/profile_urls").setValue(self.profile_urls)
                                        self.dbPhotoSetterIndex += 1
                                        

                                    }
                                })
                            }
                        })
                        lastIndex += 1
                    } else {
                        //no new image in dictionary index
                        let num = String(self.dbPhotoSetterIndex)
                        self.profile_urls["photo\(num)"] = ""
                    }
                }
                self.dbPhotoSetterIndex = 1
                
                if lastIndex == 7 {
                } else {
                    for n in (lastIndex)..<7 {
                        self.currentUserCoreData[0].setValue(nil, forKey: "photo\(n)")
                        PersistanceService.saveContext()
                    }
                }
            }
            if aboutChanged {
                if self.about.text == aboutPlaceholder {
                    self.currentUserCoreData[0].setValue(nil, forKey: "about")
                     ref.child("USERS/\(uid)/about").setValue("My bio seems to be missing: here is a fake one: I am a jedi from the land of tattoine")
                } else {
                    self.currentUserCoreData[0].setValue(self.about.text, forKey: "about")
                    ref.child("USERS/\(uid)/about").setValue(self.about.text)
                    PersistanceService.saveContext()
                }
            }
        }
        PersistanceService.saveContext()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
          self.aboutChanged = true
          self.navigationItem.rightBarButtonItem?.isEnabled = true
        
        // Combine the textView text and the replacement text to
        // create the updated text string
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if updatedText.isEmpty {
            
            textView.text = self.aboutPlaceholder
            textView.textColor = UIColor.lightGray
            
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        }
            
            // Else if the text view's placeholder is showing and the
            // length of the replacement string is greater than 0, set
            // the text color to black then set its text to the
            // replacement string
        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.textColor = UIColor.black
            textView.text = text
        }
            
            // For every other case, the text should change with the usual
            // behavior...
        else {
            return true
        }
        
        // ...otherwise return false since the updates have already
        // been made
        return false
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "This is where you put your life story into a paragraph...you are cooler than we can even comprehend!"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @objc func viewTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        tappedView = tapGestureRecognizer.view
        handleProfileFieldTapped(tag: self.tappedView.tag)
    }
    
    @objc func removeButtonPressed(sender: UIButton ) {
        self.imagePresentDictionary[sender.tag] = false
        self.imageViewArray[sender.tag - 1].image = UIImage(named: "userPic")
        self.picturesChanged = true
    }
        
    func handleProfileFieldTapped(tag: Int) {
        tabBarController?.tabBar.frame = self.initialTabBarFrame
        
        self.dismissKeyboard()
        
        switch (tag) {
        case 0:
            self.tappedViewTag = 0
            self.performSegue(withIdentifier: "toJobEducation", sender: self)
            break;
        case 1:
            self.tappedViewTag = 1
            self.performSegue(withIdentifier: "toJobEducation", sender: self)
            break;
        case 2:
            self.tappedViewTag = 2
            self.performSegue(withIdentifier: "toJobEducation", sender: self)
            break
        case 3:
            self.tappedViewTag = 3
            self.performSegue(withIdentifier: "toJobEducation", sender: self)
            break
        case 4:
            self.tappedViewTag = 4
            self.performSegue(withIdentifier: "toJobEducation", sender: self)
            break
        default:
           ()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toJobEducation") {
            let vc = segue.destination as! ProfileJobEducationViewController
            vc.selectedItem = self.tappedViewTag
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        tappedImage = tapGestureRecognizer.view as? TinderImageView
        
        let config = Configuration()
        config.doneButtonTitle = "Finish"
        config.noImagesTitle = "Sorry! There are no images here!"
        config.recordLocation = false
        config.allowVideoSelection = false
      
        
        let imagePicker = ImagePickerController(configuration: config)
        imagePicker.imageLimit = 6
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - ImagePickerDelegate
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
        self.dismissedPicker = true
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
        
        let lightboxImages = images.map {
            return LightboxImage(image: $0)
        }
        
        let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        imagePicker.present(lightbox, animated: true, completion: nil)
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        self.dismissedPicker = true
        imagePicker.dismiss(animated: true, completion: nil)
        
        self.picturesChanged = true
        
        let tag = self.tappedImage.tag
        for n in 0...images.count-1 {
            if tag != 1 {
                if tag + n > 6 {
                    self.imageViewArray[tag + n - 7].image = images[n]
                    self.imagePresentDictionary[tag + n - 6] = true
                } else  {
                    self.imageViewArray[n - 1 + tag].image = images[n]
                    self.imagePresentDictionary[n + tag] = true
                }
            } else {
                self.imageViewArray[n].image = images[n]
                self.imagePresentDictionary[n + 1] = true
            }
        }
    }
}
