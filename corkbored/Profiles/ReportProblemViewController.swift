//
//  ReportProblemViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 3/23/19.
//  Copyright © 2019 Jackson Meyer. All rights reserved.
//

import UIKit
import Firebase

class ReportProblemViewController: UIViewController, UITextViewDelegate {
    var textViewPlaceholder = "Please describe problem"
    var type: Int = 0
    var profileUidReporting = ""
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var problemTextView: UITextView!
    @IBAction func submitButtonClicked(_ sender: UIButton) {
        submitButton.isEnabled = false
        if let uid = Auth.auth().currentUser?.uid {
            switch (type) {
                 case 0:
                 //report problem
                    let dict = ["uid" : (Auth.auth().currentUser?.email)!, "problem" : problemTextView.text] as [String : Any]
                    let ref = Database.database().reference().child("EXTRAS").child("PROBLEMS").child(uid)
                    let specificRef = ref.childByAutoId()
                    
                    specificRef.setValue(dict) { (error, ref) -> Void in
                        self.problemTextView.textColor = UIColor.lightGray
                        self.problemTextView.text = self.textViewPlaceholder
                        self.problemTextView.resignFirstResponder()
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        if let myAlert = storyboard.instantiateViewController(withIdentifier: "alert") as? alertViewController {
                            myAlert.alertTitle = "Reported"
                            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            self.present(myAlert, animated: true, completion: nil)
                        }
                }
                    break;
                 case 1:
                 //report user
                    let dict = ["reportedBy" : (Auth.auth().currentUser?.email)!, "reporting": profileUidReporting,  "problem" : problemTextView.text] as [String : Any]
                    let ref = Database.database().reference().child("EXTRAS").child("REPORTED_ACCOUNTS").child(uid)
                    let specificRef = ref.childByAutoId()
                    
                    specificRef.setValue(dict) { (error, ref) -> Void in
                        self.problemTextView.textColor = UIColor.lightGray
                        self.problemTextView.text = self.textViewPlaceholder
                        self.problemTextView.resignFirstResponder()
                        
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        if let myAlert = storyboard.instantiateViewController(withIdentifier: "alert") as? alertViewController {
                            myAlert.alertTitle = "Reported"
                            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                            self.present(myAlert, animated: true, completion: nil)
                        }
                    }
                    
                    break;
            default:
                ()
                
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        submitButton.layer.cornerRadius = submitButton.frame.size.height/2
        submitButton.clipsToBounds = true
        
        problemTextView.layer.cornerRadius = 8
        problemTextView.clipsToBounds = true
    }
    
    override func viewDidLoad() {
        problemTextView.text = textViewPlaceholder
        problemTextView.textColor = UIColor.lightGray
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        problemTextView.text = ""
        problemTextView.textColor = UIColor.black
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if problemTextView.text.count == 0 {
            problemTextView.textColor = UIColor.lightGray
            problemTextView.text = textViewPlaceholder
            problemTextView.resignFirstResponder()
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        if problemTextView.text.count == 0 {
            problemTextView.textColor = UIColor.lightGray
            problemTextView.text = textViewPlaceholder
            problemTextView.resignFirstResponder()
        }
        return true
    }
}
