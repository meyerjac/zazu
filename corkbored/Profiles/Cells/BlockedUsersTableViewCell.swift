//
//  BlockedUsersTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 12/18/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class BlockedUsersTableViewCell: UITableViewCell {
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
}
