//
//  OptionsTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 10/23/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class OptionsTableViewCell: UITableViewCell {
    @IBOutlet weak var optionLabel: UILabel!
}
