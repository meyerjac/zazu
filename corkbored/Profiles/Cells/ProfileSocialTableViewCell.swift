//
//  ProfileSocialTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 3/22/19.
//  Copyright © 2019 Jackson Meyer. All rights reserved.
//

import UIKit

class ProfileSocialTableViewCell: UITableViewCell {
    @IBOutlet weak var instagram_handle: UILabel!
    @IBOutlet weak var instagram_container: UIView!
    @IBOutlet weak var snapchat_handle: UILabel!
    @IBOutlet weak var snapchat_container: UIView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let gradient: CAGradientLayer = CAGradientLayer()

        gradient.colors = [UIColor(red:1.00, green:0.85, blue:0.46, alpha:1.0).cgColor,
                           UIColor(red:0.98, green:0.49, blue:0.12, alpha:1.0).cgColor,
                           UIColor(red:0.84, green:0.16, blue:0.46, alpha:1.0).cgColor,
                           UIColor(red:0.59, green:0.18, blue:0.75, alpha:1.0).cgColor,
                           UIColor(red:0.31, green:0.36, blue:0.84, alpha:1.0).cgColor]
        gradient.locations = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.instagram_container.frame.size.width, height: self.instagram_container.frame.size.height)
        gradient.frame = bounds

        self.instagram_container.layer.insertSublayer(gradient, at: 0)
    }
}
