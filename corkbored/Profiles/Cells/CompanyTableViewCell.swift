//
//  CompanyTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/17/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class CompanyTableViewCell: UITableViewCell {
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var domain: UILabel!
}
