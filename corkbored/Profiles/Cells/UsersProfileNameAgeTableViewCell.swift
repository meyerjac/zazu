//
//  UsersProfileNameAgeTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/4/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class UsersProfileNameAgeTableViewCell: UITableViewCell {
     @IBOutlet weak var name_and_age: UILabel!
}
