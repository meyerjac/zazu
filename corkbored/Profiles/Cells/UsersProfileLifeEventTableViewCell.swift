//
//  UsersProfileLifeEventTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/4/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class UsersProfileLifeEventTableViewCell: UITableViewCell {
    @IBOutlet weak var cell_title: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var company_position: UILabel!
    @IBOutlet weak var name: UILabel!
}
