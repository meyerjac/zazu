//
//  UsersProfileAboutTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/4/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class UsersProfileAboutTableViewCell: UITableViewCell {
    @IBOutlet weak var about_label: UILabel!
    @IBOutlet weak var about_description: UILabel!
}
