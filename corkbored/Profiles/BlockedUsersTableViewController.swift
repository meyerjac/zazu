//
//  BlockedUsersTableViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 12/18/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage
import DZNEmptyDataSet

class BlockedUsersTableViewController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    //MARK: Variables
    var blockedUsersArray = [BlockedUser]()
    
    //MARK: VDL
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchDeletedUsers()
    }
    

    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "Unblock"
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            if (Auth.auth().currentUser?.uid) != nil {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as! CustomAlertViewController
                vc.mainTitle = "Unblock user?"
                vc.subtitle = "Unblocking this user means you will be able to message them, you will have access to their social profiles, and you will see their hosted events."
                vc.actionButtonTitle = "Unblock"
                vc.type = .unblockUser
                vc.selectedUserUid = self.blockedUsersArray[indexPath.row].blocked_userUid
                
                vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(vc, animated: false, completion: nil)
                
                vc.onDoneBlock = { result in
                    self.blockedUsersArray.remove(at: indexPath.row)
                    DispatchQueue.main.asyncAfter(deadline: .now()) {
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return blockedUsersArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlockedUserCell", for: indexPath) as! BlockedUsersTableViewCell
        if let url = URL.init(string: self.blockedUsersArray[indexPath.row].blocked_profilePhotoUrl) {
            cell.userProfileImage.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached)
        }
        cell.userName.text = self.blockedUsersArray[indexPath.row].blocked_userName
        return cell
    }
    
    //MARK: Functions
    func fetchDeletedUsers() {
        if let uid = Auth.auth().currentUser?.uid {
            var blockedUsersRef: DatabaseReference!
            blockedUsersRef = Database.database().reference()
            
            blockedUsersRef.child("USER_BLOCKS/\(uid)/BLOCKED_USERS").observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    if (snapshot.value as? [AnyHashable: AnyObject]) != nil {
                        for child in snapshot.children {
                            let snap = child as! DataSnapshot
                            let blocked_user = BlockedUser(snapshot: snap)
                            self.blockedUsersArray.append(blocked_user)
                            
                            DispatchQueue.main.asyncAfter(deadline: .now()) {
                                self.tableView.reloadData()
                            }
                        }
                    }
                }
            })
        }
    }
    
    //MARK: DZN EMPTY DATA
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "No Accounts Blocked"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "If you block a user account this is where they will show up. It is also where you can manage your blocked accounts and unblock them by swiping"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "blockb")
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        if self.blockedUsersArray.count == 0 {
            return true
        } else {
            return false
        }
    }
}
