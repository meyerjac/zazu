//
//  UsersProfileViewController.swift
//  corkbored
//  Created by Jackson Meyer on 7/29/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage

class UsersProfileViewController: UIViewController, UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - Variables
    var imageUrlArray = [String]()
    var profileFieldsPresent = ["name_and_age": "", "about": "", "current_job_title":  "", "current_job_employer": "", "current_job_employer_logo": "", "dream_job_title":  "", "dream_job_employer": "", "dream_job_employer_logo": "", "education_title":  "", "education_institution": "", "education_logo": ""] as [String : String]
    var indexes = [String]()
    var selectedUserUid: String = ""
    var fetchedUser: User!
    var selectedUserUidPhoto: UIImage?
    var userName: String?
    var numberOfTableRows: Int = 0
    var instagramUserName: String! = ""
    var snapchatUserName: String! = ""
    var fetchedUserIsBlockedByCurrentUser: Bool = false
    
    //MARK - Outlets
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var messageUserView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var blockMessageLabel: UILabel!
    @IBOutlet weak var blockMessageImage: UIImageView!

    //MARK: - VDL
    override func viewDidLoad() {
        super.viewDidLoad()
        handleViews()
        fetchBlocks()
    }
    
    func handleViews() {
        self.navigationItem.title = self.userName
        self.imageScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.width)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        //add more button
        let moreButton = UIButton(type: .custom)
        moreButton.frame = CGRect(x: 0.0, y: 0.0, width: 16, height: 16)
        moreButton.setImage(UIImage(named: "more_white"), for: .normal)
        moreButton.setImage(UIImage(named:"more_white"), for: .highlighted)
        moreButton.addTarget(self, action: #selector(showMoreOptions), for: .touchUpInside)
        
        let moreBarButton = UIBarButtonItem(customView: moreButton)
        let currWidth2 = moreBarButton.customView?.widthAnchor.constraint(equalToConstant: 20)
        currWidth2?.isActive = true
        let currHeight2 = moreBarButton.customView?.heightAnchor.constraint(equalToConstant: 20)
        currHeight2?.isActive = true
        self.navigationItem.rightBarButtonItem = moreBarButton
    }
    
    @objc func dismissUserDoesntExist() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numberOfTableRows
    }
    
   @objc func tappedInstagram() {
    if !self.fetchedUserIsBlockedByCurrentUser {
       if let handle = fetchedUser.social["instagram"] as? String {
            if let instagramUrl = URL(string: "instagram://user?username=" + handle) {
                UIApplication.shared.open(instagramUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                }
            }
        }
    }
    
    @objc func tappedSnapchat() {
        if !self.fetchedUserIsBlockedByCurrentUser {
            if let handle = fetchedUser.social["snapchat"] as? String {
                if let snapUrl = URL(string: "https://www.snapchat.com/add/" + handle) {
                UIApplication.shared.open(snapUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexes[indexPath.row] == "name" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "usersProfileName", for: indexPath) as! UsersProfileNameAgeTableViewCell
            cell.name_and_age.text = profileFieldsPresent["name_and_age"]
            return cell
        } else if indexes[indexPath.row] == "social" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "socialHandleCell", for: indexPath) as! ProfileSocialTableViewCell
            cell.instagram_container.layoutIfNeeded()
        
            if let handle = (fetchedUser.social["instagram"]) as? String {
                if !fetchedUserIsBlockedByCurrentUser {
                    cell.instagram_handle.text = "@" + handle
                } else {
                    cell.instagram_handle.text = "Blocked..."
                }
                
                let instagramRecognizer = UITapGestureRecognizer(target: self, action: #selector(tappedInstagram))
                instagramRecognizer.cancelsTouchesInView = false
                cell.instagram_container.addGestureRecognizer(instagramRecognizer)
                cell.instagram_container.isUserInteractionEnabled = true
                cell.instagram_container.layer.cornerRadius = cell.instagram_container.frame.size.height/4
                cell.instagram_container.clipsToBounds = true
            } else {
                cell.instagram_container.isHidden = true
            }
            
            if let handle = self.fetchedUser.social["snapchat"] as? String {
                 if !fetchedUserIsBlockedByCurrentUser {
                    cell.snapchat_handle.text = "@" + handle
                 } else {
                     cell.snapchat_handle.text = "Blocked..."
                }
                
                let snapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tappedSnapchat))
                snapRecognizer.cancelsTouchesInView = false
                cell.snapchat_container.addGestureRecognizer(snapRecognizer)
                cell.snapchat_container.isUserInteractionEnabled = true
                cell.snapchat_container.layer.cornerRadius = cell.snapchat_container.frame.size.height/4
                cell.snapchat_container.clipsToBounds = true
            } else {
                cell.snapchat_container.isHidden = true
            }
            
            if cell.snapchat_container.isHidden {
                cell.instagram_container.translatesAutoresizingMaskIntoConstraints = false
                cell.instagram_container.leadingAnchor.constraint(equalTo: cell.snapchat_container.leadingAnchor).isActive = true
                cell.instagram_container.trailingAnchor.constraint(equalTo: cell.snapchat_container.trailingAnchor).isActive = true
            }
            
            return cell
            
        } else if indexes[indexPath.row] == "about" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "usersProfileAbout", for: indexPath) as! UsersProfileAboutTableViewCell
            cell.about_label.text = "about"
            cell.about_description.text = profileFieldsPresent["about"]
            return cell
        } else if indexes[indexPath.row] == "education" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "lifeStuffCell", for: indexPath) as! UsersProfileLifeEventTableViewCell
            cell.cell_title.text = "education"
            cell.company_position.text =  profileFieldsPresent["education_title"]
            cell.name.text =  profileFieldsPresent["education_institution"]
            if let urlUrl = URL.init(string: profileFieldsPresent["education_logo"]!)  {
                cell.logo.sd_setImage(with: urlUrl, placeholderImage: nil, options: .refreshCached)
                cell.logo.layer.cornerRadius = 5
                cell.logo.clipsToBounds = true
            }
            return cell
        } else if indexes[indexPath.row] == "current_job" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "lifeStuffCell", for: indexPath) as! UsersProfileLifeEventTableViewCell
            cell.cell_title.text = "current job"
            cell.company_position.text =  profileFieldsPresent["current_job_title"]
            cell.name.text =  profileFieldsPresent["current_job_employer"]
            if let urlUrl = URL.init(string: profileFieldsPresent["current_job_employer_logo"]!)  {
                cell.logo.sd_setImage(with: urlUrl, placeholderImage: nil, options: .refreshCached)
                cell.logo.layer.cornerRadius = 5
                cell.logo.clipsToBounds = true
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "lifeStuffCell", for: indexPath) as! UsersProfileLifeEventTableViewCell
            cell.cell_title.text = "dream job"
            
            cell.company_position.text =  profileFieldsPresent["dream_job_title"]
            cell.name.text =  profileFieldsPresent["dream_job_employer"]
            
            if let urlUrl = URL.init(string: profileFieldsPresent["dream_job_employer_logo"]!) {
                cell.logo.sd_setImage(with: urlUrl, placeholderImage: nil, options: .refreshCached)
                cell.logo.layer.cornerRadius = 5
                cell.logo.clipsToBounds = true
            }
            return cell
        }
    }
    
    //MARK: Functions
    func handleMessageBlockButton() {
        self.tableView.reloadData()
        //need to register tap here so users don't tap before blocked color shows
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.messageTapped))
        self.messageUserView.addGestureRecognizer(tapRecognizer)
        
        if fetchedUserIsBlockedByCurrentUser {
            self.messageUserView.backgroundColor = UIColor.red
            self.blockMessageLabel.text = "Unblock"
            blockMessageImage.transform = CGAffineTransform(rotationAngle: .pi/4)
            blockMessageImage.image = UIImage(named:"plus_icon")
        } else {
            blockMessageImage.transform = CGAffineTransform(rotationAngle: 0)
            self.messageUserView.backgroundColor = UIColor(named: "zazuBlue")
            self.blockMessageLabel.text = "Message"
            blockMessageImage.image = UIImage(named:"send")
        }
    }
    
    @objc func messageTapped(gestureRecognizer: UITapGestureRecognizer) {
        if self.fetchedUserIsBlockedByCurrentUser {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as! CustomAlertViewController
            vc.mainTitle = "Unblock User?"
            vc.subtitle = "Unblocking this user means you can exchange messages and you will have access to all their social profiles."
            vc.actionButtonTitle = "Unblock"
            vc.type = .unblockUser
            vc.selectedUserUid = selectedUserUid
            
            vc.onDoneBlock = { result in
                self.messageUserView.backgroundColor = UIColor(named: "zazuBlue")
                self.blockMessageLabel.text = "Message"
                self.blockMessageImage.image = UIImage(named:"send")
                self.blockMessageImage.transform = CGAffineTransform(rotationAngle: 0)
                self.fetchedUserIsBlockedByCurrentUser = false
            }
            
            vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            
            self.present(vc, animated: false, completion: nil)
        } else {
            self.performSegue(withIdentifier: "quickDmSegue", sender: self)
        }
    }
    
    func fetchBlocks() {
        let blockRef = Database.database().reference().child("USER_BLOCKS").child(selectedUserUid)
        blockRef.observeSingleEvent(of: .value, with: { (snapshot) in
    
            //IF THE USER CURRENT USER IS TRYING TO FETCH HAS BLOCKED THIS USER
            let blockedBySnap = snapshot.childSnapshot(forPath: "BLOCKED_USERS")
            for child in blockedBySnap.children {
                let snap = child as! DataSnapshot
                let blocked_user = BlockedUser(snapshot: snap)
                if let uid = Auth.auth().currentUser?.uid {
                    
                if blocked_user.blocked_userUid == uid {
                    //show No user exists Alert
                    self.messageUserView.isHidden = true
                    self.showNoUserExists(subtitle: "We cannot find this user", title: "Error", buttonTitle: "okay")
                    return
                    }
                }
            }
    
            //IF THE CURRENT USER HAS BLOCKED THE USER THEY ARE TRYING TO FETCH
            let blocksSnap = snapshot.childSnapshot(forPath: "BLOCKED_BY")
            for child in blocksSnap.children {
                let snap = child as! DataSnapshot
                let blocked_by = BlockedUser(snapshot: snap)
    
                if let uid = Auth.auth().currentUser?.uid {
                    if blocked_by.blocked_userUid == uid {
                        self.fetchedUserIsBlockedByCurrentUser = true
                    }
                }
            }
            self.fetchProfile(uid: self.selectedUserUid)
            self.handleMessageBlockButton()
        })
    }
    
    func fetchProfile(uid: String) {
    self.indexes.removeAll()
    self.numberOfTableRows = 0
    let ref = Database.database().reference().child("USERS").child(selectedUserUid)
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                if (snapshot.value as? [AnyHashable: AnyObject]) != nil {
                    let user = User(snapshot: snapshot)
                    //MARK : BLOCKS 2 cases: they have blocked you - cannot find user. You have blocked them - message button is red and says unblock
                    self.fetchedUser = user
                    self.userName = user.first_name.generateUserNameFromFirstAndLast(first: user.first_name, last: user.last_name)
                    self.navigationItem.title = user.first_name.generateUserNameFromFirstAndLast(first: user.first_name, last: user.last_name)
                    
                    self.profileFieldsPresent["name_and_age"] = user.first_name.generateUserNameFromFirstAndLast(first: user.first_name, last: user.last_name) + ", \(user.birthday.calcAge(birthday: user.birthday))"
                    self.numberOfTableRows += 1
                    self.indexes.append("name")
                    
                    if ((user.social["snapchat"] != nil) || (user.social["instagram"] != nil)) {
                        self.numberOfTableRows += 1
                        self.indexes.append("social")
                    }
                    
                    if user.about != "" {
                        self.indexes.append("about")
                        self.profileFieldsPresent["about"] = user.about
                        self.numberOfTableRows += 1
                    }
                    
                    if user.education.count == 3 {
                        self.indexes.append("education")
                        self.profileFieldsPresent["education_title"] =  user.education["title"]! as? String
                        self.profileFieldsPresent["education_institution"] =  user.education["subtitle"]! as? String
                        self.profileFieldsPresent["education_logo"] =  user.education["logo"]! as? String
                        self.numberOfTableRows += 1
                    }
                    
                    if user.current_job.count == 3 {
                        self.indexes.append("current_job")
                        self.profileFieldsPresent["current_job_title"] =  user.current_job["title"]! as? String
                        self.profileFieldsPresent["current_job_employer"] =  user.current_job["subtitle"]! as? String
                        self.profileFieldsPresent["current_job_employer_logo"] =  user.current_job["logo"]! as? String
                        self.numberOfTableRows += 1
                    }
                    
                    if user.dream_job.count == 3 {
                        self.indexes.append("dream_job")
                        self.profileFieldsPresent["dream_job_title"] =  user.dream_job["title"]! as? String
                        self.profileFieldsPresent["dream_job_employer"] =  user.dream_job["subtitle"]! as? String
                        self.profileFieldsPresent["dream_job_employer_logo"] =  user.dream_job["logo"]! as? String
                        self.numberOfTableRows += 1
                    }
                    
                    self.tableView.reloadData()
     
                    if user.profile_urls.count > 0 {
                        if user.profile_urls["photo1"] as! String != "" {
                            self.imageUrlArray.append(user.profile_urls["photo1"] as! String)
                            
                            if user.profile_urls["photo2"] as! String != "" {
                                self.imageUrlArray.append(user.profile_urls["photo2"] as! String)
                                
                                if user.profile_urls["photo3"] as! String != "" {
                                    self.imageUrlArray.append(user.profile_urls["photo3"] as! String)
                                    
                                    if user.profile_urls["photo4"] as! String != "" {
                                        self.imageUrlArray.append(user.profile_urls["photo4"] as! String)
                                        if user.profile_urls["photo5"] as! String != "" {
                                            
                                            self.imageUrlArray.append(user.profile_urls["photo5"] as! String)
                                            if user.profile_urls["photo6"] as! String != "" {
                                                self.imageUrlArray.append(user.profile_urls["photo6"] as! String)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        
                        for i in 0..<self.imageUrlArray.count {
                            
                            let imageView = UIImageView()
                            imageView.tag = i
                            if let urlUrl = URL.init(string: self.imageUrlArray[i]) {
                                imageView.sd_setImage(with: urlUrl, placeholderImage: nil, options: .refreshCached)
                                
                                if i == 0 {
                                    self.selectedUserUidPhoto = imageView.image
                                }
                            } else {
                                
                            }
                            
                            imageView.contentMode = .scaleAspectFill
                            imageView.clipsToBounds = true
                            let xPosition = self.view.frame.width * CGFloat(i)
                            imageView.frame = CGRect(x: xPosition, y: 0, width: self.view.frame.width, height: self.imageScrollView.frame.height)
                            self.imageScrollView.contentSize.width = self.view.frame.width * CGFloat(i + 1)
                            self.imageScrollView.addSubview(imageView)
                        }
                    }
                    
                    self.pageControl.numberOfPages = self.imageUrlArray.count
                    self.pageControl.currentPage = 0
                    
                }
            } else {
                //show No user exists Alert
                self.messageUserView.isHidden = true
                self.showNoUserExists(subtitle: "This user no longer exists!", title: "Whoops!", buttonTitle: "okay")
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "quickDmSegue" {
            if let destinationVC = segue.destination as? QuickDmViewController {
                destinationVC.userName = self.userName
                destinationVC.selectedUserUid = self.selectedUserUid
            }
        }
    }
    
    @objc func showMoreOptions() {
        let alertController = UIAlertController(title: "\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        alertController.title = nil
        
        let blockAction = UIAlertAction(title: "Block \(self.userName ?? "user")", style: .destructive) { (action) in
            //add user to list of blocked users
            if (Auth.auth().currentUser?.uid) != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as! CustomAlertViewController
            vc.mainTitle = "Block User?"
            vc.subtitle = "Blocking this user means you cannot exchange messages, you will not have access to their social profiles, and you will not see their hosted events."
            vc.actionButtonTitle = "Block"
            vc.type = .blockUser
            vc.selectedUserUid = self.selectedUserUid
            vc.selectedUserUsername = self.fetchedUser.first_name.generateUserNameFromFirstAndLast(first: self.fetchedUser.first_name, last: self.fetchedUser.last_name)
                    
                    
                    
            vc.selectedUserProfileUrl = self.fetchedUser.thumbnail
            
            vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(vc, animated: false, completion: nil)
            
            vc.onDoneBlock = { result in
                self.fetchedUserIsBlockedByCurrentUser = true
                self.handleMessageBlockButton()
                }
            }
        }
        
        let unblockAction = UIAlertAction(title: "Unblock \(self.userName ?? "user")", style: .destructive) { (action) in
            //add user to list of blocked users
            if (Auth.auth().currentUser?.uid) != nil {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as! CustomAlertViewController
                vc.mainTitle = "Unblock user?"
                vc.subtitle = "Unblocking this user means you will be able to message, you will not have access to their social profiles, and you will see their hosted events."
                vc.actionButtonTitle = "Unblock"
                vc.type = .unblockUser
                vc.selectedUserUid = self.selectedUserUid
                
                vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(vc, animated: false, completion: nil)
                
                vc.onDoneBlock = { result in
                    self.fetchedUserIsBlockedByCurrentUser = false
                    self.handleMessageBlockButton()
                }
            }
        }
        
        let reportAction = UIAlertAction(title: "Report Profile", style: .destructive) { (action) in
            //new VC to submit problem
            if let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "reportProblemVC") as? ReportProblemViewController {
                 VC1.type = 1
                 VC1.profileUidReporting = self.selectedUserUid
                self.navigationController?.pushViewController(VC1, animated: true)
            }
        }
        
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel) { (action) in
            //no action taken
        }
 
        // Add the actions to your actionSheet
        let defaults = UserDefaults.standard
        let blockUidArray = defaults.stringArray(forKey: "blockedUidArray") ?? [String]()
        if blockUidArray.index(of: self.selectedUserUid) != nil {
            //is blocked
            alertController.addAction(unblockAction)
        } else {
            //isn't blocked
            alertController.addAction(blockAction)
        }
        alertController.addAction(reportAction)
        alertController.addAction(cancelAction)
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(imageScrollView.contentOffset.x / view.frame.width)
        pageControl.currentPage = Int(pageIndex)
        }
    }


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
