//
//  UsersExtrasViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 10/2/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import SDWebImage
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FBSDKLoginKit
import CoreData

class UsersExtrasViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Settings"
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
    }
    //sections: Account (delete account, payments, type), Privacy (Terms of Service, blocked accounts), Support (report a problem -> Spam or abuse, something isn't working, general feeback, cancel), About (who we are, Why Zazu, terms), Logins: logout
    
    func numberOfSections(in tableView: UITableView) -> Int {
            return 5
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            //ACCOUNT
            if indexPath.row == 0 {
                //delete account
                //1. Create the alert controller.
                let alert = UIAlertController(title: "Delete Account?", message: "type 'delete' in the text field below to permanently delete your account", preferredStyle: .alert)
                
                //2. Add the text field. You can configure it however you need.
                alert.addTextField { (textField) in
                    textField.text = "we'll miss you"
                }
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action : UIAlertAction!) -> Void in }))
                // 3. Grab the value from the text field, and print it when the user clicks OK.
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                    let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
                    if textField?.text == "delete" {
                        if let uid = Auth.auth().currentUser?.uid {
                            Database.database().reference().child("USERS").child(uid).removeValue()
                            let storeRef = Storage.storage().reference().child("profileImages").child(uid)
                            storeRef.delete { error in
                                ()
                            }
                            
                            self.deletePersonData()
                            self.deleteAllData("Dm")
                            self.deleteAllData("CdNotification")
                            self.deleteAllData("CdEvent")
                            
                            let user = Auth.auth().currentUser
                            
                            user?.delete { error in
                                if error != nil {
                                    let alert = UIAlertController(title: "Error", message: error as? String, preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (action : UIAlertAction!) -> Void in }))
                                } else {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let controller = storyboard.instantiateViewController(withIdentifier: "ViewController1")
                                    self.present(controller, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            } else if indexPath.row == 1 {
                //payments
                
            
            } else {
                //type
                
            }
        } else if indexPath.section == 1 {
            //PRIVACY
            if indexPath.row == 0 {
                //Terms of Service
                handleNavigationToWebView(url: "https://www.zazuapp.com/terms-of-service", title: "Terms of Service")
            } else if indexPath.row == 1 {
                //Privacy Policy
                handleNavigationToWebView(url: "https://www.zazuapp.com/privacy-policy", title: "Privacy Policy")
            } else {
                //blocked Accounts
                if let blockedVc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "blockedUsersVC") as? BlockedUsersTableViewController {
                    
                    self.navigationController?.pushViewController(blockedVc, animated: true)
                }
            }
        } else if indexPath.section == 2 {
            if indexPath.row == 0 {
                //new VC to submit problem
                let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "reportProblemVC") as! ReportProblemViewController
                self.navigationController!.pushViewController(VC1, animated: true)
            } else {
                //contact us
                 handleNavigationToWebView(url: "https://www.zazuapp.com/contact", title: "Contact us")
                }
        } else if indexPath.section == 3 {
            //ABOUT
            if indexPath.row == 0 {
                //Who we are (website)
                 handleNavigationToWebView(url: "https://www.zazuapp.com/team", title: "Who Are We?")
            } else {
                //why zazu (website)
                 handleNavigationToWebView(url: "https://www.zazuapp.com", title: "Why Zazu?")
            }
        } else if indexPath.section == 4 {
                //clear coreData
                self.deletePersonData()
                self.deleteAllData("Dm")
                self.deleteAllData("CdNotification")
                self.deleteAllData("CdEvent")
            
            //clear user defaults
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            print(Array(UserDefaults.standard.dictionaryRepresentation().keys).count)
            
    
                let firebaseAuth = Auth.auth()
                do {
                    try firebaseAuth.signOut()
                    FBSDKLoginManager().logOut()
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "ViewController1")
                    self.present(controller, animated: true, completion: nil)
                } catch {
                   ()
                    return
                }
        } else {
            //default row clicked
        }
    }
    
    func handleNavigationToWebView(url: String, title: String) {
        if let viewController1 = storyboard?.instantiateViewController(withIdentifier: "VCTOS") as? ViewControllerTermsOfService {
            viewController1.modalPresentationStyle = .overFullScreen
            viewController1.url = url
            viewController1.webViewTitle = title
            self.present(viewController1, animated: true, completion: {
            })
        }
    }
    
    func deletePersonData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            
            if peep.count > 0 {
                peep[0].about = nil
                peep[0].accepted_terms = false
                peep[0].birthday = nil
                peep[0].current_job_employer = nil
                peep[0].current_job_employer_logo = nil
                peep[0].current_job_title = nil
                peep[0].dream_job_employer = nil
                peep[0].dream_job_employer_logo = nil
                peep[0].dream_job_title = nil
                peep[0].education_institution = nil
                peep[0].education_institution_logo = nil
                peep[0].education_level = nil
                peep[0].first_name = nil
                peep[0].instagram_handle = nil
                peep[0].last_name = nil
                peep[0].location_city = nil
                peep[0].location_state = nil
                peep[0].photo1 = nil
                peep[0].photo2 = nil
                peep[0].photo3 = nil
                peep[0].photo4 = nil
                peep[0].photo5 = nil
                peep[0].photo6 = nil
                peep[0].profile_photo_url = nil
                peep[0].snapchat_handle = nil
                peep[0].user_name = nil
            }
        } catch {
            ()
        }
    }
    
    func deleteAllData(_ entity:String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        let contex = PersistanceService.context
        do {
            let results = try PersistanceService.context.fetch(fetchRequest)
            for object in results {
                guard let objectData = object as? NSManagedObject else {continue}
                contex.delete(objectData)
            }
        } catch {
            ()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        } else if section == 1 {
            return 3
        } else if section == 2 {
            return 2
        } else if section == 3 {
            return 2
        } else {
            return 1
        }
    }
    
    // MARK: Table View funcions
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 40)) as? UITableViewHeaderFooterView
        headerView?.textLabel?.font = UIFont(name: "nevis-Bold", size: 24)
        headerView?.textLabel?.textColor = UIColor.black
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(40)
    }
    
    //sections: Account (delete account, payments, type), Privacy (Terms of Service, blocked accounts), Support (report a problem -> Spam or abuse, something isn't working, general feeback, cancel), About (who we are, Why Zazu, terms), Logins: logout
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Account"
        } else if section == 1 {
            return "Privacy"
        } else if section == 2 {
            return "Support"
        } else if section == 3 {
            return "About"
        } else {
            return "Logins"
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch(indexPath.section)  {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionsCell", for: indexPath) as! OptionsTableViewCell
            if indexPath.row == 0 {
                cell.optionLabel.text = "Delete Account"
                cell.optionLabel.textColor = UIColor.red
            } else if indexPath.row == 1 {
                 cell.optionLabel.text = "Payments"
            } else if indexPath.row == 2 {
                cell.optionLabel.text = "Type"
            } else {
                cell.optionLabel.text = "Type"
            }
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionsCell", for: indexPath) as! OptionsTableViewCell
            if indexPath.row == 0 {
                cell.optionLabel.text = "Terms of Service"
            } else if indexPath.row == 1 {
                cell.optionLabel.text = "Privacy Policy"
            } else {
                cell.optionLabel.text = "Blocked accounts"
            }
            return cell
        case 2:
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "optionsCell", for: indexPath) as! OptionsTableViewCell
                cell.optionLabel.text = "Report a problem"
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "optionsCell", for: indexPath) as! OptionsTableViewCell
                cell.optionLabel.text = "Contact us"
                return cell
            }
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionsCell", for: indexPath) as! OptionsTableViewCell
            if indexPath.row == 0 {
                cell.optionLabel.text = "Who Are We?"
            } else {
                cell.optionLabel.text = "Why Zazu?"
            }
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionsCell", for: indexPath) as! OptionsTableViewCell
            cell.optionLabel.text = "Logout"
            cell.optionLabel.textColor = UIColor(named: "zazuBlue")
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "optionsCell", for: indexPath) as! OptionsTableViewCell
            return cell
        }
    }
}

