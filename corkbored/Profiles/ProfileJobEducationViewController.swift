//
//  ProfileJobEducationViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/4/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import CoreData
import Firebase

class ProfileJobEducationViewController: UIViewController, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    //MARK: - Variables
    var numberOfCharactersInLocation: Int? = 0
    var numberOfCharactersInTitle: Int? = 0
    var selectedItem: Int?
    var fieldText: String?
    let picker = UIPickerView()
    let pickerCert = ["GED", "Diploma", "A.A.", "A.S.", "AAS", "B.A.", "B.S", "BFA", "BAS", "GED", "M.A.", "M.S.", "MBA", "MFA", "Ph.D.", "J.D.", "M.D.", "DDS"]
    let pickerStudies = ["Humantities", "Business", "Nursing", "Health", "Biology", "Phsychology", "Criminal Justice", "Education", "Engineering", "Finance", "Accounting", "Communications", "Computer science", "English", "Economics", "IT", "History", "Political science", "Design", "Earth Sciences", "Culinary Arts", "Mathematics", "Dental", "Foreign language", "Film studies", "Religious studies", "Protective services", "veterinary studies", "Performing arts", "Chemistry", "Legal Studies", "Nutrition", "Dental", "Foreign language", "Pharmacy", "Physics", "Anthropology", "Music", "General studies", "Architecture", "Philosophy", "Agricultural sciences", "Arts management", "social work"]
    let pickerYears = ["2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024"]
    var companyArray = [Company]()
    var currentUserCoreData = [Person]()
    var object = ["title" : "", "subtitle" : "", "logo" : ""] as [String : String]
    var socialObject = ["instagram" : "", "snapchat" : ""] as [String : String]

    //MARK: - Outlets
    @IBOutlet weak var charactersRemaining: UILabel!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var subtitleTextField: UITextField!
    @IBOutlet weak var companyTableView: UITableView!
    
    //MARK: - VDL
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.dataSource = self
        picker.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getCoreData()
        setupViews()
    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        if self.isMovingFromParent {
            saveToCoreData()
        }
    }
    
    func setupViews() {
        self.titleTextField.layer.cornerRadius = 10
        self.titleTextField.layer.masksToBounds = true
        self.titleTextField.layer.borderWidth = 1
        self.titleTextField.layer.borderColor = UIColor(named: "zazuBlue")?.cgColor
        
        self.subtitleTextField.layer.cornerRadius = 10
        self.subtitleTextField.layer.masksToBounds = true
        self.subtitleTextField.layer.borderWidth = 1
        self.subtitleTextField.layer.borderColor = UIColor(named: "zazuBlue")?.cgColor
        
        if selectedItem == 2 || selectedItem == 3 || selectedItem == 4 {
            self.charactersRemaining.isHidden = true
        }
        switch (self.selectedItem) {
        case 0:
            self.title = "Current Job"
            self.titleTextField.placeholder = "current position"
            self.subtitleTextField.placeholder = "current employer"
            self.titleTextField.text = self.currentUserCoreData[0].current_job_title
            self.subtitleTextField.text = self.currentUserCoreData[0].current_job_employer
            break;
        case 1:
            self.title = "Dream Job"
            self.titleTextField.placeholder = "dream position"
            self.subtitleTextField.placeholder = "dream employer"
            self.titleTextField.text = self.currentUserCoreData[0].dream_job_title
            self.subtitleTextField.text = self.currentUserCoreData[0].dream_job_employer
            break;
        case 2:
            self.title = "Education"
            self.titleTextField.placeholder = "degree and class year"
            self.subtitleTextField.placeholder = "institution"
            self.titleTextField.text = self.currentUserCoreData[0].education_level
            self.subtitleTextField.text = self.currentUserCoreData[0].education_institution
            break
        case 3:
            self.title = "Instagram"
            self.titleTextField.placeholder = "MichaelScott"
            self.titleTextField.text = self.currentUserCoreData[0].instagram_handle
            self.subtitleTextField.isHidden = true
            break
        case 4:
            self.title = "Snapchat"
            self.titleTextField.placeholder = "MichaelScott"
            self.titleTextField.text = self.currentUserCoreData[0].snapchat_handle
            self.subtitleTextField.isHidden = true
            break
        default:
           ()
        }
    }
    
    func removeAllExceptSelected(index: Int) {
        let indexes = [index]
        self.companyArray = indexes.map { self.companyArray[$0] }
    }
    
    //MARK - Core date
    func saveToCoreData() {
        switch (self.selectedItem) {
        case 0:
            saveCoreDataModelAndDB(key1: "current_job_title", key2: "current_job_employer", key3: "current_job_employer_logo", key4: "current_job")
            break;
        case 1:
            saveCoreDataModelAndDB(key1: "dream_job_title", key2: "dream_job_employer", key3: "dream_job_employer_logo", key4: "dream_job")
            break;
        case 2:
            saveCoreDataModelAndDB(key1: "education_level", key2: "education_institution", key3: "education_institution_logo", key4: "education")
            break;
        case 3:
            saveCoreDataModelAndDB(key1: "dream_job_title", key2: "dream_job_employer", key3: "instagram_handle", key4: "snapchat_handle")
            break;
        case 4:
            saveCoreDataModelAndDB(key1: "dream_job_title", key2: "dream_job_employer", key3: "instagram_handle", key4: "snapchat_handle")
            break;
        default:
         ()
        }
    }

    func getCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            currentUserCoreData = peep
        } catch {
           ()
        }
    }
    
    //MARK: Birthday picker view
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return self.pickerCert.count
        } else if component == 1 {
            return self.pickerStudies.count
        } else {
            return self.pickerYears.count
        }
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view as? UILabel { label = v }
        label.font = UIFont (name: "Metropolis-Medium", size: 18)
        if component == 0 {
             label.text = self.pickerCert[row]
            label.textAlignment = .center
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.5
            return label
        } else if component == 1 {
            label.text = self.pickerStudies[row]
            label.textAlignment = .center
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.5
            return label
        } else {
            label.text = self.pickerYears[row]
            label.textAlignment = .center
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.5
            return label
        }
    }
    
    //MARK: - TextField
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if ((textField == self.titleTextField) && (self.selectedItem == 2)) {
            //toolbar
            let toolbar = UIToolbar()
            toolbar.sizeToFit()
            
            //done button for toolbar
            let selector = #selector(educationPickerDonePressed)
            let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: selector)
            toolbar.setItems([flexBarButton, done], animated: false)
            textField.inputAccessoryView = toolbar
            textField.inputView = picker
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxChar = 30
        var newLength = 0
        
        if ((range.location == 0) && (string == "@")) { return false }
        
        if textField == titleTextField {
            if range.location + range.length > (self.titleTextField.text?.count)! {
                self.companyArray.removeAll()
                return false
            }
            
            newLength = (titleTextField.text?.count)! + string.count - range.length
            let charactersLeftInt = maxChar - newLength
            numberOfCharactersInTitle = newLength
            if charactersLeftInt < 0 {
                self.charactersRemaining.text? = "0"
            } else {
                self.charactersRemaining.text? = (String(charactersLeftInt))
            }
        } else {
            let currentString: NSString = self.subtitleTextField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            
            if newString.length >= 2 {
                //having a BUG with loading more than 5 companies
                callClearbitAPI(name: self.subtitleTextField.text!)
            } else {
                self.companyArray.removeAll()
                self.companyTableView.reloadData()
            }
        }
        return newLength <= maxChar
    }

    @objc func educationPickerDonePressed() {
        let selectedValue = self.pickerCert[picker.selectedRow(inComponent: 0)] + " " + self.pickerStudies[picker.selectedRow(inComponent: 1)] + " " + self.pickerYears[picker.selectedRow(inComponent: 2)]
        self.titleTextField.text = selectedValue
        self.view.endEditing(true)
    }
    
    //synchronous Updating Profile fields to Firebase and Coredata
    func saveCoreDataModelAndDB(key1: String, key2: String, key3: String, key4: String)  {
        if let uid = Auth.auth().currentUser?.uid {
            let ref = Database.database().reference()
            
            if selectedItem == 3 || selectedItem == 4 {
                if (titleTextField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
                    if selectedItem == 3 {
                        //textfield empty
                        self.currentUserCoreData[0].setValue(nil, forKey: key3)
                        ref.child("USERS/\(uid)/social/").child("instagram").removeValue()
                    } else {
                        //textfield empty
                        self.currentUserCoreData[0].setValue(nil, forKey: key4)
                        ref.child("USERS/\(uid)/social/").child("snapchat").removeValue()
                    }
                } else {
                    if selectedItem == 3 {
                        if let handle = self.titleTextField.text {
                            self.currentUserCoreData[0].setValue(handle, forKey: key3)
                            ref.child("USERS/\(uid)/social").updateChildValues(["instagram": handle])
                        }
                    } else {
                       if let handle = self.titleTextField.text {
                            self.currentUserCoreData[0].setValue(handle, forKey: key4)
                            ref.child("USERS/\(uid)/social").updateChildValues(["snapchat": handle])
                        }
                    }
                }
            } else {
                if (titleTextField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || (subtitleTextField.text?.trimmingCharacters(in: .whitespaces).isEmpty)!  {
                    //if any field is blank, don't save data to core data, save as nil, and save to DB as emptiness
                    self.currentUserCoreData[0].setValue(nil, forKey: key1)
                    self.currentUserCoreData[0].setValue(nil, forKey: key2)
                    self.currentUserCoreData[0].setValue(nil, forKey: key3)
                    //db
                    ref.child("USERS/\(uid)/\(key4)").removeValue()
                } else {
                    //just selected new company
                    if (companyArray.count == 1) && (companyArray[0].name == subtitleTextField.text) {
                        self.currentUserCoreData[0].setValue(self.titleTextField.text!, forKey: key1)
                        self.currentUserCoreData[0].setValue(self.subtitleTextField.text!, forKey: key2)
                        let indexPath = IndexPath(row: 0, section: 0)
                        let cell = self.companyTableView.cellForRow(at: indexPath) as! CompanyTableViewCell
                        let imageData = cell.logo.image!.jpegData(compressionQuality: 0.1)
                        self.currentUserCoreData[0].setValue(imageData, forKey: key3)
                        
                        //enter title to object and add to firebase
                        self.object["title"] = self.titleTextField.text!
                        ref.child("USERS/\(uid)/\(key4)").updateChildValues(self.object)
                    } else {
                        
                        //EDITED TITLETEXTFIELD
                        self.currentUserCoreData[0].setValue(self.titleTextField.text!, forKey: key1)
                        ref.child("USERS/\(uid)/\(key4)/title").setValue(self.titleTextField.text!)
                    }
                }
            }
            PersistanceService.saveContext()
            }
        }
    
    func  callClearbitAPI(name: String) {
        self.companyArray.removeAll()
        Alamofire.request("https://autocomplete.clearbit.com/v1/companies/suggest?query=:\(name)").responseJSON { response in
            if let jso = response.result.value {
               let json = jso as! [[String : Any]]
                
                for dict in json {
                    // Condition required to check for type safety :)
                    guard let name = dict["name"] as? String,
                        let logo = dict["logo"] as? String,
                        let domain = dict["domain"] as? String else {
                            continue
                    }
                    let company = Company(logo: logo, name: name, domain: domain)
                    self.companyArray.append(company)
                    if self.companyArray.count > 4 {
                        self.companyTableView.reloadData()
                        return
                    }
                }
                self.companyTableView.reloadData()
            }
        }
    }
    
    //MARK: - Tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.companyArray.count ==  0 {
            self.companyTableView.isHidden = true
            return 0
        } else {
            self.companyTableView.isHidden = false
            return self.companyArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "companyCell", for: indexPath) as! CompanyTableViewCell
        
        if let urlUrl = URL.init(string: ((self.companyArray[indexPath.row]).logo)) {
            cell.logo.sd_setImage(with: urlUrl, placeholderImage: nil, options: .refreshCached)
            cell.logo.layer.cornerRadius = 10
            cell.logo.clipsToBounds = true
        }
        
        cell.name.text = (self.companyArray[indexPath.row]).name
        cell.domain.text = (self.companyArray[indexPath.row]).domain
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowClicked = indexPath.row + 1
        if rowClicked > self.companyArray.count {
            tableView.reloadData()
        } else {
            self.subtitleTextField.text = self.companyArray[indexPath.row].name
            removeAllExceptSelected(index: indexPath.row)
            object["subtitle"] = companyArray[0].name
            object["logo"] = companyArray[0].logo
            self.companyTableView.reloadData()
        }
    }
}
