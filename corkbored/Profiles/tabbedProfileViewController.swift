//
//  tabbedProfileViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 2/10/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import SDWebImage
import FirebaseAuth
import FirebaseDatabase
import FBSDKLoginKit
import CoreData

class tabbedProfileViewController: UIViewController {
    //MARK: - Variables
    var profileUid = String()
    var hashtagArray = [String]()
    
    //MARK: - Outlets
    @IBOutlet weak var mainProfileImage: UIImageView!
    @IBOutlet weak var editContainer: UIView!
    @IBOutlet weak var extrasContainer: UIView!
    @IBOutlet weak var getZazuCore: UIView!

    //MARK: - VDL
    override func viewDidLoad() { super.viewDidLoad() }
    
    override func didReceiveMemoryWarning() { super.didReceiveMemoryWarning() }
    
    override func viewWillAppear(_ animated: Bool) {
        getCoreData()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(editContainerTapped))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(extrasContainerTapped))
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(getZazuCoreTapped))
        
        editContainer.addGestureRecognizer(tap)
        extrasContainer.addGestureRecognizer(tap2)
        getZazuCore.addGestureRecognizer(tap3)
    }
    
    @objc func editContainerTapped(sender: UIGestureRecognizer) {
        self.performSegue(withIdentifier: "toEdit", sender: self)
        
    }
    
    @objc func extrasContainerTapped(sender: UIGestureRecognizer) {
        self.performSegue(withIdentifier: "profToExtras", sender: self)
    }
    
    @objc func getZazuCoreTapped(sender: UIGestureRecognizer) {
        showAlert1(subtitle: "This suite of advanced features are coming early 2019!", title: "Zazu Core", buttonTitle: "Sweet")
    }
    
    @objc func dismissAlert() { () }
    
    func getCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            let fir = peep[0].value(forKey: "first_name") as! String
            let las = peep[0].value(forKey: "last_name") as! String
            let total = fir + " " + las
            self.navigationItem.title = total
        
            let imageData = peep[0].value(forKey: "photo1")
        
            if imageData != nil {
                self.mainProfileImage.image = (UIImage(data: (imageData as! NSData) as Data))
            } else {
                
                if let uid = Auth.auth().currentUser?.uid {
                    let ref = Database.database().reference().child("users").child(uid).child("profile_urls")
                    ref.observeSingleEvent(of: .value, with: { (snapshot) in
                        if let dictionary = snapshot.value as? [AnyHashable: AnyObject] {
                            let photo1Url = dictionary["photo1"] as! String
                            if photo1Url.hasPrefix("https://") {
                                let url = URL(string: photo1Url)
                                self.mainProfileImage.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached)
                            }
                        }
                    })
                }
            }
        } catch {
           ()
        }
    }
}
