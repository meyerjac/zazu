//
//  ViewControllerTermsOfService.swift
//  corkbored
//
//  Created by Jackson Meyer on 12/14/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import WebKit

class ViewControllerTermsOfService: UIViewController, WKNavigationDelegate {
    @IBOutlet weak var LoadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var CloseButton: UIButton!
    @IBOutlet weak var VCTitleLabel: UILabel!
    @IBAction func CloseButtonClicked(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    var url = "https://www.zazuapp.com"
    var webViewTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.VCTitleLabel.text = webViewTitle
        
        webView.navigationDelegate = self
        self.CloseButton.transform = self.CloseButton.transform.rotated(by: CGFloat(Double.pi / 4))
        
        let filledUrl = URL(string: url)!
        webView.load(URLRequest(url: filledUrl))
        
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        toolbarItems = [refresh]
        navigationController?.isToolbarHidden = false
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.LoadingIndicator.stopAnimating()
    }
}
