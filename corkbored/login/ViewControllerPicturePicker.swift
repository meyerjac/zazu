//
//  ViewControllerPicturePicker.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/7/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import ImagePicker
import Lightbox
import CoreData
import Firebase

class ViewControllerPicturePicker: UIViewController, ImagePickerDelegate {
    //MARK: - VARIABLES
    var placeholderThumb = "https://firebasestorage.googleapis.com/v0/b/corkbored-f7b10.appspot.com/o/Zazu%2Fplaceholder.jpg?alt=media&token=e870ce1c-d0a0-45d1-8fd0-bf74549824b4"
    var tappedImage: TinderImageView!
    var imageViewArray: [UIImageView] = []
    var placeholder: String = ""
    var numberOfPhotosPicked: Int = 0
    var dbPhotoSetterIndex: Int = 1
    var imagePresentDictionary = [1: false, 2: false, 3: false, 4: false, 5: false, 6: false]
    var profile_urls = ["photo1": "", "photo2": "", "photo3": "", "photo4": "", "photo5": "", "photo6": ""]
    var coreDataDictionary = [1: "photo1", 2: "photo2", 3: "photo3", 4: "photo4", 5: "photo5", 6: "photo6"]
    var currentUserCoreData = [Person]()
    var haveRecievedThumbnail = false
    var handlingSavingPhotos = false
    
    //MARK: - OUTLETS
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var spinnerLoading: UIActivityIndicatorView!
    @IBOutlet weak var image1: TinderImageView!
    @IBOutlet weak var image2: TinderImageView!
    @IBOutlet weak var image3: TinderImageView!
    @IBOutlet weak var image4: TinderImageView!
    @IBOutlet weak var image5: TinderImageView!
    @IBOutlet weak var image6: TinderImageView!
    @IBAction func continueButtonClicked(_ sender: UIButton) {
        handlingSavingPhotos = true
        spinnerLoading.isHidden = false
        continueButton.isEnabled = false
        setThumbnailUpdateListener()
        updateData()
    }
   
    //TODO: - VDL Make more efficient
    override func viewDidLoad() {
        super.viewDidLoad()

        let imageTap1 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        let imageTap2 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        let imageTap3 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        let imageTap4 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        let imageTap5 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))
        let imageTap6 = UITapGestureRecognizer(target: self, action: #selector(imageTapped))

        self.image1.addGestureRecognizer(imageTap1)
        self.image1.isUserInteractionEnabled = true
        self.image2.addGestureRecognizer(imageTap2)
        self.image2.isUserInteractionEnabled = true
        self.image3.addGestureRecognizer(imageTap3)
        self.image3.isUserInteractionEnabled = true
        self.image4.addGestureRecognizer(imageTap4)
        self.image4.isUserInteractionEnabled = true
        self.image5.addGestureRecognizer(imageTap5)
        self.image5.isUserInteractionEnabled = true
        self.image6.addGestureRecognizer(imageTap6)
        self.image6.isUserInteractionEnabled = true

        //clear recieved image array
        self.imageViewArray.removeAll()
        self.imageViewArray.append(self.image1 as UIImageView)
        self.imageViewArray.append(self.image2 as UIImageView)
        self.imageViewArray.append(self.image3 as UIImageView)
        self.imageViewArray.append(self.image4 as UIImageView)
        self.imageViewArray.append(self.image5 as UIImageView)
        self.imageViewArray.append(self.image6 as UIImageView)

        //needs to stay at the end
        getCoreData()
    }
    
    override func viewDidLayoutSubviews() {
        self.continueButton.layer.cornerRadius = self.continueButton.frame.size.height/2
        self.continueButton.clipsToBounds = true
        
        if self.imagePresentDictionary[1] == true {
            self.continueButton.setGradientBackground(colorOne: Colors.grad1, colorTwo: Colors.grad2)
            self.view.layoutIfNeeded()
            self.continueButton.isEnabled = true
        } else {
            removeGradientSetBackground()
            self.continueButton.isEnabled = false
        }
    }
    
    override var prefersStatusBarHidden: Bool { return true }
    
    //MARK: - GET THUMBNAIL URL
    func setThumbnailUpdateListener() {
        if let uid = Auth.auth().currentUser?.uid {
            //First snap grabs nothing
            var snapCount: Int = 0
            
            Database.database().reference().child("USERS").child(uid).child("thumbnail").observe(.value, with: { (snapshot) in
                snapCount += 1
            
                if snapCount == 2 {
                    //second snap is on update
                    let storeRef = Storage.storage().reference().child("profileImages").child(uid).child("thumb_1.jpg")
                    storeRef.downloadURL(completion: { (url, error) in
                    if error != nil {
                        return
                    } else {
                    
                        self.currentUserCoreData[0].profile_photo_url = url
                        PersistanceService.saveContext()
                        self.haveRecievedThumbnail = true
                    
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller = storyboard.instantiateViewController(withIdentifier: "ViewController6")
                        self.present(controller, animated: false, completion: nil)
                        }
                    })
                }
            })
        }
    }
    
    //MARK: - CORE DATA
    func getCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            self.currentUserCoreData = peep
            
            for n in 1...6 {
                let imageData = self.currentUserCoreData[0].value(forKey: self.coreDataDictionary[n]!)
                if imageData != nil {
                    imageViewArray[n - 1].image = (UIImage(data: (imageData as! NSData) as Data))
                    self.imagePresentDictionary[n] = true
                    
                }
            }
        } catch { () }
        checkIfImagesPresentInView()
    }
    
    func imageWithImage(sourceImage:UIImage, scaledToWidth: CGFloat) -> UIImage {
        let oldWidth = sourceImage.size.width
        let scaleFactor = scaledToWidth / oldWidth
        
        let newHeight = sourceImage.size.height * scaleFactor
        let newWidth = oldWidth * scaleFactor
        
        UIGraphicsBeginImageContext(CGSize(width:newWidth, height:newHeight))
        sourceImage.draw(in: CGRect(x:0, y:0, width:newWidth, height:newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func updateData() {
    var lastIndex = 1
        //handle Saving images
        for n in 1...6 {
            if let uid = Auth.auth().currentUser?.uid {
                let ref = Database.database().reference()
                let storeRef = Storage.storage().reference().child("profileImages/\(uid)/\(lastIndex).jpg")
                if imagePresentDictionary[n] != false {
                    let imageData = imageWithImage(sourceImage:imageViewArray[n-1].image!, scaledToWidth: 400).jpegData(compressionQuality: 1.0)
                    self.currentUserCoreData[0].setValue(imageData, forKey: "photo\(lastIndex)")
                    
                    storeRef.putData(imageData!, metadata: nil) { (metadata, err) in
                        if err != nil {
                            ()
                        } else {
                            storeRef.downloadURL(completion: { (url, error) in
                                if error != nil {
                                    ()
                                    return
                                }
                                
                                if url != nil {
                                    let num = String(self.dbPhotoSetterIndex)
                                    self.profile_urls["photo\(num)"] = url!.absoluteString
                                    ref.child("USERS/\(uid)/profile_urls").setValue(self.profile_urls)
                                    self.dbPhotoSetterIndex += 1
                                }
                            })
                        }
                    }
                    lastIndex += 1
                } else {
                    //no new image in dictionary index
                    let num = String(self.dbPhotoSetterIndex)
                    self.profile_urls["photo\(num)"] = ""
                }
            }
        }
        
        self.dbPhotoSetterIndex = 1
        if lastIndex == 7 {
            
        } else {
            for n in (lastIndex)..<7 {
                self.currentUserCoreData[0].setValue(nil, forKey: "photo\(n)")
            }
        }
    }
    
    // MARK: - IMAGE PICKER
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }

        let lightboxImages = images.map {
            return LightboxImage(image: $0)
        }
        
        let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        imagePicker.present(lightbox, animated: true, completion: nil)
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        
        let tag = self.tappedImage.tag
        
        for n in 0...images.count-1 {
            if tag != 1 {
                if tag + n > 6 {
                    self.imageViewArray[tag + n - 7].image = images[n]
                    self.imagePresentDictionary[tag + n - 6] = true
                } else  {
                    self.imageViewArray[n - 1 + tag].image = images[n]
                    self.imagePresentDictionary[n + tag] = true
                }
            } else {
                self.imageViewArray[n].image = images[n]
                self.imagePresentDictionary[n + 1] = true
            }
        }
        checkIfImagesPresentInView()
    }
    
    func checkIfImagesPresentInView() {
        if self.imagePresentDictionary[1] == true {
            self.continueButton.setGradientBackground(colorOne: Colors.grad1, colorTwo: Colors.grad2)
            self.view.layoutIfNeeded()
            self.continueButton.isEnabled = true
        } else {
           removeGradientSetBackground()
           self.continueButton.isEnabled = false
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        tappedImage = tapGestureRecognizer.view as? TinderImageView
        if handlingSavingPhotos { return }
        
        let config = Configuration()
        config.doneButtonTitle = "Finish"
        config.noImagesTitle = "Sorry! There are no images here!"
        config.recordLocation = false
        config.allowVideoSelection = false
        
        let imagePicker = ImagePickerController(configuration: config)
        imagePicker.imageLimit = 6
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    @objc func removeButtonPressed(sender: UIButton ) {
        if handlingSavingPhotos { return }
        self.imagePresentDictionary[sender.tag] = false
        self.imageViewArray[sender.tag - 1].image = UIImage(named: "userPic")
        checkIfImagesPresentInView()
    }
    
    //MARK: - REMOVE GRADIENT
    func removeGradientSetBackground() {
        if self.continueButton.layer.sublayers != nil {
            for layer in (continueButton.layer.sublayers)! {
                if layer.name == "gradientLayer" {
                    layer.removeFromSuperlayer()
                }
            }
        }
    }
}
