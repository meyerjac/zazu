//
//  ViewController0.swift
//  corkbored
//
//  Created by Jackson Meyer on 9/23/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import CoreLocation
import UserNotifications
import FBSDKLoginKit

class ViewController0: UIViewController {
    //MARK: - Variables
    var currentUserCoreData = [Person]()

    //MARK: - VDL
    override func viewDidLoad() { super.viewDidLoad() }
    
    override func viewDidAppear(_ animated: Bool) { getCoreData() }
    
    override var prefersStatusBarHidden: Bool { return true }
    
    //MARK: - Navigation functions
    @objc func VC1() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ViewController1")
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated:true, completion: nil)
    }
    
    @objc func login() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "main")
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated:true, completion: nil)
    }
    
    @objc func toPhotoPicker() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "picker")
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated:true, completion: nil)
    }
    
    @objc func toPermissions() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ViewController6")
        controller.modalTransitionStyle = .crossDissolve
        self.present(controller, animated:true, completion: nil)
    }
    
    //MARK: - Check auth status
    func checkIfUserExists(uid: String, completion: @escaping (Bool?) -> ()) {
        let database = Database.database().reference()
        database.child("USERS/\(uid)").observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            if snapshot.exists() {
                 completion(true)
            } else {
                  completion(false)
            }
        })
    }
    
    func getCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
           
            self.currentUserCoreData = peep
            if self.currentUserCoreData.count != 0 {
                handleAuthStatus()
            } else {
                let person = Person(context: PersistanceService.context)
                person.accepted_terms = false
                self.currentUserCoreData.append(person)
                PersistanceService.saveContext()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                    self.VC1()
                }
            }
        } catch {
            //MARK: TODO
        }
    }
    
    func handleAuthStatus() {
        if let uid = Auth.auth().currentUser?.uid {
            self.checkIfUserExists(uid: uid, completion: { Bool in
                if Bool == true {
                    if ((self.currentUserCoreData[0].first_name != nil) && (self.currentUserCoreData[0].last_name != nil) && (self.currentUserCoreData[0].user_name != nil) && (self.currentUserCoreData[0].birthday != nil) && (self.currentUserCoreData[0].accepted_terms == true) && (self.currentUserCoreData[0].profile_photo_url != nil) && (self.permissionsGranted())) {
                        self.login()
                    } else if ((self.currentUserCoreData[0].first_name != nil) && (self.currentUserCoreData[0].last_name != nil) && (self.currentUserCoreData[0].user_name != nil) && (self.currentUserCoreData[0].birthday != nil) && (self.currentUserCoreData[0].accepted_terms == true) && (self.currentUserCoreData[0].profile_photo_url != nil) && !(self.permissionsGranted())) {
                        self.toPermissions()
                    } else {
                        if ((self.currentUserCoreData[0].device_token != nil) && (self.currentUserCoreData[0].user_name != nil) && (self.currentUserCoreData[0].birthday != nil)) {
                              self.toPhotoPicker()
                        } else {
                             self.VC1()
                        }
                    }
            } else {
                do {
                    try Auth.auth().signOut()
                    FBSDKLoginManager().logOut()
                    self.deletePersonData()
                    self.VC1()
                } catch {
                    // TO DO
                    return
                }
            }
        })
    }
        
    if Auth.auth().currentUser == nil {
        VC1()
        }
    }
    
    func deletePersonData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            
            if peep.count > 0 {
                peep[0].about = nil
                peep[0].accepted_terms = false
                peep[0].birthday = nil
                peep[0].current_job_employer = nil
                peep[0].current_job_employer_logo = nil
                peep[0].current_job_title = nil
                peep[0].dream_job_employer = nil
                peep[0].dream_job_employer_logo = nil
                peep[0].dream_job_title = nil
                peep[0].education_institution = nil
                peep[0].education_institution_logo = nil
                peep[0].education_level = nil
                peep[0].first_name = nil
                peep[0].instagram_handle = nil
                peep[0].last_name = nil
                peep[0].location_city = nil
                peep[0].location_state = nil
                peep[0].photo1 = nil
                peep[0].photo2 = nil
                peep[0].photo3 = nil
                peep[0].photo4 = nil
                peep[0].photo5 = nil
                peep[0].photo6 = nil
                peep[0].profile_photo_url = nil
                peep[0].snapchat_handle = nil
                peep[0].user_name = nil
            }
        } catch {
            //MARK TODO
        }
    }
    
    func permissionsGranted() -> Bool {
        var locationEnabled = false
        var notificationsEnabled = false
        
        let isRegisteredForNotifications = UIApplication.shared.currentUserNotificationSettings?.types.contains(UIUserNotificationType.alert) ?? false
        notificationsEnabled = isRegisteredForNotifications
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .authorizedAlways, .authorizedWhenInUse:
                locationEnabled = true
            default:
                break
            }
        }
        
        if locationEnabled && notificationsEnabled {
            return true
        } else {
            return false
        }
    }
}
