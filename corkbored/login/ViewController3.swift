//
//  ViewController3.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/5/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import CoreData
import SCLAlertView

class ViewController3: UIViewController {
    //MARK: VARIABLES
    var currentUserCoreData = [Person]()
    var DBBirthday: String!
    
    //MARK: OUTLETS
    @IBOutlet weak var birthdayLabelLine: UIView!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBAction func backButtonClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ViewController2")
        self.present(controller, animated: false, completion: nil)
    }
    
    //must be 18 to use device
    @IBAction func continueButtonClicked(_ sender: UIButton) {
        if DBBirthday.calcAge(birthday: DBBirthday) >= 18 {
            fetchAndUpdateData()
            
            //next VC
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ViewController4")
            self.present(controller, animated: false, completion: nil)
        } else {
            //alert that user is too young to use Zazu
            showAgeAlert(subtitle: "Looks like you aren't quite old enough to use Zazu, we look forward to having you in the future.", title: "Shoot", buttonTitle: "Okay")
        }
    }
    
    @IBAction func birthdayPickView(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        let dateFormatterDB = DateFormatter()
        datePicker.maximumDate = Date()
        dateFormatter.dateFormat = "MMMM dd, YYYY"
        dateFormatterDB.dateFormat = "MM dd YYYY"
        
        //string to show users -- MMMM
        let somedateString = dateFormatter.string(from: sender.date)
        self.birthdayLabel.text = somedateString
        
        //get Database string
        DBBirthday = dateFormatterDB.string(from: sender.date)
        
        // "somedateString" is your string date
        self.continueButton.setGradientBackground(colorOne: Colors.grad1, colorTwo: Colors.grad2)
        self.continueButton.isEnabled = true
        self.birthdayLabelLine.isHidden = false
    }
    
      //MARK: -VDL
    override func viewDidLoad() {
        super.viewDidLoad()
        getCoreData()
        datePicker.setValue(UIColor.white, forKey: "textColor")
    }
    
    override var prefersStatusBarHidden: Bool { return true }
    
    override func viewDidLayoutSubviews() {
        self.continueButton.layer.cornerRadius = self.continueButton.frame.size.height/2
        self.continueButton.clipsToBounds = true
        
        if let layers = self.continueButton.layer.sublayers {
            for layer in layers {
                if layer.name == "gradientLayer" {
                    layer.frame = self.continueButton.bounds
                }
            }
        }
    }
    
      //MARK: - CORE DATA
    func fetchAndUpdateData() {
        if currentUserCoreData.count > 0 {
            self.currentUserCoreData[0].setValue(self.DBBirthday, forKey: "birthday")
            PersistanceService.saveContext()
        }
    }
    
    func getCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            self.currentUserCoreData = peep
            if currentUserCoreData.count > 0 {
                if let birthdayString = self.currentUserCoreData[0].birthday {
                    datePicker.setDate(from: birthdayString, format: "MM dd yyyy")
                    let laymenBirthday = formattedDateFromString(dateString: self.currentUserCoreData[0].birthday!, withFormat: "MMM dd, yyyy")
                    self.birthdayLabel.text = laymenBirthday
                    self.DBBirthday = currentUserCoreData[0].birthday
                    
                    //Setting buttn when there valid text
                    self.continueButton.isEnabled = true
                    self.continueButton.setGradientBackground(colorOne: Colors.grad1, colorTwo: Colors.grad2)
                    self.birthdayLabelLine.isHidden = false
                } else {
                    //
                    self.birthdayLabel.text  = ""
                    self.birthdayLabelLine.isHidden = true
                }
            }
        } catch { () }
    }
    
    //MARK: -  SIMPLE FUNCTIONS
    
    @objc func handleAlertClose() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ViewController1")
        self.present(controller, animated: false, completion: nil)
    }
    
    func formattedDateFromString(dateString: String, withFormat format: String) -> String? {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "MM dd yyyy"
        
        if let date = inputFormatter.date(from: dateString) {
            let outputFormatter = DateFormatter()
            outputFormatter.dateFormat = format
            return outputFormatter.string(from: date)
        }
        return nil
    }
    
  
}
