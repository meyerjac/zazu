//
//  PasswordViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/16/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import Firebase

class PasswordViewController: UIViewController {
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBAction func backButtonClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ViewController2")
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func sendButtonClicked(_ sender: UIButton) {
        sendButton.isEnabled = false
        let emailReset = passwordTextField.text ?? ""
        Auth.auth().sendPasswordReset(withEmail: emailReset) { error in
            // Your code here
            if error != nil {
                self.sendButton.isEnabled = true
                self.alertLabel.text = error?.localizedDescription
                self.alertLabel.isHidden = false
            } else {
                self.alertLabel.text = "password reset email sent"
                self.alertLabel.isHidden = false
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sendButton.setGradientBackground(colorOne: Colors.grad1, colorTwo: Colors.grad2)
    }
    override var prefersStatusBarHidden: Bool { return true }
    
    override func viewDidLayoutSubviews() {
         sendButton.layer.cornerRadius = sendButton.frame.height / 2
         sendButton.clipsToBounds = true
        
        passwordTextField.layer.cornerRadius = passwordTextField.frame.height / 2
        passwordTextField.clipsToBounds = true
        
        if self.sendButton.layer.sublayers != nil {
            for layer in (sendButton.layer.sublayers)! {
                if layer.name == "gradientLayer" {
                    layer.frame = self.sendButton.bounds
                }
            }
        }
    }
}
