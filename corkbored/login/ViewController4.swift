//
//  ViewController4.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/5/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import CoreData
import Firebase

class ViewController4: UIViewController, UITextFieldDelegate {
    //MARK: - VARIABLES
    var currentUserCoreData = [Person]()
    var acceptedTerms: Bool = true
    var age: Int = 0
    var birthday: String = ""
    var first_name: String = ""
    var last_name: String = ""
    var user_name: String = ""
    var email: String = ""
    
    //MARK: - OUTLETS
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var TermsOfServiceLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    //@IBOutlet weak var verifyPasswordTextField: UITextField! //unneeded
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var alertLabel: UILabel!
    @IBAction func backButtonClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ViewController3")
        self.present(controller, animated: false, completion: nil)
    }
    @IBAction func textFieldDidBeginEditing(_ sender: UITextField) {
        if sender.tag == 0 { self.alertLabel.isHidden = true }
        guard let text1 = self.emailTextField.text, !text1.isEmpty else {
            removeGradientSetBackground()
            return
        }
        guard let text2 = self.passwordTextField.text, !text2.isEmpty else {
            removeGradientSetBackground()
            return
        }
        /*guard let text3 = self.verifyPasswordTextField.text, !text3.isEmpty else {
            removeGradientSetBackground()
            return
        }*/ //unneeded
        self.continueButton.isEnabled = true
        self.continueButton.setGradientBackground(colorOne: Colors.grad1, colorTwo: Colors.grad2)
    }
    
    @IBAction func continueButtonClicked(_ sender: UIButton) {
        if let emailFieldText = emailTextField?.text {
            if isValidEmail(email: emailFieldText) {
                self.alertLabel.isHidden = true
                self.email = emailFieldText
                continueButton.isEnabled = false
                handleCreateUser()
            } else {
                self.alertLabel.text = "*email is badly formatted"
                self.alertLabel.isHidden = false
            }
        }
    }
    
    //MARK: - VDL
    override func viewDidLoad() {
        super.viewDidLoad()
        getCoreData()
        emailTextField.textContentType = UITextContentType(rawValue: "")
        passwordTextField.textContentType = UITextContentType.password
        //verifyPasswordTextField.textContentType = UITextContentType.password //unneeded
        
        if #available(iOS 12.0, *) {
            self.emailTextField.textContentType = UITextContentType.oneTimeCode
            self.passwordTextField.textContentType = UITextContentType.oneTimeCode
            //self.verifyPasswordTextField.textContentType = UITextContentType.oneTimeCode //unnneeded
        }
        
 
        
        TermsOfServiceLabel.text = "By signing up you agree to our Terms of Service and Privacy Policy"
        let text = (TermsOfServiceLabel.text)!
        let underlineAttriString = NSMutableAttributedString(string: text)
        let range1 = (text as NSString).range(of: "Terms of Service")
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
        let range2 = (text as NSString).range(of: "Privacy Policy")
        underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2)
        TermsOfServiceLabel.attributedText = underlineAttriString
        
        let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(termsOfServiceTapped))
        self.TermsOfServiceLabel.addGestureRecognizer(mytapGestureRecognizer)
        
        self.continueButton.backgroundColor = Colors.DisabledButtonGrey
    }
    
    @objc func termsOfServiceTapped(recognizer: UITapGestureRecognizer) {
        let text = (TermsOfServiceLabel.text)!
        let termsRange = (text as NSString).range(of: "Terms of Service")
        let privacyRange = (text as NSString).range(of: "Privacy Policy")
        
        if recognizer.didTapAttributedTextInLabel(label: TermsOfServiceLabel, inRange: termsRange) {
            //need to show terms in new view or modal view controller that they can swipe down
            handleNavigationToWebView(url: "https://www.zazuapp.com/terms-of-service", title: "Terms of Service")
        } else if recognizer.didTapAttributedTextInLabel(label: TermsOfServiceLabel, inRange: privacyRange) {
            handleNavigationToWebView(url: "https://www.zazuapp.com/privacy-policy", title: "Privacy Policy")
        } else { () }
    }
    
    override func viewDidLayoutSubviews() {
        self.continueButton.layer.cornerRadius = self.continueButton.frame.size.height/2
        self.continueButton.clipsToBounds = true
    }
    
    override var prefersStatusBarHidden: Bool { return true }
    
    //MARK: - CORE DATA
    
    func getCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            self.currentUserCoreData = peep
            if currentUserCoreData.count > 0 {
                
                if self.currentUserCoreData[0].birthday != nil {
                    //setting fields
                    self.birthday = self.currentUserCoreData[0].birthday!
                    self.age = calcAge(birthday: self.currentUserCoreData[0].birthday!)
                    
                } else {
                }
                
                if self.currentUserCoreData[0].first_name != nil {
                    //setting fields
                    self.first_name = self.currentUserCoreData[0].first_name!
                } else {
                }
                
                if self.currentUserCoreData[0].last_name != nil {
                    //setting fields
                    self.last_name = self.currentUserCoreData[0].last_name!
                } else {
                    
                }
                
                //setting fields
                self.user_name = self.currentUserCoreData[0].user_name!
            } else {
                
            }
        } catch { () }
    }
    
    func resetCoreData() {
        self.currentUserCoreData[0].setValue(nil, forKey: "about")
        self.currentUserCoreData[0].setValue(nil, forKey: "current_job_employer")
        self.currentUserCoreData[0].setValue(nil, forKey: "current_job_employer_logo")
        self.currentUserCoreData[0].setValue(nil, forKey: "current_job_title")
        self.currentUserCoreData[0].setValue(nil, forKey: "dream_job_employer")
        self.currentUserCoreData[0].setValue(nil, forKey: "dream_job_employer_logo")
        self.currentUserCoreData[0].setValue(nil, forKey: "dream_job_title")
        self.currentUserCoreData[0].setValue(nil, forKey: "education_institution")
        self.currentUserCoreData[0].setValue(nil, forKey: "education_institution_logo")
        self.currentUserCoreData[0].setValue(nil, forKey: "education_level")
        self.currentUserCoreData[0].setValue(nil, forKey: "location_city")
        self.currentUserCoreData[0].setValue(nil, forKey: "location_state")
        self.currentUserCoreData[0].setValue(nil, forKey: "profile_photo_url")
        self.currentUserCoreData[0].setValue(nil, forKey: "photo1")
        self.currentUserCoreData[0].setValue(nil, forKey: "photo2")
        self.currentUserCoreData[0].setValue(nil, forKey: "photo3")
        self.currentUserCoreData[0].setValue(nil, forKey: "photo4")
        self.currentUserCoreData[0].setValue(nil, forKey: "photo5")
        self.currentUserCoreData[0].setValue(nil, forKey: "photo6")
        PersistanceService.saveContext()
    }
    
    //MARK - CREATE AND POPULATE USER
    func handleCreateUser() {
        let email: String = self.emailTextField.text!
        let password: String = self.passwordTextField.text!
        Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
        if error != nil {
            self.continueButton.isEnabled = true
            let alert = UIAlertController(title: "Error", message: (error?.localizedDescription)! + " Perhaps you signed in using Facebook?", preferredStyle: UIAlertController.Style.alert)
            self.present(alert, animated: true, completion: nil)
            
            alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
                switch action.style{
                case .cancel:
                ()
                case .default:
                ()
                case .destructive:
                ()
                    }
                }))
        
            alert.addAction(UIAlertAction(title: "Use Facebook", style: .default, handler: { action in
                switch action.style{
                case .cancel:
                    ()
                case .default:
                    ()
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "ViewController1")
                    self.present(controller, animated: true, completion: nil)
                    
                case .destructive:
                    ()
                }
            }))
            
            } else {
                //user accepted terms in CD
                self.currentUserCoreData[0].accepted_terms = true
                PersistanceService.saveContext()
            
                self.populateUser()
            }
        })
    }
    
    func populateUser() {
        if let uid = Auth.auth().currentUser?.uid {
            resetCoreData()
            let populatedDictionary = ["photo1": "", "photo2": "", "photo3": "", "photo4": "", "photo5": "", "photo6": ""]
            let emptyDictionary = [String: String]()
            let user_thumbnail: String = ""
            let about: String = "" 
            let current_city: String = "Pretoria"
            let current_state: String = "Transvaal"
            let current_job: NSDictionary = emptyDictionary as NSDictionary
            let dream_job: NSDictionary = emptyDictionary as NSDictionary
            let education: NSDictionary = emptyDictionary as NSDictionary
            let social: NSDictionary = emptyDictionary as NSDictionary
            let facebookId: String = ""
            let profile_urls: NSDictionary = populatedDictionary as NSDictionary
            let blocks: NSDictionary = emptyDictionary as NSDictionary

            
            
            let user = User(about: about, birthday: self.birthday, city: current_city, current_job: current_job, state: current_state, dream_job: dream_job, education: education, fb_id: facebookId, first_name: self.first_name, last_name: self.last_name, profile_urls: profile_urls, blocks: blocks, thumbnail: user_thumbnail, social: social)
    
            var ref: DatabaseReference!
            ref = Database.database().reference()
            
            let object = user.toAnyObject()
            
            ref.child("USERS").child(uid).setValue(object) { (err, ref) in
                
                if err != nil {
                    ()
                } else {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = storyboard.instantiateViewController(withIdentifier: "picker")
                    self.present(controller, animated: false, completion: nil)
                }
            }
        }
    }
    
    //MARK: - SIMPLE FUNC
    func calcAge(birthday: String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "MM dd yyyy"
        let birthdayDate = dateFormater.date(from: birthday)
        let calendar: NSCalendar! = NSCalendar(calendarIdentifier: .gregorian)
        let now = Date()
        let calcAge = calendar.components(.year, from: birthdayDate!, to: now, options: [])
        let age = calcAge.year
        return age!
    }
    
    func handleNavigationToWebView(url: String, title: String) {
        if let viewController1 = storyboard?.instantiateViewController(withIdentifier: "VCTOS") as? ViewControllerTermsOfService {
            viewController1.modalPresentationStyle = .overFullScreen
            viewController1.url = url
            viewController1.webViewTitle = title
            self.present(viewController1, animated: true, completion: {
            })
        }
    }
    
    func removeGradientSetBackground() {
        if self.continueButton.layer.sublayers != nil {
            for layer in (continueButton.layer.sublayers)! {
                if layer.name == "gradientLayer" {
                    layer.removeFromSuperlayer()
                }
            }
        }
        
        self.continueButton.backgroundColor = Colors.DisabledButtonGrey
        self.continueButton.isEnabled = false
    }
    
    //MARK: - VALIDATION ON EMAIL AND PASSWORD
    
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    func passwordsMatch(passwordOne: String, passwordTwo: String) -> Bool {
        return passwordOne == passwordTwo
    }
}
