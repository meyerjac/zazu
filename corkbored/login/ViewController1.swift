//
//  testViewController1.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/5/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase
import CoreLocation
import CoreData
import Pastel

class ViewController1: UIViewController, FBSDKLoginButtonDelegate, CLLocationManagerDelegate {
    
    //MARK: - Variables
    var accepted_terms: Bool = false
    var age: Int = 0
    var about: String = ""
    var birthday: String = ""
    var current_city: String = "Pretoria"
    var current_state: String = "Transvaal"
    var dms = [String]()
    var email: String = ""
    var facebook_id: String = ""
    var first_name: String = ""
    var blocked_users = [String]()
    var last_name: String = ""
    var user_name: String = ""
    var events = [String]()
    var profilePic = ""
    var photoUrlString: String! = ""
    var replies = [String]()
    var currentUserCoreData = [Person]()
    var beVerbs = ["yourself", "involved", "respectful", "honest", "happy", "active", "exciting", "nice", "stoked", "serious", "playful", "crazy", "natural", "accountable", "strong", "reasonable", "loyal", "friendly", "cheerful", "helpful", "clean", "true", "present", "courteus", "brave", "thrify", "engaged"]
    var timer: Timer?
    var beVerbsCounter: Int = 0
    var gettingFacebook: Bool = false
    
    var messageProfileUidsWithMessages = [String]()
    var dmObjects = [Dm]()
    var coreDataMessageContainer: [[Message]] = []
    var userMessageArray = [Message]()
    var eventObjects = [CdEvent]()
    
    //MARK: - Outlets
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var loadingSpinner: UIActivityIndicatorView!
    @IBOutlet weak var BeLabel: UILabel!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var TermsOfServiceLabel: UILabel!
    @IBAction func emailButtonClicked(_ sender: UIButton) {
        self.facebookButton.isEnabled = false
        timer?.invalidate()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ViewController2")
        self.present(controller, animated: false, completion: nil)
    }
    
    //MARK: - VDL
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.facebookButton.layer.cornerRadius = 10
        self.facebookButton.clipsToBounds = true
        
        TermsOfServiceLabel.text = "By signing up you agree to our Terms of Service and Privacy Policy"
        if let text = TermsOfServiceLabel.text {
            let underlineAttriString = NSMutableAttributedString(string: text)
            let range1 = (text as NSString).range(of: "Terms of Service")
            underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range1)
            let range2 = (text as NSString).range(of: "Privacy Policy")
            underlineAttriString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range2)
            TermsOfServiceLabel.attributedText = underlineAttriString
            
            let mytapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(termsOfServiceTapped))
            self.TermsOfServiceLabel.addGestureRecognizer(mytapGestureRecognizer)
        }
        
        getCoreData()
        startTimer()
    }
    
    override func viewDidLayoutSubviews() {
        self.emailButton.layer.cornerRadius = 10
        self.emailButton.clipsToBounds = true
    }
    
    override var prefersStatusBarHidden: Bool { return true }
    
    //MARK: - Login/Logout functions
    func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
        self.emailButton.isEnabled = false
        toggleActiviyIndicator()
        return true
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        do {
            try Auth.auth().signOut()
        } catch {
            ()
        }
    }
    
    //MARK: - Navigation items
    @objc func login() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "main")
        self.present(controller, animated: false, completion: nil)
    }
    
    @objc func toPhotoPicker() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "picker")
        self.present(controller, animated: false, completion: nil)
    }
    
    //MARK: - Short Funtions
    func checkIfUserExists(uid: String, completion: @escaping (Bool?) -> ()) {
        let database = Database.database().reference()
        database.child("USERS").child(uid).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            if snapshot.exists() {
                completion(true)
            } else {
                completion(false)
            }
        })
    }
    
    func toggleActiviyIndicator() {
        if loadingSpinner.isHidden {
             loadingSpinner.startAnimating()
             loadingSpinner.isHidden = false
        } else {
            loadingSpinner.isHidden = true
            loadingSpinner.stopAnimating()
        }
    }

    func verifyUrl(urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    @objc func termsOfServiceTapped(recognizer: UITapGestureRecognizer) {
        if let text = TermsOfServiceLabel.text {
            let termsRange = (text as NSString).range(of: "Terms of Service")
            let privacyRange = (text as NSString).range(of: "Privacy Policy")
            
            if recognizer.didTapAttributedTextInLabel(label: TermsOfServiceLabel, inRange: termsRange) {
                //need to show terms in new view or modal view controller that they can swipe down
                handleNavigationToWebView(url: "https://www.zazuapp.com/terms-of-service", title: "Terms of Service")
            } else if recognizer.didTapAttributedTextInLabel(label: TermsOfServiceLabel, inRange: privacyRange) {
                handleNavigationToWebView(url: "https://www.zazuapp.com/privacy-policy", title: "Privacy Policy")
            }
        }
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 0.50, target: self, selector: #selector(eventWith(timer:)), userInfo: [ "foo" : "bar" ], repeats: true)
    }
    
    @objc func eventWith(timer: Timer!) {
        if beVerbsCounter <= self.beVerbs.count - 1 {
            if beVerbsCounter == self.beVerbs.count - 1 {
                self.BeLabel.text = self.beVerbs[beVerbsCounter]
                beVerbsCounter = 0
            } else {
                self.BeLabel.text = self.beVerbs[beVerbsCounter]
                beVerbsCounter += 1
            }
        }
    }

    //MARK - Core Data
    func getCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            self.currentUserCoreData = peep
            if self.currentUserCoreData.count == 0 {
                //no core data, so create first model
                let person = Person(context: PersistanceService.context)
                person.accepted_terms = false
                self.currentUserCoreData.append(person)
                PersistanceService.saveContext()
            }
        } catch {
            ()
        }
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        emailButton.isEnabled = false
        if (error == nil) {
            if result.isCancelled {
                toggleActiviyIndicator()
                emailButton.isEnabled = true
                return
            } else {
                let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                    Auth.auth().signIn(with: credential) { (user, error) in
                        if error != nil {
                            self.showStandardAlert(message: error?.localizedDescription ?? "Something went wrong, try using a different account or login using differnet email")
                        }
                        
                        if let uid = Auth.auth().currentUser?.uid {
                            self.gettingFacebook = true
                            self.checkIfUserExists(uid: uid, completion: { Bool in
                                if Bool == true {
                                    //user exists //handles loading all db data into coredata....user removes app,but doesn't delete app and then logs back in
                                    self.getDms()
                                    self.getEvents()
                                    self.updateCoreDataToMatchDB()
                                } else {
                                    //user doesn't exist
                                    self.fetchFacebookProfile()
                                }
                            })
                        }
                    }
                }
            }
        }
    
    func handleProfilePostSync() {
        
        if ((self.currentUserCoreData[0].first_name != nil) && (self.currentUserCoreData[0].last_name != nil) && (self.currentUserCoreData[0].user_name != nil) && (self.currentUserCoreData[0].birthday != nil) && (self.currentUserCoreData[0].accepted_terms == true) && (self.currentUserCoreData[0].profile_photo_url != nil) && (self.currentUserCoreData[0].photo1 != nil)) {
             login()
        } else {
            
            fetchFacebookProfile()
        }
    }
    
    func fetchFacebookProfile() {
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "about,photos,birthday,email,first_name,gender,id,last_name, picture.width(1080).height(1080)"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error != nil){
                     self.showStandardAlert(message: error?.localizedDescription ?? "Something went wrong, try again")
                    return
                }
                if let dictionary = result as? [String: Any] {
                    if let about = dictionary["about"] as? String {
                        self.about = about
                    }
                    
                    if let birthday = dictionary["birthday"] as? String {
                        self.birthday = birthday.DBBirthdayConvert(birthday)
                        self.age = birthday.calcAgeFromFB(birthday: birthday)
                    }
                    
                    if let email = dictionary["email"] as? String {
                        self.email = email
                    }
                    
                    if let first_name = dictionary["first_name"] as? String {
                        self.first_name = first_name
                    }
                    
                    if let facebookId = dictionary["id"] as? String {
                        self.facebook_id = facebookId
                    }
                    
                    if let last_name = dictionary["last_name"] as? String {
                        self.last_name = last_name
                    }
                    
                    if let picture = dictionary["picture"] as? [String: Any] {
                        if let data = picture["data"] as? [String: Any] {
                            if let url = data["url"] as? String {
                                self.photoUrlString = url
                            }
                        }
                    }
                    self.saveUserInfoToCD(url: self.photoUrlString)
                }
            })
        }
    }
    
    func handleNavigationToWebView(url: String, title: String) {
        if let viewController1 = storyboard?.instantiateViewController(withIdentifier: "VCTOS") as? ViewControllerTermsOfService {
            viewController1.modalPresentationStyle = .overFullScreen
            viewController1.url = url
            viewController1.webViewTitle = title
            self.present(viewController1, animated: true, completion: {
            })
        }
    }
    
    //MARK: - GET THUMBNAIL URL
    func saveUserInfoToCD(url: String) {
        if (Auth.auth().currentUser?.uid) != nil {
            //put everything into COREDATA
            let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
            do {
                let peep = try PersistanceService.context.fetch(fetchRequest)
                self.currentUserCoreData = peep
                
                if self.currentUserCoreData.count != 0 {
                    let peep = try PersistanceService.context.fetch(fetchRequest)
                    self.currentUserCoreData = peep
                    
                    if self.currentUserCoreData.count != 0 {
                        if let url = URL(string: url) {
                            let data = try? Data(contentsOf: url)
                            
                            self.currentUserCoreData[0].setValue(true, forKey: "accepted_terms")
                            self.currentUserCoreData[0].setValue(data, forKey: "photo1")
                            self.currentUserCoreData[0].setValue(self.birthday, forKey: "birthday")
                            self.currentUserCoreData[0].setValue(self.first_name, forKey: "first_name")
                            self.currentUserCoreData[0].setValue(self.last_name, forKey: "last_name")
                            self.currentUserCoreData[0].setValue(self.first_name.generateUserNameFromFirstAndLast(first: self.first_name, last: self.last_name), forKey: "user_name")
                            PersistanceService.saveContext()
                            self.createUser()
                        }
                    }
                }
            } catch {
                ()
            }
        }
    }


    func createUser() {
         let emptyDictionary = [String: String]()
         let current_job: NSDictionary = emptyDictionary as NSDictionary
         let dream_job: NSDictionary = emptyDictionary as NSDictionary
         let education: NSDictionary = emptyDictionary as NSDictionary
         let social: NSDictionary = emptyDictionary as NSDictionary
         let blocks: NSDictionary = emptyDictionary as NSDictionary
       
         let profile_urls = ["photo1": "", "photo2": "", "photo3": "", "photo4": "", "photo5": "", "photo6": ""] as NSDictionary
         let empty_string = ""
        
         let user = User(about: self.about, birthday: self.birthday, city: self.current_city, current_job: current_job, state: self.current_state, dream_job: dream_job, education: education, fb_id: self.facebook_id, first_name: self.first_name, last_name: self.last_name, profile_urls: profile_urls, blocks: blocks, thumbnail: empty_string, social: social)
        
        var ref: DatabaseReference!
        ref = Database.database().reference()
        
        let object = user.toAnyObject()
        
        let userInfo = Auth.auth().currentUser
        
        if let uid = userInfo?.uid {
            ref.child("USERS").child(uid).setValue(object) { (err, ref) in
            if err != nil {
                ()
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "picker")
                self.present(controller, animated: false, completion: nil)
                }
            }
        }
    }
    
    func getEvents() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CdEvent")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try PersistanceService.context.execute(deleteRequest)
        } catch  {
            ()
        }
        if let uid = Auth.auth().currentUser?.uid {
            //instead of accessing the city events directly i used one query to get everything
            let ref = Database.database().reference().child("USERS_EVENTS/\(uid)")
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                if ( snapshot.value is NSNull ) {
                    return
                } else {
                    for country in snapshot.children { //loop through countries
                        if let countrySnapshot = country as? DataSnapshot {
                            let countryName = countrySnapshot.key //save name
                            for state in countrySnapshot.children { //loop through states
                                if let stateSnapshot = state as? DataSnapshot {
                                    let stateName = stateSnapshot.key //save name
                                    for city in stateSnapshot.children { //loop through cities
                                        if let citySnapshot = city as? DataSnapshot {
                                            let cityName = citySnapshot.key //save name
                                            for event in citySnapshot.children { //loop through events
                                                if let eventSnapshot = event as? DataSnapshot {
                                                    if let eventValue = eventSnapshot.value as? NSDictionary { //get event as dictionary
                                                        let cdEventObject = CdEvent(context: PersistanceService.context) //create event object
                                                        //set event values
                                                        if let stringUrl = eventValue["owner_url"] as? String {
                                                            cdEventObject.cd_event_photo_url = URL(string: stringUrl)
                                                            
                                                            if let ownerUid = eventValue["owner_uid"] as? String {
                                                                cdEventObject.cd_event_captain_uid = ownerUid
                                                                
                                                                if let event_title = eventValue["title"] as? String {
                                                                     cdEventObject.cd_event_title = event_title
                                                                    
                                                                    if let eventUid = eventValue["uid"] as? String {
                                                                        cdEventObject.cd_event_uid = eventUid
                                                                        
                                                                        if let start_time = eventValue["start_time"] as? String {
                                                                            cdEventObject.cd_event_start_time = start_time
                                                                            
                                                                            if let end_time = eventValue["end_time"] as? String {
                                                                                cdEventObject.cd_event_end_time = end_time
                                                                                
                                                                                //already safe
                                                                                cdEventObject.cd_event_accepted = eventValue["accepted"] as? Bool ?? false
                                                                                cdEventObject.cd_event_city = cityName
                                                                                cdEventObject.cd_event_state = stateName
                                                                                cdEventObject.cd_event_country = countryName
                                                                                
                                                                                self.eventObjects.append(cdEventObject) //add event to array
                                                                                PersistanceService.saveContext()
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            })
        }
    }
    
    func getDms() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Dm")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try PersistanceService.context.execute(deleteRequest)
        } catch {
            showStandardAlert(message: "Unable to download previous Direct Message's")
        }
        
        if let uid = Auth.auth().currentUser?.uid {
            let ref = Database.database().reference().child("DIRECT_MESSAGING/\(uid)")
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                if ( snapshot.value is NSNull ) {
                    return
                } else {
                    for child in snapshot.children {
                        //let uid = child as! DataSnapshot //get child as datasnapshot
                        self.userMessageArray.removeAll() //empty message array
                        guard let messageHome = child as? DataSnapshot else { return }//get child as datasnapshot
                        for child2 in messageHome.children { //loop through children
                            guard let messages = child2 as? DataSnapshot else { return }//get each child as snapshot
                            for child3 in messages.children { //loop through grandchildren (only exist under messages node)
                                guard let messageSnapshot = child3 as? DataSnapshot else { return } //get each message as snapshot
                                if (messageSnapshot.value as? [AnyHashable: AnyObject]) != nil {
                                    let message = Message(snapshot: messageSnapshot) //create message object
                                    self.userMessageArray.append(message) //append to array
                                }
                            }
                        }
                        
                        if !self.userMessageArray.isEmpty {
                            //if there were messages in the dm chat then
                            self.coreDataMessageContainer.append(self.userMessageArray) //append message array to coreData container
                            self.messageProfileUidsWithMessages.insert(messageHome.key, at: self.messageProfileUidsWithMessages.startIndex) //add uid to messageProfileUid array
                        }
                    }
                    self.createDmCoreDataObjects()
                }
            })
        }
    }
    
    func createDmCoreDataObjects() {
        if self.coreDataMessageContainer.count > 0 { //only create if there were dm chats with messages in them
            for n in 0...self.messageProfileUidsWithMessages.count - 1 { //loop through chats
                let ref = Database.database().reference().child("USERS").child(self.messageProfileUidsWithMessages[n]) //create reference to chat user
                ref.observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    if snapshot.exists() {
                        let user = User(snapshot: snapshot)
                        let primary_url = URL(string: user.thumbnail)
                        DispatchQueue.global().async {
                            if let urlurl = primary_url  {
                                let dmObject = Dm(context: PersistanceService.context)
                                dmObject.dm_profile_photo_url = urlurl
                                
                                let message_owner_user_name = user.first_name.generateUserNameFromFirstAndLast(first: user.first_name, last: user.last_name)
                                
                                dmObject.dm_any_messages_unread =  false
                                dmObject.dm_last_message = self.coreDataMessageContainer[n][self.coreDataMessageContainer[n].count - 1].text
                                dmObject.dm_last_message_time_stamp = Double(self.coreDataMessageContainer[n][self.coreDataMessageContainer[n].count - 1].interval_reference)
                                dmObject.dm_user_name = message_owner_user_name
                                dmObject.dm_owner_uid = self.messageProfileUidsWithMessages[n]
                                dmObject.dm_their_uid = self.messageProfileUidsWithMessages[n]
                                print(dmObject, "DM OBJECT")
                                self.dmObjects.append(dmObject)
                            }
                        }
                    }
                })
            }
            PersistanceService.saveContext()
        }
    }
    
    func updateCoreDataToMatchDB() {
        if let uid = Auth.auth().currentUser?.uid {
            let ref = Database.database().reference().child("USERS").child(uid)
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                let user = User(snapshot: snapshot)
                
                //setting all of the current USERS db data into coreData
                self.currentUserCoreData[0].about = user.about
                self.currentUserCoreData[0].accepted_terms = true
                self.currentUserCoreData[0].birthday = user.birthday
                self.currentUserCoreData[0].location_city = user.city
                self.currentUserCoreData[0].location_state = user.state
                self.currentUserCoreData[0].first_name = user.first_name
                self.currentUserCoreData[0].last_name = user.last_name
                self.currentUserCoreData[0].user_name = user.first_name.generateUserNameFromFirstAndLast(first: user.first_name, last: user.last_name)
                self.currentUserCoreData[0].profile_photo_url = URL(string: user.thumbnail)
                
                if let snapshotDict = snapshot.value as? [AnyHashable: AnyObject] {

                    if snapshotDict["current_job"] as? [String: String] != nil {
                        
                        if let dict = snapshotDict["current_job"] as? [String: String] {
                            self.currentUserCoreData[0].current_job_title = dict["title"]
                            self.currentUserCoreData[0].current_job_employer = dict["subtitle"]
                            if let stringUrl = dict["logo"] {
                                if let url = URL(string: stringUrl) {
                                    if let data = try? Data(contentsOf: url) {
                                        let image = UIImage(data: data)
                                        guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                        self.currentUserCoreData[0].current_job_employer_logo = imageData
                                    }
                                }
                            }
                        }
                    }
                    
                    if snapshotDict["dream_job"] as? [String: String] != nil {
                        if let dict = snapshotDict["dream_job"] as? [String: String] {
                            self.currentUserCoreData[0].dream_job_title = dict["title"]
                            self.currentUserCoreData[0].dream_job_employer = dict["subtitle"]
                            
                            if let stringUrl = dict["logo"] {
                                if let url = URL(string: stringUrl) {
                                    if let data = try? Data(contentsOf: url) {
                                        let image = UIImage(data: data)
                                        guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                        self.currentUserCoreData[0].dream_job_employer_logo = imageData
                                    }
                                }
                            }
                        }
                    }
                    
                    if snapshotDict["education"] as? [String: String] != nil {
                        if let dict = snapshotDict["education"] as? [String: String] {
                            self.currentUserCoreData[0].education_level = dict["title"]
                            self.currentUserCoreData[0].education_institution = dict["subtitle"]
                            
                            if let stringUrl = dict["logo"] {
                                if let url = URL(string: stringUrl) {
                                    if let data = try? Data(contentsOf: url) {
                                        let image = UIImage(data: data)
                                        guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                        self.currentUserCoreData[0].education_institution_logo = imageData
                                    }
                                }
                            }
                        }
                    }
                    
                    if snapshotDict["social"] as? [String: String] != nil {
                        if let dict = snapshotDict["social"] as? [String: String] {
                            self.currentUserCoreData[0].instagram_handle = dict["instagram"]
                            self.currentUserCoreData[0].snapchat_handle = dict["snapchat"]
                        }
                    }

                if snapshotDict["profile_urls"] as? [String: String] != nil {
                    if let dict = snapshotDict["profile_urls"] as? [String: String] {
                        for i in 0...5 {
                            switch(i)  {
                            case 0:
                                if let stringUrl = dict["photo1"] {
                                    if let url = URL(string: stringUrl) {
                                        if let data = try? Data(contentsOf: url) {
                                            let image = UIImage(data: data)
                                            guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                            self.currentUserCoreData[0].photo1 = imageData
                                        }
                                    }
                                }
                                break;
                            case 1:
                                if let stringUrl = dict["photo2"] {
                                    if let url = URL(string: stringUrl) {
                                        if let data = try? Data(contentsOf: url) {
                                            let image = UIImage(data: data)
                                            guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                            self.currentUserCoreData[0].photo2 = imageData
                                        }
                                    }
                                }
                                break;
                            case 2:
                                if let stringUrl = dict["photo3"] {
                                    if let url = URL(string: stringUrl) {
                                        if let data = try? Data(contentsOf: url) {
                                            let image = UIImage(data: data)
                                            guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                            self.currentUserCoreData[0].photo3 = imageData
                                        }
                                    }
                                }
                                break;
                            case 3:
                                if let stringUrl = dict["photo4"] {
                                    if let url = URL(string: stringUrl) {
                                        if let data = try? Data(contentsOf: url) {
                                            let image = UIImage(data: data)
                                            guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                            self.currentUserCoreData[0].photo4 = imageData
                                        }
                                    }
                                }
                                break;
                            case 4:
                                if let stringUrl = dict["photo5"] {
                                    if let url = URL(string: stringUrl) {
                                        if let data = try? Data(contentsOf: url) {
                                            let image = UIImage(data: data)
                                            guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                            self.currentUserCoreData[0].photo5 = imageData
                                        }
                                    }
                                }
                                break;
                            case 5:
                                if let stringUrl = dict["photo6"] {
                                    if let url = URL(string: stringUrl) {
                                        if let data = try? Data(contentsOf: url) {
                                            let image = UIImage(data: data)
                                            guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                            self.currentUserCoreData[0].photo6 = imageData
                                        }
                                    }
                                }
                                break;
                            default:
                                ()
                            }
                        }
                    }
                }
                PersistanceService.saveContext()
                self.handleProfilePostSync()
            }
            })
        }
    }
}

//MARK: - Extensions
extension UITapGestureRecognizer {

    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        guard let attributedText = label.attributedText else { return false }
        let textStorage = NSTextStorage(attributedString: attributedText)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let locationOfTouchInTextContainer = CGPoint(x: (locationOfTouchInLabel.x - textContainerOffset.x), y: (locationOfTouchInLabel.y - textContainerOffset.y))
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
}
