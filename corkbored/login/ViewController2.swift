//
//  ViewController2.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/5/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import ScrollableSegmentedControl
import Firebase
import CoreData

class ViewController2: UIViewController, UITextFieldDelegate {
     var selectedSegment:Int!
    
    //core data
    var currentUserCoreData = [Person]()
    var allMessageProfileUids = [String]()
    var messageProfileUidsWithMessages = [String]()
    var dmObjects = [Dm]()
    var coreDataMessageContainer: [[Message]] = []
    var userMessageArray = [Message]()
    var coreDataDmObjectProfileUrl = ""
    var eventObjects = [CdEvent]()
    
    // MARK: - all Outlets
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var firstNameView: UIView!
    @IBOutlet weak var lastNameView: UIView!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var ContinueButton: UIButton!
    @IBAction func textFieldDidBeginEditing(_ sender: UITextField) {
        guard let text1 = self.firstNameField.text, !text1.isEmpty else {
            removeGradientSetBackground()
            return
        }
        guard let text2 = self.lastNameField.text, !text2.isEmpty else {
            removeGradientSetBackground()
            return
        }
        //do something if it's not empty
        self.ContinueButton.isEnabled = true
        self.ContinueButton.setGradientBackground(colorOne: Colors.grad1, colorTwo: Colors.grad2)
    }
    @IBAction func backButtonClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "ViewController1")
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func ContinueButtonClicked(_ sender: UIButton) {
        ContinueButton.isEnabled = false
        forgotPasswordButton.isEnabled = false
        
        switch(self.selectedSegment) {
        case 0:
            //login
            handleLogin()
            break;
        case 1:
            //sign up
            fetchAndSaveData()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ViewController3")
            self.present(controller, animated: false, completion: nil)
            break;
        default:
            ()
        }
    }
    
    @IBAction func forgotPasswordButtonClicked(_ sender: UIButton) {
        ContinueButton.isEnabled = false
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "forgotPasswordVC")
        self.present(controller, animated: false, completion: nil)
    }
    
    // MARK: - VDL, CD, and early functions
    override func viewDidLoad() {
        super.viewDidLoad()
        getCoreData()
        setUpViews()
    }
    
    override var prefersStatusBarHidden: Bool { return true }
    
    override func viewDidLayoutSubviews() {
        self.ContinueButton.layer.cornerRadius = self.ContinueButton.frame.size.height/2
        self.ContinueButton.clipsToBounds = true
            
        guard let layers = ContinueButton.layer.sublayers else { return }
        for layer in layers {
            if layer.name == "gradientLayer" {
                layer.frame = self.ContinueButton.bounds
            }
        }
    }
    
    
    func getCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            self.currentUserCoreData = peep
            } catch {
            ()
        }
    }
    
    func setUpViews() {
        firstNameView.clipsToBounds = true
        firstNameView.layer.cornerRadius = 20
        lastNameView.clipsToBounds = true
        lastNameView.layer.cornerRadius = 20
        
        let segmentedControl = ScrollableSegmentedControl(frame: CGRect.zero)
        segmentedControl.segmentStyle = .textOnly
        segmentedControl.insertSegment(withTitle: "Log In", image: nil, at: 0)
        segmentedControl.insertSegment(withTitle: "Sign Up", image: nil, at: 1)
        segmentedControl.underlineSelected = true
        segmentedControl.tintColor = UIColor.white
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.addTarget(self, action: #selector(segmentSelected(sender:)), for: .valueChanged)
        
        // change some colors
        segmentedControl.segmentContentColor = UIColor.white;
        segmentedControl.selectedSegmentContentColor = UIColor.white;
        segmentedControl.backgroundColor = UIColor(red:0.51, green:0.79, blue:1.00, alpha:1.0)
        segmentedControl.layer.cornerRadius = segmentedControl.frame.height / 2
        segmentedControl.clipsToBounds = true
        
        let normalTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18), NSAttributedString.Key.foregroundColor: UIColor.white]
        let selectTextAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18), NSAttributedString.Key.foregroundColor: UIColor.white]
        segmentedControl.setTitleTextAttributes(normalTextAttributes, for: .normal)
        segmentedControl.setTitleTextAttributes(selectTextAttributes, for: .selected)
        self.view.addSubview(segmentedControl)
        
        if self.currentUserCoreData.count > 0 {
            if currentUserCoreData[0].accepted_terms == true {
                segmentedControl.selectedSegmentIndex = 0
            } else {
                segmentedControl.selectedSegmentIndex = 1
                firstNameField.text = self.currentUserCoreData[0].first_name
                lastNameField.text = self.currentUserCoreData[0].last_name
                
                if (firstNameField.text != "") && (lastNameField.text != "") {
                    self.ContinueButton.isEnabled = true
                    self.ContinueButton.setGradientBackground(colorOne: Colors.grad1, colorTwo: Colors.grad2)
            
                } else {
                    
                }
            }
        } else {
            //user is new and have no data yet
             segmentedControl.selectedSegmentIndex = 1
        }
        
        //set up segmented constraints
        view.addConstraint(NSLayoutConstraint(item: segmentedControl, attribute: .bottom, relatedBy: .equal, toItem: self.firstNameView, attribute: .top, multiplier: 1, constant: -36))
        view.addConstraint(NSLayoutConstraint(item: segmentedControl, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant:0))
        view.addConstraint(NSLayoutConstraint(item: segmentedControl, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant:30))
        view.addConstraint(NSLayoutConstraint(item: segmentedControl, attribute: .width, relatedBy: .equal, toItem: self.firstNameView, attribute: .width, multiplier: 1.0, constant: 0))
        
        //disable autocomplete bar
        lastNameField.textContentType = UITextContentType(rawValue: "")
        firstNameField.textContentType = UITextContentType(rawValue: "")
    }
    
      // MARK: - saving user inputs and validating
    func fetchAndSaveData() {
        var formattedFirstName: String = ""
        var formattedLastName: String = ""
        
        if let onlyFirstName = firstNameField.text {
            formattedFirstName = onlyFirstName.firstUppercased
        }
        if let onlyLastName = lastNameField.text {
            formattedLastName = onlyLastName.firstUppercased
        }
        
        if currentUserCoreData.count > 0 {
            self.currentUserCoreData[0].setValue(formattedFirstName, forKey: "first_name")
            self.currentUserCoreData[0].setValue(formattedLastName, forKey: "last_name")
            self.currentUserCoreData[0].setValue(generateUsername(first: formattedFirstName, last: formattedLastName), forKey: "user_name")
            PersistanceService.saveContext()
        } else {
            if let first = firstNameField.text {
                if let onlyFirstName = first.components(separatedBy: " ").first {
                    formattedFirstName = onlyFirstName.firstUppercased
                }
            }
            
            if let last = lastNameField.text {
                if let onlyLastName = last.components(separatedBy: " ").first {
                    formattedLastName = onlyLastName.firstUppercased
                }
            }
            
            let person = Person(context: PersistanceService.context)
            person.first_name = formattedFirstName
            person.last_name = formattedLastName
            let generatedUsername = generateUsername(first: formattedFirstName, last: formattedLastName)
            person.user_name = generatedUsername
            PersistanceService.saveContext()
        }
    }
    
    func handleLogin() {
        guard let email = firstNameField.text else { return }
        guard let password = lastNameField.text else { return }
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error != nil {
                if let err = error?.localizedDescription {
                    //need to have a more beautiful alert on screen
                    let alert = UIAlertController(title: "Error", message: err, preferredStyle: UIAlertController.Style.alert)
                    self.present(alert, animated: true, completion: nil)
                    
                    alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
                        switch action.style{
                        case .cancel:
                            ()
                        case .default:
                            ()
                            self.ContinueButton.isEnabled = true
                            self.forgotPasswordButton.isEnabled = true
                        case .destructive:
                            ()
                        }
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Use Facebook", style: .default, handler: { action in
                        switch action.style{
                        case .cancel:
                            ()
                        case .default:
                            ()
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let controller = storyboard.instantiateViewController(withIdentifier: "ViewController1")
                            self.present(controller, animated: true, completion: nil)
                            
                        case .destructive:
                            ()
                        }
                    }))
                    
                    
                }
            } else {
                //get data
                if let uid = Auth.auth().currentUser?.uid {
                    self.checkIfUserExists(uid: uid, completion: { Bool in
                        if Bool == true {
                            //user exists the handles loading all db data into coredata when a user is loggin in
                            self.updateCoreDataToMatchDB()
                            print("LOGIN USER EXISTS")
                        } else {
                            //user doesn't exist
                            ()
                        }
                    })
                }
            }
        }
    }
    
    func checkIfUserExists(uid: String, completion: @escaping (Bool?) -> ()) {
        let database = Database.database().reference()
        database.child("USERS").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists() {
                completion(true)
            } else {
                completion(false)
            }
        })
    }
    
    func getEvents() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CdEvent")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try PersistanceService.context.execute(deleteRequest)
        } catch { () }
        
        if let uid = Auth.auth().currentUser?.uid {
            //instead of accessing the city events directly i used one query to get everything
            let ref = Database.database().reference().child("USERS_EVENTS/\(uid)")
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                if ( snapshot.value is NSNull ) {
                    ()
                    return
                } else {
                    for country in snapshot.children { //loop through countries
                        guard let countrySnapshot = country as? DataSnapshot else { return }
                        let countryName = countrySnapshot.key //save name
                        for state in countrySnapshot.children { //loop through states
                            guard let stateSnapshot = state as? DataSnapshot else { return }
                            let stateName = stateSnapshot.key //save name
                            for city in stateSnapshot.children { //loop through cities
                                guard let citySnapshot = city as? DataSnapshot else { return }
                                let cityName = citySnapshot.key //save name

                                for event in citySnapshot.children { //loop through events
                                    guard let eventSnapshot = event as? DataSnapshot else { return }
                                    if let eventValue = eventSnapshot.value as? NSDictionary { //get event as dictionary
                                        let cdEventObject = CdEvent(context: PersistanceService.context)
                                        
                                        //create event CD object
                                        if let stringUrl = eventValue["owner_url"] as? String {
                                            cdEventObject.cd_event_photo_url = URL(string: stringUrl)
                                            
                                            if let ownerUid = eventValue["owner_uid"] as? String {
                                                cdEventObject.cd_event_captain_uid = ownerUid
                                                
                                                if let event_title = eventValue["title"] as? String {
                                                    cdEventObject.cd_event_title = event_title
                                                    
                                                    if let eventUid = eventValue["uid"] as? String {
                                                        cdEventObject.cd_event_uid = eventUid
                                                        
                                                        if let start_time = eventValue["start_time"] as? String {
                                                            cdEventObject.cd_event_start_time = start_time
                                                            
                                                            if let end_time = eventValue["end_time"] as? String {
                                                                cdEventObject.cd_event_end_time = end_time
                                                                
                                                                //already safe
                                                                cdEventObject.cd_event_accepted = eventValue["accepted"] as? Bool ?? false
                                                                cdEventObject.cd_event_city = cityName
                                                                cdEventObject.cd_event_state = stateName
                                                                cdEventObject.cd_event_country = countryName
                                                                
                                                                self.eventObjects.append(cdEventObject) //add event to array
                                                                PersistanceService.saveContext()
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            })
        }
    }
    
    func getDms() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Dm")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try PersistanceService.context.execute(deleteRequest)
        } catch {
            showStandardAlert(message: "Unable to download previous Direct Message's")
        }
        
        if let uid = Auth.auth().currentUser?.uid {
            let ref = Database.database().reference().child("DIRECT_MESSAGING/\(uid)")
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                if ( snapshot.value is NSNull ) {
                    return
                } else {
                    for child in snapshot.children {
                        //let uid = child as! DataSnapshot //get child as datasnapshot
                        self.userMessageArray.removeAll() //empty message array
                        guard let messageHome = child as? DataSnapshot else { return }//get child as datasnapshot
                        for child2 in messageHome.children { //loop through children
                            guard let messages = child2 as? DataSnapshot else { return } //get each child as snapshot
                            for child3 in messages.children { //loop through grandchildren (only exist under messages node)
                                let messageSnapshot = child3 as! DataSnapshot //get each message as snapshot
                                if (messageSnapshot.value as? [AnyHashable: AnyObject]) != nil {
                                    let message = Message(snapshot: messageSnapshot) //create message object
                                    self.userMessageArray.append(message) //append to array
                                }
                            }
                        }
                        
                        if self.userMessageArray.isEmpty {
                            //dont add, problem with snapshot parsing
                        } else { //if there were messages in the dm chat then
                            self.coreDataMessageContainer.append(self.userMessageArray) //append message array to coreData container
                            self.messageProfileUidsWithMessages.insert(messageHome.key, at: self.messageProfileUidsWithMessages.startIndex) //add uid to messageProfileUid array
                        }
                    }
                    self.createDmCoreDataObjects()
                }
            })
        }
    }
    
    func createDmCoreDataObjects() {
        if self.coreDataMessageContainer.count > 0 { //only create if there were dm chats with messages in them
            for n in 0...self.messageProfileUidsWithMessages.count - 1 { //loop through chats
                let ref = Database.database().reference().child("USERS").child(self.messageProfileUidsWithMessages[n]) //create reference to chat user
                ref.observeSingleEvent(of: .value, with: { (snapshot) in
                
                    if snapshot.exists() {
                        let user = User(snapshot: snapshot)
                        let primary_url = URL(string: user.thumbnail)
                        DispatchQueue.global().async {
                            if let urlurl = primary_url  {
                                let dmObject = Dm(context: PersistanceService.context)
                                dmObject.dm_profile_photo_url = urlurl
                                
                                let message_owner_user_name = user.first_name.generateUserNameFromFirstAndLast(first: user.first_name, last: user.last_name)
                                
                                dmObject.dm_any_messages_unread =  false
                                dmObject.dm_last_message = self.coreDataMessageContainer[n][self.coreDataMessageContainer[n].count - 1].text
                                dmObject.dm_last_message_time_stamp = Double(self.coreDataMessageContainer[n][self.coreDataMessageContainer[n].count - 1].interval_reference)
                                dmObject.dm_user_name = message_owner_user_name
                                dmObject.dm_owner_uid = self.messageProfileUidsWithMessages[n]
                                dmObject.dm_their_uid = self.messageProfileUidsWithMessages[n]
                                
                                self.dmObjects.append(dmObject)
                            }
                        }
                    }
                })
            }
            PersistanceService.saveContext()
        }
    }
    
    // TODO: make more efficient...like way more
    //I can make this more efficient by cutting out some of the unused stuff like accepted terms
    //though to be honest it doesn't look like the login takes all that long so i don't think it's that big of a deal
    func updateCoreDataToMatchDB() {
        getDms()
        getEvents()
        if let uid = Auth.auth().currentUser?.uid {
            let ref = Database.database().reference().child("USERS").child(uid)
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                let user = User(snapshot: snapshot)
                guard let snapshotDict = snapshot.value as? [AnyHashable: AnyObject] else { return }
                
                //setting all of the current users db data into coreData
                self.currentUserCoreData[0].about = user.about
                self.currentUserCoreData[0].birthday = user.birthday
                self.currentUserCoreData[0].location_city = user.city
                self.currentUserCoreData[0].location_state = user.state
                self.currentUserCoreData[0].first_name = user.first_name
                self.currentUserCoreData[0].last_name = user.last_name
                self.currentUserCoreData[0].setValue(self.generateUsername(first: user.first_name, last: user.last_name), forKey: "user_name")
                self.currentUserCoreData[0].accepted_terms = true
                
                let thumbnail = user.thumbnail
                    if let thumbnailUrl = URL(string: thumbnail) {
                         self.currentUserCoreData[0].profile_photo_url = thumbnailUrl
                    }
                
                
                if snapshotDict["current_job"] as? [String: String] != nil {
                    if let dict = snapshotDict["current_job"] as? [String: String] {
                        self.currentUserCoreData[0].current_job_title = dict["title"]
                        self.currentUserCoreData[0].current_job_employer = dict["subtitle"]
                        let image = UIImage(data: try! Data(contentsOf: URL(string: dict["logo"]!)!))
                        let imageData = image!.jpegData(compressionQuality: 0.1)
                        self.currentUserCoreData[0].current_job_employer_logo = imageData
                    }
                } else {
                    self.currentUserCoreData[0].current_job_title = nil
                    self.currentUserCoreData[0].current_job_employer = nil
                    self.currentUserCoreData[0].current_job_employer_logo = nil
                }
                
                if snapshotDict["dream_job"] as? [String: String] != nil {
                    if let dict = snapshotDict["dream_job"] as? [String: String] {
                        self.currentUserCoreData[0].dream_job_title = dict["title"]
                        self.currentUserCoreData[0].dream_job_employer = dict["subtitle"]
                        let image = UIImage(data: try! Data(contentsOf: URL(string: dict["logo"]!)!))
                        let imageData = image!.jpegData(compressionQuality: 0.1)
                        self.currentUserCoreData[0].current_job_employer_logo = imageData
                    }
                } else {
                    self.currentUserCoreData[0].dream_job_title = nil
                    self.currentUserCoreData[0].dream_job_employer = nil
                    self.currentUserCoreData[0].dream_job_employer_logo = nil
                }
                
                if snapshotDict["education"] as? [String: String] != nil {
                    if let dict = snapshotDict["education"] as? [String: String] {
                        self.currentUserCoreData[0].education_level = dict["title"]
                        self.currentUserCoreData[0].education_institution = dict["subtitle"]
                        let image = UIImage(data: try! Data(contentsOf: URL(string: dict["logo"]!)!))
                        let imageData = image!.jpegData(compressionQuality: 0.1)
                        self.currentUserCoreData[0].education_institution_logo = imageData
                    }
                } else {
                    self.currentUserCoreData[0].education_level = nil
                    self.currentUserCoreData[0].education_institution = nil
                    self.currentUserCoreData[0].education_institution_logo = nil
                }
                
                if snapshotDict["social"] as? [String: String] != nil {
                    if let dict = snapshotDict["social"] as? [String: String] {
                        self.currentUserCoreData[0].instagram_handle = dict["instagram"]
                        self.currentUserCoreData[0].snapchat_handle = dict["snapchat"]
                    }
                }
                
                PersistanceService.saveContext()
                
                //problem: profile photos not loading
                //solution: rename to "profile_urls"
                if snapshotDict["profile_urls"] as? [String: String] != nil {
                    if let dict = snapshotDict["profile_urls"] as? [String: String] {
                        for i in 0...5 {
                            switch(i)  {
                            case 0:
                                if let stringUrl = dict["photo1"] {
                                    if let url = URL(string: stringUrl) {
                                        if let data = try? Data(contentsOf: url) {
                                            let image = UIImage(data: data)
                                            guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                            self.currentUserCoreData[0].photo1 = imageData
                                        }
                                    }
                                }
                                break;
                            case 1:
                                if let stringUrl = dict["photo2"] {
                                    if let url = URL(string: stringUrl) {
                                        if let data = try? Data(contentsOf: url) {
                                            let image = UIImage(data: data)
                                            guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                            self.currentUserCoreData[0].photo2 = imageData
                                        }
                                    }
                                }
                                break;
                            case 2:
                                if let stringUrl = dict["photo3"] {
                                    if let url = URL(string: stringUrl) {
                                        if let data = try? Data(contentsOf: url) {
                                            let image = UIImage(data: data)
                                            guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                            self.currentUserCoreData[0].photo3 = imageData
                                        }
                                    }
                                }
                                break;
                            case 3:
                                if let stringUrl = dict["photo4"] {
                                    if let url = URL(string: stringUrl) {
                                        if let data = try? Data(contentsOf: url) {
                                            let image = UIImage(data: data)
                                            guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                            self.currentUserCoreData[0].photo4 = imageData
                                        }
                                    }
                                }
                                break;
                            case 4:
                                if let stringUrl = dict["photo5"] {
                                    if let url = URL(string: stringUrl) {
                                        if let data = try? Data(contentsOf: url) {
                                            let image = UIImage(data: data)
                                            guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                            self.currentUserCoreData[0].photo5 = imageData
                                        }
                                    }
                                }
                                break;
                            case 5:
                                if let stringUrl = dict["photo6"] {
                                    if let url = URL(string: stringUrl) {
                                        if let data = try? Data(contentsOf: url) {
                                            let image = UIImage(data: data)
                                            guard let imageData = image?.jpegData(compressionQuality: 0.1) else { return }
                                            self.currentUserCoreData[0].photo6 = imageData
                                        }
                                    }
                                }
                                break;
                            default:
                                ()
                            }
                        }
                    }
                }
                PersistanceService.saveContext()
                
                //problem: loading into picker instead of main
                //explaination: user_name and accepted_terms are not set in updatecoredatatomatchdb
                //solution: set user_name (set above when first and last name are set)
                
                self.handleProfilePostSync()
            })
        }
    }
    
    //is this needed? why would it not be a valid url since we are generating them ourselves
    func verifyIsValidUrl(urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL) //canopenurl just checks if an app can handle the url. there are no apps that can handle download links so it returns false even though the links are valid. Since we are generating profile picture urls ourselves using firebase this check really shouldn't be needed.
            }
        }
        return false
    }
    
    func handleProfilePostSync() {
        print(self.currentUserCoreData[0], "USER")
        if ((self.currentUserCoreData[0].first_name != nil) && (self.currentUserCoreData[0].last_name != nil) && (self.currentUserCoreData[0].user_name != nil) && (self.currentUserCoreData[0].birthday != nil) && (self.currentUserCoreData[0].accepted_terms == true) && (self.currentUserCoreData[0].profile_photo_url != nil) && (self.currentUserCoreData[0].photo1 != nil)) {
            login()
        } else {
           picker()
        }
    }
    
    func login() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "main")
        self.present(controller, animated: false, completion: nil)
    }
    
    func picker() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "picker")
        self.present(controller, animated: false, completion: nil)
    }
    
    @objc func segmentSelected(sender:ScrollableSegmentedControl) {
            removeGradientSetBackground()
                switch(sender.selectedSegmentIndex)  {
                case 0:
                    self.selectedSegment = 0
                    self.firstNameField.text = ""
                    self.firstNameField.placeholder = "Email"
                    self.firstNameField.keyboardType = UIKeyboardType.emailAddress
                    
                    //setting second text field to correct inputs, keyboards, placeholders ect
                    self.lastNameField.text = ""
                    self.lastNameField.placeholder = "Password"
                    self.lastNameField.isSecureTextEntry = true
                    self.forgotPasswordButton.isHidden = false
                    self.ContinueButton.setTitle("Log In",for: .normal)
                    self.ContinueButton.backgroundColor = Colors.DisabledButtonGrey
                    self.ContinueButton.isEnabled = false
                    break;
                case 1:
                    self.selectedSegment = 1
                    self.firstNameField.text = ""
                    self.firstNameField.keyboardType = UIKeyboardType.default
                    self.firstNameField.placeholder = "First Name"
                    
                    //setting second text field to correct inputs, keyboards, placeholders ect
                    self.lastNameField.text = ""
                    self.lastNameField.keyboardType = UIKeyboardType.default
                    self.lastNameField.placeholder = "Last Name"
                    self.lastNameField.isSecureTextEntry = false
                    self.forgotPasswordButton.isHidden = true
                    
                    //Continue button
                    self.ContinueButton.setTitle("Continue",for: .normal)

                    break;
                default:
                    ()
        }
    }
    
    func removeGradientSetBackground() {
        if self.ContinueButton.layer.sublayers != nil {
            for layer in (ContinueButton.layer.sublayers)! {
                if layer.name == "gradientLayer" {
                    layer.removeFromSuperlayer()
                }
            }
        }
        self.ContinueButton.backgroundColor = Colors.DisabledButtonGrey
        self.ContinueButton.isEnabled = false
    }
    

    func generateUsername(first: String, last: String) -> String {
            let index = last.index(last.startIndex, offsetBy: 0)
            let l = String(last[index])
            let VC2_user_name = first + " " + l
            return VC2_user_name
        }
    }

extension StringProtocol {
    var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
}

