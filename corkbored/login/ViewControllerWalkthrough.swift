//
//  ViewControllerWalkthrough.swift
//  corkbored
//
//  Created by Jackson Meyer on 9/24/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class ViewControllerWalkthrough: UIViewController {
    @IBOutlet weak var titleLabel1: UILabel!
    @IBOutlet weak var titleLabel2: UILabel!
    @IBOutlet weak var titleLabel3: UILabel!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image3: UIImageView!
    
    @IBOutlet weak var ContinueButton: UIButton!
    @IBAction func continueButtonClicked(_ sender: UIButton) {
        ContinueButton.isEnabled = false
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "main")
        self.present(controller, animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel1.alpha = 0.0
        titleLabel2.alpha = 0.0
        titleLabel3.alpha = 0.0
        image1.alpha = 0.0
        image2.alpha = 0.0
        image3.alpha = 0.0
        ContinueButton.alpha = 0.0
        
        self.ContinueButton.setGradientBackground(colorOne: Colors.grad1, colorTwo: Colors.grad2)
        self.ContinueButton.isEnabled = true
        // Do any additional setup after loading the view.
        animateViews()
    }
    
    override func viewDidLayoutSubviews() {
        if self.ContinueButton.layer.sublayers != nil {
            for layer in (ContinueButton.layer.sublayers)! {
                layer.frame = self.ContinueButton.bounds
            }
        }
    }
    
    func animateViews() {
        let duration = 0.75
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { // change 2 to desired number of seconds
            // Your code with delay
            UIView.animate(withDuration: duration, animations: {
                /* Do here the first animation */
                self.image1.alpha = 1.0
                self.titleLabel1.alpha = 1.0
            }) { (completed) in
                
                UIView.animate(withDuration: duration, animations: {
                    /* Do here the second animation */
                    self.image2.alpha = 1.0
                    self.titleLabel2.alpha = 1.0
                }) { (completed) in
                    
                    UIView.animate(withDuration: duration, animations: {
                        /* Do here the third animation */
                        self.image3.alpha = 1.0
                        self.titleLabel3.alpha = 1.0
                        self.ContinueButton.alpha = 1.0
                    })
                }
            }
        }
    }
}
