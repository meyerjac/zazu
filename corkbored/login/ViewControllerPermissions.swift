//
//  ViewControllerPermissions.swift
//  corkbored
//
//  Created by Jackson Meyer on 9/24/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase
import CoreData
import UserNotifications

class ViewControllerPermissions: UIViewController, CLLocationManagerDelegate {
    var locationManager = CLLocationManager()
    var userLocation: CLLocation = CLLocation()
    var geocoder = CLGeocoder()
    var notificationsEnabled: Bool = false
    var locationEnabled: Bool = false
    var currentState: String = "Pretoria"
    var currentCity :String = "Transvaal"
    
    @IBAction func notificationSettingsButtonClicked(_ sender: UIButton) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
            })
        }
    }
    @IBOutlet weak var NotificationSettingButton: UIButton!
    @IBAction func locationSettingsButtonClicked(_ sender: UIButton!) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
            })
        }
    }
    @IBOutlet weak var LocationSettingButton: UIButton!
    @IBOutlet weak var LocationPermissionView: UIView!
    @IBOutlet weak var LocationPermissionSwitch: UISwitch!
    @IBOutlet weak var NotificationPermissionView: UIView!
    @IBOutlet weak var NotificationPermissionSwitch: UISwitch!
    @IBOutlet weak var ContinueButton: UIButton!
    @IBAction func continueButtonClicked(_ sender: UIButton) {
        ContinueButton.isEnabled = false
        if isValidLocation(state: self.currentState, city: self.currentCity) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "ViewController7")
            self.present(controller, animated: false, completion: nil)
            ContinueButton.isEnabled = true
        } else {
            saveLocationToDb()
            showNotLiveInCityAlert(subtitle: "Zazu hasn't launched in your city yet, we'll email you first when we touch down, thanks for the support!", title: "You are so awesome! but...", buttonTitle: "okay")
            ContinueButton.isEnabled = true
        }
    }
    @IBAction func sampleSwitchValueChanged(_ sender: UISwitch) {
        if sender.tag == 0 {
            //location
            if LocationPermissionSwitch.isOn {
                if locationEnabled {
                    LocationPermissionSwitch.setOn(true, animated: true);
                    fetchUserLocation()
                } else {
                    LocationPermissionSwitch.setOn(false, animated: true);
                    fetchUserLocation()
                }
            } else {
                if locationEnabled {
                    LocationPermissionSwitch.setOn(true, animated: true);
                }
            }
        } else {
            //permission
            if NotificationPermissionSwitch.isOn {
               NotificationPermissionSwitch.setOn(false, animated: true);

                let application = UIApplication.shared
                
                if #available(iOS 10.0, *) {
                    // For iOS 10 display notification (sent via APNS)
                    let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                    UNUserNotificationCenter.current().requestAuthorization(options: authOptions,completionHandler: {accepted, _ in
                        if accepted {
                            DispatchQueue.main.async {
                            self.NotificationPermissionSwitch.setOn(true, animated: true)
                            }
                            self.notificationsEnabled = true
                            self.checkContinueButtonStatus()
                        } else {
                           self.notificationsEnabled = false
                            //show notification permission
                            DispatchQueue.main.async {
                                self.NotificationSettingButton.isHidden = false
                            }
                            
                            //second alert
                            let alert = UIAlertController(title: "Need Authorization", message: "This app is unusable if you don't authorize push notifications!", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                            alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { _ in
                                let url = URL(string: UIApplication.openSettingsURLString)!
                                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
                            }))
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                    })
                } else {
                    let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                    application.registerUserNotificationSettings(settings)
                }
                
                application.registerForRemoteNotifications()
                // [END register_for_notifications]
            } else {
                if notificationsEnabled {
                    NotificationPermissionSwitch.setOn(true, animated: true);
                }
            }
        }
    }
    
    //MARK: - VDL
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getPermissionsData()
    }
    
    override func viewDidLayoutSubviews() {
        self.ContinueButton.layer.cornerRadius = self.ContinueButton.frame.size.height/2
        self.ContinueButton.clipsToBounds = true
    }
    
    @objc func dismissAlert() {
        self.ContinueButton.isEnabled = true
    }
    
    func getPermissionsData() {
        let application = UIApplication.shared
        if application.isRegisteredForRemoteNotifications {
            self.notificationsEnabled = true
            self.NotificationPermissionSwitch.setOn(true, animated: true);
    }
        
        //MARK: - location
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                ()
            case .authorizedAlways, .authorizedWhenInUse:
                self.locationEnabled = true
                self.LocationPermissionSwitch.setOn(true, animated: true)
                fetchUserLocation()
            }
        }
        checkContinueButtonStatus()
    }
    
    //MARK: - LOCATION
    @objc func fetchUserLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
    func saveLocationToDb() {
        //just assign users email to user city node
        if let uid = Auth.auth().currentUser?.uid {
            let dict = [uid : (Auth.auth().currentUser?.email)!]
            let ref = Database.database().reference()
            ref.child("EXTRAS").child("REQUESTED_LOCATIONS").child(self.currentState).child(self.currentCity).child(uid).setValue(dict)
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        if (CLLocationManager.authorizationStatus() == .notDetermined) {
            //first show, display DZN empty dataset
           
        }
        
        if (CLLocationManager.authorizationStatus() == .denied) {
            //second alert
            let alert = UIAlertController(title: "Need Authorization", message: "This app is unusable if you don't authorize this app to use your location!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { _ in
                let url = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
            self.LocationSettingButton.isHidden = false
            //need to show settings button to change location to while using
        }
        
        if (CLLocationManager.authorizationStatus() == .authorizedWhenInUse) {
             LocationPermissionSwitch.setOn(true, animated: true)
             self.locationEnabled = true
             checkContinueButtonStatus()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.userLocation = locations[0]
        geocoder.reverseGeocodeLocation(userLocation,
        completionHandler: { (placemarks, error) in
            if error == nil {
                if (Auth.auth().currentUser?.uid) != nil {
                    //Gets user location every 100m
                    if let location = placemarks?[0] {
                        if let countryCode = location.isoCountryCode, !countryCode.isEmpty {
                            if let state = location.administrativeArea, !state.isEmpty {
                                if let city = location.locality, !city.isEmpty {
                                    self.currentCity = city
                                    self.currentState = state
                                    self.saveLocationToCoreData(country: countryCode, state: self.currentState, city: self.currentCity)
                                } else {
                                    //no city
                                }
                            } else {
                                //no state
                            }
                        }
                    }
                }
            }
        })
    }
    
    func saveLocationToCoreData(country: String, state: String, city: String) {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            var currentUserCoreData = peep
            if currentUserCoreData.count != 0 {
                currentUserCoreData[0].location_country = country
                currentUserCoreData[0].location_city = city
                currentUserCoreData[0].location_state = state
                PersistanceService.saveContext()
            }
        } catch { () }
    }
    
    func isValidLocation(state: String, city: String) -> Bool {
//        if (LiveLocation.validStates.contains(state) && LiveLocation.validCities.contains(city)) {
//           return true
//        } else {
//            return false
//        }
        return true
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) { () }

    func checkContinueButtonStatus() {
        if locationEnabled && notificationsEnabled {
             DispatchQueue.main.async {
                self.ContinueButton.setGradientBackground(colorOne: Colors.grad1, colorTwo: Colors.grad2)
                self.ContinueButton.isEnabled = true
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
