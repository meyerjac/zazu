//
//  CustomTypeAlertEnum.swift
//  corkbored
//
//  Created by Jackson Meyer on 12/19/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import Foundation

enum alertEnumType {
    case deleteDm
    case blockUser
    case unblockUser
    case reportEvent
    case editEvent
    case leaveEvent
    case deleteEvent
}

