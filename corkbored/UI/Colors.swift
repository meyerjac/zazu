//
//  Colors.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/5/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import Foundation
import UIKit


struct Colors {
    static let DisabledButtonGrey = UIColor(red:0.86, green:0.86, blue:0.86, alpha:1.0)
    static let flat3 = UIColor(red:0.34, green:0.22, blue:0.53, alpha:1.0)
    static let flat4 = UIColor(red:0.33, green:0.14, blue:0.62, alpha:1.0)
    
    //purple button gradient
    static let grad1 = UIColor(red:0.54, green:0.50, blue:0.73, alpha:1.0)
    static let grad2 = UIColor(red:0.39, green:0.49, blue:0.93, alpha:1.0)
}

