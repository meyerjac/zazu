//
//  alelrtViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/20/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

//Used in EditViewController when user saves profile changes
class alertViewController: UIViewController {
    @IBOutlet weak var alertLabel: UILabel?
    @IBOutlet weak var alertContainer: UIView?
    var alertTitle = "Saved"

    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissAlert(_:)))
        view.addGestureRecognizer(tap)
        view.isUserInteractionEnabled = true
        alertLabel?.text = alertTitle
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.75, execute: {
            self.dismissAlertFromTimer()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.alertContainer?.layer.cornerRadius = 5
        self.alertContainer?.clipsToBounds = true
    }
    
    @objc func dismissAlert(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func dismissAlertFromTimer() {
        self.dismiss(animated: true, completion: nil)
    }
}
