//
//  TinderImageView.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/4/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

//This class and inspectable items are required for image picker screen and edit profile screen
class TinderImageView: UIImageView {
    
    let imageIndexLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "SSS"
        label.font = UIFont.boldSystemFont(ofSize: 12)
        
        label.layer.shadowOpacity = 0.7
        label.layer.shadowOffset = .zero
        
        return label
    }()
    
    let removeButton: UIButton = {
        let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        button.backgroundColor = UIColor.clear
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named:"close"), for: .normal)
        button.imageView?.contentMode = .scaleAspectFill
        button.layer.cornerRadius = 2
        button.clipsToBounds = true

        button.addTarget(self, action: #selector(ViewControllerPicturePicker.removeButtonPressed), for: .touchUpInside)
        button.addTarget(self, action: #selector(EditProfileRootViewController.removeButtonPressed), for: .touchUpInside)
        
        return button
    }()
    
    @IBInspectable
    var imageIndex: NSNumber! {
        didSet {
            removeButton.tag = imageIndex as! Int
            
            //people cant remove first image
            if imageIndex == 1 {
                layer.borderColor = UIColor(named: "zazuPurple")?.cgColor
                layer.borderWidth = 3.0
                removeButton.isHidden = true
            }
            

            layer.cornerRadius = 10
            imageIndexLabel.text = imageIndex.stringValue

        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
   
        addSubview(imageIndexLabel)
        addSubview(removeButton)
        
        imageIndexLabel.translatesAutoresizingMaskIntoConstraints = false
        imageIndexLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        imageIndexLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 5).isActive = true
        
        removeButton.translatesAutoresizingMaskIntoConstraints = false
        removeButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
        removeButton.topAnchor.constraint(equalTo: bottomAnchor, constant: -21).isActive = true
        removeButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -5).isActive = true
        removeButton.leadingAnchor.constraint(equalTo: trailingAnchor, constant: -21).isActive = true
    }
}
