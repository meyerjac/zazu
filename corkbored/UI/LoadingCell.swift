//
//  LoadingCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 9/5/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

import UIKit

class LoadingCell: UITableViewCell {
    @IBOutlet weak var loadingSpinner: UIActivityIndicatorView?
    @IBOutlet weak var loadingLabel: UILabel?
}
