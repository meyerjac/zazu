//
//  CdNotification+CoreDataProperties.swift
//  
//
//  Created by Ryan Pate on 3/31/19.
//
//

import Foundation
import CoreData


extension CdNotification {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CdNotification> {
        return NSFetchRequest<CdNotification>(entityName: "CdNotification")
    }

    @NSManaged public var notification_event_city_id: String?
    @NSManaged public var notification_event_country_id: String?
    @NSManaged public var notification_event_state_id: String?
    @NSManaged public var notification_event_title: String?
    @NSManaged public var notification_event_uid: String?
    @NSManaged public var notification_string: String?
    @NSManaged public var notification_time_stamp: Double
    @NSManaged public var notification_type: String?
    @NSManaged public var notification_uid: String?
    @NSManaged public var notification_url: String?
    @NSManaged public var notification_user_name: String?
    @NSManaged public var notification_user_uid: String?

}
