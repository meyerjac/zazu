//
//  CdEvent+CoreDataProperties.swift
//  
//
//  Created by Ryan Pate on 3/31/19.
//
//

import Foundation
import CoreData


extension CdEvent {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CdEvent> {
        return NSFetchRequest<CdEvent>(entityName: "CdEvent")
    }

    @NSManaged public var cd_event_accepted: Bool
    @NSManaged public var cd_event_captain_uid: String?
    @NSManaged public var cd_event_city: String?
    @NSManaged public var cd_event_country: String?
    @NSManaged public var cd_event_end_time: String?
    @NSManaged public var cd_event_photo_url: URL?
    @NSManaged public var cd_event_start_time: String?
    @NSManaged public var cd_event_state: String?
    @NSManaged public var cd_event_title: String?
    @NSManaged public var cd_event_uid: String?

}
