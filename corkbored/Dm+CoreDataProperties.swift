//
//  Dm+CoreDataProperties.swift
//  
//
//  Created by Ryan Pate on 3/31/19.
//
//

import Foundation
import CoreData


extension Dm {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Dm> {
        return NSFetchRequest<Dm>(entityName: "Dm")
    }

    @NSManaged public var dm_any_messages_unread: Bool
    @NSManaged public var dm_last_message: String?
    @NSManaged public var dm_last_message_time_stamp: Double
    @NSManaged public var dm_owner_uid: String?
    @NSManaged public var dm_profile_photo_url: URL?
    @NSManaged public var dm_their_uid: String?
    @NSManaged public var dm_user_name: String?

}
