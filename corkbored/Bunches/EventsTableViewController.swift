//
//  GroupsTableViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 4/17/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import Firebase
import FirebaseAuth
import CoreData
import SDWebImage

class EventsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    @IBOutlet weak var tableView: UITableView?

    // MARK: - Data model
    var eventObjects = [CdEvent]()
    var bunchSections = [[CdEvent]]()
    var currentUserUid = ""
    var clickedBunchUid = ""
    var clickedBunchObject: CdEvent?
    var shownIndexes : [IndexPath] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchEvents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        removeNewEventDot()
        fetchEvents()
    }
    
    func removeNewEventDot() {
        if let tabVC = tabBarController {
            for subview in tabVC.tabBar.subviews {
                if subview.tag == 1314 {
                    subview.removeFromSuperview()
                    break
                }
            }
        }
    }
    
    func setupViews() {
        self.tableView?.emptyDataSetSource = self
        self.tableView?.emptyDataSetDelegate = self
        self.tableView?.dataSource = self
        self.tableView?.delegate = self
        self.tableView?.tableFooterView = UIView()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func fetchEvents() {
        let fetchRequest: NSFetchRequest<CdEvent> = CdEvent.fetchRequest()
        do {
            let coreDataBunches = try PersistanceService.context.fetch(fetchRequest)
            self.eventObjects.removeAll()
            self.bunchSections.removeAll()
            
            self.eventObjects = coreDataBunches

            if eventObjects.count > 0 { sortBunches() } else { tableView?.reloadData() }
            
            setupViews()
        } catch {
            ()
        }
    }
    
    func sortBunches() {
        for n in 0...self.eventObjects.count - 1 {
            if let end_time = self.eventObjects[n].cd_event_end_time {
               let formattedBunchEndDate = end_time.getBunchTimeSection(stringBunchEndDate: end_time)
                
                if self.bunchSections.count > 0 {
                    for i in 0...self.bunchSections.count - 1 {
                        if let bunch_section_end_time = bunchSections[i][0].cd_event_end_time {
                            if (bunch_section_end_time.getBunchTimeSection(stringBunchEndDate: bunch_section_end_time) == formattedBunchEndDate) {
                                self.bunchSections[i].append(eventObjects[n])
                                break;
                            }
                            if i == self.bunchSections.count - 1 {
                                //iterated through all and didnt hit
                                self.bunchSections.append([self.eventObjects[n]])
                            }
                        }
                    }
                } else {
                    self.bunchSections.append([self.eventObjects[n]])
                }
            }
        }

        //we can force unwrap becuse ^^ opetioanl unwrapping above will never have nil in end time in array
        self.bunchSections.sort { $0[0].cd_event_end_time! > $1[0].cd_event_end_time! }
        if self.eventObjects.count != 0 {
            self.tableView?.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            if let uid = Auth.auth().currentUser?.uid {
                if bunchSections[indexPath.section][indexPath.row].cd_event_captain_uid == uid {
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    if let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as? CustomAlertViewController {
                        if let eventEndStringTime = self.bunchSections[indexPath.section][indexPath.row].cd_event_end_time {
                            if !self.isAPastEvent(eventTime: eventEndStringTime) {
                                vc.actionButtonTitle = "Cancel"
                                vc.mainTitle = "Cancel Event?"
                                vc.subtitle = "Canceling this event will notify all the group members and remove the event and chat from everyone's feeds forever"
                            } else {
                                vc.actionButtonTitle = "Delete"
                                vc.mainTitle = "Delete Event"
                                vc.subtitle = "Deleting this event will notify all the group members and remove the event and chat from everyone's feeds forever"
                            }
                        }
                        
                        vc.cdEvent = self.bunchSections[indexPath.section][indexPath.row]
                        
                        vc.type = .deleteEvent
                        
                        vc.onDoneBlock = { result in
                            self.fetchEvents()
                        }
                        
                        vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        vc.messageUID = self.bunchSections[indexPath.section][indexPath.row].cd_event_uid
                        self.present(vc, animated: false, completion: nil)
                    }
                } else {
                    //LEAVE EVENT
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    if let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as? CustomAlertViewController {
                        vc.mainTitle = "Leave Event?"
                        vc.subtitle = "Leaving this event removes the event from your event list, removes you from the attendee list, and removes you from the chat."
                        vc.actionButtonTitle = "Leave"
                        vc.cdEvent = self.bunchSections[indexPath.section][indexPath.row]
                        
                        vc.type = .leaveEvent
                        
                        vc.onDoneBlock = { result in
                            DispatchQueue.main.async {
                                self.fetchEvents()
                            }
                        }
                        
                        vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        vc.messageUID = self.bunchSections[indexPath.section][indexPath.row].cd_event_uid
                        self.present(vc, animated: false, completion: nil)
                    }
                }
            }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return bunchSections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if eventObjects.count == 0 {
            return 0
        } else {
            return bunchSections[section].count
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            view.tintColor = UIColor(named: "zazuBlue")
            view.frame = CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 50)
            headerView.textLabel?.textColor = .white
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 50)) as? UITableViewHeaderFooterView
        headerView?.textLabel?.font = UIFont(name: "nevis-Bold", size: 22)
        headerView?.textLabel?.textColor = UIColor.white
        return headerView
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let end_time = bunchSections[section][0].cd_event_end_time else { return "Unknown" }
        
        if (end_time.getBunchTimeSection(stringBunchEndDate: end_time)) == "upcoming" {
            return "Upcoming"
        } else {
            return (end_time.getBunchTimeSection(stringBunchEndDate: end_time))
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "eventsCell1", for: indexPath) as? EventsTableViewCell {
            cell.bunch_title.text = self.bunchSections[indexPath.section][indexPath.row].cd_event_title
            
            if let firstPart = self.bunchSections[indexPath.section][indexPath.row].cd_event_start_time {
                if let firstStartTime = self.bunchSections[indexPath.section][indexPath.row].cd_event_start_time {
                    cell.bunch_time.text = firstPart.getReadableDate(time: firstStartTime)
                }
            }
            if let url = self.bunchSections[indexPath.section][indexPath.row].cd_event_photo_url {
                cell.bunch_host_picture.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached)
            }
            
            cell.bunch_host_picture.layer.cornerRadius = 10
            cell.bunch_host_picture.clipsToBounds = true
            
            cell.bunch_container_view.layer.cornerRadius = 10
            cell.bunch_container_view.clipsToBounds = true
            
            cell.bunch_dot.layer.cornerRadius = 10
            cell.bunch_dot.layer.borderColor = UIColor.white.cgColor
            cell.bunch_dot.layer.borderWidth = 3
            cell.bunch_dot.clipsToBounds = true
            
            if self.bunchSections[indexPath.section][indexPath.row].cd_event_accepted {
                cell.bunch_dot.backgroundColor = UIColor(named: "zazuBlue")
            } else {
                cell.bunch_dot.backgroundColor = UIColor(red:0.93, green:0.16, blue:0.22, alpha:1.0)
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toBunchMessaging" {
            if let destinationVC = segue.destination as? BunchMessagingViewController {
                destinationVC.event_uid = clickedBunchUid
                destinationVC.eventObject = self.clickedBunchObject
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        if self.bunchSections[indexPath.section][indexPath.row].cd_event_accepted {
            guard let clicked_event_uid = self.bunchSections[indexPath.section][indexPath.row].cd_event_uid else { return }
            self.clickedBunchUid = clicked_event_uid
            self.clickedBunchObject = self.bunchSections[indexPath.section][indexPath.row]
            self.performSegue(withIdentifier: "toBunchMessaging", sender: self)
        } else {
            showNotAcceptedYetAlert(subtitle: "The host hasn't accepted to the group yet. They don't what their missing...you're awesome!", title: "patience is a virtue", buttonTitle: "okay")
        }
    }
    
    @objc func dismissAlert() {
        ()
    }
    
    //ALL THIS FOR DZN EMPTY DATA SET
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Join some Events!"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Your city is happening, and you can find cool events hosted and attended by awesome people on the event feed!"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "message_bubble")
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> NSAttributedString? {
        let str = "Create Event"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
            self.performSegue(withIdentifier: "DZMToCreateEvent", sender: self)
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        if self.eventObjects.count == 0 { return true } else { return false }
    }
}
