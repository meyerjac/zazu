//
//  ProfileBunchesTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 6/26/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class EventsTableViewCell: UITableViewCell {
    @IBOutlet weak var bunch_title: UILabel!
    @IBOutlet weak var bunch_time: UILabel!
    @IBOutlet weak var bunch_container_view: UIView!
    @IBOutlet weak var bunch_host_picture: UIImageView!
    @IBOutlet weak var bunch_dot: UIView!
}
