//
//  NotificationTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 9/11/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet weak var notificationImageView: UIImageView!
    @IBOutlet weak var notificationLabel: UILabel!
}
