//
//  NotificationsViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/29/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import DZNEmptyDataSet

class NotificationsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    var NotificationsArray = [CdNotification]()
    var path = UIBezierPath()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.emptyDataSetSource = self;
        self.tableView.emptyDataSetDelegate = self;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        removeNewEventDot()
        fetchDmObjects()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func removeNewEventDot() {
        for subview in tabBarController!.tabBar.subviews {
            if let subview = subview as? UIView {
                if subview.tag == 1314 {
                    subview.removeFromSuperview()
                    break
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return NotificationsArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
        let notifEnum = NotificationsArray[indexPath.row].notification_type
        if notifEnum == ".dm" {
            
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let releventViewController = storyBoard.instantiateViewController(withIdentifier: "quickDM") as! QuickDmViewController
            if let userUid = NotificationsArray[indexPath.row].notification_user_uid {
                 releventViewController.selectedUserUid = userUid
            }
            
            if let userName = NotificationsArray[indexPath.row].notification_user_name {
                 releventViewController.userName = userName
            }

            self.navigationController?.pushViewController(releventViewController, animated: true)

        } else if notifEnum == ".mention" {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let releventViewController = storyBoard.instantiateViewController(withIdentifier: "BunchMessagingVC") as! BunchMessagingViewController

            if let bunchUid = NotificationsArray[indexPath.row].notification_bunch_uid {
                let fetchRequest: NSFetchRequest<CdBunch> = CdBunch.fetchRequest()
                let predicate = NSPredicate(format: "cd_bunch_uid = %@", argumentArray: [bunchUid]) // Specify your condition here
                fetchRequest.predicate = predicate
                do {
                    let result = try PersistanceService.context.fetch(fetchRequest)
                    let selectedBunch = result
                    if result.count == 0 {
                        print("no event named ther")
                    } else {
                        print("event named that")
                        if result.count == 1 {
                            releventViewController.bunchObject = result[0]
                            releventViewController.mentionCommentUid = NotificationsArray[indexPath.row].notification_uid!
                            self.navigationController?.pushViewController(releventViewController, animated: true)
                        }
                    }
                } catch {
                    print("Failed")
                    //if failed handle
                }
                releventViewController.bunch_uid = bunchUid
            }
        } else if notifEnum == ".joinedrequested" {
            //nav to accept group chat details page
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let releventViewController = storyBoard.instantiateViewController(withIdentifier: "eventDetails") as! GuestsDetailsViewController
            if let bunchUid = NotificationsArray[indexPath.row].notification_bunch_uid {
                let fetchRequest: NSFetchRequest<CdBunch> = CdBunch.fetchRequest()
                let predicate = NSPredicate(format: "cd_bunch_uid = %@", argumentArray: [bunchUid]) // Specify your condition here
                fetchRequest.predicate = predicate
                do {
                    let result = try PersistanceService.context.fetch(fetchRequest)
                    let selectedBunch = result
                    if result.count == 0 {
                        print("no event named ther")
                    } else {
                        print("event named that")
                        if result.count == 1 {
                            releventViewController.bunchObject = result[0]
                            self.navigationController?.pushViewController(releventViewController, animated: true)
                            
                        }
                    }
                } catch {
                    print("Failed")
                    //if failed handle
                }
            }
        } else if notifEnum == ".accepted" {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let releventViewController = storyBoard.instantiateViewController(withIdentifier: "BunchMessagingVC") as! BunchMessagingViewController

            if let bunchUid = NotificationsArray[indexPath.row].notification_bunch_uid {
                let fetchRequest: NSFetchRequest<CdBunch> = CdBunch.fetchRequest()
                let predicate = NSPredicate(format: "cd_bunch_uid = %@", argumentArray: [bunchUid]) // Specify your condition here
                fetchRequest.predicate = predicate
                do {
                    let result = try PersistanceService.context.fetch(fetchRequest)
                    let selectedBunch = result
                    if result.count == 0 {
                        print("no event named ther")
                    } else {
                        print("event named that")
                        if result.count == 1 {
                            releventViewController.bunchObject = result[0]
                            self.navigationController?.pushViewController(releventViewController, animated: true)
                        }
                    }
                } catch {
                    print("Failed")
                    //if failed handle
                }
                releventViewController.bunch_uid = bunchUid
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //TABLEVIEW
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationTableViewCell
        cell.selectionStyle = .none
        
        cell.notificationLabel.attributedText = getAttributedString(row: indexPath.row)
        cell.NotificationTypeImageView.image = notifImage(row: indexPath.row)
        
        let notifCase = NotificationsArray[indexPath.row].notification_type
        if notifCase == ".dm" {
            //trinagle
            cell.NotificationTypeContainerView.backgroundColor = UIColor(named: "zazuPurple")
            cell.NotificationTypeContainerView.layer.masksToBounds = true
            cell.NotificationTypeContainerView.layer.cornerRadius = 10


        } else if notifCase == ".mention" {
            //@
            cell.NotificationTypeContainerView.backgroundColor = UIColor(named: "zazuPurple")
            cell.NotificationTypeContainerView.layer.masksToBounds = true
            cell.NotificationTypeContainerView.layer.cornerRadius = 10
        } else {
            //checkmark
            cell.NotificationTypeContainerView.backgroundColor = UIColor(named: "zazuPurple")
            cell.NotificationTypeContainerView.layer.masksToBounds = true
            cell.NotificationTypeContainerView.layer.cornerRadius = 10
        }

        cell.notificationImageView.sd_setImage(with: URL(string: NotificationsArray[indexPath.row].notification_url!), placeholderImage: UIImage(named: "placeholder.png"))
        
        return cell
    }
    
    func getAttributedString(row: Int) -> NSMutableAttributedString {
        switch(NotificationsArray[row].notification_type)  {
        case ".dm":
            let font = UIFont(name: "Helvetica", size: 14)
            let font1 = UIFont(name: "HelveticaNeue-Bold", size: 14)
            
            let attributes = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: UIColor.black]
            let attributes1 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.black]
            let attributes2 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.gray]
            
            let yourAttributes = attributes
            let yourOtherAttributes = attributes1
            let timeStampAttributes = attributes2
            
            let mainString = NotificationsArray[row].notification_string
            let notifUserName = NotificationsArray[row].notification_user_name
            let notifTimeStamp = NotificationsArray[row].notification_time_stamp.getTimeStamp(interval: String(NotificationsArray[row].notification_time_stamp))
            
            let partOne = NSMutableAttributedString(string: notifUserName!, attributes: yourOtherAttributes)
            
            let str = mainString
            let result = String((str?.characters.prefix(5))!)
            if result == "https" {
                let partTwo = NSMutableAttributedString(string: " sent you a media message ", attributes: yourAttributes)
                let partThree = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes)
                let combination = NSMutableAttributedString()
                
                combination.append(partOne)
                combination.append(partTwo)
                combination.append(partThree)
                return combination
            } else {
                let partTwo = NSMutableAttributedString(string: " sent you a message: \"\(mainString!)\" ", attributes: yourAttributes)
                let partThree = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes)
                let combination = NSMutableAttributedString()
                
                combination.append(partOne)
                combination.append(partTwo)
                combination.append(partThree)
                return combination
            }
            break;
        case ".mention":
            let font = UIFont(name: "Helvetica", size: 14)
            let font1 = UIFont(name: "HelveticaNeue-Bold", size: 14)

            let attributes = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: UIColor.black]
            let attributes1 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.black]
            let attributes2 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.gray]

            let yourAttributes = attributes
            let yourOtherAttributes = attributes1
            let bunchTitleAttributes = attributes1
            let timeStampAttributes = attributes2

            let messageString = NotificationsArray[row].notification_string
            let bunchTitle = NotificationsArray[row].notification_bunch_title
            let notifUserName = NotificationsArray[row].notification_user_name
            let notifTimeStamp = NotificationsArray[row].notification_time_stamp.getTimeStamp(interval: String(NotificationsArray[row].notification_time_stamp))

            let partOne = NSMutableAttributedString(string: notifUserName!, attributes: yourOtherAttributes)
            let partTwo = NSMutableAttributedString(string: " mentioned you in the group chat: ", attributes: yourAttributes)
            let partThree = NSMutableAttributedString(string: "\(bunchTitle!) ", attributes: bunchTitleAttributes)
            let partFour = NSMutableAttributedString(string: "\"\(messageString!)\" ", attributes: yourAttributes)
            let partFive = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes)

            let combination = NSMutableAttributedString()

            combination.append(partOne)
            combination.append(partTwo)
            combination.append(partThree)
            combination.append(partFour)
            combination.append(partFive)
            
            return combination
            break;
        case ".joinedrequested":
            //if users joined or requested to join your bunch
                let font = UIFont(name: "Helvetica", size: 14)
                let font1 = UIFont(name: "HelveticaNeue-Bold", size: 14)
                
                let attributes = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: UIColor.black]
                let attributes1 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.black]
                let attributes2 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.gray]
                
                let usernameBold = attributes1
                let regularAttributes = attributes
                let bunchTitleAttributes = attributes1
                let timeStampAttributes = attributes2
                
                let notifUserName = NotificationsArray[row].notification_user_name
                let messageString = NotificationsArray[row].notification_string
                let bunchTitle = NotificationsArray[row].notification_bunch_title
                let notifTimeStamp = NotificationsArray[row].notification_time_stamp.getTimeStamp(interval: String(NotificationsArray[row].notification_time_stamp))
                
                let partOne = NSMutableAttributedString(string: notifUserName!, attributes: usernameBold)
                let partTwo = NSMutableAttributedString(string: "\(messageString!): ", attributes: regularAttributes)
                let partThree = NSMutableAttributedString(string: "\(bunchTitle!) ", attributes: bunchTitleAttributes)
                let partFour = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes)
                
                let combination = NSMutableAttributedString()
                
                combination.append(partOne)
                combination.append(partTwo)
                combination.append(partThree)
                combination.append(partFour)
                
                return combination
            break;
             case ".accepted":
            let font = UIFont(name: "Helvetica", size: 14)
            let font1 = UIFont(name: "HelveticaNeue-Bold", size: 14)
            
            let attributes = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: UIColor.black]
            let attributes1 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.black]
            let attributes2 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.gray]
            
            let usernameBold = attributes1
            let regularAttributes = attributes
            let bunchTitleAttributes = attributes1
            let timeStampAttributes = attributes2
            
            let notifUserName = NotificationsArray[row].notification_user_name
            let messageString = NotificationsArray[row].notification_string
            let bunchTitle = NotificationsArray[row].notification_bunch_title
            let notifTimeStamp = NotificationsArray[row].notification_time_stamp.getTimeStamp(interval: String(NotificationsArray[row].notification_time_stamp))
            
            let partOne = NSMutableAttributedString(string: notifUserName!, attributes: usernameBold)
            let partTwo = NSMutableAttributedString(string: " \(messageString!)", attributes: regularAttributes)
            let partThree = NSMutableAttributedString(string: "\(bunchTitle!) ", attributes: bunchTitleAttributes)
            let partFour = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes)
            
            let combination = NSMutableAttributedString()
            
            combination.append(partOne)
            combination.append(partTwo)
            combination.append(partThree)
            combination.append(partFour)
            
            return combination
            break;
        default:
            print("hit default statment in notification Table Switch")
            //default Return
            let defualtNotificationString = "Notification was lost in the world wide web...we're tryin to find it!"
            let defaultFont = UIFont(name: "Metropolis-Medium", size: 16)
            let attributes = [NSAttributedStringKey.font: defaultFont, NSAttributedStringKey.foregroundColor: UIColor.black]
            let partTwo = NSMutableAttributedString(string: defualtNotificationString, attributes: attributes)
            let combination = NSMutableAttributedString()
            combination.append(partTwo)
            return combination
            
        }
        //default Return
        let defualtNotificationString = "Notification was lost in the world wide web...we're tryin to find it!"
        let defaultFont = UIFont(name: "Metropolis-Medium", size: 16)
        let attributes = [NSAttributedStringKey.font: defaultFont, NSAttributedStringKey.foregroundColor: UIColor.black]
        let partTwo = NSMutableAttributedString(string: defualtNotificationString, attributes: attributes)
        let combination = NSMutableAttributedString()
        combination.append(partTwo)
        return combination
    }
    
    func notifImage(row: Int) -> UIImage {
        let notifCase = NotificationsArray[row].notification_type
        if notifCase == ".dm" {
            let str = NotificationsArray[row].notification_string
            let result = String((str?.characters.prefix(5))!)
            if result == "https" {
                 return UIImage(named: "notificationMedia")!
            } else {
                return UIImage(named: "notifications_tableview_dm")!
            }
        } else if notifCase == ".mention" {
            return UIImage(named: "notifications_tableview_mention")!
        } else if notifCase == ".joinedrequested" {
             return UIImage(named: "follower")!
        } else if notifCase == ".accepted" {
             return UIImage(named: "verified")!
        } else {
            return UIImage(named: "profile_placeholder")!
        }
    }
    
    func fetchDmObjects() {
        let fetchRequest: NSFetchRequest<CdNotification> = CdNotification.fetchRequest()
        do {
            let coreDataNotifications = try PersistanceService.context.fetch(fetchRequest)
            self.NotificationsArray = coreDataNotifications
            
            if self.NotificationsArray.count > 0 {
                self.tableView.reloadData()
            } else {
                 self.tableView.reloadData()
            }
        } catch {
            print("error fetching")
        }
    }
    
    //MARK: - DZN EMPTY DATA SET
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
            let str = "No notifications yet!"
            let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
            return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Explore your city by attending events with awesome people, get out there and get involved!"
        let attrs = [NSAttributedStringKey.font: UIFont.preferredFont(forTextStyle: UIFontTextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "notifications")
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        if self.NotificationsArray.count == 0 {
            return true
        } else {
            return false
        }
    }
}
