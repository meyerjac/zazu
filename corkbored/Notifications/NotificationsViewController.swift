//
//  NotificationsViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/29/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import CoreData
import Firebase
import DZNEmptyDataSet

class NotificationsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    //MARK: - Variables
    var NotificationsArray = [CdNotification]()
    var path = UIBezierPath()
    let defaultName = "NOTHING FOUND NAME"
    let defaultTitle = "NOTHING FOUND TITLE"
    let defaultBody = "NOTHING FOUND BODY"
    
    //MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - VDL
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.emptyDataSetSource = self;
        self.tableView.emptyDataSetDelegate = self;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(false, forKey: "newNotificationsBoolean")
        
        removeNotificationDot()
        fetchDmObjects()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func removeNotificationDot() {
        if let tabBC = tabBarController {
            for subview in tabBC.tabBar.subviews {
                if subview.tag == 1314 {
                    subview.removeFromSuperview()
                    break
                }
            }
        }
    }
    
    //MARK: - Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return NotificationsArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
        if let notifEnum = NotificationsArray[indexPath.row].notification_type {
            switch (notifEnum) {
            case ".dm":
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                if let releventViewController = storyBoard.instantiateViewController(withIdentifier: "quickDM") as? QuickDmViewController {
                    
                    guard let userUid = NotificationsArray[indexPath.row].notification_user_uid else { return }
                    releventViewController.selectedUserUid = userUid
                    
                    guard let userName = NotificationsArray[indexPath.row].notification_user_name else { return }
                    releventViewController.userName = userName
                
                    self.navigationController?.pushViewController(releventViewController, animated: true)
                }
            case ".mention":
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                if let releventViewController = storyBoard.instantiateViewController(withIdentifier: "BunchMessagingVC") as? BunchMessagingViewController {
                    
                    if let bunchUid = NotificationsArray[indexPath.row].notification_event_uid {
                        let fetchRequest: NSFetchRequest<CdEvent> = CdEvent.fetchRequest()
                        let predicate = NSPredicate(format: "cd_event_uid = %@", argumentArray: [bunchUid]) // Specify your condition here
                        fetchRequest.predicate = predicate
                        do {
                            let result = try PersistanceService.context.fetch(fetchRequest)
                            if result.count == 0 {
                            } else {
                                if result.count == 1 {
                                    releventViewController.eventObject = result[0]
                                    self.navigationController?.pushViewController(releventViewController, animated: true)
                                }
                            }
                        } catch {
                           ()
                        }
                        releventViewController.event_uid = bunchUid
                    }
                }
            case ".joinedrequested", ".edited":
                //nav to accept group chat details page
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                if let releventViewController = storyBoard.instantiateViewController(withIdentifier: "eventDetails") as? GuestsDetailsViewController {
                    if let bunchUid = NotificationsArray[indexPath.row].notification_event_uid {
                        let fetchRequest: NSFetchRequest<CdEvent> = CdEvent.fetchRequest()
                        let predicate = NSPredicate(format: "cd_event_uid = %@", argumentArray: [bunchUid]) // Specify your condition here
                        fetchRequest.predicate = predicate
                        do {
                            let result = try PersistanceService.context.fetch(fetchRequest)
                            if result.count != 0 {
                                if result.count == 1 {
                                    releventViewController.eventObject = result[0]
                                    self.navigationController?.pushViewController(releventViewController, animated: true)
                                }
                            }
                        } catch {
                           ()
                        }
                    }
                }
            case ".accepted":
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                if let releventViewController = storyBoard.instantiateViewController(withIdentifier: "BunchMessagingVC") as? BunchMessagingViewController {
                    if let bunchUid = NotificationsArray[indexPath.row].notification_event_uid {
                        let fetchRequest: NSFetchRequest<CdEvent> = CdEvent.fetchRequest()
                        let predicate = NSPredicate(format: "cd_event_uid = %@", argumentArray: [bunchUid]) // Specify your condition here
                        fetchRequest.predicate = predicate
                        do {
                            let result = try PersistanceService.context.fetch(fetchRequest)
                            if result.count == 0 {
                                
                            } else {
                                
                                if result.count == 1 {
                                    releventViewController.eventObject = result[0]
                                    self.navigationController?.pushViewController(releventViewController, animated: true)
                                }
                            }
                        } catch {
                           ()
                        }
                        releventViewController.event_uid = bunchUid
                    }
                }
            case ".eventMessageLike":
                let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                if let releventViewController = storyBoard.instantiateViewController(withIdentifier: "BunchMessagingVC") as? BunchMessagingViewController {
                    
                    if let bunchUid = NotificationsArray[indexPath.row].notification_event_uid {
                        let fetchRequest: NSFetchRequest<CdEvent> = CdEvent.fetchRequest()
                        let predicate = NSPredicate(format: "cd_event_uid = %@", argumentArray: [bunchUid]) // Specify your condition here
                        fetchRequest.predicate = predicate
                        do {
                            let result = try PersistanceService.context.fetch(fetchRequest)
                            if result.count == 0 {
                            } else {
                                if result.count == 1 {
                                    releventViewController.eventObject = result[0]
                                    self.navigationController?.pushViewController(releventViewController, animated: true)
                                }
                            }
                        } catch {
                            ()
                        }
                        releventViewController.event_uid = bunchUid
                    }
                }
            default:
              ()
            }
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as? NotificationTableViewCell {
            cell.selectionStyle = .none
            cell.notificationLabel.attributedText = getAttributedString(row: indexPath.row)
            
            if let url = NotificationsArray[indexPath.row].notification_url {
                cell.notificationImageView.sd_setImage(with: URL(string: url), placeholderImage: nil, options: .refreshCached)
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func getAttributedString(row: Int) -> NSMutableAttributedString {
        switch(NotificationsArray[row].notification_type)  {
        case ".dm":
            let font = UIFont(name: "Metropolis-Medium", size: 16)
            let font1 = UIFont(name: "Metropolis-SemiBold", size: 16)
            
            let attributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black]
            let attributes1 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.black]
            let attributes2 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.gray]
            
            let yourAttributes = attributes
            let yourOtherAttributes = attributes1
            let timeStampAttributes = attributes2
            let notifTimeStamp = Int(NotificationsArray[row].notification_time_stamp).getTimeStamp(interval: Int(NotificationsArray[row].notification_time_stamp))
            
            if let mainString = NotificationsArray[row].notification_string {
                if let notifUserName = NotificationsArray[row].notification_user_name {
                    let partOne = NSMutableAttributedString(string: notifUserName, attributes: yourOtherAttributes as [NSAttributedString.Key : Any])
                    
                    let result = String(mainString.prefix(5))
                    
                    if result == "https" {
                        let partTwo = NSMutableAttributedString(string: " sent you a media message ", attributes: yourAttributes as [NSAttributedString.Key : Any])
                        let partThree = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes as [NSAttributedString.Key : Any])
                        let combination = NSMutableAttributedString()
                        
                        combination.append(partOne)
                        combination.append(partTwo)
                        combination.append(partThree)
                        return combination
                    } else {
                        let partTwo = NSMutableAttributedString(string: " sent you a message: \"\(mainString)\" ", attributes: yourAttributes as [NSAttributedString.Key : Any])
                        let partThree = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes as [NSAttributedString.Key : Any])
                        let combination = NSMutableAttributedString()
                        
                        combination.append(partOne)
                        combination.append(partTwo)
                        combination.append(partThree)
                        return combination
                    }
                }
            }
        case ".mention":
            let font = UIFont(name: "Metropolis-Medium", size: 16)
            let font1 = UIFont(name: "Metropolis-SemiBold", size: 16)

            let attributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black]
            let attributes1 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.black]
            let attributes2 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.gray]

            let yourAttributes = attributes
            let yourOtherAttributes = attributes1
            let bunchTitleAttributes = attributes1
            let timeStampAttributes = attributes2
            let notifTimeStamp = Int(NotificationsArray[row].notification_time_stamp).getTimeStamp(interval: Int(NotificationsArray[row].notification_time_stamp))

            if let messageString = NotificationsArray[row].notification_string {
                if let bunchTitle = NotificationsArray[row].notification_event_title {
                    if let notifUserName = NotificationsArray[row].notification_user_name {
                        let partOne = NSMutableAttributedString(string: notifUserName, attributes: yourOtherAttributes as [NSAttributedString.Key : Any])
                        let partTwo = NSMutableAttributedString(string: " mentioned you in the group chat:", attributes: yourAttributes as [NSAttributedString.Key : Any])
                        let partThree = NSMutableAttributedString(string: " \(bunchTitle)", attributes: bunchTitleAttributes as [NSAttributedString.Key : Any])
                        let partFour = NSMutableAttributedString(string: " \"\(messageString)\" ", attributes: yourAttributes as [NSAttributedString.Key : Any])
                        let partFive = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes as [NSAttributedString.Key : Any])
                        
                        let combination = NSMutableAttributedString()
                        
                        combination.append(partOne)
                        combination.append(partTwo)
                        combination.append(partThree)
                        combination.append(partFour)
                        combination.append(partFive)
                        return combination
                    }
                }
            }
        case ".joinedrequested":
            //if users joined or requested to join your bunch
                let font = UIFont(name: "Metropolis-Medium", size: 16)
                let font1 = UIFont(name: "Metropolis-SemiBold", size: 16)
                
                let attributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black]
                let attributes1 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.black]
                let attributes2 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.gray]
                
                let usernameBold = attributes1
                let regularAttributes = attributes
                let bunchTitleAttributes = attributes1
                let timeStampAttributes = attributes2
                let notifTimeStamp = Int(NotificationsArray[row].notification_time_stamp).getTimeStamp(interval: Int((NotificationsArray[row].notification_time_stamp)))
                
                if let notifUserName = NotificationsArray[row].notification_user_name {
                    if let messageString = NotificationsArray[row].notification_string {
                        if let bunchTitle = NotificationsArray[row].notification_event_title {
                            let partOne = NSMutableAttributedString(string: notifUserName, attributes: usernameBold as [NSAttributedString.Key : Any])
                            let partTwo = NSMutableAttributedString(string: " \(messageString):", attributes: regularAttributes as [NSAttributedString.Key : Any])
                            let partThree = NSMutableAttributedString(string: " \(bunchTitle) ", attributes: bunchTitleAttributes as [NSAttributedString.Key : Any])
                            let partFour = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes as [NSAttributedString.Key : Any])
                            
                            let combination = NSMutableAttributedString()
                            
                            combination.append(partOne)
                            combination.append(partTwo)
                            combination.append(partThree)
                            combination.append(partFour)
                            
                            return combination
                        }
                    }
                }
            
            case ".accepted":
                let font = UIFont(name: "Metropolis-Medium", size: 16)
                let font1 = UIFont(name: "Metropolis-SemiBold", size: 16)
                
                let attributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black]
                let attributes1 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.black]
                let attributes2 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.gray]
                
                let usernameBold = attributes1
                let regularAttributes = attributes
                let bunchTitleAttributes = attributes1
                let timeStampAttributes = attributes2
                
                let notifUserName = NotificationsArray[row].notification_user_name
                let messageString = NotificationsArray[row].notification_string
                let bunchTitle = NotificationsArray[row].notification_event_title
                let notifTimeStamp = Int(NotificationsArray[row].notification_time_stamp).getTimeStamp(interval: Int(NotificationsArray[row].notification_time_stamp))
                
                let partOne = NSMutableAttributedString(string: notifUserName ?? defaultName, attributes: usernameBold as [NSAttributedString.Key : Any])
                let partTwo = NSMutableAttributedString(string: " \(messageString ?? defaultBody)" , attributes: regularAttributes as [NSAttributedString.Key : Any])
                let partThree = NSMutableAttributedString(string: " \(bunchTitle ?? defaultTitle) " , attributes: bunchTitleAttributes as [NSAttributedString.Key : Any])
                let partFour = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes as [NSAttributedString.Key : Any])
                
                let combination = NSMutableAttributedString()
                
                combination.append(partOne)
                combination.append(partTwo)
                combination.append(partThree)
                combination.append(partFour)
                
                return combination
            
        case ".eventEdit":
            let font = UIFont(name: "Metropolis-Medium", size: 16)
            let font1 = UIFont(name: "Metropolis-SemiBold", size: 16)
            
            let defaultName = "NOTHING FOUND"
            let defaultTitle = "NOTHING FOUND"
            let defaultBody = "NOTHING FOUND"
            
            let attributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black]
            let attributes1 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.black]
            let attributes2 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.gray]
            
            let usernameBold = attributes1
            let regularAttributes = attributes
            let bunchTitleAttributes = attributes1
            let timeStampAttributes = attributes2
            
            let notifUserName = NotificationsArray[row].notification_user_name
            let messageString = NotificationsArray[row].notification_string
            let bunchTitle = NotificationsArray[row].notification_event_title
            let notifTimeStamp = Int(NotificationsArray[row].notification_time_stamp).getTimeStamp(interval: Int(NotificationsArray[row].notification_time_stamp))
            
            let partOne = NSMutableAttributedString(string: notifUserName ?? defaultName, attributes: usernameBold as [NSAttributedString.Key : Any])
            let partTwo = NSMutableAttributedString(string: " \(messageString ?? defaultBody)" , attributes: regularAttributes as [NSAttributedString.Key : Any])
            let partThree = NSMutableAttributedString(string: " \(bunchTitle ?? defaultTitle) " , attributes: bunchTitleAttributes as [NSAttributedString.Key : Any])
            let partFour = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes as [NSAttributedString.Key : Any])
            
            let combination = NSMutableAttributedString()
            
            combination.append(partOne)
            combination.append(partTwo)
            combination.append(partThree)
            combination.append(partFour)
            
            return combination
        case ".eventMessageLike":
            let font = UIFont(name: "Metropolis-Medium", size: 16)
            let font1 = UIFont(name: "Metropolis-SemiBold", size: 16)

            let attributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black]
            let attributes1 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.black]
            let attributes2 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.gray]
            
            let usernameBold = attributes1
            let regularAttributes = attributes
            let timeStampAttributes = attributes2
            
            let notifUserName = NotificationsArray[row].notification_user_name
            let messageString = NotificationsArray[row].notification_string
            let notifTimeStamp = Int(NotificationsArray[row].notification_time_stamp).getTimeStamp(interval: Int(NotificationsArray[row].notification_time_stamp))
            
            let partOne = NSMutableAttributedString(string: notifUserName ?? defaultName, attributes: usernameBold as [NSAttributedString.Key : Any])
            let partTwo = NSMutableAttributedString(string: " \(messageString ?? defaultBody)" , attributes: regularAttributes as [NSAttributedString.Key : Any])
            let partFour = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes as [NSAttributedString.Key : Any])
            
            let combination = NSMutableAttributedString()
            
            combination.append(partOne)
            combination.append(partTwo)
            combination.append(partFour)
            
            return combination
        case ".deleteEvent":
            let font = UIFont(name: "Metropolis-Medium", size: 16)
            let font1 = UIFont(name: "Metropolis-SemiBold", size: 16)
            
            let attributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black]
            let attributes1 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.black]
            let attributes2 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor.gray]
            
            let usernameBold = attributes1
            let regularAttributes = attributes
            let timeStampAttributes = attributes2
            
            let notifUserName = NotificationsArray[row].notification_user_name
            let messageString = NotificationsArray[row].notification_string
            let notifTimeStamp = Int(NotificationsArray[row].notification_time_stamp).getTimeStamp(interval: Int(NotificationsArray[row].notification_time_stamp))
            
            let partOne = NSMutableAttributedString(string: notifUserName ?? defaultName, attributes: usernameBold as [NSAttributedString.Key : Any])
            let partTwo = NSMutableAttributedString(string: " \(messageString ?? defaultBody)" , attributes: regularAttributes as [NSAttributedString.Key : Any])
            let partFour = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes as [NSAttributedString.Key : Any])
            
            let combination = NSMutableAttributedString()
            
            combination.append(partOne)
            combination.append(partTwo)
            combination.append(partFour)
            
            return combination
        default:
            //default Return
            let defualtNotificationString = "Notification was lost in the world wide web...we're tryin to find it!"
            let defaultFont = UIFont(name: "Metropolis-Medium", size: 16)
            let attributes = [NSAttributedString.Key.font: defaultFont, NSAttributedString.Key.foregroundColor: UIColor.black]
            let partTwo = NSMutableAttributedString(string: defualtNotificationString, attributes: attributes as [NSAttributedString.Key : Any])
            let combination = NSMutableAttributedString()
            combination.append(partTwo)
            return combination
        }
        
        //default Return
        let defualtNotificationString = "Notification was lost in the world wide web...we're tryin to find it!"
        let defaultFont = UIFont(name: "Metropolis-Medium", size: 16)
        let attributes = [NSAttributedString.Key.font: defaultFont, NSAttributedString.Key.foregroundColor: UIColor.black]
        let partTwo = NSMutableAttributedString(string: defualtNotificationString, attributes: attributes as [NSAttributedString.Key : Any])
        let combination = NSMutableAttributedString()
        combination.append(partTwo)
        return combination
    }
    
    
    func fetchDmObjects() {
        let fetchRequest: NSFetchRequest<CdNotification> = CdNotification.fetchRequest()
        do {
            let coreDataNotifications = try PersistanceService.context.fetch(fetchRequest)
            
            self.NotificationsArray = coreDataNotifications.sorted(by: { $0.notification_time_stamp > $1.notification_time_stamp})
            
            if self.NotificationsArray.count > 0 {
                self.tableView.reloadData()
            } else {
                 self.tableView.reloadData()
            }
        } catch {
            ()
        }
    }
    
    //MARK: - DZN EMPTY DATA SET
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
            let str = "No notifications yet!"
            let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
            return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Get connected to your city by attending events with awesome people, have fun!"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "notifications")
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        if self.NotificationsArray.count == 0 {
            return true
        } else {
            return false
        }
    }
}
