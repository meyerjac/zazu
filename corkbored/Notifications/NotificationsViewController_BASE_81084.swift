//
//  NotificationsViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 7/29/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import CoreData
import Firebase

class NotificationsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var NotificationsArray = [CdNotification]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Notifications"
    }
    
    override func viewWillAppear(_ animated: Bool) {
       fetchDmObjects()
        
        tableView.alwaysBounceVertical = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return NotificationsArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //TABLEVIEW
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationTableViewCell
        
        cell.notificationLabel.attributedText = getAttributedString(row: indexPath.row)
        cell.NotificationTypeImageView.image = notifImage(row: indexPath.row)

        cell.notificationImageView.sd_setImage(with: URL(string: NotificationsArray[indexPath.row].notification_url!), placeholderImage: UIImage(named: "placeholder.png"))
        
//        cell.notificationTimeStamp.text = NotificationsArray[indexPath.row].notification_time_stamp.getTimeStamp(interval: String(NotificationsArray[indexPath.row].notification_time_stamp))
        
        //interesting

        return cell
    }
    
    func getAttributedString(row: Int) -> NSMutableAttributedString {
        switch(NotificationsArray[row].notification_type)  {
        case ".dm":
            let font = UIFont(name: "Helvetica", size: 14)
            let font1 = UIFont(name: "HelveticaNeue-Bold", size: 14)
            
            let attributes = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: UIColor.black]
            let attributes1 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.black]
            let attributes2 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.gray]
            
            let yourAttributes = attributes
            let yourOtherAttributes = attributes1
            let timeStampAttributes = attributes2
            
            let mainString = NotificationsArray[row].notification_string
            let notifUserName = NotificationsArray[row].notification_user_name
            let notifTimeStamp = NotificationsArray[row].notification_time_stamp.getTimeStamp(interval: String(NotificationsArray[row].notification_time_stamp))
            
            let partOne = NSMutableAttributedString(string: notifUserName!, attributes: yourOtherAttributes)
            let partTwo = NSMutableAttributedString(string: " sent you a message: \"\(mainString!)\" ", attributes: yourAttributes)
            let partThree = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes)
            
            let combination = NSMutableAttributedString()
            
            combination.append(partOne)
            combination.append(partTwo)
            combination.append(partThree)
            return combination
            break;
        case ".mention":
            let font = UIFont(name: "Helvetica", size: 14)
            let font1 = UIFont(name: "HelveticaNeue-Bold", size: 14)

            let attributes = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: UIColor.black]
            let attributes1 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.black]
            let attributes2 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.gray]

            let yourAttributes = attributes
            let yourOtherAttributes = attributes1
            let bunchTitleAttributes = attributes1
            let timeStampAttributes = attributes2

            let messageString = NotificationsArray[row].notification_string
            let bunchTitle = NotificationsArray[row].notification_bunch_title
            let notifUserName = NotificationsArray[row].notification_user_name
            let notifTimeStamp = NotificationsArray[row].notification_time_stamp.getTimeStamp(interval: String(NotificationsArray[row].notification_time_stamp))

            let partOne = NSMutableAttributedString(string: notifUserName!, attributes: yourOtherAttributes)
            let partTwo = NSMutableAttributedString(string: " mentioned you in the group chat: ", attributes: yourAttributes)
            let partThree = NSMutableAttributedString(string: "\(bunchTitle!) ", attributes: bunchTitleAttributes)
            let partFour = NSMutableAttributedString(string: "\"\(messageString!)\" ", attributes: yourAttributes)
            let partFive = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes)

            let combination = NSMutableAttributedString()

            combination.append(partOne)
            combination.append(partTwo)
            combination.append(partThree)
            combination.append(partFour)
            combination.append(partFive)
            
            return combination
            break;
        case ".joinedrequested":
            //if users joined or requested to join your bunch
                let font = UIFont(name: "Helvetica", size: 14)
                let font1 = UIFont(name: "HelveticaNeue-Bold", size: 14)
                
                let attributes = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: UIColor.black]
                let attributes1 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.black]
                let attributes2 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.gray]
                
                let usernameBold = attributes1
                let regularAttributes = attributes
                let bunchTitleAttributes = attributes1
                let timeStampAttributes = attributes2
                
                let notifUserName = NotificationsArray[row].notification_user_name
                let messageString = NotificationsArray[row].notification_string
                let bunchTitle = NotificationsArray[row].notification_bunch_title
                let notifTimeStamp = NotificationsArray[row].notification_time_stamp.getTimeStamp(interval: String(NotificationsArray[row].notification_time_stamp))
                
                let partOne = NSMutableAttributedString(string: notifUserName!, attributes: usernameBold)
                let partTwo = NSMutableAttributedString(string: "\(messageString!): ", attributes: regularAttributes)
                let partThree = NSMutableAttributedString(string: "\(bunchTitle!) ", attributes: bunchTitleAttributes)
                let partFour = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes)
                
                let combination = NSMutableAttributedString()
                
                combination.append(partOne)
                combination.append(partTwo)
                combination.append(partThree)
                combination.append(partFour)
                
                return combination
            break;
             case ".accepted":
            let font = UIFont(name: "Helvetica", size: 14)
            let font1 = UIFont(name: "HelveticaNeue-Bold", size: 14)
            
            let attributes = [NSAttributedStringKey.font: font, NSAttributedStringKey.foregroundColor: UIColor.black]
            let attributes1 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.black]
            let attributes2 = [NSAttributedStringKey.font: font1, NSAttributedStringKey.foregroundColor: UIColor.gray]
            
            let usernameBold = attributes1
            let regularAttributes = attributes
            let bunchTitleAttributes = attributes1
            let timeStampAttributes = attributes2
            
            let notifUserName = NotificationsArray[row].notification_user_name
            let messageString = NotificationsArray[row].notification_string
            let bunchTitle = NotificationsArray[row].notification_bunch_title
            let notifTimeStamp = NotificationsArray[row].notification_time_stamp.getTimeStamp(interval: String(NotificationsArray[row].notification_time_stamp))
            
            let partOne = NSMutableAttributedString(string: notifUserName!, attributes: usernameBold)
            let partTwo = NSMutableAttributedString(string: " \(messageString!)", attributes: regularAttributes)
            let partThree = NSMutableAttributedString(string: "\(bunchTitle!) ", attributes: bunchTitleAttributes)
            let partFour = NSMutableAttributedString(string: notifTimeStamp, attributes: timeStampAttributes)
            
            let combination = NSMutableAttributedString()
            
            combination.append(partOne)
            combination.append(partTwo)
            combination.append(partThree)
            combination.append(partFour)
            
            return combination
            break;
        default:
            print("hit default statment in notification Table Switch")
            //default Return
            let defualtNotificationString = "Notification was lost in the world wide web...we're tryin to find it!"
            let defaultFont = UIFont(name: "Metropolis-Medium", size: 16)
            let attributes = [NSAttributedStringKey.font: defaultFont, NSAttributedStringKey.foregroundColor: UIColor.black]
            let partTwo = NSMutableAttributedString(string: defualtNotificationString, attributes: attributes)
            let combination = NSMutableAttributedString()
            combination.append(partTwo)
            return combination
            
        }
        //default Return
        let defualtNotificationString = "Notification was lost in the world wide web...we're tryin to find it!"
        let defaultFont = UIFont(name: "Metropolis-Medium", size: 16)
        let attributes = [NSAttributedStringKey.font: defaultFont, NSAttributedStringKey.foregroundColor: UIColor.black]
        let partTwo = NSMutableAttributedString(string: defualtNotificationString, attributes: attributes)
        let combination = NSMutableAttributedString()
        combination.append(partTwo)
        return combination
    }
    
    func notifImage(row: Int) -> UIImage {
        let notifCase = NotificationsArray[row].notification_type
        if notifCase == ".dm" {
            return UIImage(named: "notifications_tableview_dm")!
        } else if notifCase == ".mention" {
            return UIImage(named: "notifications_tableview_mention")!
        } else {
            return UIImage(named: "notifications_tableview_checkmark")!
        }
        return UIImage(named: "profile_placeholder")!
    }
    
    func fetchDmObjects() {
        let fetchRequest: NSFetchRequest<CdNotification> = CdNotification.fetchRequest()
        do {
            let coreDataNotifications = try PersistanceService.context.fetch(fetchRequest)
            self.NotificationsArray = coreDataNotifications
            
            if self.NotificationsArray.count > 0 {
                self.tableView.reloadData()
            }
        } catch {
            print("error fetching")
        }
    }
}
