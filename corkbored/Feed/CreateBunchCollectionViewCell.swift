//
//  CreateBunchCollectionViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 9/25/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class CreateBunchCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var selectedDot: UIView!
}
