//
//  FeedViewControllerTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 12/15/17.
//  Copyright © 2017 Jackson Meyer. All rights reserved.
//

import UIKit

class FeedViewControllerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profilePhotoImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var timeSincePostedLabel: UILabel!
    @IBOutlet weak var eventDistance: UILabel!
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var descriptionIntro: UILabel!
    @IBOutlet weak var eventTimeLabel: UILabel!
    @IBOutlet weak var eventEmojiLabelView: UIView!
    @IBOutlet weak var eventEmojiLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

