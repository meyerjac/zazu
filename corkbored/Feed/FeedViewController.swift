//
//  FeedViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 6/26/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import CoreLocation
import CRRefresh
import DZNEmptyDataSet
import CoreData
import PulsingHalo

public var createdEvents: Array = [Event]()

class FeedViewController: UIViewController, CLLocationManagerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, UITableViewDelegate, UITableViewDataSource,  UITableViewDataSourcePrefetching, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //MARK: - Variables
    var eventsRef: DatabaseReference!
    
    //Location Variables
    var locationManager = CLLocationManager()
    var geocoder = CLGeocoder()
    var userLocation: CLLocation = CLLocation()
    var city: String!
    var state: String!
    var country: String!
    
    //Persistant Data - Passing data
    var currentUserCoreData = [Person]()
    var events = [Event]()
    var sortedEvents: Array = [[Event]()]
    var coreDataMessageContainer: [[Message]] = []
    var userMessageArray = [Message]()
    var coreDataDmObjectProfileUrl = ""
    var messageBody: String = ""
    var profilePhotoFileName: String = ""
    var currentState: String = ""
    var allMessageProfileUids = [String]()
    var clickedProfilePicOwnerUid = ""
    
    //???
    var selectedEvent: Event!
    var selectedEventHostPicture: UIImage?
    
    //IMAGES/LOADING
    var loadingImageView: UIImageView!
    var haloLayer: PulsingHaloLayer!
    var haloLayer2: PulsingHaloLayer!
    var animationTimer: Timer?
    var loadingView = UIView()
    fileprivate var tasks = [URLSessionTask]()
    
    //FETCHING EVENTS
    var numberOfEventsToFetch: Int = 10
    var selectedCategoryTag: Int = 0
    var newEventCategoryIndexesForCollectionNewImageView = [Int]()
    var animationInitialLoad = true
    var numberOfNewEvents: Int = 0
    var animationCounter: Int = 0
    var loadingViewisGone: Int = 0
    var currentlyFetching: Bool = false
    var initialEventsFetched: Bool = false
    let categoryLabelTitles = Constants.feedViewControllerCategoryTitles
    let categoryImageTitles = Constants.feedViewControllerCategoryImages

    //MARK: Outlets
    @IBOutlet weak var entireView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var TopView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - VDL
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handleViews()
        getCoreData()
        startLoadingAnimation()
        fetchUserLocation()
        print("FETCH FROM VDL")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
    }

    override func viewWillAppear(_ animated: Bool) {
        HandleNewMessageIcon()
        HandleNewNotifications()
        
        if createdEvents.count != 0 {
            guard let bnchIndex = self.categoryLabelTitles.index(of: createdEvents[0].category) else { return }

            //ADD TO SPECIFIC CATEGORY
            if !self.sortedEvents[bnchIndex].contains(where: {$0.uid == createdEvents[0].uid}) {
                self.sortedEvents[bnchIndex].insert(createdEvents[0], at: self.sortedEvents[bnchIndex].startIndex)
                
                //UPDATE SPECIFIC CATEGORY NUMBER
                if Constants.FeedViewControllerEventCategoryDictionary[createdEvents[0].category] == nil {
                    Constants.FeedViewControllerEventCategoryDictionary[createdEvents[0].category] = 1
                } else {
                    if let oldEventCount =  Constants.FeedViewControllerEventCategoryDictionary[createdEvents[0].category] {
                        Constants.FeedViewControllerEventCategoryDictionary[createdEvents[0].category] = oldEventCount + 1
                    }
                }
            }
            
            //ADD TO ALL
            if !self.sortedEvents[0].contains(where: {$0.uid == createdEvents[0].uid}) {
                self.sortedEvents[0].insert(createdEvents[0], at: self.sortedEvents[0].startIndex)
                
                //UPDATE ALL ALL
                if Constants.FeedViewControllerEventCategoryDictionary["all"] == nil {
                    Constants.FeedViewControllerEventCategoryDictionary["all"] = 1
                } else {
                    if let oldEventCount =  Constants.FeedViewControllerEventCategoryDictionary["all"] {
                        Constants.FeedViewControllerEventCategoryDictionary["all"] = oldEventCount + 1
                    }
                }
            }
            
            DispatchQueue.main.async {
                //remove created event so when another gets added it will add only newest
                createdEvents.removeAll()
                self.tableView.reloadData()
                self.collectionView.reloadData()
            }
        }
        
        
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            currentUserCoreData = peep
        } catch { () }
    }
    
    override func viewDidDisappear(_ animated: Bool) { NotificationCenter.default.removeObserver(self) }
    
    override var prefersStatusBarHidden: Bool {
        if loadingViewisGone > 1  {
            return false
        }
        self.loadingViewisGone += 1
        return true
    }
    
    //MARK: - COLLECTIONVIEW
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryLabelTitles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let oldSelectedIndexPath = IndexPath(row: self.selectedCategoryTag, section: 0)
        //if click currently selected category
        if oldSelectedIndexPath == indexPath { return }
        
        //set new
        self.selectedCategoryTag = indexPath.row
        let currentCategoryIndexPath = IndexPath(row: indexPath.row, section: 0)
        
        //remove new event sticker logic
        let substractingArray = [indexPath.row]
        self.newEventCategoryIndexesForCollectionNewImageView = Array(Set(self.newEventCategoryIndexesForCollectionNewImageView).subtracting(substractingArray))

        //OLD SELECTED INDEX PATH
        let cell = collectionView.cellForItem(at: oldSelectedIndexPath) as? CategoryCollectionViewCell
        cell?.containerView.layer.borderColor = UIColor.gray.cgColor
        cell?.containerView.layer.borderWidth = 2
        cell?.categoryLabel.textColor = UIColor.gray
        cell?.selectedDot.isHidden = true
   
        //NEW SELECTED INDEX PATH
        let cell1 = collectionView.cellForItem(at: indexPath) as? CategoryCollectionViewCell
        cell1?.containerView.layer.borderColor = UIColor(red:0.51, green:0.79, blue:1.00, alpha:1.0).cgColor
        cell1?.containerView.layer.borderWidth = 2
        cell1?.categoryLabel.textColor = UIColor.black
        cell1?.selectedDot.isHidden = false
        cell1?.newImageView.isHidden = true
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
            let indexPath = IndexPath(item: self.selectedCategoryTag, section: 0)
            collectionView.reloadItems(at: [indexPath])
        }
        
        if !self.currentlyFetching {
            if ((self.selectedCategoryTag != 0) && (self.selectedCategoryTag != 1)) {
                handleFetchingFromClickingCategoryCell(indexPath: currentCategoryIndexPath, completion: { (success) -> Void in
                    if success {
                        self.currentlyFetching = false
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            self.collectionView.reloadData()
                        }
                    }
                })
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath as IndexPath) as! CategoryCollectionViewCell
        
            //text
            cell.categoryLabel.text = categoryLabelTitles[indexPath.row]
            cell.categoryLabel.tag = indexPath.row
        
            //photo
            cell.categoryImageView.image = UIImage(named: categoryImageTitles[indexPath.row])
            cell.categoryImageView.layer.masksToBounds = true
            cell.categoryImageView.layer.cornerRadius = cell.categoryImageView.frame.size.height/2
    
            cell.layoutIfNeeded()
            cell.containerView.layer.cornerRadius = cell.containerView.frame.size.height/2
            cell.containerView.layer.masksToBounds = true
            cell.containerView.layer.borderWidth = 2
        
            //number of events in the city
            let index = indexPath.row
            let categoryName = categoryLabelTitles[index]
        
            //FOR ALL CATEGORIES
            cell.numberOfBunchesInCategoryLabel.text = "0"
            if let tallyValue = Constants.FeedViewControllerEventCategoryDictionary[categoryName] {
                cell.numberOfBunchesInCategoryLabel.text = String(tallyValue)
            }
        
            cell.numberOfBunchesLabelView.layer.masksToBounds = true
            cell.numberOfBunchesLabelView.layer.cornerRadius = 5
        
            cell.selectedDot.tag = indexPath.row
            cell.selectedDot.layer.cornerRadius = cell.frame.size.height * (0.06 / 2)
            cell.selectedDot.clipsToBounds = true
        
        if indexPath.row == selectedCategoryTag {
             cell.categoryLabel.textColor = UIColor.black
             cell.newImageView.isHidden = true
             cell.containerView.layer.borderColor = UIColor(red:0.51, green:0.79, blue:1.00, alpha:1.0).cgColor
             cell.selectedDot.isHidden = false
        } else {
             cell.categoryLabel.textColor = UIColor.gray
             cell.newImageView.tag = indexPath.row
             cell.containerView.layer.borderColor = UIColor.gray.cgColor
             cell.containerView.tag = indexPath.row
             cell.selectedDot.isHidden = true
            
            if self.newEventCategoryIndexesForCollectionNewImageView.contains(indexPath.row) {
                cell.newImageView.isHidden = false
            } else {
                cell.newImageView.isHidden = true
            }
        }
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let kWhateverHeightYouWant = collectionView.frame.size.height
        return CGSize(width: collectionView.frame.size.height * 0.95, height: CGFloat(kWhateverHeightYouWant))
    }
    
    //MARK: - loading screen animation
    func startLoadingAnimation() {
        let imageName = "iconW"
        let image = UIImage(named: imageName)
        loadingImageView = UIImageView(image: image!)
        loadingImageView.contentMode = .scaleAspectFit
        loadingImageView.frame.size = CGSize(width: 50, height: 64)
        loadingImageView.center = super.view.center
        self.loadingView.addSubview(loadingImageView)

        let loadingPulseLayer = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        loadingPulseLayer.duration = 0.75
        loadingPulseLayer.fromValue = 0.3
        loadingPulseLayer.toValue = 1
        loadingPulseLayer.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        loadingPulseLayer.autoreverses = true
        loadingPulseLayer.repeatCount = .greatestFiniteMagnitude
        loadingImageView.layer.add(loadingPulseLayer, forKey: "animateOpacity")

        self.haloLayer = PulsingHaloLayer()
        self.haloLayer.position = self.view.center
        self.haloLayer.haloLayerNumber = 2
        self.haloLayer.radius = self.view.frame.height
        self.haloLayer.backgroundColor = UIColor.white.cgColor
        loadingView.layer.addSublayer(self.haloLayer)
        self.haloLayer.keyTimeForHalfOpacity = 0.15
        self.haloLayer.start()
       
        let mainWindow = UIApplication.shared.keyWindow!
        self.loadingView.frame = CGRect(x: mainWindow.frame.origin.x, y: mainWindow.frame.origin.y, width: mainWindow.frame.width, height: mainWindow.frame.height)
        self.loadingView.backgroundColor = UIColor(red:0.51, green:0.79, blue:1.00, alpha:1.0)
        self.loadingView.layer.cornerRadius = 10
        self.loadingView.clipsToBounds = true
        mainWindow.addSubview(loadingView)
    }
    
    func stopLoadingAnimation() {
        self.animationTimer?.invalidate()
        self.animationInitialLoad = false
        UIView.animate(withDuration: 0.15, delay: 0, options: .curveEaseIn, animations: {
            self.loadingView.alpha = 0
            self.setNeedsStatusBarAppearanceUpdate()
            self.navigationController?.isNavigationBarHidden = false
            self.tabBarController?.tabBar.isHidden = false
        }) { _ in
            self.loadingView.removeFromSuperview()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height

        if offsetY > contentHeight - scrollView.frame.height {
            let currentCategoryTitle = Constants.feedViewControllerCategoryTitles[selectedCategoryTag]
            if let eventCatNum = Constants.FeedViewControllerEventCategoryDictionary[currentCategoryTitle] {
                if (!currentlyFetching && (self.sortedEvents[self.selectedCategoryTag].count != eventCatNum) && eventCatNum > 8) {
                    fetchMore(completion: { (success) -> Void in
                        if success {
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                                self.currentlyFetching = false
                            }
                        }
                    })
                }
            }
        }
    }
    
    func fetchMore(completion: @escaping (Bool) -> ()) {
        self.currentlyFetching = true
        guard let city = self.city else { completion(true); return }
        guard let state = self.state else { completion(true); return }
        guard let country = self.country else { completion(true); return }

        var newEvents = [Event]()

        if selectedCategoryTag == 0 {
            self.eventsRef = Database.database().reference().child("EVENTS/\(country)/\(state)/\(city)/EVENTS_ACTIVE")
            self.eventsRef.queryOrderedByKey().queryStarting(atValue: self.sortedEvents[0][sortedEvents[0].count - 2].uid).queryLimited(toFirst: 15).observe(.value) { (snapshot) in
                
                if (snapshot.value as? [AnyHashable: AnyObject]) != nil {
                    var snapCount: Int = 0
                    for item in snapshot.children.allObjects as! [DataSnapshot] {
                        let event = Event(snapshot: item)
                        
                        if !self.sortedEvents[0].contains(where: {$0.uid == event.uid}) {
                            newEvents.insert(event, at: newEvents.endIndex)
                        }
                        snapCount += 1

                        if snapCount == snapshot.children.allObjects.count {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) {
                                self.sortedEvents[0] = self.sortedEvents[0] + newEvents
                                completion(true)
                            }
                        }
                    }
                } else {
                    completion(true)
                }
            }
        } else {
            var startingValue: String?
            var endingValue: String?
            
            startingValue = String(self.sortedEvents[selectedCategoryTag][sortedEvents[selectedCategoryTag].count - 2].category_endInterval)
            endingValue = self.categoryLabelTitles[selectedCategoryTag] + "_" + String(getCurrentTimeAsEpoch() + 604800)
            
            self.eventsRef = Database.database().reference().child("EVENTS/\(country)/\(state)/\(city)/EVENTS_ACTIVE")
            self.eventsRef.queryOrdered(byChild: "category_endInterval").queryStarting(atValue: startingValue).queryEnding(atValue: endingValue).queryLimited(toFirst: 15).observeSingleEvent(of: .value) { (snapshot) in
                
                if (snapshot.value as? [AnyHashable: AnyObject]) != nil {
                    var snapCount: Int = 0
                    for item in snapshot.children.allObjects as! [DataSnapshot] {
                        let event = Event(snapshot: item)
                        if !self.sortedEvents[self.selectedCategoryTag].contains(where: {$0.uid == event.uid}) {
                            newEvents.insert(event, at: newEvents.endIndex)
                        }
                        snapCount += 1
                        if snapCount == snapshot.children.allObjects.count {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.50) {
                                self.sortedEvents[self.selectedCategoryTag] = self.sortedEvents[self.selectedCategoryTag] + newEvents
                                completion(true)
                            }
                        }
                    }
                } else {
                    completion(true)
                }
            }
        }
    }
    
    //MARK: - TABLE VIEW
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 234
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        
        
        
        
        
        
        
        
        if let lastVisibleIndexPath = tableView.indexPathsForVisibleRows?.last {
            if indexPath == lastVisibleIndexPath {
                self.animationCounter = 0
                if self.animationTimer == nil {
                    if self.animationInitialLoad {
                        self.animationTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
                    }
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedEvents[selectedCategoryTag].count + 1
    }
    
    func getDistanceInMiles(eventLocation: CLLocation, userLocation: CLLocation) -> String {
        let distanceInMeters = eventLocation.distance(from: userLocation)
        let y = Double(round(10*(distanceInMeters/1609.344))/10)
        return String(y)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < sortedEvents[selectedCategoryTag].count {
            //EVENT CELLS
            if let cell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as? FeedViewControllerTableViewCell {
                let event = sortedEvents[selectedCategoryTag][indexPath.row]
                
                let eventCoord = CLLocation(latitude: event.lat, longitude: event.long)
                let userCoord = CLLocation(latitude: self.userLocation.coordinate.latitude, longitude: self.userLocation.coordinate.longitude)
                
                cell.eventDistance.text = getDistanceInMiles(eventLocation: eventCoord, userLocation: userCoord) + " miles"
                
                //setting tag for profile image and username
                let TapGesture = UITapGestureRecognizer(target: self, action: #selector(handleProfile))
                cell.profilePhotoImageView.tag = indexPath.row
                cell.profilePhotoImageView.addGestureRecognizer(TapGesture)
                cell.profilePhotoImageView.isUserInteractionEnabled = true
                cell.usernameLabel.text = event.owner_name
                
                if let image = sortedEvents[selectedCategoryTag][indexPath.row].image {
                    cell.profilePhotoImageView.image = image
                } else {
                    cell.profilePhotoImageView.image = nil
                    self.downloadImage(forItemAtIndex: indexPath.row)
                }
                
                cell.profilePhotoImageView?.layer.cornerRadius = 10
                cell.profilePhotoImageView?.clipsToBounds = true
                cell.timeSincePostedLabel.text = event.interval_reference.getTimeStamp(interval: event.interval_reference) + " ago"
                
                cell.eventTitleLabel.text = event.title
                cell.descriptionIntro.text = event.description
                cell.eventTimeLabel.text = event.start_time.getReadableDate(time: event.start_time)
                cell.eventEmojiLabel.text = event.emojis
                
                cell.eventEmojiLabelView.layer.cornerRadius =  cell.eventEmojiLabelView.frame.size.height / 2
                cell.eventEmojiLabelView.clipsToBounds = true
                cell.eventEmojiLabelView.layer.borderColor = UIColor(named: "zazuBlue")?.cgColor
                cell.eventEmojiLabelView.layer.borderWidth = 1
                
                return cell
            }
            return UITableViewCell()
        } else {
            //CELL IS LOADING CELL
            if let cell = tableView.dequeueReusableCell(withIdentifier: "feedLoadingCell", for: indexPath) as? FeedLoadingTableViewCell {
                let currentCategoryTitle = Constants.feedViewControllerCategoryTitles[selectedCategoryTag]
                
                if let eventCatNum = Constants.FeedViewControllerEventCategoryDictionary[currentCategoryTitle] {
                    if self.sortedEvents[self.selectedCategoryTag].count == eventCatNum {
                        cell.noMorePostsLabel.text = "no more events"
                        cell.loadingIndicator.isHidden = true
                        cell.loadingIndicator.stopAnimating()
                        cell.noMorePostsLabel.isHidden = false
                        if eventCatNum == 0 {
                            cell.noMorePostsLabel.text = "no events, host one?"
                            cell.loadingIndicator.isHidden = true
                            cell.loadingIndicator.stopAnimating()
                            cell.noMorePostsLabel.isHidden = false
                        }
                    } else {
                        cell.loadingIndicator.isHidden = false
                        cell.loadingIndicator.startAnimating()
                        cell.noMorePostsLabel.isHidden = true
                    }
                }
                return cell
            }
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != self.sortedEvents[selectedCategoryTag].count {
            selectedEvent = self.sortedEvents[selectedCategoryTag][indexPath.row]
            let cell = tableView.cellForRow(at: indexPath) as! FeedViewControllerTableViewCell
            selectedEventHostPicture = cell.profilePhotoImageView?.image
            
            self.performSegue(withIdentifier: "toDetails", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { self.downloadImage(forItemAtIndex: $0.row) }
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
        //        indexPaths.forEach { self.cancelDownloadingImage(forItemAtIndex: $0.row) }
        //        print("cancel")
    }
    
    //MARK: - LOCATION
    @objc func fetchUserLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
   func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            if self.initialEventsFetched { return }
            let ref = Database.database().reference()
            self.userLocation = locations[0]
            geocoder.reverseGeocodeLocation(userLocation,
            completionHandler: { (placemarks, error) in
            if error == nil {
                if let uid = Auth.auth().currentUser?.uid {
                    if let location = placemarks?[0] {
                        //STATE
                        if let state = location.administrativeArea, !state.isEmpty {
                            if let city = location.locality, !city.isEmpty {
                                if let countryCode = location.isoCountryCode, !countryCode.isEmpty {
                                self.country = countryCode
                                self.state = state
                                self.city = city
                                self.navigationController?.navigationBar.topItem?.title = city
                                
                                self.currentUserCoreData[0].setValue(countryCode, forKey: "location_country")
                                self.currentUserCoreData[0].setValue(state, forKey: "location_state")
                                self.currentUserCoreData[0].setValue(city, forKey: "location_city")
                                PersistanceService.saveContext()
                                ref.child("USERS/\(uid)/country").setValue(self.country)
                                ref.child("USERS/\(uid)/state").setValue(state)
                                ref.child("USERS/\(uid)/city").setValue(city)
                                let validLocationsRef = Database.database().reference().child("VALID_LOCATIONS/\(countryCode)/\(state)")
                                validLocationsRef.observeSingleEvent(of: .value) { (snapshot) in
                                    if let validLocationsArray = snapshot.value as? NSArray {
                                        if validLocationsArray.contains(city) {
                                            self.fetchInProgressEvents(country: self.country, state: self.state, city: self.city, completion: { (success) -> Void in
                                                if success { // this will be equal to whatever value is set in this method call
                                                    self.fetchCategoryNumbers(country: self.country, state: self.state, city: self.city, completion: { (success) -> Void in
                                                        if success {
                                                            DispatchQueue.main.async {
                                                                self.collectionView.reloadData()
                                                            }
                                                            self.fetchInitialEvents(country: self.country, state: self.state, city: self.city, completion: { (success) -> Void in
                                                                if success {
                                                                    self.currentlyFetching = false
                                                                    self.initialEventsFetched = true
                                                                
                                                                    DispatchQueue.main.async {
                                                                        self.tableView.reloadData()
                                                                        self.collectionView.reloadData()
                                                                    }
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        } else {
                                            self.showNotLiveInCityAlertFeed(subtitle: "Zazu isn't active in \(city) yet, we're coming soon and we'll email you", title: "You are amazing...", buttonTitle: "got it")
                                            self.saveLocationToDb()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        })
    }
    
    func saveLocationToDb() {
        //just assign users email to user city node
        if let uid = Auth.auth().currentUser?.uid {
            let dict = [uid: Auth.auth().currentUser?.email]
            let ref = Database.database().reference()
            ref.child("EXTRAS/REQUESTED_LOCATIONS/\(self.country!)/\(self.state!)/\(self.city!)/\(uid)").setValue(dict)
        }
    }
    
    @objc func dismissLocationNotLiveAlert() {
       //do nothing.
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //must have
    }
    
    //MARK: - EVENTS
    func fetchCategoryNumbers(country: String, state: String, city: String, completion: @escaping (Bool) -> ()) {
        var snapCount = 0
        Database.database().reference().child("EVENTS/\(country)/\(state)/\(city)/EVENTS_TALLY").observeSingleEvent(of: .value, with: { (snapshot) in
            if let snapshot = snapshot.children.allObjects as? [DataSnapshot] {
                for snap in snapshot {
                if let value = snap.value as? Int {
                    Constants.FeedViewControllerEventCategoryDictionary[snap.key] = value
                    if let all = Constants.FeedViewControllerEventCategoryDictionary["all"] {
                            Constants.FeedViewControllerEventCategoryDictionary["all"] = value + all
                        } else {
                             Constants.FeedViewControllerEventCategoryDictionary["all"] = value
                            }
                        }
                        snapCount += 1
                    
                        if snapshot.count == snapCount {
                        //handle loading in 0 for nil in the dictionary.
                        
                            
                            
                            
                        completion(true)
                        }
                    }
                }
            })
        }
    
    func fetchInProgressEvents(country: String, state: String, city: String, completion: @escaping (Bool) -> ()) {
        let inProgressRef = Database.database().reference().child("EVENTS/\(country)/\(state)/\(city)/EVENTS_ACTIVE")
        inProgressRef.queryOrdered(byChild: "end_interval").queryEnding(atValue: self.getCurrentTimeAsEpoch()).observeSingleEvent(of: .value) { (snapshot) in
            if (snapshot.value as? [AnyHashable: AnyObject]) != nil {
                for item in snapshot.children.allObjects as! [DataSnapshot] {
                    Constants.FeedViewControllerEventCategoryDictionary["in progress"] = snapshot.children.allObjects.count
                    
                    let event = Event(snapshot: item)
                    
                    if !self.eventIsAlreadyloadedInCategory(eventUid: event.uid, category: 1) {
                        self.sortedEvents[1].insert(event, at: self.sortedEvents[1].endIndex)
                    }
                }
                completion(true)
            } else {
                Constants.FeedViewControllerEventCategoryDictionary["in progress"] = 0
                completion(true)
            }
        }
    }
    
    
    func eventIsAlreadyloadedInCategory(eventUid: String, category: Int) -> Bool {
            if self.sortedEvents[category].contains(where: {$0.uid == eventUid}) {
                return true
            } else {
                return false
            }
    }
    
    func fetchInitialEvents(country: String, state: String, city: String, completion: @escaping (Bool) -> ()) {
        self.setNewEventsListener()
        var initialFetchChildrenCount = 0
        
        self.eventsRef = Database.database().reference().child("EVENTS/\(country)/\(state)/\(city)/EVENTS_ACTIVE")
        self.eventsRef.queryOrdered(byChild: "end_interval").queryStarting(atValue: self.getCurrentTimeAsEpoch()).queryLimited(toFirst: UInt(self.numberOfEventsToFetch)).observeSingleEvent(of: .value) { (snapshot) in
            //If there are no Events under EVENTS_ACTIVE
            if !snapshot.exists() {
                self.tableView.reloadData()
                self.stopLoadingAnimation()
                completion(true)
                return
            }
            
            if (snapshot.value as? [AnyHashable: AnyObject]) != nil {
                if (Auth.auth().currentUser?.uid) != nil {
                    for item in snapshot.children.allObjects as! [DataSnapshot] {
                        initialFetchChildrenCount += 1

                
                        let event = Event(snapshot: item)
                        
                        if !self.eventIsAlreadyloadedInCategory(eventUid: event.uid, category: 0) {
                            self.sortedEvents[0].insert(event, at: self.sortedEvents[0].endIndex)
                        }
                        
                        if snapshot.children.allObjects.count == initialFetchChildrenCount {
                            //if event number is less than the number fetched
                            if let numOfAllEvents = Constants.FeedViewControllerEventCategoryDictionary["all"] {
                                if numOfAllEvents < 10 {
                                    if snapshot.children.allObjects.count < self.numberOfEventsToFetch {
                                        Constants.FeedViewControllerEventCategoryDictionary["all"] = snapshot.children.allObjects.count
                                    }
                                }
                            }
                            completion(true)
                        }
                    }
                }
            }
        }
    }
    
    func setNewEventsListener() {
        self.eventsRef = Database.database().reference().child("EVENTS/\(country!)/\(state!)/\(city!)/EVENTS_ACTIVE")
        self.eventsRef.queryLimited(toLast: 1).observe(.childAdded) { (snapshot) in
            let event = Event(snapshot: snapshot)
            if let uid = Auth.auth().currentUser?.uid {
                guard let bnchIndex = self.categoryLabelTitles.index(of: event.category) else { return }
                if !self.initialEventsFetched { return }
                
                //user created, don't add, already handled logic on create event
                if !self.sortedEvents[bnchIndex].contains(where: {$0.uid == event.uid}) {
                    self.sortedEvents[bnchIndex].insert(event, at: self.sortedEvents[bnchIndex].startIndex)
                    //update number of events in specific category
                    if let oldEventCount =  Constants.FeedViewControllerEventCategoryDictionary[event.category] {
                        Constants.FeedViewControllerEventCategoryDictionary[event.category] = oldEventCount + 1
                    }
                }
                
                if !self.eventIsAlreadyloadedInCategory(eventUid: event.uid, category: 0) {
                    self.sortedEvents[0].insert(event, at: self.sortedEvents[0].startIndex)
                    
                    if let oldEventCount = Constants.FeedViewControllerEventCategoryDictionary["all"] {
                        Constants.FeedViewControllerEventCategoryDictionary["all"] = oldEventCount + 1
                    }
                }
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.collectionView.reloadData()
                }
                
                if event.owner_uid == uid {
                    //your own post, immediatly loaded
                    return
                } else {
                    //not current users post, new event view
                    let bnchIndex = self.categoryLabelTitles.index(of: event.category)
                    self.newEventCategoryIndexesForCollectionNewImageView.append(bnchIndex!)
                    
                    self.numberOfNewEvents += 1
                    self.showNewEventAlertView()
                }
            }
        }
    }
    
    func handleFetchingFromClickingCategoryCell(indexPath: IndexPath, completion: @escaping (Bool) -> ()) {
        self.currentlyFetching = true
        let eventsLoadedInCategory = self.sortedEvents[indexPath.row].count
     
        if eventsLoadedInCategory >= self.numberOfEventsToFetch {
            //NO MORE EVENTS TO LOAD FROM CLICK ON, HANDLE WITH SCROLL FUNCTIONALITY
            completion(true)
            return
        } else {
            if let cell = self.collectionView.cellForItem(at: indexPath) as? CategoryCollectionViewCell {
                if let eventLabelNum = cell.numberOfBunchesInCategoryLabel.text {
                    guard let city = self.city else { return }
                    guard let state = self.state else { return }
                    guard let country = self.country else { return }
                    
                    if (eventsLoadedInCategory == (Int(eventLabelNum))) {
                         //NO MORE EVENTS TO LOAD
                        completion(true)
                        
                        } else {
                        //MORE EVENTS TO LOAD FOR INDEX PATHS
                        var startingValue: String?
                        var endingValue: String?
                        
                        startingValue = self.categoryLabelTitles[selectedCategoryTag] + "_" + String(getCurrentTimeAsEpoch())
                        endingValue = self.categoryLabelTitles[selectedCategoryTag] + "_" + String(getCurrentTimeAsEpoch() + 604800)
                        
                        self.eventsRef = Database.database().reference().child("EVENTS/\(country)/\(state)/\(city)/EVENTS_ACTIVE")
                        self.eventsRef.queryOrdered(byChild: "category_endInterval").queryStarting(atValue: startingValue).queryEnding(atValue: endingValue).queryLimited(toFirst: UInt(self.numberOfEventsToFetch)).observeSingleEvent(of: .value) { (snapshot) in
                          
                            for child in snapshot.children.allObjects as! [DataSnapshot] {
                                let event = Event(snapshot: child)

                                if !self.sortedEvents[self.selectedCategoryTag].contains(where: {$0.uid == event.uid}) {
                                    if !self.sortedEvents[0].contains(where: {$0.uid == event.uid}) {
                                        //ADD TO ALL IF NOT ALREADY THERE
                                        self.sortedEvents[0].insert(event, at: self.sortedEvents[0].startIndex)
                                    }
                                    //ADD TO CATEGORY
                                     self.sortedEvents[self.selectedCategoryTag].insert(event, at: self.sortedEvents[self.selectedCategoryTag].endIndex)
                                }
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                                if snapshot.children.allObjects.count < self.numberOfEventsToFetch {
                                    //HANDLE LAG BETWEEN CRON AND ACTUAL VALID NUMBERS
                                    
                                    let currentCategoryTitle = Constants.feedViewControllerCategoryTitles[self.selectedCategoryTag]
                                    if Constants.FeedViewControllerEventCategoryDictionary[currentCategoryTitle] != nil {
                                        
                                        Constants.FeedViewControllerEventCategoryDictionary[currentCategoryTitle] = self.sortedEvents[self.selectedCategoryTag].count
                                    }
                                }
                                 completion(true)
                            }
                        }
                    }
                }
            }
        }
    }

    //MARK: - CORE DATA
    func getCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            currentUserCoreData = peep
            
            //error when logging back in
            let deviceTokenString = self.currentUserCoreData[0].device_token
            if deviceTokenString != nil {
                if let uid = Auth.auth().currentUser?.uid {
                    let profileRef = Database.database().reference().child("USERS/\(uid)/device_tokens")
                    profileRef.child(deviceTokenString!).setValue(true)
                }
            }
        } catch { () }
    }
    
    //MARK: - Functions
    func handleViews() {
        let topViewHieght = self.view.frame.size.height/7
        
        if topViewHieght > 100 {
            TopView.frame.size.height = 100
        } else {
            TopView.frame.size.height = topViewHieght
        }
        
        //instantiante sortedEvents with 19 array indexes
        let emptyEvent = [Event]()
        if sortedEvents.count < 19 {
            while sortedEvents.count < 19 {
                self.sortedEvents.insert(emptyEvent, at: (sortedEvents.count))
            }
        }
        self.selectedCategoryTag = 0
        
        entireView.layer.cornerRadius = 10
        entireView.layer.masksToBounds = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchUserLocation), name: UIApplication.willEnterForegroundNotification, object: UIApplication.shared)
        tableView.prefetchDataSource = self
        
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.tableView.estimatedSectionHeaderHeight = 0
        self.tableView.estimatedSectionFooterHeight = 0
        
        let tabBarHeight = CGFloat(49)
        edgesForExtendedLayout = UIRectEdge.all
        tableView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: tabBarHeight, right: 0.0)
        
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
        
        //NavBar items
        let menuBtn = UIButton(type: .custom)
        menuBtn.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        menuBtn.setImage(UIImage(named:"plus_icon"), for: .normal)
        menuBtn.setImage(UIImage(named:"plus_icon"), for: .highlighted)
        menuBtn.addTarget(self, action: #selector(createEvent), for: .touchUpInside)
        
        let menuBarItem = UIBarButtonItem(customView: menuBtn)
        let currWidth = menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 20)
        currWidth?.isActive = true
        let currHeight = menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 20)
        currHeight?.isActive = true
        self.navigationItem.leftBarButtonItem = menuBarItem
        
        
    }
    
    func HandleNewNotifications() {
        let defaults = UserDefaults.standard
        let newNotifications = defaults.bool(forKey: "newNotificationsBoolean")
        print(newNotifications, "NOTIFICAITONS")
        if newNotifications {
            let RedDotRadius: CGFloat = 3
            let RedDotDiameter = RedDotRadius * 2
            let TabBarItemCount = CGFloat(self.tabBarController!.tabBar.items!.count)
            let HalfItemWidth = (self.view.frame.width) / (TabBarItemCount * 2)
            let  xOffset = HalfItemWidth * CGFloat(3 * 2 + 1)
            let imageHalfWidth: CGFloat = (self.tabBarController!.tabBar.items![3]).selectedImage!.size.width / 2
            let BottomMagin:CGFloat = (imageHalfWidth * 2) + 12
            let redDot = UIView(frame: CGRect(x: xOffset - RedDotRadius, y: BottomMagin, width: RedDotDiameter, height: RedDotDiameter))
            
            redDot.tag = 1314
            redDot.backgroundColor = UIColor(named: "zazuPurple")
            redDot.layer.cornerRadius = RedDotRadius
            
            self.tabBarController?.tabBar.addSubview(redDot)
        }
    }
    
    func HandleNewMessageIcon() {
        let fetchRequest: NSFetchRequest<Dm> = Dm.fetchRequest()
        do {
            let coreDataMessages = try PersistanceService.context.fetch(fetchRequest)
            if coreDataMessages.contains(where: {$0.dm_any_messages_unread == true}) {
                let unreadArray = coreDataMessages.filter { $0.dm_any_messages_unread == true }
                let unreadArrayInt: Int = unreadArray.count
                let viewFN = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
                viewFN.tag = 0
                
                let newMessageView = UIView(frame: CGRect(x: 0, y: 0, width: 32, height: 32))
                newMessageView.backgroundColor = UIColor(named: "zazuPurple")
                newMessageView.clipsToBounds = true
                newMessageView.layer.cornerRadius = 16
                
                let button1 = UIButton(frame: CGRect(x: 0, y: 0, width: 18, height: 18))
                button1.setImage(UIImage(named:"send"), for: .normal)
                button1.setImage(UIImage(named:"send"), for: .highlighted)
                button1.addTarget(self, action: #selector(toMessages), for: .touchUpInside)
                newMessageView.addSubview(button1)
                button1.center = newMessageView.center
                
                //setting UILabel of # of Unreads.
                let numberOfMessages = UILabel(frame: CGRect(x: viewFN.frame.width - 16, y: 0, width: 16, height: 16))
                numberOfMessages.backgroundColor = UIColor.white
                numberOfMessages.clipsToBounds = true
                numberOfMessages.layer.cornerRadius = 8
                numberOfMessages.text = String(unreadArrayInt)
                numberOfMessages.textColor = UIColor(named: "zazuPurple")
                numberOfMessages.font = UIFont(name:"Metropolis-Medium", size:10)
                numberOfMessages.textAlignment = .center
                
                //adding subviews
                viewFN.addSubview(newMessageView)
                viewFN.addSubview(numberOfMessages)
                newMessageView.center = viewFN.center
                
                //adding to view
                let rightBarButton = UIBarButtonItem(customView: viewFN)
                self.navigationItem.rightBarButtonItem = rightBarButton
            } else {
                let viewFN = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
                viewFN.tag = 0
                viewFN.layer.masksToBounds = true
                viewFN.layer.cornerRadius = 8
                
                let button1 = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                button1.setImage(UIImage(named:"send"), for: .normal)
                button1.setImage(UIImage(named:"send"), for: .highlighted)
                button1.addTarget(self, action: #selector(toMessages), for: .touchUpInside)
                viewFN.addSubview(button1)
                button1.center = viewFN.center
                
                let rightBarButton = UIBarButtonItem(customView: viewFN)
                self.navigationItem.rightBarButtonItem = rightBarButton
            }
        } catch { () }
    }
    
    // MARK: - IMAGE DOWNLOADING
    fileprivate func downloadImage(forItemAtIndex index: Int) {
        if index != sortedEvents[selectedCategoryTag].count {
            
            let url = URL(string: sortedEvents[selectedCategoryTag][index].owner_url)
            
            let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
                // Perform UI changes only on main thread.
                DispatchQueue.main.async {
                    if let data = data, let image = UIImage(data: data) {
                        if index > (self.sortedEvents[self.selectedCategoryTag].count - 1) { return }
                        self.sortedEvents[self.selectedCategoryTag][index].image = image
                        
                        //ISSUE ON LAST LINE IN BLOCK throws error occasionally
                        // Reload cell with fade animation.
                        let indexPath = IndexPath(row: index, section: 0)
                        if self.tableView.indexPathsForVisibleRows?.contains(indexPath) ?? false {
                            if index > (self.sortedEvents[self.selectedCategoryTag].count - 1) { return }
                            self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                        }
                    }
                }
            }
            task.resume()
            tasks.append(task)
        }
    }
    
    fileprivate func cancelDownloadingImage(forItemAtIndex index: Int) {
        let url = URL(string: sortedEvents[selectedCategoryTag][index].owner_url)
        // Find a task with given URL, cancel it and delete from `tasks` array.
        guard let taskIndex = tasks.index(where: { $0.originalRequest?.url == url }) else {
            return
        }
        let task = tasks[taskIndex]
        task.cancel()
        tasks.remove(at: taskIndex)
    }
    
    //MARK: - Profile / segues / Alerts
    @objc func handleProfile(sender: UIGestureRecognizer) {
        clickedProfilePicOwnerUid = sortedEvents[selectedCategoryTag][(sender.view?.tag)!].owner_uid
        
        if let uid = Auth.auth().currentUser?.uid {
            if clickedProfilePicOwnerUid == uid {
                self.tabBarController?.selectedIndex = 1
            } else {
                let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "UsersProfileVC") as! UsersProfileViewController
                VC1.selectedUserUid = clickedProfilePicOwnerUid
                VC1.userName = sortedEvents[selectedCategoryTag][(sender.view?.tag)!].owner_name
                self.navigationController!.pushViewController(VC1, animated: true)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "toDetails") {
            let vc = segue.destination as! DetailsViewController
            vc.event = self.selectedEvent
            vc.city = self.city
            vc.isPrivate = self.selectedEvent.is_private
        }
    }
    
    func showNewEventAlertView() {
        self.view.addSubview(self.newEventAlertView)
    }
    
    @objc func showNewEvents(sender:UITapGestureRecognizer) {
        self.numberOfNewEvents = 0
        let subViews = self.view.subviews
        for subview in subViews{
            if subview.tag == 100 {
                UIView.animate(withDuration: 0.2, animations: {subview.alpha = 0.0},
                               completion: {(value: Bool) in
                                subview.removeFromSuperview()
                })
            }
        }
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.tableView.reloadData()
        }
    }
    
    @objc func closeButtonPressed(sender: UIButton!) {
        let subViews = self.view.subviews
        for subview in subViews{
            if subview.tag == 100 {
                UIView.animate(withDuration: 0.2, animations: {subview.alpha = 0.0},
                               completion: {(value: Bool) in
                                subview.removeFromSuperview()
                })
            }
        }
    }
    
    var newEventAlertView: UIView {
        let newMessageAlertView = UIView()
        newMessageAlertView.tag = 100
        let newMessageAlertViewHeight = CGFloat(40)
        newMessageAlertView.frame = CGRect(x: CGFloat(0), y: (self.navigationController?.navigationBar.frame.maxY)!, width: self.view.frame.width, height: newMessageAlertViewHeight)
        newMessageAlertView.backgroundColor = UIColor(named: "zazuPurple")
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.showNewEvents))
        newMessageAlertView.addGestureRecognizer(gesture)
        
        let alertLabel:UILabel = UILabel()
        alertLabel.frame = CGRect(x: 100, y: 100, width: 200, height: 200)
        alertLabel.textAlignment = .center
        if numberOfNewEvents > 1 {
            alertLabel.text = String(numberOfNewEvents) + " new events"
        } else {
            alertLabel.text = String(numberOfNewEvents) + " new event"
        }
        
        alertLabel.numberOfLines=1
        alertLabel.textColor = .white
        
        let font = UIFont(name: "Metropolis-Medium", size: 16)
        
        
        alertLabel.font = font
        
        newMessageAlertView.addSubview(alertLabel)
        alertLabel.translatesAutoresizingMaskIntoConstraints = false
        alertLabel.heightAnchor.constraint(equalToConstant: newMessageAlertViewHeight).isActive = true
        alertLabel.widthAnchor.constraint(equalToConstant: self.view.frame.width * 3 / 4).isActive = true
        alertLabel.centerXAnchor.constraint(equalTo: alertLabel.superview!.centerXAnchor).isActive = true
        alertLabel.centerYAnchor.constraint(equalTo: alertLabel.superview!.centerYAnchor).isActive = true
        
        let closeButton:UIButton = UIButton()
        closeButton.frame = CGRect(x: 100, y: 100, width: 15, height: 15)
        closeButton.setImage(#imageLiteral(resourceName: "new events alert close"), for: UIControl.State.normal)
        closeButton.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        
        newMessageAlertView.addSubview(closeButton)
        closeButton.translatesAutoresizingMaskIntoConstraints = false
        closeButton.heightAnchor.constraint(equalToConstant: 15).isActive = true
        closeButton.widthAnchor.constraint(equalToConstant: 15).isActive = true
        closeButton.rightAnchor.constraint(equalTo: alertLabel.superview!.rightAnchor, constant: -15).isActive = true
        closeButton.centerYAnchor.constraint(equalTo: alertLabel.superview!.centerYAnchor).isActive = true
        
        return newMessageAlertView
    }
    
    @objc func dismissAlert() { () }
    
    @objc func toMessages() {
        let viewController = storyboard?.instantiateViewController(withIdentifier: "DmsOverviewTableVC") as! DmsOverviewTableViewController
        viewController.hidesBottomBarWhenPushed = true
        if let nvc = tabBarController?.viewControllers?[0] as? UINavigationController {
            nvc.pushViewController(viewController, animated: true)
        }
    }
    
    @objc func createEvent() {
        let status = CLLocationManager.authorizationStatus()
        if status == .authorizedWhenInUse {
            self.performSegue(withIdentifier: "createEvent", sender: self)
        } else {
            //handle alert
            showNoEventsAlert(subtitle: "Looks like your location isn't turned on, we would love to show you events near you (parties, game nights, sports, meetups, ect.) but we need your location!", title: "Ahh Bananas!", buttonTitle: "Okay")
        }
    }
    
    @objc func timerAction() {
        self.animationCounter += 1
        if self.animationCounter > 2 {
            self.animationTimer?.invalidate()
            self.animationTimer = nil
            stopLoadingAnimation()
        }
    }
    
    //MARK: DZN EMPTY DATA
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let status = CLLocationManager.authorizationStatus()
        if status != .authorizedWhenInUse {
            let str = "Turn that location on!"
            let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
            return NSAttributedString(string: str, attributes: attrs)
            
        } else {
            let str = "Early Adopter"
            let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
            return NSAttributedString(string: str, attributes: attrs)
        }
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let status = CLLocationManager.authorizationStatus()
        if status != .authorizedWhenInUse {
            let str = "People are you are busy hosting awesome group events near you... join events, group chat, and have fun!"
            let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)]
            return NSAttributedString(string: str, attributes: attrs)
        } else {
            let str = "We just arrived in your city! Get involved by creating and leading a local event, and help us create a more adventurous, fun, and social community!"
            let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)]
            return NSAttributedString(string: str, attributes: attrs)
            
        }
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "banana_heart")
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> NSAttributedString? {
        let status = CLLocationManager.authorizationStatus()
        if status != .authorizedWhenInUse {
            let str = "go to settings"
            let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout)]
            return NSAttributedString(string: str, attributes: attrs)
        } else {
            let str = "Create Event"
            let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.callout)]
            return NSAttributedString(string: str, attributes: attrs)
        }
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
        let status = CLLocationManager.authorizationStatus()
        if status != .authorizedWhenInUse {
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                })
            }
        } else {
            //create event segue
            createEvent()
        }
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        let status = CLLocationManager.authorizationStatus()
        if status != .authorizedWhenInUse {
            return true
        } else {
            if self.sortedEvents[0].count == 0 && selectedCategoryTag == 0 {
                return true
            }
            return false
        }
    }
}

extension Dictionary where Value: Equatable {
    func containsValue(value : Value) -> Bool {
        return self.contains { $0.1 == value }
    }
}
