//
//  detailsViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 6/19/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import CoreData
import SCLAlertView
import MapKit
import SDWebImage

class DetailsViewController: UIViewController {
    var event: Event!
    var city: String!
    var state: String!
    var country: String!
    var isPrivate: Bool = true
    var thisCdBunchObject = [CdEvent]()
    var eventObjects = [CdEvent]()
    var currentUserCoreData = [Person]()

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var eventOwnerProfileImage: UIImageView!
    @IBOutlet weak var eventOwnerUserName: UILabel!
    @IBOutlet weak var eventEmojis: UILabel!
    @IBOutlet weak var eventEmojiContainer: UIView!
    @IBOutlet weak var event_title: UILabel!
    @IBOutlet weak var eventDescription: UILabel!
    @IBOutlet weak var eventPrivacy: UILabel!
    @IBOutlet weak var eventCasualTime: UILabel!
    @IBOutlet weak var eventLocation: UILabel!
    @IBOutlet weak var joinEventButton: UIButton!
    @IBOutlet weak var timeSincePostedLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBAction func moreButtonClicked(_ sender: UIButton) {
        let defaults = UserDefaults.standard
        let blockUidArray = defaults.stringArray(forKey: "blockedUidArray") ?? [String]()
        
        let alertController = UIAlertController(title: "\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let cancelAction = UIAlertAction(title: "cancel", style: .cancel) { (action) in
            ()
        }
        
        //conditional for if the event clicked is not owned by current user
        if event.owner_uid != Auth.auth().currentUser?.uid {
            let margin:CGFloat = 10.0
            let rect = CGRect(x: margin, y: margin, width: alertController.view.bounds.size.width - margin * 4.0, height: 120)
            let customView = UIView(frame: rect)
            
            let alertImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            alertImage.center = CGPoint(x: alertController.view.bounds.size.width/2 - 25, y: alertController.view.frame.minY + 35)
            alertImage.layer.cornerRadius = 10
            alertImage.clipsToBounds = true
            if let url = URL(string: self.event.owner_url) {
                alertImage.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached)
            }
            
            customView.addSubview(alertImage)
            let primarylabel = UILabel(frame: CGRect(x: 0, y: 0, width: alertController.view.bounds.size.width, height: 20))
            primarylabel.center = CGPoint(x: alertController.view.bounds.size.width/2 - margin * 2.0, y: alertController.view.frame.minY + margin + alertImage.bounds.size.height + margin + primarylabel.bounds.size.height/2)
            primarylabel.font = primarylabel.font.withSize(18)
            primarylabel.textColor = UIColor.gray
            primarylabel.textAlignment = .center
            primarylabel.text = "We don't tell them"
            customView.addSubview(primarylabel)
            alertController.view.addSubview(customView)
            
            var blockUnblock: String!
            if blockUidArray.index(of: self.event.owner_uid) != nil { blockUnblock = "Unblock" } else { blockUnblock = "Block"}
            
            //block user
            let blockAction = UIAlertAction(title: "\(blockUnblock!) \(event.owner_name)", style: .destructive) { (action) in
                
                //add user to list of blocked users
                if (Auth.auth().currentUser?.uid) != nil {
                    if blockUidArray.index(of: self.event.owner_uid) != nil {
                        //user was blocked already...unblocking will ensue below
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as! CustomAlertViewController
                        vc.mainTitle = "Unblock User?"
                        vc.subtitle = "Unblocking this user means you can exchange messages, you will have access to their social profiles, and you will see their hosted events."
                        vc.actionButtonTitle = "Unblock"
                        vc.type = .unblockUser
                        vc.selectedUserUid = self.event.owner_uid
                        vc.onDoneBlock = { result in
                            self.showTransparentConfirmationAlert(word: "Unblocked")
                        }
                        
                        vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        self.present(vc, animated: false, completion: nil)
                    } else {
                        //user wasn't blocked..blocking will ensure below
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as! CustomAlertViewController
                        vc.mainTitle = "Block User?"
                        vc.subtitle = "Blocking this user means you cannot exchange messages, you will not have access to their social profiles, and you will not see their hosted events."
                        vc.actionButtonTitle = "Block"
                        vc.type = .blockUser
                        vc.selectedUserUid = self.event.owner_uid
                        vc.selectedUserUsername = self.event.owner_name
                        vc.selectedUserProfileUrl = self.event.owner_url
                        vc.onDoneBlock = { result in
                            //Blocked
                             self.showTransparentConfirmationAlert(word: "Blocked")
                        }
                        vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                        self.present(vc, animated: false, completion: nil)
                    }
                }
            }
            
            let reportAction = UIAlertAction(title: "Report Event", style: .destructive) { (action) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as! CustomAlertViewController
                vc.mainTitle = "Report this Event?"
                vc.subtitle = "Thankyou for making our community safer, we love people like you who monitor the content other people create!"
                vc.actionButtonTitle = "Report"
                vc.type = .reportEvent
                vc.Event = self.event
                vc.city = self.city
                vc.state = self.state
                vc.country = self.country
                
                vc.onDoneBlock = { result in
                   self.showTransparentConfirmationAlert(word: "Reported")
                }
                
                vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(vc, animated: false, completion: nil)
            }
            
            alertController.addAction(blockAction)
            alertController.addAction(reportAction)
            alertController.addAction(cancelAction)
            
        } else {
            alertController.title = nil
            let deleteAction = UIAlertAction(title: "delete", style: .destructive) { (action) in
               
            }
            // Add the actions to your actionSheet
            alertController.addAction(deleteAction)
            alertController.addAction(cancelAction)
        }
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion:{})
        }
    }
                
    @IBAction func joinEventButtonClicked(_ sender: UIButton) {
        if let uid = Auth.auth().currentUser?.uid {
            if self.event.owner_uid == uid {
                showAlert(subtitle: "C'mon \(self.currentUserCoreData[0].first_name ?? "Jack Sparrow"), you are already a part of this event... you are the captain!", title: "Ahh bananas!", buttonTitle: "okay")

            } else {
                switch (sender.title(for: .normal)) {
                    case "Join":
                        sender.setTitle("Joined!", for: .normal)
                    break;
                    case "Request":
                        sender.setTitle("Requested!", for: .normal)
                    break;
                    case "Joined!":
                        presentLeaveAlert(message: "Are you sure you want to leave this public event?", isAPrivateBunch: false, uid: uid)
                        return
                    case "Requested!":
                        presentLeaveAlert(message: "Do you wish to remove your request to join this private event? ...we won't tell anyone", isAPrivateBunch: true, uid: uid)
                        return
                    default:
                      ()
                }
                requestEvent(private: isPrivate)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let uid = Auth.auth().currentUser?.uid {
            if uid == event.owner_uid {
                self.moreButton.isHidden = true
            }
        }
        
        
        let defaults = UserDefaults.standard
        let blockUidArray = defaults.stringArray(forKey: "blockedUidArray") ?? [String]()
        
        if blockUidArray.index(of: self.event.owner_uid) != nil {
            showWarningAlert(subtitle: "This event is hosted by someone that you have blocked, please be aware of the people you are going to events with", title: "WARNING", buttonTitle: "Okay")
        }
    }
    
    @objc func dismissME() { () }
    
    override func viewWillAppear(_ animated: Bool) {
        getCoreData()
        getCDBunch()
        setupViews()
    }
    
    func getCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            currentUserCoreData = peep
            self.country = currentUserCoreData[0].location_country
            self.state = currentUserCoreData[0].location_state
            self.city = currentUserCoreData[0].location_city
        } catch { () }
    }
    
    func getCDBunch() {
        //getting event data
        let fetchRequest2: NSFetchRequest<CdEvent> = CdEvent.fetchRequest()
        do {
            let coreDataBunches = try PersistanceService.context.fetch(fetchRequest2)
            self.eventObjects = coreDataBunches
            if self.eventObjects.count >= 1 {
                for n in 0...self.eventObjects.count - 1 {
                    //if CurrentBunch is already joined/requested/accepted
                    
                    if self.eventObjects[n].cd_event_uid == self.event.uid {
                        self.thisCdBunchObject.removeAll()
                        self.thisCdBunchObject.append(self.eventObjects[n])
                        return
                    }
                }
            }
        } catch { () }
    }
    
    func handleCdBunchObjects(accepted: Bool) {
        let cdBunchObject = CdEvent(context: PersistanceService.context)
        if self.thisCdBunchObject.count != 1 {
            cdBunchObject.cd_event_photo_url =  URL(string: event.owner_url)
            cdBunchObject.cd_event_accepted = accepted
            cdBunchObject.cd_event_captain_uid = event.owner_uid
            cdBunchObject.cd_event_city = city
            cdBunchObject.cd_event_state = state
            cdBunchObject.cd_event_country = country
            cdBunchObject.cd_event_title = event.title
            cdBunchObject.cd_event_uid = event.uid
            cdBunchObject.cd_event_start_time = event.start_time
            cdBunchObject.cd_event_end_time = event.start_time
            
            self.eventObjects.append(cdBunchObject)
            PersistanceService.saveContext()
        }
    }
    
    func removeObjectFromCd() {
        let managedContext = PersistanceService.context
        for n in 0...eventObjects.count - 1 {
            if self.eventObjects[n].cd_event_uid == self.event.uid {
                managedContext.delete(eventObjects[n])
                PersistanceService.saveContext()
                self.thisCdBunchObject.removeAll()
                return
            }
        }
    }
    
    func setupViews() {
        //handle profile select
        //setting tag for profile image and username
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(handleProfile))
        self.eventOwnerProfileImage.addGestureRecognizer(TapGesture)
        self.eventOwnerProfileImage.isUserInteractionEnabled = true
        
        if let urlUrl = URL.init(string: event.owner_url) {
            eventOwnerProfileImage.sd_setImage(with: urlUrl, placeholderImage: nil, options: .refreshCached)
        }
        
        scrollView.contentLayoutGuide.bottomAnchor.constraint(equalTo: self.joinEventButton.bottomAnchor).isActive = true
        scrollView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 24, right: 0);
        
        eventOwnerProfileImage.layer.cornerRadius = 10
        eventOwnerProfileImage.clipsToBounds = true
        
        eventOwnerUserName.text = event.owner_name
        eventEmojis.text = event.emojis
        event_title.text = event.title
        eventDescription.text = event.description
        eventCasualTime.text = event.start_time.getReadableDate(time: event.start_time)
        timeSincePostedLabel.text = event.interval_reference.getTimeStamp(interval: event.interval_reference)
        
        if event.is_private {
            eventPrivacy.text = "Private"
            eventLocation.text = "Hidden"
            eventLocation.textColor = UIColor(named: "zazuPurple")
            self.isPrivate = true
        } else {
            eventPrivacy.text = "Public"
            
            let text = event.location
            eventLocation.text = text
            let textRange = NSRange(location: 0, length: (text.count))
            let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: textRange)
            eventLocation.attributedText = attributedText
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.eventLocationTapped))
            eventLocation.isUserInteractionEnabled = true
            eventLocation.addGestureRecognizer(tap)
            self.isPrivate = false
        }
        handleButton()
    }
    
    @objc func eventLocationTapped() {
        openMapForPlace()
    }
    
    func openMapForPlace() {
        let latitude: CLLocationDegrees = event.lat
        let longitude: CLLocationDegrees = event.long
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Zazu Event"
        mapItem.openInMaps(launchOptions: options)
    }
    
    func handleButton() {
        if isPrivate {
            //event is Private
            if self.thisCdBunchObject.count == 1 {
                //is attending that event
                if self.thisCdBunchObject[0].cd_event_accepted {
                    joinEventButton.setTitle("Accepted!", for: .normal)
                } else {
                    joinEventButton.setTitle("Requested!", for: .normal)
                }
            } else {
            //is NOT attending that event
            joinEventButton.setTitle("Request", for: .normal)
            }
        } else {
            //event is NOT Private
            if self.thisCdBunchObject.count == 1 {
                //is attending that event
                if self.thisCdBunchObject[0].cd_event_accepted {
                    joinEventButton.setTitle("Joined!", for: .normal)
                } else {
                    joinEventButton.setTitle("Join", for: .normal)
                }
            } else {
                //is NOT attending that event
                joinEventButton.setTitle("Join", for: .normal)
            }
        }
    }
    
    func requestEvent(private: Bool) {
        if let validCountry = self.country {
            if let validState = self.state {
                if let validCity = self.city {
                    var ref: DatabaseReference!
                    ref = Database.database().reference().child("EVENT_ATTENDEES/\(validCountry)/\(validState)/\(validCity)/\(event.uid)/ATTENDEES")
                    if let profile_picture_url = self.currentUserCoreData[0].profile_photo_url {
                        let user_name = self.currentUserCoreData[0].user_name
                        if let uid = Auth.auth().currentUser?.uid {
                            let profileFeedRef = Database.database().reference().child("USERS_EVENTS/\(uid)/\(validCountry)/\(validState)/\(validCity)")
                            switch (isPrivate) {
                            case true:
                                //ATTENDEE NODE
                                let attend = Attendee(url: profile_picture_url.absoluteString, user_name: user_name!, uid: uid, accepted: false, event_captain_uid: event.owner_uid, event_uid: event.uid)
                                let attenderObject = attend.toAnyObject()
                                
                                //PROFILE NODE
                                let userEventCast = UserEvent(accepted: false, title: event.title, location: event.location, start_interval: event.start_interval, start_time: event.start_time, end_interval: event.end_interval, end_time: event.end_time, description: event.description, emojis: event.emojis, owner_uid: event.owner_uid, owner_name: event.owner_name, owner_url: event.owner_url, uid: event.uid, interval_reference: event.interval_reference, is_private: event.is_private, category: event.category, lat: event.lat, long: event.long, type: 0, category_endInterval: event.category_endInterval)
                                let userEventCastF = userEventCast.toAnyObject()
                            
                                profileFeedRef.child(event.uid).setValue(userEventCastF)
                                ref.child(uid).setValue(attenderObject)
                                
                                handleCdBunchObjects(accepted: false)
                                break;
                            case false:
                                //ATTENDEE NODE
                                let attend = Attendee(url: profile_picture_url.absoluteString, user_name: user_name!, uid: uid, accepted: true, event_captain_uid: event.owner_uid, event_uid: event.uid)
                                let attenderObject = attend.toAnyObject()
                                
                                //PROFILE NODE
                                let userEventCast = UserEvent(accepted: true, title: event.title, location: event.location, start_interval: event.start_interval, start_time: event.start_time, end_interval: event.end_interval, end_time: event.end_time, description: event.description, emojis: event.emojis, owner_uid: event.owner_uid, owner_name: event.owner_name, owner_url: event.owner_url, uid: event.uid, interval_reference: event.interval_reference, is_private: event.is_private, category: event.category, lat: event.lat, long: event.long, type: 0, category_endInterval: event.category_endInterval)
                                let userEventCastF = userEventCast.toAnyObject()

                                ref.child(uid).setValue(attenderObject)
                                profileFeedRef.child(event.uid).setValue(userEventCastF)
                                handleCdBunchObjects(accepted: true)
                                break;
                            }
                            addNotifDotAtTabBarItemIndex(index: 2)
                        }
                    }
                }
            }
        }
    }
    
    public func removeFromBunch(uid: String, isAPrivateBunch: Bool) {
        if let validCountry = self.country {
            if let validState = self.state {
                if let validCity = self.city {
                    var ref: DatabaseReference!
                    ref = Database.database().reference().child("EVENT_ATTENDEES/\(validCountry)/\(validState)/\(validCity)/\(event.uid)/ATTENDEES")
                    let eventRef = ref.child(uid)
                    eventRef.removeValue()
                    
                    let profileFeedRef = Database.database().reference().child("USERS_EVENTS/\(uid)/\(validCountry)/\(validState)/\(validCity)/\(event.uid)")
                    profileFeedRef.removeValue()
                    removeObjectFromCd()
                    
                    if isAPrivateBunch {
                        joinEventButton.setTitle("Request", for: .normal)
                    } else {
                        joinEventButton.setTitle("Join", for: .normal)
                    }
                }
            }
        }
    }
    
    func presentLeaveAlert(message: String, isAPrivateBunch: Bool, uid: String) {
        let alertController = UIAlertController(title: "No don't go! We'll miss you!", message: message, preferredStyle: UIAlertController.Style.actionSheet)

        let removeAction = UIAlertAction(title: "leave event", style: .destructive) { (action) in
            self.removeFromBunch(uid: uid, isAPrivateBunch: isAPrivateBunch)
            self.showTransparentConfirmationAlert(word: "left event")
        }

        let cancelAction = UIAlertAction(title: "cancel", style: .cancel) { (action) in }

        // Add the actions to your actionSheet
        alertController.addAction(removeAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - NOTIFICATION DOT
    func addNotifDotAtTabBarItemIndex(index: Int) {
        for subview in tabBarController!.tabBar.subviews {
            if subview.tag == 1314 {
                subview.removeFromSuperview()
                break
            }
        }
        
        let RedDotRadius: CGFloat = 3
        let RedDotDiameter = RedDotRadius * 2
        let TabBarItemCount = CGFloat(self.tabBarController!.tabBar.items!.count)
        let HalfItemWidth = (self.view.frame.width) / (TabBarItemCount * 2)
        let  xOffset = HalfItemWidth * CGFloat(index * 2 + 1)
        let imageHalfWidth: CGFloat = (self.tabBarController!.tabBar.items![index]).selectedImage!.size.width / 2
        let BottomMagin:CGFloat = (imageHalfWidth * 2) + 12
        let redDot = UIView(frame: CGRect(x: xOffset - RedDotRadius, y: BottomMagin, width: RedDotDiameter, height: RedDotDiameter))
        
        redDot.tag = 1314
        redDot.backgroundColor = UIColor(named: "zazuPurple")
        redDot.layer.cornerRadius = RedDotRadius
        
        self.tabBarController?.tabBar.addSubview(redDot)
    }
    
    func showTransparentConfirmationAlert(word: String) {
        if let myAlert = storyboard?.instantiateViewController(withIdentifier: "alert") as? alertViewController {
            myAlert.alertTitle = word
            myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            self.present(myAlert, animated: true, completion: nil)
        }
    }
    
    //MARK: - Profile / segues / Alerts
    @objc func handleProfile(sender: UIGestureRecognizer) {
        let clickedProfilePicOwnerUid = self.event.owner_uid
        
        if let uid = Auth.auth().currentUser?.uid {
            if clickedProfilePicOwnerUid == uid {
                self.tabBarController?.selectedIndex = 1
            } else {
                let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "UsersProfileVC") as! UsersProfileViewController
                VC1.selectedUserUid = self.event.owner_uid
                VC1.userName = event.owner_name
                self.navigationController!.pushViewController(VC1, animated: true)
            }
        }
    }
}
