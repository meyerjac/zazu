//
//  CategoryCollectionViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 9/14/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
     @IBOutlet weak var categoryImageView: UIImageView!
     @IBOutlet weak var categoryLabel: UILabel!
     @IBOutlet weak var containerView: UIView!
     @IBOutlet weak var selectedDot: UIView!
     @IBOutlet weak var numberOfBunchesInCategoryLabel: UILabel!
     @IBOutlet weak var numberOfBunchesLabelView: UIView!
     @IBOutlet weak var newImageView: UIImageView!
}
