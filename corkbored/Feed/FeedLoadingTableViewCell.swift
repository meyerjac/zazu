//
//  FeedLoadingTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 11/2/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class FeedLoadingTableViewCell: UITableViewCell {
    @IBOutlet weak var noMorePostsLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
}
