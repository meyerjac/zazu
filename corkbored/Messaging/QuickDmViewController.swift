//
//  QuickDmViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/3/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import ImagePicker

class QuickDmViewController: UIViewController, UITextViewDelegate, UITableViewDataSource, UITableViewDelegate, ImagePickerDelegate {
    var currentUserCoreData = [Person]()
    var messages = [Message]()
    var currentDmStream = [Dm]()
    var isAlreadyADmObject = false
    var bottomConstraint: NSLayoutConstraint?
    var event_uid = ""
    var user_name = ""
    var user_photo_url = ""
    var userName: String?
    var selectedUserUid: String = ""
    var my_uid: String = ""
    var placeholderGrayText = "write message"
    var fetchingMore = false
    var noMoreMessages = false
    var lastMessageSent: String = ""
    var otherUserPhotoUrl: String?

    weak var typingTimer: Timer?
    var secondsOfInactivity: Int = 0
    var currentUserRef: DatabaseReference!
    var otherUserRef: DatabaseReference!
    var currentUserTypingRef: DatabaseReference!
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var entireView: UIView!
    @IBOutlet weak var isTypingIndicatorView: UIView!
    @IBOutlet weak var isTypingLabel: UILabel!
    @IBOutlet weak var typingTextViewContainer: UIView!
    @IBOutlet weak var typingMessagingTextView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    @IBAction func sendButton(_ sender: UIButton) {
        if self.typingTimer != nil {
            self.typingTimer?.invalidate()
        }
        sendMessage(text: typingMessagingTextView.text, mediaPresent: false, ref: 2)
        handleImageContainerViews()
        lastMessageSent = self.typingMessagingTextView.text
    }
    
    @IBAction func addImageButtonClicked(_ sender: UIButton) {
        let config = Configuration()
        config.doneButtonTitle = "Done"
        config.noImagesTitle = "Sorry, There are no images here"
        config.recordLocation = false
        config.allowVideoSelection = false
        
        let imagePicker = ImagePickerController(configuration: config)
        imagePicker.imageLimit = 1
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let uid = Auth.auth().currentUser?.uid {
            self.currentUserRef = Database.database().reference().child("DIRECT_MESSAGING/\(uid)/\(self.selectedUserUid)/messages")
            self.otherUserRef = Database.database().reference().child("DIRECT_MESSAGING/\(self.selectedUserUid)/\(uid)/messages")
            self.currentUserTypingRef = Database.database().reference().child("DIRECT_MESSAGING/\(self.selectedUserUid)/\(uid)/is_typing")
        }
       
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.keyboardDismissMode = .onDrag
        
        if let uid = Auth.auth().currentUser?.uid {
            self.my_uid = uid
        }
        getUserCoreData()
        setupViews()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissIt))
        tableView.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        bottomConstraint = NSLayoutConstraint(item: typingTextViewContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        guard let bot = bottomConstraint else { return }
        view.addConstraint(bot)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(self.selectedUserUid, forKey: "lastDmUid")
        
        changeisPresentFlagOnDmNode(condition: true)
        tableView.estimatedRowHeight = 64
        tableView.rowHeight = UITableView.automaticDimension
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        fetchDMCoreData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        saveUpdatesToCoreData()
        changeisPresentFlagOnDmNode(condition: false)
        
        self.removeTypingIndicator()
        changeIsTypingIndicator(typing: false)
        NotificationCenter.default.removeObserver(self)
        if self.typingTimer != nil {
            self.typingTimer?.invalidate()
        }
        if !isAlreadyADmObject { handleNewDmCdObject() }
    }
    
    func changeisPresentFlagOnDmNode(condition: Bool) {
        Database.database().reference().child("DIRECT_MESSAGING/\(self.selectedUserUid)/\(self.my_uid)/is_present").setValue(condition)
    }
    
    @objc func dismissIt() {
        typingTextViewContainer.endEditing(true)
        tableView.reloadData()
    }
    
    func  fetchDMCoreData() {
        let fetchRequest: NSFetchRequest<Dm> = Dm.fetchRequest()
        let predicate = NSPredicate(format: "dm_owner_uid = %@", argumentArray: [self.selectedUserUid]) // Specify your condition here
        fetchRequest.predicate = predicate
        do {
            let result = try PersistanceService.context.fetch(fetchRequest)
            currentDmStream = result
            if result.count == 0 {
                isAlreadyADmObject = false
            } else {
                isAlreadyADmObject = true
            }
        } catch { () }
    }
    
    func handleNewDmCdObject() {
        if !self.lastMessageSent.isEmpty {
            Database.database().reference().child("USERS/\(self.selectedUserUid)/thumbnail").observe(.value) { (snapshot) in
                   if let thumbnailUrl = snapshot.value as? String {
                    let url = URL(string: thumbnailUrl)
                    
                    // doSomething succeeded, and result is unwrapped.
                    DispatchQueue.main.async {
                        let dmObject = Dm(context: PersistanceService.context)
                        dmObject.dm_any_messages_unread = false
                        dmObject.dm_last_message = self.messages[self.messages.count - 1].text
                        dmObject.dm_last_message_time_stamp = Double(self.getCurrentTimeAsEpoch())
                        dmObject.dm_owner_uid = self.selectedUserUid
                        dmObject.dm_profile_photo_url = url
                        dmObject.dm_user_name = self.userName
                        dmObject.dm_their_uid = self.selectedUserUid
                        PersistanceService.saveContext()
                    }
                }
            }
        }
    }
    
    func saveUpdatesToCoreData() {
        if currentDmStream.count != 0 {
            if self.messages.count > 0 {
                currentDmStream[0].dm_last_message_time_stamp = Double(self.messages[self.messages.count - 1].interval_reference)
                currentDmStream[0].dm_last_message = self.messages[self.messages.count - 1].text
                currentDmStream[0].dm_any_messages_unread = false
            }
        } else {
            //handle creation of new dm object
        }
    }

    func setupViews() {
        checkButtons(isEmpty: true)
        self.navigationItem.title = userName
        typingMessagingTextView.text = placeholderGrayText
        typingMessagingTextView.textColor = UIColor.lightGray
        tableView.contentInsetAdjustmentBehavior = .never
        
        self.typingTextViewContainer.layer.cornerRadius = 10
        self.typingTextViewContainer.clipsToBounds = true
        self.typingTextViewContainer.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
         tableView.keyboardDismissMode = .onDrag
    }
    
    func sendMessage(text: String, mediaPresent: Bool, ref: Int) {
        if mediaPresent {
             self.progressView.setProgress(0.9, animated: false)
        }
        if let uid = Auth.auth().currentUser?.uid {
            if !mediaPresent {
                //regular text dm
                self.currentUserTypingRef.setValue(false)
                
                let current_message_uid = self.currentUserRef.childByAutoId()
                let other_message_uid = self.otherUserRef.childByAutoId()
                let message_owner_uid = uid
                let message_owner_user_name = self.user_name
                let message_interval_reference = self.getCurrentTimeAsEpoch()
                let message_text = self.trimWhiteSpace(text: text)
                let message_owner_profile_picture_url = self.user_photo_url
                
                let current_message = Message(uid: current_message_uid.key, owner_uid: message_owner_uid, owner_user_name: message_owner_user_name, interval_reference:  message_interval_reference, text: message_text, owner_url: message_owner_profile_picture_url, media: false)
                
                let other_message = Message(uid: other_message_uid.key, owner_uid: message_owner_uid, owner_user_name: message_owner_user_name, interval_reference:  message_interval_reference, text: message_text, owner_url: message_owner_profile_picture_url, media: false)
                
                let current_message_object = current_message.toAnyObject()
                let other_message_object = other_message.toAnyObject()
                current_message_uid.setValue(current_message_object)
                other_message_uid.setValue(other_message_object)
            } else {
                //media dm
                if ref == 0 {
                    self.progressView.setProgress(1.0, animated: false)
                    let current_message_uid = self.currentUserRef.childByAutoId()
                    let message_owner_uid = uid
                    let message_owner_user_name = self.user_name
                    let message_interval_reference = self.getCurrentTimeAsEpoch()
                    let message_text = self.trimWhiteSpace(text: text)
                    let message_owner_profile_picture_url = self.user_photo_url

                    let current_message = Message(uid: current_message_uid.key, owner_uid: message_owner_uid, owner_user_name: message_owner_user_name, interval_reference:  message_interval_reference, text: message_text, owner_url: message_owner_profile_picture_url, media: true)
                    
                    let current_message_object = current_message.toAnyObject()
                    current_message_uid.setValue(current_message_object)
                    
                } else if ref == 1 {
                    self.progressView.setProgress(0.9, animated: false)
                    let other_message_uid = self.otherUserRef.childByAutoId()
                    let message_owner_uid = uid
                    let message_owner_user_name = self.user_name
                    let message_interval_reference = self.getCurrentTimeAsEpoch()
                    let message_text = self.trimWhiteSpace(text: text)
                    let message_owner_profile_picture_url = self.user_photo_url
                    
                    let other_message = Message(uid: other_message_uid.key, owner_uid: message_owner_uid, owner_user_name: message_owner_user_name, interval_reference:  message_interval_reference, text: message_text, owner_url: message_owner_profile_picture_url, media: true)
                    
                    let other_message_object = other_message.toAnyObject()
                    other_message_uid.setValue(other_message_object)
                    
                }
            }
        }
    }
    
    func handleImageContainerViews() {
        typingMessagingTextView.text = placeholderGrayText
        typingMessagingTextView.textColor = UIColor.lightGray
        tableView.contentInsetAdjustmentBehavior = .never
       
        sendButton.layer.cornerRadius = 10
        sendButton.clipsToBounds = true
        sendButton.layer.borderColor = UIColor.lightGray.cgColor
        sendButton.layer.borderWidth = 1
        self.checkButtons(isEmpty: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.view.layoutIfNeeded()
    }
    
    func getUserCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            self.currentUserCoreData = peep
            if let uName = currentUserCoreData[0].user_name { self.user_name = uName }
            if let photoUrl = currentUserCoreData[0].profile_photo_url { self.user_photo_url = photoUrl.absoluteString }
            
            fetchMessages()
        } catch { () }
    }
    
    @objc func handleKeyboardNotification(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            
            self.bottomConstraint?.constant = -(keyboardRectangle.height)
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            self.bottomConstraint?.constant = isKeyboardShowing ? -(keyboardRectangle.height) : 0

            UIView.animate(withDuration: 0, delay: 0, animations: {
                self.view.layoutIfNeeded()
                
            }, completion: { (completed) in
                if self.messages.count > 1 {
                    self.scrollToBott()
                } else {
                    
                }
            })
        }
    }
    
    func scrollToBott() {
        DispatchQueue.main.async {
            if self.messages.count > 5 {
            let indexPath = IndexPath(row: self.messages.count-1, section: 1)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }

    func fetchMessages() {
        if let uid = Auth.auth().currentUser?.uid {
            //true messages
            currentUserRef.queryOrderedByKey().queryLimited(toLast: 20).observe(.childAdded) { (snapshot) in
                if (snapshot.value as? [AnyHashable: AnyObject]) != nil {
                    let message = Message(snapshot: snapshot)
                    self.messages.append(message)
        
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.scrollToBott()
                    }
                }
            }
            
            Database.database().reference().child("DIRECT_MESSAGING/\(uid)/\(self.selectedUserUid)/is_typing").observe(.value) { (snapshot) in
                if let typingIndication = snapshot.value as? Bool {
                    if typingIndication == true {
                        self.displayTypingIndicator()
                    } else {
                        self.removeTypingIndicator()
                    }
                }
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        if offsetY < 0  {
            if !fetchingMore {
                beginBatchFetch()
            }
        }
    }
    
    func beginBatchFetch() {
        fetchingMore = true
        tableView.reloadSections(IndexSet(integer: 0), with: .none)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // change 2 to desired number of seconds
            // Your code with delay
            if (Auth.auth().currentUser?.uid) != nil {
                self.currentUserRef.queryOrderedByKey().queryEnding(atValue: self.messages[0].uid).queryLimited(toLast: 15).observe(.value) { (snapshot) in
                    var index = 0
                    //NO MORE MESSAGES TO FETCH
                    
                    let enumerator = snapshot.children
                    while let msg = enumerator.nextObject() as? DataSnapshot {
                        
                        if (msg.value as? [AnyHashable: AnyObject]) != nil {
                            
                            let message = Message(snapshot: msg)
                            
                            
                            if index != snapshot.childrenCount - 1 {
                                self.messages.insert(message, at: index)
                                index += 1
                            }
                        }
                    }
                    
                    //handle scroll refresh
                    if snapshot.childrenCount == 1 {
                        self.noMoreMessages = true
                        self.tableView.reloadData()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        self.fetchingMore = false
                        self.tableView.reloadData()
                        return
                        }
                    } else {
                        self.fetchingMore = false
                        let initialOffset = self.tableView.contentOffset.y
                        self.tableView.reloadData()
                        
                        //@numberOfCellsAdded: number of items added at top of the table
                        let row = (snapshot.childrenCount) - 2
                        let indexPath: IndexPath = IndexPath(row: Int(row), section:1)
                        self.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
                        self.tableView.contentOffset.y += initialOffset
                    }
                }
            }
        }
    }
    
    func changeIsTypingIndicator(typing: Bool) {
        if self.typingTimer != nil {
            self.typingTimer?.invalidate()
        }
        //Database.database().reference().child("DIRECT_MESSAGING/\(self.my_uid)/\(self.selectedUserUid)/is_typing").setValue(typing)
        Database.database().reference().child("DIRECT_MESSAGING/\(self.selectedUserUid)/\(self.my_uid)/is_typing").setValue(typing)
    }

    func startTimer(timer: inout Timer?, interval: Int, _selector: Selector, _repeats: Bool) {
            if (timer == nil) {
                self.secondsOfInactivity = 0
                timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(countUp), userInfo: nil, repeats: true)
            }
        }
    
    @objc func countUp() {
            self.secondsOfInactivity += 2
            if secondsOfInactivity >= 10 {
                if self.typingTimer != nil {
                    self.typingTimer?.invalidate()
                }
                changeIsTypingIndicator(typing: false)
            }
        }
    
    func checkButtons(isEmpty: Bool) {
        sendButton.layer.cornerRadius = 10
        sendButton.clipsToBounds = true
        if isEmpty {
            sendButton.layer.borderColor = UIColor.lightGray.cgColor
            sendButton.layer.borderWidth = 1
            sendButton.backgroundColor = UIColor.white
            sendButton.setTitleColor(.lightGray, for: .normal)
            sendButton.isEnabled = false
        } else {
            sendButton.backgroundColor = UIColor(red:0.51, green:0.79, blue:1.00, alpha:1.0)
            sendButton.layer.borderWidth = 0.0
            sendButton.setTitleColor(.white, for: .normal)
            sendButton.isEnabled = true
        }
    }
    
    
    //MARK: - Typing indicator
    func displayTypingIndicator() {
     isTypingIndicatorView.isHidden = false
        
        if let font = UIFont(name: "Metropolis-Medium", size: 14) {
            if let font1 = UIFont(name: "Metropolis-Medium", size: 14) {
                //both fonts present
                
                let attributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor(named: "zazuBlue")]
                let attributes1 = [NSAttributedString.Key.font: font1, NSAttributedString.Key.foregroundColor: UIColor(named: "zazuBlue")]
                let yourAttributes = attributes
                let yourOtherAttributes = attributes1
                
                let partOne = NSMutableAttributedString(string: " is typing...", attributes: yourAttributes as [NSAttributedString.Key : Any])
                
                if let uName = self.userName {
                    let partTwo = NSMutableAttributedString(string: uName, attributes: yourOtherAttributes as [NSAttributedString.Key : Any])
                    
                    let combination = NSMutableAttributedString()
                    
                    combination.append(partTwo)
                    combination.append(partOne)
                    
                    isTypingLabel.attributedText = combination
                    
                    let contentH = tableView.contentSize.height - tableView.frame.size.height + 28
                    if contentH > tableView.frame.size.height {
                        self.tableView.setContentOffset(CGPoint(x: 0, y: contentH), animated: true)
                    }
                    
                }
            }
        }
    }
    
    func removeTypingIndicator() {
       isTypingIndicatorView.isHidden = true
    }
    
    //MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 && fetchingMore {
            return 1
        } else if section == 1 {
            return self.messages.count
        } else {
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    //TABLEVIEW
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if noMoreMessages {
                let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath) as! LoadingCell
                cell.loadingSpinner?.isHidden = true
                cell.loadingLabel?.isHidden = false
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath) as! LoadingCell
                cell.loadingSpinner?.startAnimating()
                return cell
            }
        } else {
            if indexPath.row >= 1 {
                if messages[indexPath.row - 1].owner_uid != messages[indexPath.row].owner_uid {
                    //different user messages
                    if messages[indexPath.row].owner_uid != self.my_uid {
                        //other user first  picker
                        if messages[indexPath.row].media {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "otherInitialPic", for: indexPath) as! DmOtherInitialPicTableViewCell
                            cell.ImageView.sd_setImage(with: URL(string: messages[indexPath.row].text))
                            
                            if let urlUrl = URL.init(string: messages[indexPath.row].owner_url) {
                                cell.profileImageView.sd_setImage(with: urlUrl, placeholderImage: nil, options: .refreshCached)
                                cell.profileImageView.layer.cornerRadius = 10
                                cell.profileImageView.clipsToBounds = true
                                let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleProfile(_:)))
                                cell.profileImageView.addGestureRecognizer(tap)
                                cell.profileImageView.isUserInteractionEnabled = true
                                progressView.setProgress(0.0, animated: false)
                            }
                            return cell
                        } else {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "messageOther", for: indexPath) as! dmOtherTextTableViewCell
                            
                            if let urlUrl = URL.init(string: messages[indexPath.row].owner_url) {
                                cell.profileImageView.sd_setImage(with: urlUrl, placeholderImage: nil, options: .refreshCached)
                                cell.profileImageView.layer.cornerRadius = 10
                                cell.profileImageView.clipsToBounds = true
                                let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleProfile(_:)))
                                cell.profileImageView.addGestureRecognizer(tap)
                                cell.profileImageView.isUserInteractionEnabled = true
                            }
                            
                            cell.messageBodyLabel.text = trimWhiteSpace(text: messages[indexPath.row].text)
                            
                            return cell
                        }
                    } else {
                        if messages[indexPath.row].media {
                            //picture message from current user
                            let cell = tableView.dequeueReusableCell(withIdentifier: "currentPic", for: indexPath) as! DmCurrentPicTableViewCell
                            cell.ImageView.sd_setImage(with: URL(string: messages[indexPath.row].text))
                            progressView.setProgress(0.0, animated: false)
                            return cell
                        } else {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "messageCurrent", for: indexPath) as! dmCurrentTextTableViewCell
                            cell.messageBodyLabel.text = trimWhiteSpace(text: messages[indexPath.row].text)
                            return cell
                        }
                    }
                } else {
                    //additional messages, same user
                    if messages[indexPath.row].owner_uid != self.my_uid {
                         if messages[indexPath.row].media {
                            //other users media message
                            //picture message from current user
                            let cell = tableView.dequeueReusableCell(withIdentifier: "onlyOtherPic", for: indexPath) as! DmOtherAdditionalPicTableViewCell
                            cell.ImageView.sd_setImage(with: URL(string: messages[indexPath.row].text))
                            progressView.setProgress(0.0, animated: false)
                            return cell
                         } else {
                            //other users text message
                            let cell = tableView.dequeueReusableCell(withIdentifier: "messageOnlyOther", for: indexPath) as! dmOtherAdditionalTextTableViewCell
                            cell.messageBodyLabel.text = trimWhiteSpace(text: messages[indexPath.row].text)
                            return cell
                        }
                    } else {
                        //current user Messages
                        if messages[indexPath.row].media {
                            //picture message from current user
                            let cell = tableView.dequeueReusableCell(withIdentifier: "currentPic", for: indexPath) as! DmCurrentPicTableViewCell
                            cell.ImageView.sd_setImage(with: URL(string: messages[indexPath.row].text))
                            progressView.setProgress(0.0, animated: false)
                            return cell
                        } else {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "messageOnlyCurrent", for: indexPath) as! dmCurrentAdditionalTextTableViewCell
                            cell.messageBodyLabel.text = trimWhiteSpace(text: messages[indexPath.row].text)
                            return cell
                        }
                    }
                }
            } else {
                //first row
                if messages[indexPath.row].owner_uid != self.my_uid {
                     if messages[indexPath.row].media {
                         let cell = tableView.dequeueReusableCell(withIdentifier: "otherInitialPic", for: indexPath) as! DmOtherInitialPicTableViewCell
                         cell.ImageView.sd_setImage(with: URL(string: messages[indexPath.row].text))
                        
                        if let urlUrl = URL.init(string: messages[indexPath.row].owner_url) {
                            cell.profileImageView.sd_setImage(with: urlUrl, placeholderImage: nil, options: .refreshCached)
                            cell.profileImageView.layer.cornerRadius = 10
                            cell.profileImageView.clipsToBounds = true
                            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleProfile(_:)))
                            cell.profileImageView.addGestureRecognizer(tap)
                            cell.profileImageView.isUserInteractionEnabled = true
                            progressView.setProgress(0.0, animated: false)
                         }
                         return cell
                     } else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "messageOther", for: indexPath) as! dmOtherTextTableViewCell
                        
                        if let urlUrl = URL.init(string: messages[indexPath.row].owner_url) {
                            cell.profileImageView.sd_setImage(with: urlUrl, placeholderImage: nil, options: .refreshCached)
                            cell.profileImageView.layer.cornerRadius = 10
                            cell.profileImageView.clipsToBounds = true
                            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleProfile(_:)))
                            cell.profileImageView.addGestureRecognizer(tap)
                            cell.profileImageView.isUserInteractionEnabled = true
                        }
                        cell.messageBodyLabel.text = trimWhiteSpace(text: messages[indexPath.row].text)
                        return cell
                    }
                } else {
                    //currentUser
                    if messages[indexPath.row].media {
                        //picture message from current user
                        let cell = tableView.dequeueReusableCell(withIdentifier: "currentPic", for: indexPath) as! DmCurrentPicTableViewCell
                        cell.ImageView.sd_setImage(with: URL(string: messages[indexPath.row].text))
                        progressView.setProgress(0.0, animated: false)
                        return cell
                    } else {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "messageCurrent", for: indexPath) as! dmCurrentTextTableViewCell
                        cell.messageBodyLabel.text = trimWhiteSpace(text: messages[indexPath.row].text)
                        return cell
                    }
                }
            }
        }
    }
    
    //MARK: - Image picker
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        bottomConstraint = NSLayoutConstraint(item: typingTextViewContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        guard let bot = bottomConstraint else { return }
        view.addConstraint(bot)

        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        bottomConstraint = NSLayoutConstraint(item: typingTextViewContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        guard let bot = bottomConstraint else { return }
        view.addConstraint(bot)
        
        progressView.setProgress(0.2, animated: false)
        
        imagePicker.dismiss(animated: true, completion: nil)
        if images.count > 0 {
            //send to firebase
            uploadImageToFirebase(image: images[0])
        }
    }
    
    func uploadImageToFirebase(image: UIImage) {
         self.progressView.setProgress(0.4, animated: false)
        let currentUid = Auth.auth().currentUser?.uid
        let otherUid = self.selectedUserUid
        
        let imageName = NSUUID().uuidString
        let refCurrent = Storage.storage().reference().child("message_images").child("dm").child(currentUid!).child(otherUid).child(imageName)
        let refOther = Storage.storage().reference().child("message_images").child("dm").child(otherUid).child(currentUid!).child(imageName)
        
        if let uploadData = image.jpegData(compressionQuality: 0.2) {
            refCurrent.putData(uploadData, metadata: nil, completion: { (metadata, err) in
                if err == nil {
                    let path = metadata?.downloadURL()?.absoluteString
                    self.progressView.setProgress(0.7, animated: false)
                    self.sendMessage(text: path!, mediaPresent: true, ref: 0)
                    
                }
            })
        }
        
        if let uploadDataOther = image.jpegData(compressionQuality: 0.2) {
            refOther.putData(uploadDataOther, metadata: nil, completion: { (metadata, err) in
                if err == nil {
                    let path = metadata?.downloadURL()?.absoluteString
                    self.sendMessage(text: path!, mediaPresent: true, ref: 1)
                }
            })
        }
    }
    
    
    //MARK - text view
    func textViewDidChangeSelection(_ textView: UITextView) {
        if self.view.window != nil {
            if textView.textColor == UIColor.lightGray {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.becomeFirstResponder()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)
        if updatedText.isEmpty {
            checkButtons(isEmpty: true)
            textView.text = placeholderGrayText
            textView.textColor = UIColor.lightGray
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        } else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            checkButtons(isEmpty: false)
            textView.textColor = UIColor.black
            textView.text = text
        } else {
            textView.textContainer.heightTracksTextView = true
            textView.isScrollEnabled = false
            changeIsTypingIndicator(typing: true)
            startTimer(timer: &self.typingTimer, interval: 2, _selector: #selector(countUp), _repeats: true)
            
            var frame = textView.frame
            frame.origin.y = frame.maxY - textView.contentSize.height;
            frame.size.height = textView.contentSize.height;
            textView.frame = frame;
            
            return true
        }
        return false
    }
    
    //MARK - simple functions
    func trimWhiteSpace(text: String) -> String {
        var trimNeeded: Bool = true
        
        //characters depreciated accessing string directly
        var arrayOfString = Array(text)
        let cnt = arrayOfString.count
        var lastIndex = cnt - 1
        
        while(trimNeeded == true) {
            // Repeat this
            if arrayOfString[lastIndex] != "\n" {
                trimNeeded = false
            } else {
                arrayOfString.remove(at: lastIndex)
                lastIndex -= 1
                
                while(trimNeeded == true) {
                    // Repeat this
                    if arrayOfString[lastIndex] == "\n" {
                        arrayOfString.remove(at: lastIndex)
                        lastIndex -= 1
                    } else {
                        trimNeeded = false
                    }
                }
            }
        }
        
        let sanitizedString = String(arrayOfString)
        return sanitizedString
    }
    
    
    //MARK: - Profile / segues / Alerts
    @objc func handleProfile(_ recognizer: UITapGestureRecognizer?) {

        let thierUid = selectedUserUid
        if (Auth.auth().currentUser?.uid) != nil {
            if let story = self.storyboard {
                let VC1 = story.instantiateViewController(withIdentifier: "UsersProfileVC") as! UsersProfileViewController
                VC1.selectedUserUid = thierUid
                VC1.userName = self.userName
                if let navController = self.navigationController {
                    navController.pushViewController(VC1, animated: true)
                }
            }
        }
    }
    
    func checkIfUserExists(uid: String, completion: @escaping (Bool?) -> ()) {
        let database = Database.database().reference()
        database.child("USERS/\(uid)").observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
            if snapshot.exists() {
                 completion(true)
            } else {
                completion(false)
            }
        })
    }
    
    func scrollToBottomAnimated() {
        if self.messages.count != 0 {
            let indexPathIndicator = IndexPath(row: 0, section: 1) as NSIndexPath
            if self.tableView.hasRowAtIndexPath(indexPath: indexPathIndicator) {
                DispatchQueue.main.async {
                    self.tableView.scrollToRow(at: indexPathIndicator as IndexPath, at: .bottom, animated: true)
                }
            } else {
                DispatchQueue.main.async {
                    let indexPath = IndexPath(row: self.messages.count-1, section: 1)
                    
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
                }
            }
        } else {
            //noMessages to Display
        }
    }
}

