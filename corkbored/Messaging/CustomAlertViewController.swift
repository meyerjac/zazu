//
//  DeleteDmAlertViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 11/8/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import Firebase
import CoreData

class CustomAlertViewController: UIViewController {
    @IBOutlet weak var alertTitle: UILabel!
    @IBOutlet weak var alertSubtitle: UILabel!
    @IBOutlet weak var alertActionButton: UIButton!
    @IBOutlet weak var alertCancelButton: UIButton!
    @IBAction func cancelButtonClicked(_ sender: UIButton) {
       self.dismiss(animated: true, completion: nil)
    }
    @IBAction func actionButtonClicked(_ sender: UIButton) {
        switch(type) {
        case .deleteDm?:
            if let uid = Auth.auth().currentUser?.uid {
                if let mes = messageUID {
                    let messageRef = Database.database().reference().child("DIRECT_MESSAGING/\(uid)/\(mes)")
                    messageRef.removeValue()
                    removeObjectFromCd()
                }
            }
        case .unblockUser?:
            self.removeBlocks()
        case .blockUser?:
            self.createBlocks()
        case .reportEvent?:
            self.reportEvent()
        case .editEvent?:
            self.editEvent()
        case .leaveEvent?:
            self.leaveEvent()
        case .deleteEvent?:
            self.deleteEvent()
        default:
           ()
        }
    }
    
    var messageUID: String? = nil
    var dmObjects = [Dm]()
    var onDoneBlock : ((Bool) -> Void)?
    var type: alertEnumType!
    var mainTitle: String!
    var subtitle: String!
    var actionButtonTitle: String!
    var currentUserCoreData = [Person]()
    var Event: Event!
    var cdEvent = CdEvent()
    var city: String!
    var state: String!
    var country: String!
    
    //blocked user fields
    var selectedUserUid: String? = nil
    var selectedUserUsername: String? = nil
    var selectedUserProfileUrl: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCurrentCoreData()
        handleViews()
    }
    
    func getCurrentCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            self.currentUserCoreData = peep
        } catch { () }
    }
    
    func reportEvent() {
        //push to db reported post
        if let uid = Auth.auth().currentUser?.uid {
            //statements using 'constantName'
            var reportRef: DatabaseReference!
            reportRef = Database.database().reference().child("EXTRAS/REPORTED_EVENTS/\(country!)/\(state!)/\(city!)/\(uid)/\(Event.uid)")
                
            reportRef.setValue(self.Event.toAnyObject())
            onDoneBlock!(true)
            self.dismiss(animated: true, completion: nil)
            return
        }
    }
    
    func handleViews() {
         view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
         alertTitle.text = mainTitle
         alertSubtitle.text = subtitle
         alertActionButton.setTitle(actionButtonTitle,for: .normal)
    }
    
    func removeObjectFromCd() {
        let fetchRequest: NSFetchRequest<Dm> = Dm.fetchRequest()
        do {
            let coreDataMessages = try PersistanceService.context.fetch(fetchRequest)
            self.dmObjects = coreDataMessages
            if dmObjects.count > 0 {
                for n in 0...dmObjects.count - 1 {
                    if self.dmObjects[n].dm_their_uid == self.messageUID! {
                        let managedContext = PersistanceService.context
                        managedContext.delete(self.dmObjects[n])
                        PersistanceService.saveContext()
                        onDoneBlock!(true)
                        self.dismiss(animated: true, completion: nil)
                        return
                    }
                }
            }
        } catch {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func removeBlocks() {
        if let uid = Auth.auth().currentUser?.uid {
            let defaults = UserDefaults.standard
            var blockUidArray = defaults.stringArray(forKey: "blockedUidArray") ?? [String]()
            if selectedUserUid != nil {
                if let index = blockUidArray.index(of: selectedUserUid!) {
                    var blockRef: DatabaseReference!
                    blockRef = Database.database().reference()
                    blockRef.child("USER_BLOCKS/\(uid)/BLOCKED_USERS/\(selectedUserUid!)").removeValue()
                    blockRef.child("USER_BLOCKS/\(self.selectedUserUid!)/BLOCKED_BY/\(uid)").removeValue()
                    
                    blockUidArray.remove(at: index)
                    defaults.set(blockUidArray, forKey: "blockedUidArray")
                    onDoneBlock!(true)
                    self.dismiss(animated: true, completion: nil)
                    return
                }
            }
        }
    }
    
    func editEvent() {
        self.dismiss(animated: false, completion: {
        self.onDoneBlock!(true)
        })
    }
    
    func createBlocks() {
        if self.currentUserCoreData.count == 0 {
            return
        }
        
        guard let myPic = self.currentUserCoreData[0].profile_photo_url else { return }
        
        guard let myName = self.currentUserCoreData[0].user_name, myName.count > 1 else {
            //What ever condition you want to do on fail or simply return
            return
        }
        
        if selectedUserUid != nil {
            if let uid = Auth.auth().currentUser?.uid {
                let defaults = UserDefaults.standard
                var blockUidArray = defaults.stringArray(forKey: "blockedUidArray") ?? [String]()
                let blockingUser = BlockedUser(blocked_userName: myName, blocked_userUid: uid, blocked_profilePhotoUrl: myPic.absoluteString)
                let blockedUser = BlockedUser(blocked_userName: self.selectedUserUsername!, blocked_userUid: selectedUserUid!, blocked_profilePhotoUrl: self.selectedUserProfileUrl!)
                
                var blockRef: DatabaseReference!
                blockRef = Database.database().reference()
                blockRef.child("USER_BLOCKS/\(uid)/BLOCKED_USERS/\(selectedUserUid!)").updateChildValues(blockedUser.toAnyObject(), withCompletionBlock: { error, ref in
                    if error != nil {
                        self.presentAlertVC(title: "error")
                    } else {
                        //saving blocks from other user blocks
                        blockRef.child("USER_BLOCKS/\(self.selectedUserUid!)/BLOCKED_BY/\(uid)").updateChildValues(blockingUser.toAnyObject(), withCompletionBlock: { error, ref in
                            if error != nil {
                                self.presentAlertVC(title: "error")
                            } else {
                                blockUidArray.append(self.selectedUserUid!)
                                defaults.set(blockUidArray, forKey: "blockedUidArray")
                                self.onDoneBlock!(true)
                                self.dismiss(animated: true, completion: nil)
                                return
                            }
                        })
                    }
                })
            }
        }
    }
    
    public func leaveEvent() {
        if let uid = cdEvent.cd_event_uid {
            if let userUid = Auth.auth().currentUser?.uid {
                if let validCountry = cdEvent.cd_event_country {
                    if let validState = cdEvent.cd_event_state {
                        if let validCity = cdEvent.cd_event_city {
                            var ref: DatabaseReference!
                            ref = Database.database().reference().child("EVENT_ATTENDEES/\(validCountry)/\(validState)/\(validCity)/\(uid)/ATTENDEES/\(userUid)")
                            ref.removeValue()
                            
                            let profileFeedRef = Database.database().reference().child("USERS_EVENTS/\(userUid)/\(validCountry)/\(validState)/\(validCity)/\(uid)")
                            profileFeedRef.removeValue()
                            
                            removeEventFromCD()
                        }
                    }
                }
            }
        }
    }
    
    public func deleteEvent() {
        //EVENTS, EVENT ATTENDEES, EVENT MESSAGING, USER EVENT
        if let userUid = Auth.auth().currentUser?.uid {
            if let uid = cdEvent.cd_event_uid {
                if let validCountry = cdEvent.cd_event_country {
                    if let validState = cdEvent.cd_event_state {
                        if let validCity = cdEvent.cd_event_city {
                            let messagingFeedRef = Database.database().reference().child("EVENT_MESSAGING/\(validCountry)/\(validState)/\(validCity)/\(uid)")
                            messagingFeedRef.removeValue()
                            var ref: DatabaseReference!
                            ref = Database.database().reference().child("EVENT_ATTENDEES/\(validCountry)/\(validState)/\(validCity)/\(uid)")
                            ref.removeValue()
                            
                            let profileFeedRef = Database.database().reference().child("USERS_EVENTS/\(userUid)/\(validCountry)/\(validState)/\(validCity)/\(uid)")
                            profileFeedRef.removeValue()
                            removeEventFromCD()
                        }
                    }
                }
            }
        }
    }
    
    func removeEventFromCD() {
        let fetchRequest: NSFetchRequest<CdEvent> = CdEvent.fetchRequest()
        do {
            let coreDataEvents = try PersistanceService.context.fetch(fetchRequest)
            var eventObjects = [CdEvent]()
            eventObjects = coreDataEvents
            if eventObjects.count > 0 {
                for n in 0...eventObjects.count - 1 {
                    if eventObjects[n].cd_event_uid == self.messageUID! {
                        let managedContext = PersistanceService.context
                        managedContext.delete(eventObjects[n])
                        PersistanceService.saveContext()
                        onDoneBlock!(true)
                        self.dismiss(animated: true, completion: nil)
                        return
                    }
                }
            }
        } catch {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func presentAlertVC(title: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let VC = storyboard.instantiateViewController(withIdentifier: "alert") as! alertViewController
        VC.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        VC.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        VC.alertTitle = title
        self.present(VC, animated: true, completion: nil)
    }
}
