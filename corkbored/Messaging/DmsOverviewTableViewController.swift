//
//  DmsOverviewTableViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/4/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import DZNEmptyDataSet

class DmsOverviewTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    var dmObjects = [Dm]()
    var allMessageProfileUid = [String]()
    var ref: DatabaseReference!
    var usersRef: DatabaseReference!
    var uid: String?
    var messageSelectedUid: String = ""
    var user_name = ""
    var selectedDmUserName: String = ""
    var selectedUserUid: String = ""
    var selectedUserUidPhoto: UIImage?
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Messages"
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        uid = (Auth.auth().currentUser?.uid)!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchDmObjects()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as! CustomAlertViewController
            vc.mainTitle = "Delete Conversation?"
            vc.subtitle = "Deleting removes this message stream from your inbox but nobody else's inbox."
            vc.actionButtonTitle = "Delete"
            vc.type = .deleteDm
            
            vc.onDoneBlock = { result in
                self.fetchDmObjects()
            }
            
            vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
            vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            vc.messageUID = self.dmObjects[indexPath.row].dm_their_uid
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dmObjects.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? DmsOverviewTableViewCell {
        self.selectedDmUserName = cell.dmUserNameView.text!
        if let theirUid = self.dmObjects[indexPath.row].dm_owner_uid {
              self.selectedUserUid = theirUid
        }
        self.selectedUserUidPhoto = cell.dmProfileImageView.image
        self.performSegue(withIdentifier: "toSelectedDm", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSelectedDm" {
            if let destinationVC = segue.destination as? QuickDmViewController {
                destinationVC.userName = self.selectedDmUserName
                destinationVC.selectedUserUid = self.selectedUserUid
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dmOverviewCell", for: indexPath) as! DmsOverviewTableViewCell
        
        if let url = self.dmObjects[indexPath.row].dm_profile_photo_url {
            cell.dmProfileImageView.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached)
        }
    
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleProfile(_:)))
        cell.dmProfileImageView.tag = indexPath.row
        cell.dmProfileImageView.addGestureRecognizer(tap)
        cell.dmProfileImageView.isUserInteractionEnabled = true
        cell.dmProfileImageView.layer.cornerRadius = 10
        cell.dmProfileImageView.clipsToBounds = true
        cell.dmNewMessagesIndicator.isHidden = !(self.dmObjects[indexPath.row].dm_any_messages_unread)
        
        let str = self.dmObjects[indexPath.row].dm_last_message
        let result = (str?.prefix(5))!
        if result == "https" {
            cell.dmLastMessageView.text = "attachment: 1 image"
        } else {
            cell.dmLastMessageView.text = str
        }
        
        if !cell.dmNewMessagesIndicator.isHidden {
            cell.dmLastMessageView.font = UIFont(name:"Metropolis-SemiBold", size: 16.0)
        } else {
           cell.dmLastMessageView.font = UIFont(name:"Metropolis-Medium", size: 16.0)
        }
        
        cell.dmLastActivityTimeView.text = Int(self.dmObjects[indexPath.row].dm_last_message_time_stamp).getTimeStamp(interval: Int(self.dmObjects[indexPath.row].dm_last_message_time_stamp))

        cell.dmUserNameView.text = self.dmObjects[indexPath.row].dm_user_name

        return cell
    }
    
    func fetchDmObjects() {
        let fetchRequest: NSFetchRequest<Dm> = Dm.fetchRequest()
        do {
            let coreDataMessages = try PersistanceService.context.fetch(fetchRequest)
            self.dmObjects = coreDataMessages
            self.dmObjects = dmObjects.sorted(by: { $0.dm_last_message_time_stamp > $1.dm_last_message_time_stamp})
          
            self.tableView.reloadData()
            
        } catch { () }
    }
    
    //MARK: - Profile / segues / Alerts
    @objc func handleProfile(_ recognizer: UITapGestureRecognizer?) {
        
        let tag = recognizer?.view?.tag ?? 0
        
        if let thierUid = self.dmObjects[tag].dm_owner_uid {
            if let uid = Auth.auth().currentUser?.uid {
                if thierUid == uid {
                    self.tabBarController?.selectedIndex = 1
                } else {
                    let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "UsersProfileVC") as! UsersProfileViewController
                    VC1.selectedUserUid = thierUid
                    VC1.userName = self.dmObjects[tag].dm_user_name
                    self.navigationController!.pushViewController(VC1, animated: true)
                }
            }
        }
    }
    
    //MARK: DZN EMPTY DATA
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "No connections yet!"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "This is where all your messages with epic people will be"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "sendB")
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        if dmObjects.count == 0 {
            return true
        } else {
            return false
        }
    }
}

