//
//  dmOtherTextTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/6/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class dmOtherTextTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var messageContainerView: UIView!
    @IBOutlet weak var messageBodyLabel: UILabel!
}
