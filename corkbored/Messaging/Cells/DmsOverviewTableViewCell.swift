//
//  DmsOverviewTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/2/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class DmsOverviewTableViewCell: UITableViewCell {
    @IBOutlet weak var dmNewMessagesIndicator: UIView!
    @IBOutlet weak var dmProfileImageView: UIImageView!
    @IBOutlet weak var dmContainerView: UIView!
    @IBOutlet weak var dmUserNameView: UILabel!
    @IBOutlet weak var dmLastMessageView: UILabel!
    @IBOutlet weak var dmLastActivityTimeView: UILabel!
}
