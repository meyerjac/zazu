//
//  dmCurrentAdditionalTextTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/7/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class dmCurrentAdditionalTextTableViewCell: UITableViewCell {
    @IBOutlet weak var messageContainerView: UIView!
    @IBOutlet weak var messageBodyLabel: UILabel!
}
