//
//  dmCurrentTextTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/6/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class dmCurrentTextTableViewCell: UITableViewCell {
    @IBOutlet weak var messageContainerView: UIView!
    @IBOutlet weak var messageBodyLabel: UILabel!
}
