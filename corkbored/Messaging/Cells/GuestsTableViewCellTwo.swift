//
//  GuestsTableViewCellTwo.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/25/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class GuestsTableViewCellTwo: UITableViewCell {
    @IBOutlet weak var detailIconImageView: UIImageView!
    @IBOutlet weak var profilePhotoImageView: UIImageView!
    @IBOutlet weak var detailLabel: UILabel!
}
