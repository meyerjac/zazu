//
//  BunchMessagingThreeTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/2/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit
import ActiveLabel

class BunchMessagingCell: UITableViewCell {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var messageContainerView: UIView!
    @IBOutlet weak var messageUserNameView: UILabel!
    @IBOutlet weak var messageTimeReference: UILabel!
    @IBOutlet weak var messageBodyLabel: ActiveLabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeCount: UILabel!
    @IBAction func likeButton(_ sender: UIButton) {
        if let likeButtonTapped = self.likeButtonTapped {
            likeButtonTapped()
        }
    }
    
    var likeButtonTapped : (() -> Void)? = nil
    
    @IBOutlet weak var replyButton: UIButton!
    @IBAction func replyButton(_ sender: UIButton) {
        if let replyButtonTapped = self.replyButtonTapped {
            replyButtonTapped()
        }
    }
    
     var replyButtonTapped : (() -> Void)? = nil
}
