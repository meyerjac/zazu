//
//  DmOtherInitialPicTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 9/20/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class DmOtherInitialPicTableViewCell: UITableViewCell {
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var ImageView: UIImageView!
}
