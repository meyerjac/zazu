//
//  DmOtherAdditionalPicTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 9/20/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class DmOtherAdditionalPicTableViewCell: UITableViewCell {
    @IBOutlet weak var ImageView: UIImageView!
}
