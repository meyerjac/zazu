//
//  mentionTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/20/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class mentionTableViewCell: UITableViewCell {
    @IBOutlet weak var mentionImageView: UIImageView!
    @IBOutlet weak var mentionContainerView: UIView!
    @IBOutlet weak var mentionUsernameLabel: UILabel!
}
