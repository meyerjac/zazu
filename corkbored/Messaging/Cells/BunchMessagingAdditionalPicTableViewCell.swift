//
//  BunchMessagingAdditionalPicTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 9/2/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class BunchMessagingAdditionalPicTableViewCell: UITableViewCell {
    @IBOutlet weak var messageContainerView: UIView!
    @IBOutlet weak var messageTimeReference: UILabel!
    @IBOutlet weak var messageImage: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeCount: UILabel!
    @IBAction func likeButton(_ sender: UIButton) {
        if let likeButtonTapped = self.likeButtonTapped {
            likeButtonTapped()
        }
    }
    
    var likeButtonTapped : (() -> Void)? = nil
    
    @IBOutlet weak var replyButton: UIButton!
    @IBAction func replyButton(_ sender: UIButton) {
        if let replyButtonTapped = self.replyButtonTapped {
            replyButtonTapped()
        }
    }
    var replyButtonTapped : (() -> Void)? = nil
}
