//
//  GuestsTableViewCell.swift
//  corkbored
//
//  Created by Jackson Meyer on 6/24/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//

import UIKit

class GuestsTableViewCell: UITableViewCell {
    @IBOutlet weak var profilePhotoImageView: UIImageView!
    @IBOutlet weak var profileUserName: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBAction func acceptButtonClicked(_ sender: UIButton) {
        if let acceptedButtonTapped = self.acceptedButtonTapped {
            acceptedButtonTapped()
        }
    }
    var acceptedButtonTapped : (() -> Void)? = nil
}
