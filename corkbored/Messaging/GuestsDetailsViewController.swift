//
//  GuestsDetailsViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 8/25/18.
//  Copyright © 2018 Jackson Meyer. All rights reserved.
//
import UIKit
import Firebase
import CoreData
import Firebase
import MapKit


class GuestsDetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    //testing
    var attendeeSortedArray = [[Attendee](),[Attendee]()]
    var eventObject: CdEvent!
    var event: Event!
    var detailsArray = [String]()
    var eventUrl: String?
    var eventUserName: String?
    var isCaptain: Bool = false
    
    //first 7 tableview rows
    let detailIconsArray = [#imageLiteral(resourceName: "details_captain"), #imageLiteral(resourceName: "details_title"), #imageLiteral(resourceName: "details_description"), #imageLiteral(resourceName: "details_location"), #imageLiteral(resourceName: "details_clock"), #imageLiteral(resourceName: "details_emoji"), #imageLiteral(resourceName: "details_lock")]
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var deleteEventButton: UIButton!
    @IBAction func deleteEventButtonClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as! CustomAlertViewController
        
        if let eventEndStringTime = self.eventObject?.cd_event_end_time {
            if !self.isAPastEvent(eventTime: eventEndStringTime) {
                vc.actionButtonTitle = "Cancel"
                vc.mainTitle = "Cancel Event?"
                vc.subtitle = "Canceling this event will notify all the group members and remove the event and chat from everyone's feeds forever"
            } else {
                vc.actionButtonTitle = "Delete"
                vc.mainTitle = "Delete Event"
                vc.subtitle = "Deleting this event will notify all the group members and remove the event and chat from everyone's feeds forever"
            }
        }
        
        vc.cdEvent = self.eventObject
        
        vc.type = .deleteEvent
        
        vc.onDoneBlock = { result in
            //deleted
            self.navigationController?.popToRootViewController(animated:true)
        }
        
        vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        vc.messageUID = self.eventObject.cd_event_uid
        self.present(vc, animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        
        if let uid = Auth.auth().currentUser?.uid {
            if uid == self.eventObject.cd_event_captain_uid {
                tableViewBottomConstraint.constant = -(self.view.frame.height * 0.08) - 20
                UIView.animate(withDuration: 0.5, animations: {
                        self.view.layoutIfNeeded()
                    })
    
                self.deleteEventButton.clipsToBounds = true
                self.deleteEventButton.layer.cornerRadius = 8
                self.deleteEventButton.isHidden = false
                self.isCaptain = true
                if let eventEndStringTime = self.eventObject?.cd_event_end_time {
                    if !self.isAPastEvent(eventTime: eventEndStringTime) {
                        self.addEditButton(title: "Edit")
                    }
                }
            } else {
                self.deleteEventButton.isHidden = true
                self.addEditButton(title: "Leave")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchData()
        if let eventEndStringTime = self.eventObject?.cd_event_end_time {
            if !self.isAPastEvent(eventTime: eventEndStringTime) {
                deleteEventButton.setTitle("Cancel Event",for: .normal)
            } else {
                deleteEventButton.setTitle("Delete Event?",for: .normal)
            }
        }
    }
    
    func addEditButton(title: String) {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(editTapped))
    }
    
    @objc func editTapped() {
        if let uid = Auth.auth().currentUser?.uid {
            if uid == self.eventObject.cd_event_captain_uid {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as! CustomAlertViewController
                vc.mainTitle = "Edit Event"
                vc.subtitle = "Editing event will alert all attendees of the event of the change"
                vc.actionButtonTitle = "Edit"
                vc.type = .editEvent
                
                vc.onDoneBlock = { result in
                    self.performSegue(withIdentifier: "createEventEdit", sender: self)
                }
                
                vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(vc, animated: false, completion: nil)
                
            } else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "customAlertVC") as! CustomAlertViewController
                vc.mainTitle = "Leave Event?"
                vc.subtitle = "Leaving this event removes the event from your event list, removes you from the attendee list, and removes you from the chat."
                vc.actionButtonTitle = "Leave"
                vc.cdEvent = self.eventObject
                
                vc.type = .leaveEvent
                
                vc.onDoneBlock = { result in
                     self.navigationController?.popToRootViewController(animated:true)
                }
                
                vc.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
                vc.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                vc.messageUID = self.eventObject.cd_event_uid
                self.present(vc, animated: false, completion: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let navVC = segue.destination as? UINavigationController
        let VC1 = navVC?.viewControllers.first as! CreateEventVC1
        VC1.is_editing_existing_event = true
        VC1.event = self.event
    }
    
    func getPrivacy(privacyStatus: Bool) -> String {
        if privacyStatus == true {
            return "Private"
        } else {
            return "Public"
        }
    }
    
    @objc func eventLocationTapped() {
         openMapForPlace()
    }
    
    func openMapForPlace() {
        let latitude: CLLocationDegrees = event.lat
        let longitude: CLLocationDegrees = event.long
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Zazu Event"
        mapItem.openInMaps(launchOptions: options)
    }
    
    func fetchData() {
        self.attendeeSortedArray[0].removeAll()
        self.attendeeSortedArray[1].removeAll()
        if let BunchCity = self.eventObject?.cd_event_city {
            if let BunchState = self.eventObject?.cd_event_state {
                if let BunchCountry = self.eventObject?.cd_event_country {
                    if let BunchUid = self.eventObject?.cd_event_uid {
                        guard let eventEndStringTime = self.eventObject?.cd_event_end_time else { return }
                        let eventExistsRef: DatabaseReference!
                        if !self.isAPastEvent(eventTime: eventEndStringTime) {
                            //use current ref
                             eventExistsRef = Database.database().reference().child("EVENTS/\(BunchCountry)/\(BunchState)/\(BunchCity)/EVENTS_ACTIVE/\(BunchUid)")
                        } else {
                            //use past ref
                             eventExistsRef = Database.database().reference().child("PAST_EVENTS/\(BunchCountry)/\(BunchState)/\(BunchCity)/EVENTS_PAST/\(BunchUid)")
                        }
        
                        eventExistsRef.observeSingleEvent(of: .value, with: { (snapshot) in
                            if !snapshot.exists() {
                                //handles lag time between cron and current time, check other locatin
                                let handleExistsRef: DatabaseReference!
                                handleExistsRef = Database.database().reference().child("EVENTS/\(BunchCountry)/\(BunchState)/\(BunchCity)/EVENTS_ACTIVE/\(BunchUid)")
                                
                                handleExistsRef.observeSingleEvent(of: .value, with: { (snapshot) in
                                    if !snapshot.exists() {
                                        self.showNoEventExists(subtitle: "We cannot find details to this event, it was likely deleted.", title: "Error", buttonTitle: "Okay")
                                        return
                                    }
                                    
                                    self.event = Event(snapshot: snapshot)
                                    let value = snapshot.value as? NSDictionary
                                    
                                    self.eventUserName = value?["owner_name"] as? String ?? ""
                                    self.eventUrl = value?["owner_url"] as? String ?? ""
                                    let eventTitle3 = value?["title"] as? String ?? ""
                                    let eventTitle4 = value?["description"] as? String ?? ""
                                    let eventTitle5 = value?["location"] as? String ?? ""
                                    let eventTitle7 = value?["emojis"] as? String ?? ""
                                    let eventTitle8 = value?["is_private"] as? Bool ?? true
                                    let cdStartTime = value?["start_time"] as? String ?? ""
                                    
                                    let startTime = cdStartTime.getReadableDate(time: cdStartTime)
                                    
                                    self.detailsArray = [eventTitle3, eventTitle4, eventTitle5, startTime, eventTitle7, self.getPrivacy(privacyStatus: eventTitle8)]
                                    self.tableView.reloadData()
                                    
                                    Database.database().reference().child("EVENT_ATTENDEES/\(BunchCountry)/\(BunchState)/\(BunchCity)/\(BunchUid)/ATTENDEES").observe(.childAdded) { (snapshot) in
                                        if (snapshot.value as? [AnyHashable: AnyObject]) != nil {
                                            let atten = Attendee(snapshot: snapshot)
                                            
                                            if atten.accepted {
                                                self.attendeeSortedArray[0].append(atten)
                                            } else {
                                                self.attendeeSortedArray[1].append(atten)
                                            }
                                            self.tableView.reloadData()
                                        }
                                    }
                                })
                            } else {
                                
                                self.event = Event(snapshot: snapshot)
                                let value = snapshot.value as? NSDictionary
                                
                                self.eventUserName = value?["owner_name"] as? String ?? ""
                                self.eventUrl = value?["owner_url"] as? String ?? ""
                                let eventTitle3 = value?["title"] as? String ?? ""
                                let eventTitle4 = value?["description"] as? String ?? ""
                                let eventTitle5 = value?["location"] as? String ?? ""
                                let eventTitle7 = value?["emojis"] as? String ?? ""
                                let eventTitle8 = value?["is_private"] as? Bool ?? true
                                let cdStartTime = value?["start_time"] as? String ?? ""
                                
                                let startTime = cdStartTime.getReadableDate(time: cdStartTime)
                                
                                self.detailsArray = [eventTitle3, eventTitle4, eventTitle5, startTime, eventTitle7, self.getPrivacy(privacyStatus: eventTitle8)]
                                
                                Database.database().reference().child("EVENT_ATTENDEES/\(BunchCountry)/\(BunchState)/\(BunchCity)/\(BunchUid)/ATTENDEES").observe(.childAdded) { (snapshot) in
                                    if (snapshot.value as? [AnyHashable: AnyObject]) != nil {
                                        let atten = Attendee(snapshot: snapshot)
                                        
                                        if atten.accepted {
                                            self.attendeeSortedArray[0].append(atten)
                                        } else {
                                            self.attendeeSortedArray[1].append(atten)
                                        }
                                        self.tableView.reloadData()
                                    }
                                }
                            }
                        })
                    }
                }
            }
        }
    }
    
    //MARK: - Profile / segues / Alerts
    func handleProfile(eventOwnerUsername: String, targetUid: String) {
        let selectedUid = targetUid
        if let uid = Auth.auth().currentUser?.uid {
            if selectedUid != uid {
                let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "UsersProfileVC") as! UsersProfileViewController
                VC1.selectedUserUid = selectedUid
                VC1.userName = eventOwnerUsername
                self.navigationController!.pushViewController(VC1, animated: true)
            } else {
                self.tabBarController?.selectedIndex = 1
            }
        }
    }
    
   @objc func dismissAlert() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch(indexPath.section) {
        case 0:
            switch(indexPath.row) {
            case 0:
                if let name = eventUserName {
                    if let captainUid = eventObject.cd_event_captain_uid {
                        handleProfile(eventOwnerUsername: name, targetUid: captainUid)
                    }
                }
            default:
                break;
            }
            break;
        case 1:
            handleProfile(eventOwnerUsername: attendeeSortedArray[0][indexPath.row].user_name, targetUid: attendeeSortedArray[0][indexPath.row].uid)
            break;
        case 2:
            break;
        default:
            break;
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let uid = Auth.auth().currentUser?.uid {
            if uid == eventObject.cd_event_captain_uid {
                return 3
            } else {
                return 2
            }
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.detailsArray.count + 1
        } else if section == 1 {
            return self.attendeeSortedArray[0].count
            
        } else {
            return self.attendeeSortedArray[1].count
        }
    }
    
    // MARK: Table View funcions
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 40)) as? UITableViewHeaderFooterView
        headerView?.textLabel?.font = UIFont(name: "Metropolis-Medium", size: 24)
        headerView?.textLabel?.textColor = UIColor.black
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(40)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Event Details"
        } else if section == 1 {
            return String(self.attendeeSortedArray[0].count) + " Users Attending"
        } else {
            return String(self.attendeeSortedArray[1].count)  + " Users Pending"
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch(indexPath.section)  {
        case 0:
            //DETAILS SECTIONS
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "detailsCellHost", for: indexPath) as!  GuestsTableViewCellTwo
                cell.detailIconImageView.image = self.detailIconsArray[indexPath.row]
                
                if let url = self.eventObject.cd_event_photo_url {
                     cell.profilePhotoImageView.sd_setImage(with: url, placeholderImage: nil, options: .refreshCached)
                }
                
                cell.detailLabel.text = self.eventUserName
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath) as!  GuestsTableViewCellOne
                cell.detailIconImageView.image = self.detailIconsArray[indexPath.row]
                cell.detailLabel.text = self.detailsArray[indexPath.row - 1]
                
                if indexPath.row == 3 {
                    let text = self.detailsArray[indexPath.row - 1]
                    let textRange = NSRange(location: 0, length: (text.count))
                    let attributedText = NSMutableAttributedString(string: text)
                    attributedText.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: textRange)
                    cell.detailLabel.attributedText = attributedText
                    
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.eventLocationTapped))
                    cell.detailLabel.isUserInteractionEnabled = true
                    cell.detailLabel.addGestureRecognizer(tap)
                }
                return cell
            }
        case 1:
            //ATTENDEES ACCEPTED SECION
            let cell = tableView.dequeueReusableCell(withIdentifier: "guestsCell", for: indexPath) as! GuestsTableViewCell
            //setting URL picture
            let guestProfilePictureStringURL = self.attendeeSortedArray[0][indexPath.row].url
           
            cell.profileUserName.text = self.attendeeSortedArray[0][indexPath.row].user_name
            cell.acceptButton.setTitle("going", for: .normal)
            cell.acceptButton.backgroundColor = UIColor(named: "zazuBlue")
            if !isCaptain {
                cell.acceptButton.isEnabled = false
            }
            
            if let urlUrl = URL.init(string: guestProfilePictureStringURL) {
                cell.profilePhotoImageView.sd_setImage(with: urlUrl, placeholderImage: nil, options: .refreshCached)
            }
            
            cell.acceptedButtonTapped = {
                if cell.acceptButton.titleLabel!.text == "going" {
                    //remove from group?
                }
            }
            return cell
        case 2:
            //ATTENDEES WAITING SECION
            let cell = tableView.dequeueReusableCell(withIdentifier: "guestsCell", for: indexPath) as! GuestsTableViewCell
            //setting URL picture
            let guestProfilePictureStringURL = self.attendeeSortedArray[1][indexPath.row].url
            if let urlUrl = URL.init(string: guestProfilePictureStringURL) {
                cell.profilePhotoImageView.sd_setImage(with: urlUrl, placeholderImage: nil, options: .refreshCached)
            }
            
            //setting Username
            cell.profileUserName.text = self.attendeeSortedArray[1][indexPath.row].user_name
            if let eventEndStringTime = self.eventObject?.cd_event_end_time {
                
                if !self.isAPastEvent(eventTime: eventEndStringTime) {
                    cell.acceptButton.setTitle("waiting", for: .normal)
                    cell.acceptButton.setTitleColor(UIColor(named: "zazuBlue"), for: .normal)
                    cell.acceptButton.layer.borderColor = UIColor(named: "zazuBlue")?.cgColor
                    cell.acceptButton.backgroundColor = UIColor.white
                    cell.acceptButton.layer.borderWidth = 1
                } else {
                    cell.acceptButton.setTitle("too late", for: .normal)
                    cell.acceptButton.setTitleColor(UIColor(named: "zazuDarkGray"), for: .normal)
                    cell.acceptButton.layer.borderColor = UIColor(named: "zazuDarkGray")?.cgColor
                    cell.profileUserName.textColor = UIColor(named: "zazuDarkGray")
                    cell.acceptButton.backgroundColor = UIColor.white
                    cell.acceptButton.layer.borderWidth = 1
                }
                
                if isCaptain && !self.isAPastEvent(eventTime: eventEndStringTime) {
                    cell.acceptButton.isEnabled = true
                } else {
                    cell.acceptButton.isEnabled = false
                }
                
                cell.acceptedButtonTapped = {
                    if cell.acceptButton.titleLabel!.text == "waiting" {
                        cell.acceptButton.backgroundColor =  UIColor(named: "zazuBlue")
                        cell.acceptButton.setTitle("going", for: .normal)
                        cell.acceptButton.setTitleColor(UIColor.white, for: .normal)
                        
                        //handle changing in db
                        if let BunchUid = self.eventObject?.cd_event_uid {
                            if let BunchCity = self.eventObject?.cd_event_city {
                                if let BunchState = self.eventObject?.cd_event_state {
                                    if let BunchCountry = self.eventObject?.cd_event_country {
                                        let acceptRef = Database.database().reference().child("EVENT_ATTENDEES/\(BunchCountry)/\(BunchState)/\(BunchCity)/\(BunchUid)/ATTENDEES/\(self.attendeeSortedArray[1][indexPath.row].uid)")
                                        acceptRef.updateChildValues(["accepted": true])
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "detailsCellHost", for: indexPath) as!  GuestsTableViewCellTwo
            return cell
        }
    }
}
