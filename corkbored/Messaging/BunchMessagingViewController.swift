//
//  BunchMessagingViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 6/26/18.
//  Copyright ©  18 Jackson Meyer. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import ActiveLabel
import Foundation
import PulsingHalo
import ImagePicker
import Lightbox
import SDWebImage
import DZNEmptyDataSet

class BunchMessagingViewController: UIViewController, UITextViewDelegate, UITableViewDataSource, UITableViewDelegate, ImagePickerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    var messages = [GroupMessage]()
    var currentUserCoreData = [Person]()
    var bottomConstraint: NSLayoutConstraint?
    var mentionTableViewBottomConstraint: NSLayoutConstraint?
    var event_uid = ""
    var current_city = ""
    var current_state = ""
    var current_country = ""
    var user_name = ""
    var user_photo_url = ""
    var keyboardHeight: CGFloat = 0.0
    var placeholderGrayText = "Join the convo!"
    var currentUid: String?
    
    var mentionMatches = [Attendee]()
    var groupAttendeeObjects = [Attendee]()
    var eventObject: CdEvent?
    var lastWordTyped = ""
    var mentionCommentUid: String = ""
    var mentionCommentIndexPath: IndexPath!
    let emptyLikes = [String: Bool]()
    var messageUidLikes = [String]()
    
    //INFINITE SCROLL
    var fetchingMore = false
    var noMoreMessages = false
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var typingTextViewContainer: UIView!
    @IBOutlet weak var typingMessagingTextView: AttrTextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mentionTableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    @IBAction func sendButton(_ sender: UIButton) {
        if !self.typingMessagingTextView.text.isEmpty {
            sendMessage(text: typingMessagingTextView.text, mediaPresent: false)
            self.typingMessagingTextView.text = ""
            handleViews()
        }
    }
    
    @IBAction func addMentionButtonClicked(_ sender: UIButton) {
        self.mentionMatches = groupAttendeeObjects
        self.mentionTableView.reloadData()
        self.mentionTableView.isHidden = false
    }
    
    @IBAction func addImageButtonClicked(_ sender: UIButton) {
        let config = Configuration()
        config.doneButtonTitle = "Done"
        config.noImagesTitle = "Sorry! There are no images here!"
        config.recordLocation = false
        config.allowVideoSelection = false
        
        let imagePicker = ImagePickerController(configuration: config)
        imagePicker.imageLimit = 1
        imagePicker.delegate = self
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        getCoreData()
        setupViews()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        bottomConstraint = NSLayoutConstraint(item: typingTextViewContainer, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
        
        tableView.keyboardDismissMode = .onDrag
    }
    
    //INFINITE SCROLL
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        if offsetY < 0  {
            if !fetchingMore {
                beginBatchFetch()
            }
        }
    }
    
    //INFINITE SCROLL
    func beginBatchFetch() {
        fetchingMore = true
        tableView.reloadSections(IndexSet(integer: 0), with: .none)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // change 2 to desired number of seconds
            // Your code with delay
            guard let EventCity = self.eventObject?.cd_event_city else { return }
            guard let EventState = self.eventObject?.cd_event_state else { return }
            guard let EventCountry = self.eventObject?.cd_event_country else { return }
            guard let eventUid = self.eventObject?.cd_event_uid else { return }
            
            if (Auth.auth().currentUser?.uid) != nil {
                let fetchMore = Database.database().reference().child("EVENT_MESSAGING/\(EventCountry)/\(EventState)/\(EventCity)/\(eventUid)/MESSAGES")
        
                fetchMore.queryOrderedByKey().queryEnding(atValue: self.messages[0].uid).queryLimited(toLast: 15).observe(.value) { (snapshot) in
                    var index = 0
                    if snapshot.exists() {
                        let enumerator = snapshot.children
                        while let msg = enumerator.nextObject() as? DataSnapshot {
                            if (msg.value as? [AnyHashable: AnyObject]) != nil {
                                let message = GroupMessage(snapshot: msg)
                                
                                if index != snapshot.childrenCount - 1 {
                                    self.messages.insert(message, at: index)
                                    index += 1
                                }
                            }
                        }
                        //handle scroll refresh
                        if snapshot.childrenCount == 1 {
                            self.noMoreMessages = true
                            self.tableView.reloadData()
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                                self.fetchingMore = false
                                self.tableView.reloadData()
                                return
                            }
                        } else {
                            self.fetchingMore = false
                            let initialOffset = self.tableView.contentOffset.y
                            self.tableView.reloadData()
                            
                            //@numberOfCellsAdded: number of items added at top of the table
                            if snapshot.childrenCount >= 5 {
                                let row = (snapshot.childrenCount) - 5
                                let indexPath: IndexPath = IndexPath(row: Int(row), section:1)
                                self.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
                                self.tableView.contentOffset.y += initialOffset
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    @objc func dismissIt() {
        typingTextViewContainer.endEditing(true)
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem?.title = ""
        tableView.estimatedRowHeight = 64
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //this is so after an image is selected, keyboard still moves up and performs lik normal
        //        NotificationCenter.default.removeObserver(self)
    }
    
    func setupViews() {
        typingMessagingTextView.text = placeholderGrayText
        typingMessagingTextView.textColor = UIColor.lightGray
        tableView.contentInsetAdjustmentBehavior = .never
        self.mentionTableView.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        sendButton.layer.cornerRadius = 10
        sendButton.clipsToBounds = true
        sendButton.layer.borderColor = UIColor.lightGray.cgColor
        sendButton.layer.borderWidth = 1
        
        self.mentionTableView.tableFooterView = UIView()
        
        let infoButton = UIButton(type: .custom)
        infoButton.frame = CGRect(x: 0.0, y: 0.0, width: 25, height: 25)
        infoButton.setImage(UIImage(named:"info"), for: .normal)
        infoButton.setImage(UIImage(named:"info"), for: .highlighted)
        infoButton.addTarget(self, action: #selector(toBunchInfo), for: .touchUpInside)
        
        let infoButton2 = UIBarButtonItem(customView: infoButton)
        let currWidth2 = infoButton2.customView?.widthAnchor.constraint(equalToConstant: 25)
        currWidth2?.isActive = true
        let currHeight2 = infoButton2.customView?.heightAnchor.constraint(equalToConstant: 25)
        currHeight2?.isActive = true
        self.navigationItem.rightBarButtonItem = infoButton2
        
        self.typingTextViewContainer.layer.cornerRadius = 10
        self.typingTextViewContainer.clipsToBounds = true
        self.typingTextViewContainer.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    @objc func toBunchInfo() {
        self.performSegue(withIdentifier: "toBunchInfo", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! GuestsDetailsViewController
        vc.eventObject = self.eventObject
    }
    
    func callBack(string: String, wordType: wordType) {
        if wordType == .hashtag {
            print("segue to hashtage")
        } else {
            print("segue to other user vs")
        }
    }
    
    //MARK: - Image picker
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
    }
    
    func uploadImageToFirebase(image: UIImage) {
        self.progressView.setProgress(0.4, animated: false)
        let imageName = NSUUID().uuidString
        let ref = Storage.storage().reference().child("message_images").child(self.event_uid).child(imageName)
        
        if let uploadData = image.jpegData(compressionQuality: 0.2) {
            self.progressView.setProgress(0.5, animated: false)
            ref.putData(uploadData, metadata: nil, completion: { (metadata, err) in
                if err == nil {
                    let path = metadata?.downloadURL()?.absoluteString
                    self.progressView.setProgress(0.7, animated: false)
                    self.sendMessage(text: path!, mediaPresent: true)
                }
            })
        }
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        imagePicker.dismiss(animated: true, completion: nil)
        if images.count > 0 {
            //send to firebase
            self.progressView.setProgress(0.3, animated: true)
            uploadImageToFirebase(image: images[0])
        }
    }
    
    func removeExistingMention(userName: String) {
        if let selectedRange = self.typingMessagingTextView.selectedTextRange {
            
            let cursorOffset = self.typingMessagingTextView.offset(from: self.typingMessagingTextView.beginningOfDocument, to: selectedRange.start)
            if let myText = self.typingMessagingTextView.text {
                
                let index = myText.index(myText.startIndex, offsetBy: cursorOffset)
                let substring = myText[..<index]
                if let lastword = substring.components(separatedBy: " ").last {
                    
                    if (lastword.hasPrefix("@")) {
                        //Check complete word
                        //SETTING CURSOR
                        
                        var messageArray = substring.components(separatedBy: " ")
                        messageArray.removeLast()
                        let editedString = messageArray.joined(separator: " ") + " " + userName
                        
                        self.typingMessagingTextView.setText(text: editedString, withHashtagColor: UIColor.red, andMentionColor: UIColor.red, andCallBack: callBack, normalFont: UIFont(name: "Metropolis-Medium", size: 16)!, hashTagFont: UIFont(name: "Metropolis-semiBold", size: 16)!, mentionFont: UIFont(name: "Metropolis-semiBold", size: 16)!)
                    } else {
                        
                        let editedString = substring + " " + userName
                        
                        self.typingMessagingTextView.setText(text: editedString, withHashtagColor: UIColor.red, andMentionColor: UIColor.red, andCallBack: callBack, normalFont: UIFont(name: "Metropolis-Medium", size: 16)!, hashTagFont: UIFont(name: "Metropolis-semiBold", size: 16)!, mentionFont: UIFont(name: "Metropolis-semiBold", size: 16)!)
                    }
                }
            }
        }
    }
    
    //MARK: - Profile / segues / Alerts
    @objc func handleProfile(sender: UIGestureRecognizer) {
        let selectedUid = self.messages[sender.view?.tag ?? 0].owner_uid
        if let uid = Auth.auth().currentUser?.uid {
            if selectedUid != uid {
                let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "UsersProfileVC") as! UsersProfileViewController
                VC1.selectedUserUid = selectedUid
                VC1.userName = self.messages[sender.view?.tag ?? 0].owner_user_name
                self.navigationController!.pushViewController(VC1, animated: true)
            } else {
                self.tabBarController?.selectedIndex = 1
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int { return 2 }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.typingMessagingTextView.becomeFirstResponder()
        if tableView == mentionTableView {
            if let cell = tableView.cellForRow(at: indexPath) as? mentionTableViewCell {
                if self.typingMessagingTextView.text == placeholderGrayText {
                    self.typingMessagingTextView.text = ""
                }
                
                let mentionName = cell.mentionUsernameLabel.text! + " "
                removeExistingMention(userName: mentionName)
                self.mentionTableView.isHidden = true
                checkButtons(isEmpty: false)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == mentionTableView {
            return self.mentionMatches.count
        } else {
            if section == 0 && fetchingMore {
                return 1
            } else if section == 1 {
                return self.messages.count
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView,
                   shouldSpringLoadRowAt indexPath: IndexPath,
                   with context: UISpringLoadedInteractionContext) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == mentionTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "mentionCell", for: indexPath) as! mentionTableViewCell
            cell.transform = CGAffineTransform(scaleX: 1, y: -1)
            
            if self.mentionMatches[indexPath.row].user_name == "Everyone" {
                cell.mentionImageView.image = UIImage(named: "profile_placeholder")
                cell.mentionImageView.layer.cornerRadius = 10
                cell.mentionImageView.clipsToBounds = true
                cell.mentionUsernameLabel.text = "@" + self.mentionMatches[indexPath.row].user_name
            } else {
                if let mentionUrl = URL.init(string: self.mentionMatches[indexPath.row].url) {
                    cell.mentionImageView.sd_setImage(with: mentionUrl, placeholderImage: nil)
                    cell.mentionImageView.layer.cornerRadius = 10
                    cell.mentionImageView.clipsToBounds = true
                    cell.mentionUsernameLabel.text = "@" + self.mentionMatches[indexPath.row].user_name
                }
            }
            return cell
        } else {
            if indexPath.section == 0 {
                if noMoreMessages {
                    if let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath) as? LoadingCell {
                        cell.loadingSpinner?.isHidden = true
                        cell.loadingLabel?.isHidden = false
                        return cell
                    }
                   return UITableViewCell()
                } else {
                    if let cell = tableView.dequeueReusableCell(withIdentifier: "loadingCell", for: indexPath) as? LoadingCell {
                        cell.loadingSpinner?.startAnimating()
                        return cell
                    }
                    return UITableViewCell()
                }
            } else {
                if indexPath.row == 0 {
                    //FIRST MESSAGE
                    if messages[indexPath.row].media == true {
                        //FIRST MESSAGE, MEDIA MESSAGE
                        let cell = tableView.dequeueReusableCell(withIdentifier: "intialPic", for: indexPath) as! BunchMessagingInitialPicCell
                        
                        if let urlUrl = URL.init(string: messages[indexPath.row].owner_url) {
                            cell.profileImageView.sd_setImage(with: urlUrl, placeholderImage: nil)
                            cell.profileImageView.layer.cornerRadius = 10
                            cell.profileImageView.clipsToBounds = true
                            
                            let TapGesture = UITapGestureRecognizer(target: self, action: #selector(handleProfile))
                            cell.profileImageView.tag = indexPath.row
                            cell.profileImageView.addGestureRecognizer(TapGesture)
                            cell.profileImageView.isUserInteractionEnabled = true
                        }
                        
                        if !noLikes(row: indexPath.row) {
                            cell.likeCount.textColor = UIColor(named: "zazuPurple")
                            cell.likeCount.text = String(self.messages[indexPath.row].likes.count)
                            if let text = cell.likeCount.text, let value = Int(text) {
                                cell.likeCount.text = String(value)
                            }
                        } else {
                            cell.likeCount.text = ""
                        }
                        
                        //clicked like BUTTON ON FIRST MESSAGE
                        cell.likeButtonTapped = {
                            if self.messageUidLikes.contains(self.messages[indexPath.row].uid) {
                                cell.likeButton.setImage(UIImage(named: "heart_not_liked"), for: .normal)
                                if let index = self.messageUidLikes.index(of: self.messages[indexPath.row].uid) {
                                    self.messageUidLikes.remove(at: index)
                                }
                                self.handleLike(row: indexPath.row, add: false)
                                
                                if let text = cell.likeCount.text, let value = Int(text) {
                                    cell.likeCount.textColor = UIColor.black
                                    if value == 1 {
                                        cell.likeCount.text = ""
                                    } else {
                                        cell.likeCount.text = String(value - 1)
                                    }
                                }
                            } else {
                                cell.likeCount.textColor = UIColor(named: "zazuPurple")
                                if let text = cell.likeCount.text, let value = Int(text) {
                                    cell.likeCount.text = String(value + 1)
                                }
                                if cell.likeCount.text == "" {cell.likeCount.text = "1"}
                                
                                self.handleLike(row: indexPath.row, add: true)
                                self.messageUidLikes.append(self.messages[indexPath.row].uid)
                                cell.likeButton.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                                cell.likeButton.setImage(UIImage(named: "heart_liked"), for: .normal)
                                
                                let loadingPulseLayer = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
                                loadingPulseLayer.duration = 0.5
                                loadingPulseLayer.fromValue = 0.3
                                loadingPulseLayer.toValue = 1
                                loadingPulseLayer.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                                cell.likeButton.layer.add(loadingPulseLayer, forKey: "animateOpacity")
                                
                                UIView.animate(withDuration: 0.50,
                                               delay: 0,
                                               usingSpringWithDamping: 0.5,
                                               initialSpringVelocity: 5.0,
                                               options: .allowUserInteraction,
                                               animations: {
                                                cell.likeButton.transform = .identity
                                    },
                                               completion: nil)
                            }
                        }
                        
                        //clicked reply button
                        cell.replyButtonTapped = {
                            self.handleReply(uid: self.messages[indexPath.row].owner_uid, username: self.messages[indexPath.row].owner_user_name)
                        }
                        
                        cell.messageUserNameView.text = messages[indexPath.row].owner_user_name
                        cell.messageImage.sd_setImageWithURLWithFade(url: URL(string: messages[indexPath.row].text)! as NSURL, placeholderImage: nil)
                        cell.messageImage.layer.cornerRadius = 10
                        cell.messageImage.clipsToBounds = true
                        cell.messageImage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
                        cell.messageTimeReference.text = messages[indexPath.row].interval_reference.getTimeStamp(interval: messages[indexPath.row].interval_reference)
                        cell.messageContainerView.layer.cornerRadius = 10
                        cell.messageContainerView.clipsToBounds = true
                        return cell
                    } else {
                        //FIRST ROW, NOT MEDIA MESSAGE
                        let cell = tableView.dequeueReusableCell(withIdentifier: "message", for: indexPath) as! BunchMessagingCell
                        if let urlUrl = URL.init(string: messages[indexPath.row].owner_url) {
                            cell.profileImageView.sd_setImage(with: urlUrl, placeholderImage: nil)
                            cell.profileImageView.layer.cornerRadius = 10
                            cell.profileImageView.clipsToBounds = true
                            
                            let TapGesture = UITapGestureRecognizer(target: self, action: #selector(handleProfile))
                            cell.profileImageView.tag = indexPath.row
                            cell.profileImageView.addGestureRecognizer(TapGesture)
                            cell.profileImageView.isUserInteractionEnabled = true
                        }
                        
                        if self.messageUidLikes.contains(self.messages[indexPath.row].uid) {
                            //user has liked message
                            cell.likeButton.setImage(UIImage(named: "heart_liked"), for: .normal)
                        } else {
                            cell.likeButton.setImage(UIImage(named: "heart_not_liked"), for: .normal)
                        }
                        
                        if !noLikes(row: indexPath.row) {
                            cell.likeCount.textColor = UIColor(named: "zazuPurple")
                            cell.likeCount.text = String(self.messages[indexPath.row].likes.count)
                            if let text = cell.likeCount.text, let value = Int(text) {
                                cell.likeCount.text = String(value)
                            }
                        } else {
                            cell.likeCount.text = ""
                        }
                        
                        cell.messageUserNameView.text = messages[indexPath.row].owner_user_name
                        cell.messageTimeReference.text = messages[indexPath.row].interval_reference.getTimeStamp(interval: messages[indexPath.row].interval_reference)
                        cell.messageBodyLabel.text = trimWhiteSpace(text: messages[indexPath.row].text)
                        
                        cell.messageBodyLabel.customize { label in
                            label.mentionColor = UIColor(named: "zazuPurple") ?? UIColor.green
                            label.URLColor = UIColor(red: 85.0/255, green: 238.0/255, blue: 151.0/255, alpha: 1)
                            label.handleMentionTap  { mention in
                                if mention != "Everyone" {
                                    var keys = [String]()
                                    for (key, list) in self.messages[indexPath.row].mentions {
                                        if ((list as AnyObject).contains(mention)) {
                                            keys.append(key as! String)
                                            if let uid = Auth.auth().currentUser?.uid {
                                                if uid != keys[0] {
                                                    if let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "UsersProfileVC") as? UsersProfileViewController {
                                                        VC1.selectedUserUid = keys[0]
                                                        self.navigationController!.pushViewController(VC1, animated: true)
                                                    }
                                                } else {
                                                    self.tabBarController?.selectedIndex = 1
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    self.toBunchInfo()
                                }
                            }
                            label.handleURLTap {url in
                                if let viewController1 = self.storyboard?.instantiateViewController(withIdentifier: "VCTOS") as? ViewControllerTermsOfService {
                                    viewController1.modalPresentationStyle = .overFullScreen
                                    viewController1.url = url.absoluteString
                                    viewController1.webViewTitle = ""
                                    self.present(viewController1, animated: true, completion: {
                                    })
                                }
                            }
                        }
                        
                        cell.messageContainerView.layer.cornerRadius = 10
                        cell.messageContainerView.clipsToBounds = true
                        
                        //clicked like BUTTON ON FIRST MESSAGE
                        cell.likeButtonTapped = {
                            if self.messageUidLikes.contains(self.messages[indexPath.row].uid) {
                                cell.likeButton.setImage(UIImage(named: "heart_not_liked"), for: .normal)
                                if let index = self.messageUidLikes.index(of: self.messages[indexPath.row].uid) {
                                    self.messageUidLikes.remove(at: index)
                                }
                                self.handleLike(row: indexPath.row, add: false)
                                if let text = cell.likeCount.text, let value = Int(text) {
                                    //Write your code here
                                    cell.likeCount.textColor = UIColor.black
                                    if value == 1 {
                                        cell.likeCount.text = ""
                                    } else {
                                        cell.likeCount.text = String(value - 1)
                                    }
                                }
                            } else {
                                cell.likeCount.textColor = UIColor(named: "zazuPurple")
                                if let text = cell.likeCount.text, let value = Int(text) {
                                    cell.likeCount.text = String(value + 1)
                                }
                                if cell.likeCount.text == "" {cell.likeCount.text = "1"}
                                
                                self.handleLike(row: indexPath.row, add: true)
                                self.messageUidLikes.append(self.messages[indexPath.row].uid)
                                cell.likeButton.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                                cell.likeButton.setImage(UIImage(named: "heart_liked"), for: .normal)
                                
                                let loadingPulseLayer = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
                                loadingPulseLayer.duration = 0.5
                                loadingPulseLayer.fromValue = 0.3
                                loadingPulseLayer.toValue = 1
                                loadingPulseLayer.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                                cell.likeButton.layer.add(loadingPulseLayer, forKey: "animateOpacity")
                                
                                UIView.animate(withDuration: 0.50,
                                               delay: 0,
                                               usingSpringWithDamping: 0.5,
                                               initialSpringVelocity: 5.0,
                                               options: .allowUserInteraction,
                                               animations: {
                                                cell.likeButton.transform = .identity
                                    },
                                               completion: nil)
                            }
                        }
                        
                        //clicked reply button
                        cell.replyButtonTapped = {
                            self.handleReply(uid: self.messages[indexPath.row].owner_uid, username: self.messages[indexPath.row].owner_user_name)
                        }
                        
                        return cell
                    }
                } else {
                    //not the first message in table view
                    if self.messages[indexPath.row - 1].owner_uid != messages[indexPath.row].owner_uid {
                        
                        //not the same owner, need message with profile pic
                        if messages[indexPath.row].media == true {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "intialPic", for: indexPath) as!                 BunchMessagingInitialPicCell
                            
                            if let urlUrl = URL.init(string: messages[indexPath.row].owner_url) {
                                cell.profileImageView.sd_setImage(with: urlUrl, placeholderImage: nil)
                                cell.profileImageView.layer.cornerRadius = 10
                                cell.profileImageView.clipsToBounds = true
                                
                                let TapGesture = UITapGestureRecognizer(target: self, action: #selector(handleProfile))
                                cell.profileImageView.tag = indexPath.row
                                cell.profileImageView.addGestureRecognizer(TapGesture)
                                cell.profileImageView.isUserInteractionEnabled = true
                            }
                            
                            if self.messageUidLikes.contains(self.messages[indexPath.row].uid) {
                                //user has liked message
                                cell.likeButton.setImage(UIImage(named: "heart_liked"), for: .normal)
                            } else {
                                cell.likeButton.setImage(UIImage(named: "heart_not_liked"), for: .normal)
                            }
                            
                            if !noLikes(row: indexPath.row) {
                                cell.likeCount.textColor = UIColor(named: "zazuPurple")
                                cell.likeCount.text = String(self.messages[indexPath.row].likes.count)
                                if let text = cell.likeCount.text, let value = Int(text) {
                                    cell.likeCount.text = String(value)
                                }
                            } else {
                                cell.likeCount.text = ""
                            }
                            
                            cell.messageUserNameView.text = messages[indexPath.row].owner_user_name
                            cell.messageImage.sd_setImageWithURLWithFade(url: URL(string: messages[indexPath.row].text)! as NSURL, placeholderImage: nil)
                            cell.messageImage.layer.cornerRadius = 10
                            cell.messageImage.clipsToBounds = true
                            cell.messageImage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
                            
                            //clicked like BUTTON ON FIRST MESSAGE
                            cell.likeButtonTapped = {
                                if self.messageUidLikes.contains(self.messages[indexPath.row].uid) {
                                    cell.likeButton.setImage(UIImage(named: "heart_not_liked"), for: .normal)
                                    if let index = self.messageUidLikes.index(of: self.messages[indexPath.row].uid) {
                                        self.messageUidLikes.remove(at: index)
                                    }
                                    self.handleLike(row: indexPath.row, add: false)
                                    
                                    if let text = cell.likeCount.text, let value = Int(text) {
                                        //Write your code here
                                        cell.likeCount.textColor = UIColor.black
                                        if value == 1 {
                                            cell.likeCount.text = ""
                                        } else {
                                            cell.likeCount.text = String(value - 1)
                                        }
                                    }
                                } else {
                                    cell.likeCount.textColor = UIColor(named: "zazuPurple")
                                    if let text = cell.likeCount.text, let value = Int(text) {
                                        cell.likeCount.text = String(value + 1)
                                    }
                                    if cell.likeCount.text == "" {cell.likeCount.text = "1"}
                                    self.handleLike(row: indexPath.row, add: true)
                                    self.messageUidLikes.append(self.messages[indexPath.row].uid)
                                    cell.likeButton.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                                    cell.likeButton.setImage(UIImage(named: "heart_liked"), for: .normal)
                                    
                                    let loadingPulseLayer = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
                                    loadingPulseLayer.duration = 0.5
                                    loadingPulseLayer.fromValue = 0.3
                                    loadingPulseLayer.toValue = 1
                                    loadingPulseLayer.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                                    cell.likeButton.layer.add(loadingPulseLayer, forKey: "animateOpacity")
                                    
                                    UIView.animate(withDuration: 0.50,
                                                   delay: 0,
                                                   usingSpringWithDamping: 0.5,
                                                   initialSpringVelocity: 5.0,
                                                   options: .allowUserInteraction,
                                                   animations: {
                                                    cell.likeButton.transform = .identity
                                        },
                                                   completion: nil)
                                }
                            }
                            
                            //clicked reply button
                            cell.replyButtonTapped = {
                                self.handleReply(uid: self.messages[indexPath.row].owner_uid, username: self.messages[indexPath.row].owner_user_name)
                            }
                            
                            cell.messageTimeReference.text = messages[indexPath.row].interval_reference.getTimeStamp(interval: messages[indexPath.row].interval_reference)
                            cell.messageContainerView.layer.cornerRadius = 10
                            cell.messageContainerView.clipsToBounds = true
                            return cell
                        } else {
                            //not the same owner, need message with profile pic
                            let cell = tableView.dequeueReusableCell(withIdentifier: "message", for: indexPath) as! BunchMessagingCell
                            
                            if let urlUrl = URL.init(string: messages[indexPath.row].owner_url) {
                                cell.profileImageView.sd_setImage(with: urlUrl, placeholderImage: nil)
                                cell.profileImageView.layer.cornerRadius = 10
                                cell.profileImageView.clipsToBounds = true
                                
                                let TapGesture = UITapGestureRecognizer(target: self, action: #selector(handleProfile))
                                cell.profileImageView.tag = indexPath.row
                                cell.profileImageView.addGestureRecognizer(TapGesture)
                                cell.profileImageView.isUserInteractionEnabled = true
                            }
                            
                            if self.messageUidLikes.contains(self.messages[indexPath.row].uid) {
                                //user has liked message
                                cell.likeButton.setImage(UIImage(named: "heart_liked"), for: .normal)
                            } else {
                                cell.likeButton.setImage(UIImage(named: "heart_not_liked"), for: .normal)
                            }
                            
                            if !noLikes(row: indexPath.row) {
                                cell.likeCount.textColor = UIColor(named: "zazuPurple")
                                cell.likeCount.text = String(self.messages[indexPath.row].likes.count)
                                if let text = cell.likeCount.text, let value = Int(text) {
                                    cell.likeCount.text = String(value)
                                }
                            } else {
                                cell.likeCount.text = ""
                            }
                            
                            //clicked like BUTTON ON FIRST MESSAGE
                            cell.likeButtonTapped = {
                                if self.messageUidLikes.contains(self.messages[indexPath.row].uid) {
                                    cell.likeButton.setImage(UIImage(named: "heart_not_liked"), for: .normal)
                                    if let index = self.messageUidLikes.index(of: self.messages[indexPath.row].uid) {
                                        self.messageUidLikes.remove(at: index)
                                    }
                                    self.handleLike(row: indexPath.row, add: false)
                                    
                                    if let text = cell.likeCount.text, let value = Int(text) {
                                        //Write your code here
                                        cell.likeCount.textColor = UIColor.black
                                        if value == 1 {
                                            cell.likeCount.text = ""
                                        } else {
                                            cell.likeCount.text = String(value - 1)
                                        }
                                    }
                                } else {
                                    cell.likeCount.textColor = UIColor(named: "zazuPurple")
                                    if let text = cell.likeCount.text, let value = Int(text) {
                                        cell.likeCount.text = String(value + 1)
                                    }
                                    if cell.likeCount.text == "" {cell.likeCount.text = "1"}
                                    self.handleLike(row: indexPath.row, add: true)
                                    self.messageUidLikes.append(self.messages[indexPath.row].uid)
                                    cell.likeButton.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                                    cell.likeButton.setImage(UIImage(named: "heart_liked"), for: .normal)
                                    
                                    let loadingPulseLayer = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
                                    loadingPulseLayer.duration = 0.5
                                    loadingPulseLayer.fromValue = 0.3
                                    loadingPulseLayer.toValue = 1
                                    loadingPulseLayer.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                                    cell.likeButton.layer.add(loadingPulseLayer, forKey: "animateOpacity")
                                    
                                    UIView.animate(withDuration: 0.50,
                                                   delay: 0,
                                                   usingSpringWithDamping: 0.5,
                                                   initialSpringVelocity: 5.0,
                                                   options: .allowUserInteraction,
                                                   animations: {
                                                    cell.likeButton.transform = .identity
                                        },
                                                   completion: nil)
                                }
                            }
                            
                            cell.replyButtonTapped = {
                                self.handleReply(uid: self.messages[indexPath.row].owner_uid, username: self.messages[indexPath.row].owner_user_name)
                            }
                            
                            cell.messageUserNameView.text = messages[indexPath.row].owner_user_name
                            cell.messageTimeReference.text = messages[indexPath.row].interval_reference.getTimeStamp(interval: messages[indexPath.row].interval_reference)
                            cell.messageBodyLabel.text = trimWhiteSpace(text: messages[indexPath.row].text)
                            
                            cell.messageBodyLabel.customize { label in
                                label.mentionColor = UIColor(named: "zazuPurple") ?? UIColor.green
                                label.URLColor = UIColor(red: 85.0/255, green: 238.0/255, blue: 151.0/255, alpha: 1)
                                label.handleMentionTap  { mention in
                                    if mention != "Everyone" {
                                        var keys = [String]()
                                        for (key, list) in self.messages[indexPath.row].mentions {
                                            if ((list as AnyObject).contains(mention)) {
                                                keys.append(key as! String)
                                                if let uid = Auth.auth().currentUser?.uid {
                                                    if uid != keys[0] {
                                                        if let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "UsersProfileVC") as? UsersProfileViewController {
                                                            VC1.selectedUserUid = keys[0]
                                                            self.navigationController!.pushViewController(VC1, animated: true)
                                                        }
                                                    } else {
                                                        self.tabBarController?.selectedIndex = 1
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        self.toBunchInfo()
                                    }
                                }
                                label.handleURLTap {url in
                                    if let viewController1 = self.storyboard?.instantiateViewController(withIdentifier: "VCTOS") as? ViewControllerTermsOfService {
                                        viewController1.modalPresentationStyle = .overFullScreen
                                        viewController1.url = url.absoluteString
                                        viewController1.webViewTitle = "u"
                                        self.present(viewController1, animated: true, completion: {
                                        })
                                    }
                                }
                            }
                            
                            cell.messageContainerView.layer.cornerRadius = 10
                            cell.messageContainerView.clipsToBounds = true
                            
                            return cell
                        }
                    } else {
                        if messages[indexPath.row].media == true {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "additionalPic", for: indexPath) as! BunchMessagingAdditionalPicTableViewCell
                            
                            cell.messageImage.sd_setImageWithURLWithFade(url: URL(string: messages[indexPath.row].text)! as NSURL, placeholderImage: nil)
                            cell.messageImage.layer.cornerRadius = 10
                            cell.messageImage.clipsToBounds = true
                            cell.messageImage.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
                            
                            let TapGesture = UITapGestureRecognizer(target: self, action: #selector(handleProfile))
                            cell.messageImage.tag = indexPath.row
                            cell.messageImage.addGestureRecognizer(TapGesture)
                            cell.messageImage.isUserInteractionEnabled = true
                            
                            if self.messageUidLikes.contains(self.messages[indexPath.row].uid) {
                                //user has liked message
                                cell.likeButton.setImage(UIImage(named: "heart_liked"), for: .normal)
                            } else {
                                cell.likeButton.setImage(UIImage(named: "heart_not_liked"), for: .normal)
                            }
                            
                            if !noLikes(row: indexPath.row) {
                                cell.likeCount.textColor = UIColor(named: "zazuPurple")
                                cell.likeCount.text = String(self.messages[indexPath.row].likes.count)
                                if let text = cell.likeCount.text, let value = Int(text) {
                                    cell.likeCount.text = String(value)
                                }
                            } else {
                                cell.likeCount.text = ""
                            }
                            
                            //clicked like BUTTON ON FIRST MESSAGE
                            cell.likeButtonTapped = {
                                if self.messageUidLikes.contains(self.messages[indexPath.row].uid) {
                                    cell.likeButton.setImage(UIImage(named: "heart_not_liked"), for: .normal)
                                    if let index = self.messageUidLikes.index(of: self.messages[indexPath.row].uid) {
                                        self.messageUidLikes.remove(at: index)
                                    }
                                    self.handleLike(row: indexPath.row, add: false)
                                    
                                    if let text = cell.likeCount.text, let value = Int(text) {
                                        //Write your code here
                                        cell.likeCount.textColor = UIColor.black
                                        if value == 1 {
                                            cell.likeCount.text = ""
                                        } else {
                                            cell.likeCount.text = String(value - 1)
                                        }
                                    }
                                } else {
                                    cell.likeCount.textColor = UIColor(named: "zazuPurple")
                                    if let text = cell.likeCount.text, let value = Int(text) {
                                        cell.likeCount.text = String(value + 1)
                                    }
                                    if cell.likeCount.text == "" {cell.likeCount.text = "1"}
                                    
                                    self.handleLike(row: indexPath.row, add: true)
                                    self.messageUidLikes.append(self.messages[indexPath.row].uid)
                                    cell.likeButton.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                                    cell.likeButton.setImage(UIImage(named: "heart_liked"), for: .normal)
                                    
                                    let loadingPulseLayer = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
                                    loadingPulseLayer.duration = 0.5
                                    loadingPulseLayer.fromValue = 0.3
                                    loadingPulseLayer.toValue = 1
                                    loadingPulseLayer.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                                    cell.likeButton.layer.add(loadingPulseLayer, forKey: "animateOpacity")
                                    
                                    UIView.animate(withDuration: 0.50,
                                                   delay: 0,
                                                   usingSpringWithDamping: 0.5,
                                                   initialSpringVelocity: 5.0,
                                                   options: .allowUserInteraction,
                                                   animations: {
                                                    cell.likeButton.transform = .identity
                                        },
                                                   completion: nil)
                                }
                            }
                            
                            //clicked reply button
                            cell.replyButtonTapped = {
                                self.handleReply(uid: self.messages[indexPath.row].owner_uid, username: self.messages[indexPath.row].owner_user_name)
                            }
                            
                            cell.messageTimeReference.text = messages[indexPath.row].interval_reference.getTimeStamp(interval: messages[indexPath.row].interval_reference)
                            cell.messageContainerView.layer.cornerRadius = 10
                            cell.messageContainerView.clipsToBounds = true
                            return cell
                        } else {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "messageAdditional", for: indexPath) as! BunchAdditionalMessagingCell
                            
                            if self.messageUidLikes.contains(self.messages[indexPath.row].uid) {
                                //user has liked message
                                cell.likeButton.setImage(UIImage(named: "heart_liked"), for: .normal)
                            } else {
                                cell.likeButton.setImage(UIImage(named: "heart_not_liked"), for: .normal)
                            }
                            
                            if !noLikes(row: indexPath.row) {
                                cell.likeCount.textColor = UIColor(named: "zazuPurple")
                                cell.likeCount.text = String(self.messages[indexPath.row].likes.count)
                                if let text = cell.likeCount.text, let value = Int(text) {
                                    cell.likeCount.text = String(value)
                                }
                            } else {
                                cell.likeCount.text = ""
                            }
                            
                            //clicked like BUTTON ON FIRST MESSAGE
                            cell.likeButtonTapped = {
                                if self.messageUidLikes.contains(self.messages[indexPath.row].uid) {
                                    cell.likeButton.setImage(UIImage(named: "heart_not_liked"), for: .normal)
                                    if let index = self.messageUidLikes.index(of: self.messages[indexPath.row].uid) {
                                        self.messageUidLikes.remove(at: index)
                                    }
                                    self.handleLike(row: indexPath.row, add: false)
                                    
                                    if let text = cell.likeCount.text, let value = Int(text) {
                                        //Write your code here
                                        cell.likeCount.textColor = UIColor.black
                                        if value == 1 {
                                            cell.likeCount.text = ""
                                        } else {
                                            cell.likeCount.text = String(value - 1)
                                        }
                                    }
                                } else {
                                    cell.likeCount.textColor = UIColor(named: "zazuPurple")
                                    if let text = cell.likeCount.text, let value = Int(text) {
                                        cell.likeCount.text = String(value + 1)
                                    }
                                    if cell.likeCount.text == "" {cell.likeCount.text = "1"}
                                    self.handleLike(row: indexPath.row, add: true)
                                    self.messageUidLikes.append(self.messages[indexPath.row].uid)
                                    cell.likeButton.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                                    cell.likeButton.setImage(UIImage(named: "heart_liked"), for: .normal)
                                    
                                    let loadingPulseLayer = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
                                    loadingPulseLayer.duration = 0.5
                                    loadingPulseLayer.fromValue = 0.3
                                    loadingPulseLayer.toValue = 1
                                    loadingPulseLayer.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                                    cell.likeButton.layer.add(loadingPulseLayer, forKey: "animateOpacity")
                                    
                                    UIView.animate(withDuration: 0.50,
                                                   delay: 0,
                                                   usingSpringWithDamping: 0.5,
                                                   initialSpringVelocity: 5.0,
                                                   options: .allowUserInteraction,
                                                   animations: {
                                                    cell.likeButton.transform = .identity
                                        },
                                                   completion: nil)
                                }
                            }
                            
                            cell.replyButtonTapped = {
                                self.handleReply(uid: self.messages[indexPath.row].owner_uid, username: self.messages[indexPath.row].owner_user_name)
                            }
                            
                            cell.messageTimeReference.text = messages[indexPath.row].interval_reference.getTimeStamp(interval: messages[indexPath.row].interval_reference)
                            cell.messageBodyLabel.text = trimWhiteSpace(text: messages[indexPath.row].text)
                            
                            cell.messageBodyLabel.customize { label in
                                label.mentionColor = UIColor(named: "zazuPurple") ?? UIColor.green
                                label.URLColor = UIColor(red: 85.0/255, green: 238.0/255, blue: 151.0/255, alpha: 1)
                                label.handleMentionTap { mention in
                                    if mention != "Everyone" {
                                        var keys = [String]()
                                        for (key, list) in self.messages[indexPath.row].mentions {
                                            if ((list as AnyObject).contains(mention)) {
                                                keys.append(key as! String)
                                                if let uid = Auth.auth().currentUser?.uid {
                                                    if uid != keys[0] {
                                                        if let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "UsersProfileVC") as? UsersProfileViewController {
                                                            VC1.selectedUserUid = keys[0]
                                                            self.navigationController!.pushViewController(VC1, animated: true)
                                                        }
                                                    } else {
                                                        self.tabBarController?.selectedIndex = 1
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        self.toBunchInfo()
                                    }
                                }
                                label.handleURLTap {url in
                                    if let viewController1 = self.storyboard?.instantiateViewController(withIdentifier: "VCTOS") as? ViewControllerTermsOfService {
                                        viewController1.modalPresentationStyle = .overFullScreen
                                        viewController1.url = url.absoluteString
                                        viewController1.webViewTitle = "url"
                                        self.present(viewController1, animated: true, completion: {
                                        })
                                    }
                                }
                            }
                            cell.messageContainerView.layer.cornerRadius = 10
                            cell.messageContainerView.clipsToBounds = true
                            
                            return cell
                        }
                    }
                }
            }
        }
    }
    
    
    func noLikes(row: Int) -> Bool {
        let keyExists = self.messages[row].likes[""] != nil
        if keyExists {
            return true
        } else {
            return false
        }
    }
    
    func handleLike(row: Int, add: Bool) {
        if let uid = Auth.auth().currentUser?.uid {
            if let EventCity = self.eventObject?.cd_event_city {
                if let EventState = self.eventObject?.cd_event_state {
                    if let EventCountry = self.eventObject?.cd_event_country {
                        if let BunchUid = self.eventObject?.cd_event_uid {
                            let groupMessageUid = self.messages[row].uid
                            
                            if add {
                                //add like
                                let likeRef = Database.database().reference().child("EVENT_MESSAGING/\(EventCountry)/\(EventState)/\(EventCity)/\(BunchUid)/MESSAGES/\(groupMessageUid)/likes/\(uid)")
                                let obj = ["uid": uid, "thumbnail_url":  self.user_photo_url, "user_name": self.user_name]
                                likeRef.setValue(obj)
                            } else {
                                //remove like
                                let likeRef = Database.database().reference().child("EVENT_MESSAGING/\(EventCountry)/\(EventState)/\(EventCity)/\(BunchUid)/MESSAGES/\(groupMessageUid)/likes/\(uid)")
                                likeRef.removeValue()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func handleReply(uid: String, username: String) {
        let mention = "@" + String(username.filter { !" \n\t\r".contains($0) }) + " "
        self.typingMessagingTextView.text = mention
        self.typingMessagingTextView.textColor = UIColor.black
        checkButtons(isEmpty: false)
    }
    
    func sendMessage(text: String, mediaPresent: Bool) {
        if mediaPresent {
            self.progressView.setProgress(0.8, animated: false)
        }
        
        guard let owner_uid = Auth.auth().currentUser?.uid else { return }
        let owner_user_name = self.user_name
        let interval_reference = getCurrentTimeAsEpoch()
        let message_text = trimWhiteSpace(text: text)
        let owner_url = self.user_photo_url
        
        let mentionDictionary = filterMessageMentions(message: text)
        let chat: NSDictionary = mentionDictionary as NSDictionary
        
        if let BunchUid = self.eventObject?.cd_event_uid {
            if let EventCity = self.eventObject?.cd_event_city {
                if let EventState = self.eventObject?.cd_event_state {
                    if let EventCountry = self.eventObject?.cd_event_country {
                        let messageRef = Database.database().reference().child("EVENT_MESSAGING/\(EventCountry)/\(EventState)/\(EventCity)/\(BunchUid)/MESSAGES")
                        let messageKeyRef = messageRef.childByAutoId()
                        
                        Database.database().reference().child("EVENT_MESSAGING/\(EventCountry)/\(EventState)/\(EventCity)/\(BunchUid)/MESSAGES").observeSingleEvent(of: .value, with: { (snapshot) in
                            
                            let mes = GroupMessage(uid: messageKeyRef.key, owner_uid: owner_uid, owner_user_name: owner_user_name, interval_reference:  interval_reference, text: message_text, owner_url: owner_url, mentions: chat, likes: self.emptyLikes as NSDictionary, media: mediaPresent)
                            
                            let messageObject = mes.toAnyObject()
                            messageKeyRef.setValue(messageObject, withCompletionBlock: {error, ref in
                                if error == nil{
                                    self.scrollToBottomAnimated()
                                }
                            })
                        })
                    }
                }
            }
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        if self.mentionCommentIndexPath != nil {
            animateMentionCell()
        }
    }
    
    func animateMentionCell() {
        let cell = tableView.cellForRow(at: self.mentionCommentIndexPath!)
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.06
        animation.repeatCount = 2
        animation.autoreverses = true
        
        animation.fromValue = NSValue(cgPoint: CGPoint(x: (cell?.center.x)! - 5, y: (cell?.center.y)!))
        animation.toValue = NSValue(cgPoint: CGPoint(x: (cell?.center.x)! + 5, y: (cell?.center.y)!))
        cell?.layer.add(animation, forKey: "position")
    }
    
    
    func filterMessageMentions(message: String) -> NSDictionary {
        var emptyDictionary = [String: Any]()
        var isEveryoneAlreadyTagged = false
        
        if self.groupAttendeeObjects.count - 1 < 0 {
            return emptyDictionary as NSDictionary
        }
        
        for n in 0...self.groupAttendeeObjects.count - 1 {
            if message.contains("@" + self.groupAttendeeObjects[n].user_name) {
                if self.groupAttendeeObjects[n].user_name == "Everyone" && !isEveryoneAlreadyTagged {
                    isEveryoneAlreadyTagged = true
                    for n in 0...self.groupAttendeeObjects.count - 1 {
                        if self.groupAttendeeObjects[n].user_name != "Everyone" {
                            emptyDictionary[self.groupAttendeeObjects[n].uid] = self.groupAttendeeObjects[n].user_name
                        }
                    }
                } else {
                    emptyDictionary[self.groupAttendeeObjects[n].uid] = self.groupAttendeeObjects[n].user_name
                }
            }
        }
        return emptyDictionary as NSDictionary
    }
    
    func scrollToBottomAnimated(){
        if self.messages.count > 0 {
            let indexPath = IndexPath(row: self.messages.count-1, section: 1)
            
            DispatchQueue.main.async {
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    
    func scrollToBottom() {
        DispatchQueue.main.async {
            if self.messages.count != 0 {
                let indexPath = IndexPath(row: self.messages.count-1, section: 1)
                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
            } else {
                //zero messages ?? ZaZu should send introductory message
            }
        }
    }
    
    func handleViews() {
        self.typingMessagingTextView.text = placeholderGrayText
        self.typingMessagingTextView.textColor = UIColor.lightGray
        self.typingMessagingTextView.selectedTextRange = self.typingMessagingTextView.textRange(from: self.typingMessagingTextView.beginningOfDocument, to: self.typingMessagingTextView.beginningOfDocument)
        self.checkButtons(isEmpty: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.scrollToBottomAnimated()
    }
    
    func getCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            self.currentUserCoreData = peep
            self.current_city = currentUserCoreData[0].location_city!
            self.current_state = currentUserCoreData[0].location_state!
            self.current_country = currentUserCoreData[0].location_country!
            self.user_name = currentUserCoreData[0].user_name!
            self.user_photo_url = currentUserCoreData[0].profile_photo_url!.absoluteString
            
            fetchMessages()
            fetchAttendees()
        } catch {
            print("error fetching")
        }
    }
    
    @objc func handleKeyboardNotification(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            
            self.bottomConstraint?.constant = -(keyboardRectangle.height)
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            if isKeyboardShowing {
                scrollToBottomAnimated()
            }
            
            self.bottomConstraint?.constant = isKeyboardShowing ? -(keyboardRectangle.height) : 0
            
            UIView.animate(withDuration: 0, delay: 0, animations:{
                self.view.layoutIfNeeded()
            })
        }
    }
    
    func fetchMessages() {
        if let uid = Auth.auth().currentUser?.uid {
            self.currentUid = uid
        }
        
        if let BunchUid = self.eventObject?.cd_event_uid {
            if let EventCity = self.eventObject?.cd_event_city {
                if let EventState = self.eventObject?.cd_event_state {
                    if let EventCountry = self.eventObject?.cd_event_country {
                        Database.database().reference().child("EVENT_MESSAGING/\(EventCountry)/\(EventState)/\(EventCity)/\(BunchUid)/MESSAGES").queryLimited(toLast: 15).observe(.childAdded) { (snapshot) in
                            
                            
                            if (snapshot.value as? [AnyHashable: AnyObject]) != nil {
                                let message = GroupMessage(snapshot: snapshot)
                                self.messages.append(message)
                                
                                for like in message.likes {
                                    if like.key as? String == self.currentUid {
                                        //add messageUid to like array
                                        self.messageUidLikes.append(message.uid)
                                    }
                                }
                                
                                DispatchQueue.main.async {
                                    self.progressView.setProgress(0.0, animated: false)
                                    self.tableView.reloadData()
                                }
                            }
                        }
                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func fetchAttendees() {
        if let EventCity = self.eventObject?.cd_event_city {
            if let EventState = self.eventObject?.cd_event_state {
                if let EventCountry = self.eventObject?.cd_event_country {
                    if let BunchUid = self.eventObject?.cd_event_uid {
                        Database.database().reference().child("EVENT_ATTENDEES/\(EventCountry)/\(EventState)/\(EventCity)/\(BunchUid)/ATTENDEES").observe(.childAdded) { (snapshot) in
                            if snapshot.exists() {
                                if (snapshot.value as? [AnyHashable: AnyObject]) != nil {
                                    let user = Attendee(snapshot: snapshot)
                                    self.generateAndValidateUserObjectAndNames(user: user)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func generateAndValidateUserObjectAndNames(user: Attendee) {
        let incomingUser = user
        if incomingUser.accepted != true {
            //isn't added to chat yet, dont do anything
            return
        }
        
        if !self.groupAttendeeObjects.isEmpty {
            for n in 0...self.groupAttendeeObjects.count - 1 {
                if self.groupAttendeeObjects[n].user_name == incomingUser.user_name.removingWhitespaces() {
                    incomingUser.user_name = getUniqueUserName(user_name: incomingUser.user_name.removingWhitespaces())
                    self.groupAttendeeObjects.append(incomingUser)
                    return
                } else {
                    //no errors, append to array
                    let conjoinedUserName = incomingUser.user_name.removingWhitespaces()
                    incomingUser.user_name = conjoinedUserName
                    self.groupAttendeeObjects.append(incomingUser)
                    return
                }
            }
        } else {
            incomingUser.user_name = incomingUser.user_name.removingWhitespaces()
            let everyone = Attendee(url: "", user_name: "Everyone", uid: "", accepted: true, event_captain_uid: "", event_uid: "")
            
            self.groupAttendeeObjects.append(incomingUser)
            self.groupAttendeeObjects.append(everyone)
        }
    }
    
    func getUniqueUserName(user_name: String) -> String {
        var sameNameIndex: Int = 1
        var uniqueUserName: String = ""
        for n in 0...self.groupAttendeeObjects.count - 1 {
            if self.groupAttendeeObjects[n].user_name == (user_name + String(sameNameIndex)) {
                while (self.groupAttendeeObjects[n].user_name == (user_name + String(sameNameIndex))) {
                    sameNameIndex += 1
                }
                uniqueUserName = user_name + String(sameNameIndex)
                
            } else {
                uniqueUserName = (user_name + String(sameNameIndex))
            }
        }
        return uniqueUserName
    }
    
    
    func characterBeforeCursor() -> String? {
        // get the cursor position
        if let cursorRange = self.typingMessagingTextView.selectedTextRange {
            
            // get the position one character before the cursor start position
            if let newPosition = self.typingMessagingTextView.position(from: cursorRange.start, offset: -1) {
                
                let range = self.typingMessagingTextView.textRange(from: newPosition, to: cursorRange.start)
                return self.typingMessagingTextView.text(in: range!)
            }
        }
        return nil
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = self.placeholderGrayText
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)
        
        if text == "" {
            //IF BACKSPACE PUSHED
            if let myText = textView.text {
                //if last character is emoji
                let lastCharacter = myText.suffix(1)
                let lastCharacterString = String(lastCharacter)
                if !lastCharacterString.containsEmoji {
                    if let selectedRange = textView.selectedTextRange {
                        if let myText = textView.text {
                            let cursorOffset = textView.offset(from: textView.beginningOfDocument, to: selectedRange.start)
                            let index = myText.index(myText.startIndex, offsetBy: myText.count)
                            let substring = myText[..<index]
                            let endSubstring = myText[index...]
                            if let lastword = substring.components(separatedBy: " ").last {
                                
                                let currentLw = lastword.dropLast(1)
                                let saniLw = currentLw.dropFirst(1)
                                
                                let matchingTerms = self.groupAttendeeObjects.filter { $0.user_name.localizedCaseInsensitiveContains(saniLw) }
                                if currentLw == "@" {
                                    self.mentionMatches = self.groupAttendeeObjects
                                    self.mentionTableView.reloadData()
                                } else {
                                    self.mentionMatches = matchingTerms
                                    self.mentionTableView.reloadData()
                                }
                                
                                if ((lastword.hasPrefix("@")) && (self.mentionTableView.isHidden)) {
                                    //Check complete word, and only removing one occurance
                                    //SETTING CURSOR
                                    var messageArray = substring.components(separatedBy: " ")
                                    messageArray.removeLast()
                                    
                                    let editedString = messageArray.joined(separator: " ") + " " + endSubstring
                                    self.typingMessagingTextView.text = editedString
                                    
                                    let newCursorPosition = NSMakeRange(cursorOffset - lastword.count, 0);
                                    textView.selectedRange = newCursorPosition
                                    
                                    return false
                                }
                            }
                        }
                    }
                }
            }
        } else if text == " " {
            //Typing
            self.mentionTableView.isHidden = true
        } else {
            let currentText = self.typingMessagingTextView.text + text
            let fullNameArr = currentText.components(separatedBy: " ")
            let lastWord = fullNameArr[fullNameArr.count - 1]
            let firstLetter = lastWord.prefix(1)
            
            if firstLetter == "@" || text == "@" {
                self.mentionTableView.isHidden = false
                let matchingTerms = self.groupAttendeeObjects.filter { $0.user_name.localizedCaseInsensitiveContains(lastWord.dropFirst(1)) }
                if lastWord == "@" {
                    self.mentionMatches = self.groupAttendeeObjects
                    self.mentionTableView.reloadData()
                } else {
                    self.mentionMatches = matchingTerms
                    self.mentionTableView.reloadData()
                }
            } else {
                self.mentionTableView.isHidden = true
            }
        }
        
        //handling cursosr and send Button
        if updatedText.isEmpty {
            checkButtons(isEmpty: true)
            
            textView.text = placeholderGrayText
            textView.textColor = UIColor.lightGray
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        } else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            checkButtons(isEmpty: false)
            
            textView.textColor = UIColor.black
            textView.text = text
        } else if textView.text == self.placeholderGrayText {
            checkButtons(isEmpty: true)
            
        } else {
            checkButtons(isEmpty: false)
            
            textView.textContainer.heightTracksTextView = true
            textView.isScrollEnabled = false
            var frame = textView.frame
            frame.origin.y = frame.maxY - textView.contentSize.height;
            frame.size.height = textView.contentSize.height;
            textView.frame = frame;
            return true
        }
        return false
    }
    
    func checkButtons(isEmpty: Bool) {
        sendButton.layer.cornerRadius = 10
        sendButton.clipsToBounds = true
        if isEmpty {
            sendButton.layer.borderColor = UIColor.lightGray.cgColor
            sendButton.layer.borderWidth = 1
            sendButton.backgroundColor = UIColor.white
            sendButton.setTitleColor(.lightGray, for: .normal)
            sendButton.isEnabled = false
        } else {
            sendButton.backgroundColor = UIColor(named: "zazuBlue")
            sendButton.layer.borderWidth = 0.0
            sendButton.setTitleColor(.white, for: .normal)
            sendButton.isEnabled = true
        }
    }
    
    func trimWhiteSpace(text: String) -> String {
        let sanitizedString = text.trimmingCharacters(in: .whitespacesAndNewlines)
        return sanitizedString
    }
    
    //MARK: DZN EMPTY DATA
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Initiate the Chat!"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "When the attendees of this event start chatting you will see the chat here"
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        if let igm = UIImage(named: "chattingDZN") {
            let resizedImg = resizeImage(image: igm, newWidth: self.view.frame.width/6)
            return resizedImg
        }
        return UIImage(named: "AppIcon")
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        
        return true
        
    }
}

extension String {
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
}

extension String {
    func contains(word : String) -> Bool
    {
        return self.range(of: "\\b\(word)\\b", options: .regularExpression) != nil
    }
}

extension UIImageView {
    
    public func sd_setImageWithURLWithFade(url: NSURL!, placeholderImage placeholder: UIImage!)
    {
        self.sd_setImage(with: url as! URL, placeholderImage: placeholder) { (image, error, cacheType, url) -> Void in
            
            if let downLoadedImage = image
            {
                if cacheType == .none
                {
                    self.alpha = 0
                    UIView.transition(with: self, duration: 0.3, options: UIView.AnimationOptions.transitionCrossDissolve, animations: { () -> Void in
                        self.image = downLoadedImage
                        self.alpha = 1
                    }, completion: nil)
                    
                }
            }
            else
            {
                self.image = placeholder
            }
        }
    }
}

extension String {
    var isInt: Bool {
        return Int(self) != nil
    }
}

func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
    
    let scale = newWidth / image.size.width
    let newHeight = image.size.height * scale
    UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
    image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
    
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage
}

