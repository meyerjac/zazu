//
//  CreateEventVC2.swift
//  corkbored
//
//  Created by Jackson Meyer on 2/25/19.
//  Copyright © 2019 Jackson Meyer. All rights reserved.
//

import UIKit
import ISEmojiView

class CreateEventVC2: UIViewController, EmojiViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    var event: Event!
    var is_editing_existing_event: Bool = false
    var selectedCategoryTag: Int?
    let emojiLimit = 4
    let categoryLabels = Constants.CreateEventViewControllerCategoryTitles
    let categoryImageTitles =  Constants.CreateEventViewControllerCategoryImageTitles
    
    @IBOutlet weak var emojiContainerView: UIView!
    @IBOutlet weak var emojiTextField: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var eventIsPrivate: UISwitch!
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func nextButtonClicked(_ sender: UIButton) {
        handleValidationAndNavigation()
    }
    
    func  handleValidationAndNavigation() {
        guard let emojis = emojiTextField.text, !emojis.isEmpty else {
            shakeInvalidView(view: self.emojiContainerView)
            return
        }
        
        if let gIndex = self.selectedCategoryTag {
            if gIndex == 0 { shakeInvalidView(view: self.collectionView); return }
            
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "createEvent3") as? CreateEventVC3
            self.event.emojis = emojis
            self.event.is_private = self.eventIsPrivate.isOn
            self.event.category = self.categoryLabels[gIndex]
            self.event.category_endInterval = self.event.category + "_" + String(event.end_interval)
            
            vc?.event = self.event
            if is_editing_existing_event {
                //make sure event is edited before sending data
                vc?.is_editing_existing_event = true
            }
            self.navigationController?.pushViewController(vc!, animated: false)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedCategoryTag = 0
        
        if self.is_editing_existing_event {
            if let editEvent = event {
                self.emojiTextField.text = editEvent.emojis
                self.eventIsPrivate.setOn(editEvent.is_private, animated: true)
                if let catTag = self.categoryLabels.index(of: editEvent.category) {
                    self.selectedCategoryTag = catTag
                    DispatchQueue.main.asyncAfter(deadline: .now()) {
                        self.collectionView.scrollToItem(at:IndexPath(item: catTag, section: 0), at: .right, animated: true)
                    }
                }
            }
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                self.emojiTextField.becomeFirstResponder()
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self.collectionView.scrollToItem(at:IndexPath(item: 8, section: 0), at: .right, animated: true)
                }
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
            self.emojiTextField.becomeFirstResponder()
            DispatchQueue.main.asyncAfter(deadline: .now()) {
                 self.collectionView.scrollToItem(at:IndexPath(item: 8, section: 0), at: .right, animated: true)
            }
        }
        
        let keyboardSettings = KeyboardSettings(bottomType: .categories)
        let emojiView = EmojiView(keyboardSettings: keyboardSettings)
        
        emojiView.delegate = self
        emojiView.translatesAutoresizingMaskIntoConstraints = false
        emojiTextField.inputView = emojiView
    }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryLabels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let oldSelectedIndexPath = IndexPath(row: self.selectedCategoryTag!, section: 0)
        if oldSelectedIndexPath == indexPath { return }
        
        //set new
        self.selectedCategoryTag = indexPath.row
        
        let cell = collectionView.cellForItem(at: oldSelectedIndexPath) as? CategoryCollectionViewCell
        //remove last
        cell?.containerView.layer.masksToBounds = true
        cell?.containerView.layer.cornerRadius = 22
        cell?.containerView.layer.borderColor = UIColor.gray.cgColor
        cell?.containerView.layer.borderWidth = 2
        cell?.categoryLabel.textColor = UIColor.gray
        
        let cell1 = collectionView.cellForItem(at: indexPath) as? CategoryCollectionViewCell
        
        //add new
        cell1?.containerView.layer.masksToBounds = true
        cell1?.containerView.layer.cornerRadius = 22
        cell1?.containerView.layer.borderColor = UIColor(red:0.51, green:0.79, blue:1.00, alpha:1.0).cgColor
        cell1?.containerView.layer.borderWidth = 2
        cell1?.categoryLabel.textColor = UIColor.black
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "createBunchCatCell", for: indexPath as IndexPath) as! CreateBunchCollectionViewCell
        
        if indexPath.row == selectedCategoryTag {
            cell.categoryLabel.textColor = UIColor.black
            cell.containerView.layer.borderColor = UIColor(red:0.51, green:0.79, blue:1.00, alpha:1.0).cgColor
            cell.selectedDot.isHidden = false
        } else {
            cell.categoryLabel.textColor = UIColor.gray
            cell.containerView.layer.borderColor = UIColor.gray.cgColor
            cell.selectedDot.isHidden = true
        }
        
        //text
        cell.categoryLabel.text = categoryLabels[indexPath.row]
        cell.categoryLabel.tag = indexPath.row
        
        
        //photo
        cell.categoryImageView.image = UIImage(named: categoryImageTitles[indexPath.row])
        cell.containerView.layer.masksToBounds = true
        cell.containerView.layer.cornerRadius = 22
        cell.containerView.layer.borderWidth = 2
        
        //dot
        cell.selectedDot.tag = indexPath.row
        return cell
    }
    
    func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {
        if let text = emojiTextField.text {
            if text.count != self.emojiLimit {
                 emojiTextField.insertText(emoji)
            }
        }
    }
    
    func emojiViewDidPressDeleteBackwardButton(_ emojiView: EmojiView) {
        self.emojiTextField.deleteBackward()
    }

    // callback when tap dismiss button on keyboard
    func emojiViewDidPressDismissKeyboardButton(_ emojiView: EmojiView) {
        self.emojiTextField.resignFirstResponder()
    }
    
    func shakeInvalidView(view: UIView) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: view.center.x - 5, y: view.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: view.center.x + 5, y: view.center.y))
        
        view.layer.add(animation, forKey: "position")
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
