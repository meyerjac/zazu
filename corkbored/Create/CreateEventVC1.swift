//
//  CreateEventSetupVCViewController.swift
//  corkbored
//
//  Created by Jackson Meyer on 2/25/19.
//  Copyright © 2019 Jackson Meyer. All rights reserved.
//

import UIKit

class CreateEventVC1: UIViewController,  UITextViewDelegate, UITextFieldDelegate, UINavigationControllerDelegate {
    var dif: CGFloat = 0.0
    var y: CGFloat = 0.0
    var event: Event!
    var is_editing_existing_event: Bool = false
    
    var clickedTextField: UITextField?
    var senderNumber: Int?
    var startTimeFromPicker: String?
    var endTimeFromPicker: String?
    var numberOfCharactersInTitle: Int? = 0
    let picker = UIDatePicker()
    var startDateDateObject: String = ""
    var eventStartDateTime = Date()
    var eventEndDateTime = Date()
    var threeEventEmojis: String = ""
    var firstResponderTag: Int? = 100

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var charactersRemaining: UILabel!
    @IBOutlet weak var shortTextDescription: UITextView!
    @IBOutlet weak var startTimeTextFieldContainer: UIView!
    @IBOutlet weak var endTimeTextFieldContainer: UIView!
    @IBOutlet weak var startTimeTextField: UITextField!
    @IBOutlet weak var endTimeTextField: UITextField!
    @IBAction func arrowOnTimeInputViewClicked(_ sender: UIButton) {
        if sender.tag == 0 {
            startTimeTextField.becomeFirstResponder()
        } else {
            endTimeTextField.becomeFirstResponder()
        }
    }
    
    @IBAction func cancelButtonClicked(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
    
    @IBAction func nextButtonClicked(_ sender: UIButton) {
        handleValidationAndNavigation()
    }
    
    func handleValidationAndNavigation() {
        guard let title = titleTextField.text, !title.isEmpty else {
            self.shakeInvalidView(view: titleTextField)
            return
        }
        
        guard let description = shortTextDescription.text, !description.isEmpty else {
            self.shakeInvalidView(view: self.shortTextDescription)
            return
        }
        
        guard let startTime = startTimeTextField.text, !startTime.isEmpty else {
            self.shakeInvalidView(view: self.startTimeTextFieldContainer)
            return
        }
        
        guard let endTime = endTimeTextField.text, !endTime.isEmpty else {
            self.shakeInvalidView(view: self.endTimeTextFieldContainer)
            return
        }
        
        if is_editing_existing_event {
            self.event.title = title
            self.event.description = description
            self.event.start_time = dateToString(sentDate: eventStartDateTime)
            self.event.start_interval = getEventDateAsEpoch(passedDate: self.eventStartDateTime)
            self.event.end_time = dateToString(sentDate: eventEndDateTime)
            self.event.end_interval = getEventDateAsEpoch(passedDate: self.eventEndDateTime)
            
            if  self.event.start_interval >  self.event.end_interval {
                createAndShowAlertDialog(message: "start time must be before end time")
            } else {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "createEvent2") as? CreateEventVC2
                vc?.event = event
                vc?.is_editing_existing_event = true
                self.navigationController?.pushViewController(vc!, animated: false)
            }
        } else {
            let event = Event(title: title, location: "", start_interval: getEventDateAsEpoch(passedDate: self.eventStartDateTime), start_time: dateToString(sentDate: eventStartDateTime), end_interval: getEventDateAsEpoch(passedDate: self.eventEndDateTime), end_time: dateToString(sentDate: eventEndDateTime), description: description, emojis: "", owner_uid: "", owner_name: "", owner_url: "", uid: "", interval_reference: 0, is_private: true, category: "", lat: 0.00, long: 0.00, type: 0, category_endInterval: "")
            
            if  getEventDateAsEpoch(passedDate: self.eventStartDateTime) >  getEventDateAsEpoch(passedDate: self.eventEndDateTime) {
                startTimeBeforeEndTime(subtitle: "Start time must be before start time!", title: "Small error, nothing crazy", buttonTitle: "Okay")
            } else {
                let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "createEvent2") as? CreateEventVC2
                vc?.event = event
                self.navigationController?.pushViewController(vc!, animated: false)
            }
        }
    }

    //MARK : VDL
    override func viewDidLoad() {
        super.viewDidLoad()
        views()
        addKeyboardObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) { () }
    
    override func viewWillDisappear(_ animated : Bool) {
        super.viewWillDisappear(animated)
        self.view.endEditing(true)
    }
    
    @objc func dismissAlert() { () }
    
    func addKeyboardObservers() {
        setDoneOnKeyboard()
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateEventVC1.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateEventVC1.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setDoneOnKeyboard() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(CreateEventVC1.dismissKeyboard))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.shortTextDescription.inputAccessoryView = keyboardToolbar
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardFrame = keyboardFrame.cgRectValue
            
            if shortTextDescription.isFirstResponder {
                self.titleTextField.isUserInteractionEnabled = false
                self.startTimeTextField.isUserInteractionEnabled = false
                self.endTimeTextField.isUserInteractionEnabled = false
                if self.firstResponderTag == 0 {
                    
                } else {
                    self.firstResponderTag = 0
                    
                    if (self.view.frame.size.height - keyboardFrame.height) < self.shortTextDescription.frame.maxY {
                        let difference = (self.shortTextDescription.frame.maxY - self.view.frame.size.height + 8) + keyboardFrame.height
                        self.view.frame.origin.y -= difference
                        
                        self.dif = difference
                        self.y = self.view.frame.origin.y
                    } else {
                        self.dif = 0
                        self.y = 0
                        return
                    }
                    return
                }
            }
            
            if endTimeTextField.isFirstResponder || startTimeTextField.isFirstResponder {
                self.titleTextField.isUserInteractionEnabled = false
                if self.firstResponderTag == 1 {
                    
                } else {
                    self.firstResponderTag = 1
                    
                    
                    if (self.view.frame.size.height - keyboardFrame.height) < self.startTimeTextField.frame.maxY {
                        let difference = (self.startTimeTextField.frame.maxY - self.view.frame.size.height + 8) + keyboardFrame.height
                        self.view.frame.origin.y -= difference
                        
                        self.dif = difference
                        self.y = self.view.frame.origin.y
                    } else {
                        self.dif = 0
                        self.y = 0
                        return
                    }
                    return
                }
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let duration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber
        
        self.firstResponderTag = 100
        self.titleTextField.isUserInteractionEnabled = true
        self.startTimeTextField.isUserInteractionEnabled = true
        self.endTimeTextField.isUserInteractionEnabled = true
        
        UIView.animate(withDuration: duration as! TimeInterval, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {}, completion: {(completed) in })
        
        if endTimeTextField.isFirstResponder || startTimeTextField.isFirstResponder || shortTextDescription.isFirstResponder {
            self.view.frame.origin.y += self.dif
            self.dif = 0
            self.y = 0
            return
        }
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.becomeFirstResponder()
    }
    
    func views() {
        if let editEvent = self.event {
            self.titleTextField.text = editEvent.title
            self.shortTextDescription.text = editEvent.description
            self.startTimeTextField.text = todayOrTommorrow(selectedDate: editEventStringToReadableDate(sentString: editEvent.start_time))
            self.endTimeTextField.text = todayOrTommorrow(selectedDate: editEventStringToReadableDate(sentString: editEvent.end_time))
            
                self.eventStartDateTime = editEventStringToSavedDate(sentString: editEvent.start_time)
                self.eventEndDateTime = editEventStringToSavedDate(sentString: editEvent.end_time)
        }
       
        self.shortTextDescription.layer.borderWidth = 0.5
        self.shortTextDescription.layer.borderColor = UIColor.gray.withAlphaComponent(0.5).cgColor
        self.shortTextDescription.layer.cornerRadius = 5
        self.shortTextDescription.clipsToBounds = true
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
           self.titleTextField.becomeFirstResponder()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag == 0 {
            clickedTextField = startTimeTextField
            createDatePicker(startTimeTextField)
        } else if textField.tag == 1 {
            clickedTextField = endTimeTextField
            createDatePicker(endTimeTextField)
        }
    }
    
    func createDatePicker(_ textField: UITextField) {
        //toolbar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        //picker
        let date = Date()
        let calendar = Calendar.current
        let minute = calendar.component(.minute, from: date)
        var newMinute = minute
        var counter = 0
        
        //Round up minute Picker to nearest 5 minutes and is greater than current time
        while((newMinute % 15 == 0) != true) {
            counter += 1
            newMinute += 1
        }
        //Picker only will allow users to create a date that begins in the next week
        picker.minuteInterval = 15
        picker.maximumDate = Calendar.current.date(byAdding: .minute, value: 10080 + (counter), to: Date())
        picker.minimumDate = Calendar.current.date(byAdding: .minute, value: +(counter), to: Date())
        
        //Add Done button to Picker bar
        let selector = #selector(timePickerDonePressed)
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: selector)
        toolbar.setItems([flexBarButton, done], animated: false)
        textField.inputAccessoryView = toolbar
        textField.inputView = picker
    }
    
    @objc func timePickerDonePressed() {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        
        let dateString = formatter.string(from: picker.date)
        clickedTextField?.text = "\(todayOrTommorrow(selectedDate: dateString))"
        
        //When time is picked, the pciker data as a data-rich string and casual string is stored in local variable
        if clickedTextField?.tag == 0 {
            eventStartDateTime = picker.date
        } else {
            eventEndDateTime = picker.date
        }
        self.view.endEditing(true)
    }
    
    func todayOrTommorrow(selectedDate: String) -> String {
        //Text box will say 'today' or will display the date and time as such: 11/23/18, 2:15pm
        let currentToday = Date()
        let calendar = Calendar.current
        let month = calendar.component(.month, from: currentToday)
        let day = calendar.component(.day, from: currentToday)
        
        //This block is the logic to seperating between components
        let comp = selectedDate.components(separatedBy: "/")
        let shortTimes = comp[2].components(separatedBy: ",")
        
        
        if Int(comp[0]) == month && Int(comp[1]) == day {
            return "today, \(shortTimes[1])"
        } else {
            return("\(comp[0]) / \(comp[1]) / \(comp[2])")
        }
    }
    
    //This block handles the number of characters remaining on the event title
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxChar = 45
        var newLength = 0
        
        if textField == titleTextField {
            
            if range.location + range.length > (self.titleTextField.text?.count)! {
                return false
            }
            
            newLength = (titleTextField.text?.count)! + string.count - range.length
            let charactersLeftInt = maxChar - newLength
            numberOfCharactersInTitle = newLength
            if charactersLeftInt < 0 {
                self.charactersRemaining.text? = "0"
            } else {
                self.charactersRemaining.text? = (String(charactersLeftInt))
            }
        }
        return newLength <= maxChar
    }
    
    func createAndShowAlertDialog(message: String) {
        let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: { action in
            switch action.style{
            case .default:
                ()
            case .destructive:
                ()
            case .cancel:
                ()
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getStartIntervalAsEpoch() -> Int {
        let date = self.eventStartDateTime
        // convert Date to TimeInterval (typealias for Double)
        let timeInterval = date.timeIntervalSince1970
        
        // convert to Integer
        let myInt = Int(timeInterval)
        return myInt
    }
    
    func dateToString(sentDate: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z" //Your New Date format as per requirement change it own
        let DateString = dateFormatter.string(from: sentDate) //pass Date here
        return DateString
    }

    func editEventStringToReadableDate(sentString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        if let date = dateFormatter.date(from: sentString) {
            let formatter1 = DateFormatter()
            formatter1.dateStyle = .short
            formatter1.timeStyle = .short
            
            let dateString = formatter1.string(from: date)
            return dateString
        }
        return "error"
    }
    
    func editEventStringToSavedDate(sentString: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        if let date = dateFormatter.date(from: sentString) {
            return date
        }
        return Date()
    }
    
    func getEventDateAsEpoch(passedDate: Date) -> Int {
        let date = passedDate
        // convert Date to TimeInterval (typealias for Double)
        let timeInterval = date.timeIntervalSince1970
        
        // convert to Integer
        let myInt = Int(timeInterval)
        return myInt
    }
    
    func shakeInvalidView(view: UIView) {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: view.center.x - 5, y: view.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: view.center.x + 5, y: view.center.y))
        
        view.layer.add(animation, forKey: "position")
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
