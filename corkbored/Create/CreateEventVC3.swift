//
//  CreateEventVC3.swift
//  corkbored
//
//  Created by Jackson Meyer on 2/25/19.
//  Copyright © 2019 Jackson Meyer. All rights reserved.
//

import UIKit
import Mapbox
import CoreLocation
import Firebase
import CoreData
import UserNotifications
import MapboxGeocoder

class CreateEventVC3: UIViewController, CLLocationManagerDelegate, MGLMapViewDelegate, UIGestureRecognizerDelegate {
    var locationManager = CLLocationManager()
    var usersLocation: CLLocation = CLLocation()
    var geocoder: Geocoder!
    var geocodingDataTask: URLSessionDataTask?
    var event: Event!
    var is_editing_existing_event: Bool = false
    var currentUserCoreData = [Person]()
    var cdEvents = [CdEvent]()
    
    @IBOutlet weak var MapView: MGLMapView!
    @IBOutlet weak var centerRelocationContainerView: UIView!
    @IBOutlet weak var relocateButton: UIButton!
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var LocationContainerView: UIView!
    @IBOutlet weak var LocationAddressLabel: UILabel!
    @IBOutlet weak var CreateEventButton: UIButton!
    @IBOutlet weak var mapPinView: UIImageView!
    @IBAction func backButtonClicked(_ sender: UIButton) {
       self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func centerCurrentLocation(_ sender: UIButton) {
        MapView.userTrackingMode = .follow
        MapView.showsUserLocation = true
    }
    
    @IBAction func createEventButtonClicked(_ sender: UIButton) {
        self.CreateEventButton.isEnabled = false
        handleValidationAndCreation()
    }
    
    func handleValidationAndCreation() {
        guard let uid = (Auth.auth().currentUser?.uid) else { return }
        guard let country = self.currentUserCoreData[0].location_country else { return }
        guard let state = self.currentUserCoreData[0].location_state else { return }
        guard let city = self.currentUserCoreData[0].location_city else { return }
        guard let url = self.currentUserCoreData[0].profile_photo_url else { return }
        guard let user_name = self.currentUserCoreData[0].user_name else { return }
            if let userLoc = self.LocationAddressLabel.text {
                self.event.location = userLoc
            } else {
                return
            }
            
            if is_editing_existing_event {
                let fetchRequest: NSFetchRequest<CdEvent> = CdEvent.fetchRequest()
                let predicate = NSPredicate(format: "cd_event_uid = %@", argumentArray: [event.uid])
                fetchRequest.predicate = predicate
                
                do {
                    let result = try PersistanceService.context.fetch(fetchRequest)
                    if result.count != 0 {
                        if (((event?.title) == nil) || ((event?.start_time) == nil) || ((event?.end_time) == nil)) { return }
                        result[0].cd_event_title = event.title
                        result[0].cd_event_start_time = event.start_time
                        result[0].cd_event_end_time = event.end_time
                        PersistanceService.saveContext()
                        
                        let cityFeedRef = Database.database().reference().child("EVENTS/\(result[0].cd_event_country!)/\(result[0].cd_event_state!)/\(result[0].cd_event_city!)/EVENTS_ACTIVE/\(event.uid)")
                        let profileEventsRef = Database.database().reference().child("USERS_EVENTS/\(uid)/\(result[0].cd_event_country!)/\(result[0].cd_event_state!)/\(result[0].cd_event_city!)/\(event.uid)")
                        
                        //Update USER_EVENT
                        profileEventsRef.updateChildValues(["category": event.category, "description": event.description, "emojis": event.emojis, "end_interval": event.end_interval, "end_time": event.end_time, "interval_reference": getCurrentTimeAsEpoch(), "is_private": event.is_private, "lat": MapView.centerCoordinate.latitude, "location": event.location, "long": MapView.centerCoordinate.longitude, "owner_name": event.owner_name, "owner_uid": event.owner_uid, "owner_url": url.absoluteString, "start_interval": event.start_interval, "start_time": event.start_time, "title": event.title, "uid": event.uid])
                        
                        
                        //Update ACTIVE_EVENTS
                        cityFeedRef.updateChildValues(["category": event.category, "description": event.description, "emojis": event.emojis, "end_interval": event.end_interval, "end_time": event.end_time, "interval_reference": getCurrentTimeAsEpoch(), "is_private": event.is_private, "lat": MapView.centerCoordinate.latitude, "location": event.location, "long": MapView.centerCoordinate.longitude, "owner_name": event.owner_name, "owner_uid": event.owner_uid, "owner_url": url.absoluteString, "start_interval": event.start_interval, "start_time": event.start_time, "title": event.title, "uid": event.uid, "type": 0, "category_endInterval": event.category + "_" + String(event.end_interval)]) { (error, ref) -> Void in
                            if error == nil {

                            self.dismiss(animated: true, completion: nil)
                            self.tabBarController?.selectedIndex = 0
                            }
                        }
                    }
                } catch { () }
            } else {
                let cityFeedRef = Database.database().reference().child("EVENTS/\(country)/\(state)/\(city)/EVENTS_ACTIVE")
                let eventAttendeesRef = Database.database().reference().child("EVENT_ATTENDEES/\(country)/\(state)/\(city)")
                let profileEventsRef = Database.database().reference().child("USERS_EVENTS/\(uid)/\(country)/\(state)/\(city)")
                
                let feedRef = cityFeedRef.childByAutoId()
                let eventUid = feedRef.key
                if let userLoc = self.LocationAddressLabel.text {
                    self.event.location =  userLoc
                }
                
                self.event.owner_uid = uid
                self.event.owner_name = user_name
                self.event.owner_url = url.absoluteString
                self.event.uid = eventUid
                self.event.interval_reference = getCurrentTimeAsEpoch()
                self.event.lat = MapView.centerCoordinate.latitude
                self.event.long = MapView.centerCoordinate.longitude
                
                //set feed view controller new event
                createdEvents.append(event)
                
                //attach attendee to attendee node
                let attend = Attendee(url: url.absoluteString, user_name: user_name, uid: uid, accepted: true, event_captain_uid: uid, event_uid: eventUid)
                let attenderObject = attend.toAnyObject()
                
                let userEventCast = UserEvent(accepted: true, title: event.title, location: event.location, start_interval: event.start_interval, start_time: event.start_time, end_interval: event.end_interval, end_time: event.end_time, description: event.description, emojis: event.emojis, owner_uid: event.owner_uid, owner_name: event.owner_name, owner_url: event.owner_url, uid: eventUid, interval_reference: event.interval_reference, is_private: event.is_private, category: event.category, lat: event.lat, long: event.long, type: 0, category_endInterval: event.category_endInterval)
                
                let userEventF = userEventCast.toAnyObject()
                
                handleCdEventObjects(cd_event_title: event.title, cd_event_captain_uid: uid, cd_event_city: city, cd_event_state: state, cd_event_country: country, cd_event_uid: feedRef.key, cd_event_start_time: event.start_time, cd_event_end_time: event.end_time)
                
                //SET NEW EVENT
                let eventF = event.toAnyObject()
                feedRef.setValue(eventF) { (error, ref) -> Void in
                    if error == nil {
                        //SET ATTENDEE AND USER NODE
                        profileEventsRef.child(eventUid).setValue(userEventF)
                        eventAttendeesRef.child(eventUid).child("ATTENDEES").child(uid).setValue(attenderObject) { (error, ref) -> Void in

                        self.dismiss(animated: true, completion: nil)
                        self.tabBarController?.selectedIndex = 0
                        }
                    }
                }
            }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MGLAccountManager.accessToken = Constants.MapboxAccessToken
        geocoder = Geocoder(accessToken: Constants.MapboxAccessToken)
        
        if is_editing_existing_event {
            MapView.setCenter(CLLocationCoordinate2D(latitude: event.lat, longitude: event.long), zoomLevel: 14, animated: false)
            self.LocationAddressLabel.text = event.location
        } else {
            MapView.userTrackingMode = .follow
            MapView.showsUserLocation = true
        }
        
        fetchUserLocation()
        handleViews()
    }
    
    func handleViews() {
        // add pan gesture to detect when the map moves
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.didDragMap(_:)))
        
        // make your class the delegate of the pan gesture
        panGesture.delegate = self
        
        // add the gesture to the mapView
        MapView.addGestureRecognizer(panGesture)
        
        let addressTapGesture = UITapGestureRecognizer(target: self, action: #selector(addressFieldTapped))
        self.LocationAddressLabel.isUserInteractionEnabled = true
        self.LocationAddressLabel.addGestureRecognizer(addressTapGesture)
        
        self.centerRelocationContainerView.layer.masksToBounds = true
        self.centerRelocationContainerView.layer.cornerRadius = self.centerRelocationContainerView.frame.size.height/4
        
        self.LocationContainerView.layer.cornerRadius = 8
        self.LocationContainerView.layer.borderWidth = 2
        self.LocationContainerView.layer.borderColor = UIColor(named: "zazuMiddleGray")?.cgColor
        
        if is_editing_existing_event {
            self.CreateEventButton.setTitle("Update Event",for: .normal)
        } else {
            self.CreateEventButton.setTitle("Create Event",for: .normal)
        }
    }
    
    @objc func addressFieldTapped() {
        //Go to address impur VC
    }
    
    func getUserCoreData() {
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        do {
            let peep = try PersistanceService.context.fetch(fetchRequest)
            self.currentUserCoreData = peep
        } catch { () }
    }
    
    func getEventCoreData() {
        let fetchRequest: NSFetchRequest<CdEvent> = CdEvent.fetchRequest()
        do {
            let events = try PersistanceService.context.fetch(fetchRequest)
            self.cdEvents = events
        } catch { () }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getUserCoreData()
        getEventCoreData()
    }
    
    @objc func fetchUserLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        if (CLLocationManager.authorizationStatus() == .notDetermined) {
            //first show, display DZN empty dataset
           ()
        }
        
        if (CLLocationManager.authorizationStatus() == .denied) {
            //second alert
            let alert = UIAlertController(title: "Need Authorization", message: "This app is unusable if you don't authorize this app to use your location!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { _ in
                let url = URL(string: UIApplication.openSettingsURLString)!
                UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.usersLocation = locations[0]
        self.handlemapDraggingViews(isBeingDragged: false)
        let coordinates = CLLocationCoordinate2D(latitude: usersLocation.coordinate.latitude
            , longitude: self.usersLocation.coordinate.longitude)
        if !self.is_editing_existing_event {
            MapView.setCenter(CLLocationCoordinate2D(latitude: usersLocation.coordinate.latitude, longitude: self.usersLocation.coordinate.longitude), zoomLevel: 14, animated: false)
        }

        let options = ReverseGeocodeOptions(coordinate: coordinates)
        geocodingDataTask = geocoder.geocode(options) { [unowned self] (placemarks, attribution, error) in
            if let error = error {
                NSLog("%@", error)
            } else if let placemarks = placemarks, !placemarks.isEmpty {
                if self.is_editing_existing_event {
                    self.LocationAddressLabel.text = self.event.location
                } else {
                    self.LocationAddressLabel.text = placemarks[0].qualifiedName
                }
            } else {
                self.LocationAddressLabel.text = "No results"
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
       ()
    }
    
    //MARK: - MAP FUNCTIONS
    @objc func didDragMap(_ sender: UIGestureRecognizer) {
        handlemapDraggingViews(isBeingDragged: true)
        if sender.state == .ended {
            handlemapDraggingViews(isBeingDragged: false)
            
            let options = ReverseGeocodeOptions(coordinate: MapView.centerCoordinate)
            geocodingDataTask = geocoder.geocode(options) { [unowned self] (placemarks, attribution, error) in
                if let error = error {
                    NSLog("%@", error)
                } else if let placemarks = placemarks, !placemarks.isEmpty {
                    self.LocationAddressLabel.text = placemarks[0].qualifiedName
                } else {
                    self.LocationAddressLabel.text = "No results"
                }
            }
        }
    }
    
    func handlemapDraggingViews(isBeingDragged: Bool) {
        if isBeingDragged {
            self.LocationContainerView.backgroundColor = UIColor(named: "zazuMiddleGray")
            self.LocationAddressLabel.font = UIFont(name: "Metropolis-RegularItalic", size: 16)
        } else {
            self.LocationContainerView.backgroundColor = UIColor.white
            self.LocationAddressLabel.font = UIFont(name: "Metropolis-Medium", size: 16)
        }
    }
    
    func mapView(_ mapView: MGLMapView, regionWillChangeAnimated animated: Bool) {
        geocodingDataTask?.cancel()
    }
    
    func mapView(_ mapView: MGLMapView, regionDidChangeAnimated animated: Bool) {
        geocodingDataTask?.cancel()
        let options = ReverseGeocodeOptions(coordinate: mapView.centerCoordinate)
        geocodingDataTask = geocoder.geocode(options) { [unowned self] (placemarks, attribution, error) in
            if let error = error {
                NSLog("%@", error)
            } else if let placemarks = placemarks, !placemarks.isEmpty {
                self.LocationAddressLabel.text = placemarks[0].qualifiedName
            } else {
                self.LocationAddressLabel.text = "No results"
            }
        }
    }
    
    func handleCdEventObjects(cd_event_title: String, cd_event_captain_uid: String, cd_event_city: String, cd_event_state: String, cd_event_country: String, cd_event_uid: String, cd_event_start_time: String, cd_event_end_time: String) {
        let cdEventObject = CdEvent(context: PersistanceService.context)
    
        DispatchQueue.global().async {
            if let url = self.currentUserCoreData[0].profile_photo_url {
                DispatchQueue.main.async {
                    
                    cdEventObject.cd_event_accepted = true
                    cdEventObject.cd_event_captain_uid = cd_event_captain_uid
                    cdEventObject.cd_event_city = cd_event_city
                    cdEventObject.cd_event_photo_url = url
                    cdEventObject.cd_event_state = cd_event_state
                    cdEventObject.cd_event_country = cd_event_country
                    cdEventObject.cd_event_title = cd_event_title
                    cdEventObject.cd_event_uid = cd_event_uid
                    cdEventObject.cd_event_start_time = cd_event_start_time
                    cdEventObject.cd_event_end_time = cd_event_end_time
                    
                    self.cdEvents.append(cdEventObject)
                    
                    PersistanceService.saveContext()
                }
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
    return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
