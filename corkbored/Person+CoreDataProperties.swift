//
//  Person+CoreDataProperties.swift
//  
//
//  Created by Ryan Pate on 3/31/19.
//
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person")
    }

    @NSManaged public var about: String?
    @NSManaged public var accepted_terms: Bool
    @NSManaged public var birthday: String?
    @NSManaged public var current_job_employer: String?
    @NSManaged public var current_job_employer_logo: NSData?
    @NSManaged public var current_job_title: String?
    @NSManaged public var device_token: String?
    @NSManaged public var dream_job_employer: String?
    @NSManaged public var dream_job_employer_logo: NSData?
    @NSManaged public var dream_job_title: String?
    @NSManaged public var education_institution: String?
    @NSManaged public var education_institution_logo: NSData?
    @NSManaged public var education_level: String?
    @NSManaged public var first_name: String?
    @NSManaged public var instagram_handle: String?
    @NSManaged public var last_name: String?
    @NSManaged public var location_city: String?
    @NSManaged public var location_country: String?
    @NSManaged public var location_state: String?
    @NSManaged public var photo1: NSData?
    @NSManaged public var photo2: NSData?
    @NSManaged public var photo3: NSData?
    @NSManaged public var photo4: NSData?
    @NSManaged public var photo5: NSData?
    @NSManaged public var photo6: NSData?
    @NSManaged public var profile_photo_url: URL?
    @NSManaged public var snapchat_handle: String?
    @NSManaged public var user_name: String?

}
